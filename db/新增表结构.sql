-- User表新增身份证号
ALTER TABLE `sys_user`
ADD COLUMN `id_card`  varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '身份证号' AFTER `id_card`;
-- hhm 文章期刊分类表
CREATE TABLE `tp_article_classify` (
  `id` varchar(64) COLLATE utf8_bin NOT NULL,
  `name` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '期刊名称',
  `type` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '期刊分类',
  `category` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '期刊类别',
  `year` varchar(10) COLLATE utf8_bin DEFAULT NULL COMMENT '年限',
  `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
  `create_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
  `update_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='文章期刊分类表';

-- yhg 2018-04-07
ALTER TABLE `sys_user`
ADD COLUMN `open_id`  varchar(50) NULL COMMENT '标识微信用户' AFTER `del_flag`;

ALTER TABLE `tp_fileupload_info`
MODIFY COLUMN `model`  varchar(64) NULL DEFAULT NULL COMMENT '文件所属模块' AFTER `file_type`;

ALTER TABLE `tp_teach_active`
ADD COLUMN `score`  varchar(50) NULL COMMENT '得分';

ALTER TABLE `tp_teach_active`
MODIFY COLUMN `stu_num`  double(11,2) NULL DEFAULT NULL COMMENT '带教量' AFTER `name`;

-- yhg 20180416
UPDATE `sys_dict` SET `id`='07f4a8f0a27049e3b7312e4390455ee8', `value`='1', `label`='必修课', `type`='teach_type', `description`='带教分类', `sort`='10', `parent_id`='0', `create_by`='1', `create_date`='2018-03-04 15:32:53', `update_by`='1', `update_date`='2018-03-04 15:45:06', `remarks`='带教分类包括理论授课和实践教学两种', `del_flag`='0' WHERE (`id`='07f4a8f0a27049e3b7312e4390455ee8');
UPDATE `sys_dict` SET `id`='54a60b0a19f24bd49981b2aa904d1d21', `value`='11', `label`='指导教师工作', `type`='teach_type', `description`='带教分类', `sort`='110', `parent_id`='0', `create_by`='1', `create_date`='2018-03-06 13:21:38', `update_by`='1', `update_date`='2018-03-06 13:21:38', `remarks`='', `del_flag`='0' WHERE (`id`='54a60b0a19f24bd49981b2aa904d1d21');
UPDATE `sys_dict` SET `id`='6368c6f21c1449f5b754e196f16a3135', `value`='8', `label`='科室级病例讨论', `type`='teach_type', `description`='带教分类', `sort`='80', `parent_id`='0', `create_by`='1', `create_date`='2018-03-04 15:47:15', `update_by`='1', `update_date`='2018-03-04 15:47:15', `remarks`='', `del_flag`='0' WHERE (`id`='6368c6f21c1449f5b754e196f16a3135');
UPDATE `sys_dict` SET `id`='65d31e287ab34efa82d203bbba0e7c70', `value`='6', `label`='院级教学查房', `type`='teach_type', `description`='带教分类', `sort`='60', `parent_id`='0', `create_by`='1', `create_date`='2018-03-04 15:46:41', `update_by`='1', `update_date`='2018-03-04 15:46:41', `remarks`='', `del_flag`='0' WHERE (`id`='65d31e287ab34efa82d203bbba0e7c70');
UPDATE `sys_dict` SET `id`='93416030fc7c4759bffa01a26bf4105c', `value`='9', `label`='科室级小讲课', `type`='teach_type', `description`='带教分类', `sort`='90', `parent_id`='0', `create_by`='1', `create_date`='2018-03-04 15:47:29', `update_by`='1', `update_date`='2018-03-04 15:47:29', `remarks`='', `del_flag`='0' WHERE (`id`='93416030fc7c4759bffa01a26bf4105c');
UPDATE `sys_dict` SET `id`='a3744d452c2f4232b4c2f4cc31fd9aef', `value`='4', `label`='临床见习带教', `type`='teach_type', `description`='带教分类', `sort`='40', `parent_id`='0', `create_by`='1', `create_date`='2018-03-04 15:46:04', `update_by`='1', `update_date`='2018-03-04 15:46:04', `remarks`='', `del_flag`='0' WHERE (`id`='a3744d452c2f4232b4c2f4cc31fd9aef');
UPDATE `sys_dict` SET `id`='a5279ab6a07748758bfe8f3b238712b2', `value`='7', `label`='科室级教学查房', `type`='teach_type', `description`='带教分类', `sort`='70', `parent_id`='0', `create_by`='1', `create_date`='2018-03-04 15:47:00', `update_by`='1', `update_date`='2018-03-04 15:47:00', `remarks`='', `del_flag`='0' WHERE (`id`='a5279ab6a07748758bfe8f3b238712b2');
UPDATE `sys_dict` SET `id`='b1ce6cc9288c4a2bac62d38d83602fda', `value`='3', `label`='脱产带教', `type`='teach_type', `description`='带教分类', `sort`='30', `parent_id`='0', `create_by`='1', `create_date`='2018-03-04 15:45:46', `update_by`='1', `update_date`='2018-03-04 15:45:46', `remarks`='', `del_flag`='0' WHERE (`id`='b1ce6cc9288c4a2bac62d38d83602fda');
UPDATE `sys_dict` SET `id`='bba13a01c5754d62a2897b207d272272', `value`='10', `label`='指导教师工作', `type`='teach_type', `description`='带教分类', `sort`='100', `parent_id`='0', `create_by`='1', `create_date`='2018-03-04 16:27:41', `update_by`='1', `update_date`='2018-03-06 13:19:02', `remarks`='', `del_flag`='0' WHERE (`id`='bba13a01c5754d62a2897b207d272272');
UPDATE `sys_dict` SET `id`='d767876317b34827be17106fd40e68c4', `value`='5', `label`='生产实习带教', `type`='teach_type', `description`='带教分类', `sort`='50', `parent_id`='0', `create_by`='1', `create_date`='2018-03-04 15:46:23', `update_by`='1', `update_date`='2018-03-04 15:46:23', `remarks`='', `del_flag`='0' WHERE (`id`='d767876317b34827be17106fd40e68c4');
UPDATE `sys_dict` SET `id`='de338c15e65c4394aae627785de2d60b', `value`='12', `label`='指导教师工作', `type`='teach_type', `description`='带教分类', `sort`='120', `parent_id`='0', `create_by`='1', `create_date`='2018-03-06 13:22:07', `update_by`='1', `update_date`='2018-03-06 13:22:07', `remarks`='', `del_flag`='0' WHERE (`id`='de338c15e65c4394aae627785de2d60b');
UPDATE `sys_dict` SET `id`='f6242d0c047b47cfa6b85d1da6fef3d0', `value`='2', `label`='选修课', `type`='teach_type', `description`='带教分类', `sort`='20', `parent_id`='0', `create_by`='1', `create_date`='2018-03-04 15:44:42', `update_by`='1', `update_date`='2018-03-04 15:45:23', `remarks`='', `del_flag`='0' WHERE (`id`='f6242d0c047b47cfa6b85d1da6fef3d0');


ALTER TABLE `tp_teach_award`
ADD COLUMN `winning_time`  datetime NULL DEFAULT NULL COMMENT '获奖时间' AFTER `title`;
ALTER TABLE `tp_teach_excellent_award`
ADD COLUMN `winning_time`  datetime NULL DEFAULT NULL COMMENT '获奖时间' ;
ALTER TABLE `tp_teach_achievement`
ADD COLUMN `winning_time`  datetime NULL DEFAULT NULL COMMENT '成果时间' ;


ALTER TABLE `tp_teach_articles`
ADD COLUMN `onl_jcr_url`  varchar(255) NULL comment '链接' AFTER `OnlJcr`;

-- 2018年6月5日 hhm 教学专利表、教材建设表增加所属人字段
alter table `tp_teach_patent` add column `teach_id` varchar(64) not null comment 'userId' after `id`;
alter table `tp_teach_material` add column `teach_id` varchar(64) not null comment 'userId' after `id`;

-- yhg
ALTER TABLE `tp_article_classify`
ADD COLUMN `del_flag`  char(1) NULL COMMENT '删除标识' AFTER `update_date`;
ALTER TABLE `tp_article_classify`
MODIFY COLUMN `del_flag`  char(1) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT 0 COMMENT '删除标识' AFTER `update_date`;
-- yhg 20180619
delete from sys_dict where type = 'enrolment_way';

INSERT INTO `sys_dict` (`id`, `value`, `label`, `type`, `description`, `sort`, `parent_id`, `create_by`, `create_date`, `update_by`, `update_date`, `remarks`, `del_flag`) VALUES ('09bc5969677641a3a224add3d48f2285', '6', '推荐直博', 'enrolment_way', '入学方式', '60', '0', '1', '2018-06-19 09:08:36', '1', '2018-06-19 09:08:36', '', '0');
INSERT INTO `sys_dict` (`id`, `value`, `label`, `type`, `description`, `sort`, `parent_id`, `create_by`, `create_date`, `update_by`, `update_date`, `remarks`, `del_flag`) VALUES ('407bb1b45d084222ad75b07f52989de8', '3', '临硕转博', 'enrolment_way', '入学方式', '30', '0', '1', '2018-06-19 09:08:04', '1', '2018-06-19 09:08:04', '', '0');
INSERT INTO `sys_dict` (`id`, `value`, `label`, `type`, `description`, `sort`, `parent_id`, `create_by`, `create_date`, `update_by`, `update_date`, `remarks`, `del_flag`) VALUES ('44bcc9223ac7492599fd6003aa86487e', '4', '科硕择优攻博', 'enrolment_way', '入学方式', '40', '0', '1', '2018-06-19 09:08:14', '1', '2018-06-19 09:08:14', '', '0');
INSERT INTO `sys_dict` (`id`, `value`, `label`, `type`, `description`, `sort`, `parent_id`, `create_by`, `create_date`, `update_by`, `update_date`, `remarks`, `del_flag`) VALUES ('7d9d3ba985ea40cbaca57f2b2f4f54b2', '5', '推荐免试', 'enrolment_way', '入学方式', '50', '0', '1', '2018-06-19 09:08:26', '1', '2018-06-19 09:08:26', '', '0');
INSERT INTO `sys_dict` (`id`, `value`, `label`, `type`, `description`, `sort`, `parent_id`, `create_by`, `create_date`, `update_by`, `update_date`, `remarks`, `del_flag`) VALUES ('cd3fbcb615444312a92d6a13da47e98a', '1', '全国统考', 'enrolment_way', '入学方式', '10', '0', '1', '2018-06-19 09:06:53', '1', '2018-06-19 09:06:53', '', '0');
INSERT INTO `sys_dict` (`id`, `value`, `label`, `type`, `description`, `sort`, `parent_id`, `create_by`, `create_date`, `update_by`, `update_date`, `remarks`, `del_flag`) VALUES ('d782d1f405d74ec8a3e7203e55b67397', '2', '申请考核制', 'enrolment_way', '入学方式', '20', '0', '1', '2018-06-19 09:07:47', '1', '2018-06-19 09:07:47', '', '0');

ALTER TABLE `tp_p_student`
ADD COLUMN `student_no`  varchar(50) NULL AFTER `del_flag`;

delete from sys_dict where type = 'journal_type';
delete from sys_dict where type = 'article_type';
INSERT INTO `sys_dict` (`id`, `value`, `label`, `type`, `description`, `sort`, `parent_id`, `create_by`, `create_date`, `update_by`, `update_date`, `remarks`, `del_flag`) VALUES ('2a154aef28d84629ab86d84b52037c26', '3', '核心期刊', 'journal_type', '期刊类型', '30', '0', '1', '2018-03-04 15:20:17', '1', '2018-03-04 15:20:17', '', '0');
INSERT INTO `sys_dict` (`id`, `value`, `label`, `type`, `description`, `sort`, `parent_id`, `create_by`, `create_date`, `update_by`, `update_date`, `remarks`, `del_flag`) VALUES ('6ea8f99fb47b4cafb572a0ab48dc28de', '1', 'SCI', 'journal_type', '期刊类型', '10', '0', '1', '2018-03-04 15:19:54', '1', '2018-03-04 15:19:54', 'SCI、中文核心期刊及中国统计源期刊论著', '0');
INSERT INTO `sys_dict` (`id`, `value`, `label`, `type`, `description`, `sort`, `parent_id`, `create_by`, `create_date`, `update_by`, `update_date`, `remarks`, `del_flag`) VALUES ('75ef3e941a8243c192219fb9b0f990a4', '2', '统计源', 'journal_type', '期刊类型', '20', '0', '1', '2018-03-04 15:20:01', '1', '2018-03-04 15:20:01', '', '0');


INSERT INTO `sys_dict` (`id`, `value`, `label`, `type`, `description`, `sort`, `parent_id`, `create_by`, `create_date`, `update_by`, `update_date`, `remarks`, `del_flag`) VALUES ('1d758e1bed6f4d4c911962c2b59687a3', '3', '个案报道', 'article_type', '文章类型', '30', '0', '1', '2018-06-19 20:07:31', '1', '2018-06-19 20:07:31', '', '0');
INSERT INTO `sys_dict` (`id`, `value`, `label`, `type`, `description`, `sort`, `parent_id`, `create_by`, `create_date`, `update_by`, `update_date`, `remarks`, `del_flag`) VALUES ('45bbd6d0f1d24cd1b2d0977b3aa0d460', '5', '论著摘要', 'article_type', '文章类型', '50', '0', '1', '2018-06-19 20:07:54', '1', '2018-06-19 20:08:00', '', '0');
INSERT INTO `sys_dict` (`id`, `value`, `label`, `type`, `description`, `sort`, `parent_id`, `create_by`, `create_date`, `update_by`, `update_date`, `remarks`, `del_flag`) VALUES ('7df3200b6b25462b9c41e5bf0204022e', '2', '综述', 'article_type', '文章类型', '20', '0', '1', '2018-03-04 15:23:48', '1', '2018-03-04 15:23:48', '', '0');
INSERT INTO `sys_dict` (`id`, `value`, `label`, `type`, `description`, `sort`, `parent_id`, `create_by`, `create_date`, `update_by`, `update_date`, `remarks`, `del_flag`) VALUES ('9d0bf4f9b2b04b049c29827372fb45a0', '1', '论著', 'article_type', '文章类型', '10', '0', '1', '2018-03-04 15:23:37', '1', '2018-03-04 15:23:37', '', '0');
INSERT INTO `sys_dict` (`id`, `value`, `label`, `type`, `description`, `sort`, `parent_id`, `create_by`, `create_date`, `update_by`, `update_date`, `remarks`, `del_flag`) VALUES ('b5c2838edc694c309475f91106613afe', '4', '短篇论著', 'article_type', '文章类型', '40', '0', '1', '2018-06-19 20:07:42', '1', '2018-06-19 20:07:42', '', '0');

ALTER TABLE `tp_student_exam`
  ADD COLUMN `not_pass_course`  varchar(255) NULL COMMENT '未通过科目' AFTER `del_flag`;

-- 2018年6月20日 hhm
ALTER TABLE `tp_student_graduate`
  ADD COLUMN `result`  varchar(255) NULL COMMENT '结果' AFTER `question_num`;

ALTER TABLE `tp_p_student`
  ADD COLUMN `root_in`  varchar(64) NULL COMMENT '来源(字典类型=tp_stu_root_in)' AFTER `enrolment_way`;
--  yhg 20180720
ALTER TABLE `tp_teach_articles`
  MODIFY COLUMN `push_year`  varchar(10) NULL DEFAULT NULL COMMENT '发表年份' AFTER `journal_type`;
-- yhg 20180902
UPDATE `train_prerformance`.`sys_menu` SET `id`='154835c75c1d4cc383de20e50373b6cf', `parent_id`='070f8af0afb84a3bab46808dd2ef81f4', `parent_ids`='0,1,9c6c3c0b680b4b7eb504ceb89780201b,070f8af0afb84a3bab46808dd2ef81f4,', `name`='教学干事', `sort`='100', `href`='/teach/tpTeachActive?teachTypes=13', `target`='', `icon`='fire', `is_show`='1', `permission`='', `create_by`='1', `create_date`='2018-03-29 20:13:29', `update_by`='1', `update_date`='2018-03-29 20:13:29', `remarks`='{&quot;name&quot;:&quot;课程名&quot;,&quot;user.loginName&quot;:&quot;工号&quot;,&quot;user.name&quot;:&quot;授课老师&quot;,&quot;teachType&quot;:&quot;活动类型&quot;,&quot;stuNum&quot;:&quot;带教量&quot;,&quot;startTime&quot;:&quot;开始时间&quot;,&quot;endTime&quot;:&quot;结束时间&quot;,&quot;stuType&quot;:&quot;学生类别&quot;,&quot;stuGrade&quot;:&quot;学员年级&quot;,&quot;department.id&quot;:&quot;所属科室&quot;,&quot;trc.id&quot;:&quot;所属教研室&quot;,&quot;administrative&quot;:&quot;管理部门&quot;,&quot;activePlace&quot;:&quot;活动地点&quot;,&quot;activeIntroduce&quot;:&quot;活动介绍&quot;,&quot;remarks&quot;:&quot;备注信息&quot;,&quot;menu_remark&quot;:&quot;&quot;}', `del_flag`='0', `type`='1' WHERE (`id`='154835c75c1d4cc383de20e50373b6cf');
UPDATE `train_prerformance`.`sys_menu` SET `id`='4cb0b0a67cad448a9145d3c281b26a52', `parent_id`='070f8af0afb84a3bab46808dd2ef81f4', `parent_ids`='0,1,9c6c3c0b680b4b7eb504ceb89780201b,070f8af0afb84a3bab46808dd2ef81f4,', `name`='实践教学', `sort`='60', `href`='/teach/tpTeachActive?teachTypes=3,4,5,6,7,8,9', `target`='', `icon`='google-plus-sign', `is_show`='1', `permission`='', `create_by`='1', `create_date`='2018-03-29 20:08:10', `update_by`='1', `update_date`='2018-03-29 20:10:45', `remarks`='{&quot;name&quot;:&quot;课程名&quot;,&quot;user.loginName&quot;:&quot;工号&quot;,&quot;user.name&quot;:&quot;授课老师&quot;,&quot;teachType&quot;:&quot;活动类型&quot;,&quot;stuNum&quot;:&quot;带教量&quot;,&quot;startTime&quot;:&quot;开始时间&quot;,&quot;endTime&quot;:&quot;结束时间&quot;,&quot;stuType&quot;:&quot;学生类别&quot;,&quot;stuGrade&quot;:&quot;学员年级&quot;,&quot;department.id&quot;:&quot;所属科室&quot;,&quot;trc.id&quot;:&quot;所属教研室&quot;,&quot;administrative&quot;:&quot;管理部门&quot;,&quot;activePlace&quot;:&quot;活动地点&quot;,&quot;activeIntroduce&quot;:&quot;活动介绍&quot;,&quot;remarks&quot;:&quot;备注信息&quot;,&quot;menu_remark&quot;:&quot;&quot;}', `del_flag`='0', `type`='1' WHERE (`id`='4cb0b0a67cad448a9145d3c281b26a52');
UPDATE `train_prerformance`.`sys_menu` SET `id`='7005cc22a69946da9c1b53746d8eae92', `parent_id`='070f8af0afb84a3bab46808dd2ef81f4', `parent_ids`='0,1,9c6c3c0b680b4b7eb504ceb89780201b,070f8af0afb84a3bab46808dd2ef81f4,', `name`='指导教师工作', `sort`='90', `href`='/teach/tpTeachActive?teachTypes=10,11,12', `target`='', `icon`='paste', `is_show`='1', `permission`='', `create_by`='1', `create_date`='2018-03-29 20:08:43', `update_by`='1', `update_date`='2018-03-29 20:12:28', `remarks`='{&quot;name&quot;:&quot;课程名&quot;,&quot;user.loginName&quot;:&quot;工号&quot;,&quot;user.name&quot;:&quot;授课老师&quot;,&quot;teachType&quot;:&quot;活动类型&quot;,&quot;stuNum&quot;:&quot;带教量&quot;,&quot;startTime&quot;:&quot;开始时间&quot;,&quot;endTime&quot;:&quot;结束时间&quot;,&quot;stuType&quot;:&quot;学生类别&quot;,&quot;stuGrade&quot;:&quot;学员年级&quot;,&quot;department.id&quot;:&quot;所属科室&quot;,&quot;trc.id&quot;:&quot;所属教研室&quot;,&quot;administrative&quot;:&quot;管理部门&quot;,&quot;activePlace&quot;:&quot;活动地点&quot;,&quot;activeIntroduce&quot;:&quot;活动介绍&quot;,&quot;remarks&quot;:&quot;备注信息&quot;,&quot;menu_remark&quot;:&quot;&quot;}', `del_flag`='0', `type`='1' WHERE (`id`='7005cc22a69946da9c1b53746d8eae92');
UPDATE `train_prerformance`.`sys_menu` SET `id`='d7c9e959894e43abbb349b0ab2cb8d30', `parent_id`='070f8af0afb84a3bab46808dd2ef81f4', `parent_ids`='0,1,9c6c3c0b680b4b7eb504ceb89780201b,070f8af0afb84a3bab46808dd2ef81f4,', `name`='培训考核命题等', `sort`='120', `href`='/teach/tpTeachActive?teachTypes=14,15,16,17', `target`='', `icon`='hdd', `is_show`='1', `permission`='', `create_by`='1', `create_date`='2018-03-29 20:09:23', `update_by`='1', `update_date`='2018-06-26 19:43:52', `remarks`='{&quot;name&quot;:&quot;课程名&quot;,&quot;user.loginName&quot;:&quot;工号&quot;,&quot;user.name&quot;:&quot;授课老师&quot;,&quot;teachType&quot;:&quot;活动类型&quot;,&quot;stuNum&quot;:&quot;带教量&quot;,&quot;startTime&quot;:&quot;开始时间&quot;,&quot;endTime&quot;:&quot;结束时间&quot;,&quot;stuType&quot;:&quot;学生类别&quot;,&quot;stuGrade&quot;:&quot;学员年级&quot;,&quot;department.id&quot;:&quot;所属科室&quot;,&quot;trc.id&quot;:&quot;所属教研室&quot;,&quot;administrative&quot;:&quot;管理部门&quot;,&quot;activePlace&quot;:&quot;活动地点&quot;,&quot;activeIntroduce&quot;:&quot;活动介绍&quot;,&quot;remarks&quot;:&quot;备注信息&quot;,&quot;menu_remark&quot;:&quot;培训：由教研室、教育处、继教处、能力中心等教学管理部门组织及认定的培训，涵盖八年制、住院医、研究生、护理学生等，含早期接触临床和指导海外交流学生；参加培训：参加教育处组织或认定的培训等；考核：由教研室、教育处、继教处、能力中心等教学管理部门组织及认定的考核，涵盖八年制、住院医、研究生、护理学生等；命题：由教研室、教育处、继教处、能力中心等部门组织及认定的命题工作，涵盖八年制、住院医等&quot;}', `del_flag`='0', `type`='1' WHERE (`id`='d7c9e959894e43abbb349b0ab2cb8d30');
UPDATE `train_prerformance`.`sys_menu` SET `id`='ebb235c8fc6f4930a0f4817434bd7212', `parent_id`='070f8af0afb84a3bab46808dd2ef81f4', `parent_ids`='0,1,9c6c3c0b680b4b7eb504ceb89780201b,070f8af0afb84a3bab46808dd2ef81f4,', `name`='理论课管理', `sort`='30', `href`='/teach/tpTeachActive?teachTypes=1,2', `target`='', `icon`='table', `is_show`='1', `permission`='', `create_by`='1', `create_date`='2018-03-04 16:50:51', `update_by`='1', `update_date`='2018-04-07 17:21:07', `remarks`='{&quot;name&quot;:&quot;课程名&quot;,&quot;user.loginName&quot;:&quot;工号&quot;,&quot;user.name&quot;:&quot;授课老师&quot;,&quot;teachType&quot;:&quot;活动类型&quot;,&quot;stuNum&quot;:&quot;带教量&quot;,&quot;startTime&quot;:&quot;开始时间&quot;,&quot;endTime&quot;:&quot;结束时间&quot;,&quot;stuType&quot;:&quot;学生类别&quot;,&quot;stuGrade&quot;:&quot;学员年级&quot;,&quot;department.id&quot;:&quot;所属科室&quot;,&quot;trc.id&quot;:&quot;所属教研室&quot;,&quot;administrative&quot;:&quot;管理部门&quot;,&quot;activePlace&quot;:&quot;活动地点&quot;,&quot;activeIntroduce&quot;:&quot;活动介绍&quot;,&quot;remarks&quot;:&quot;备注信息&quot;,&quot;menu_remark&quot;:&quot;理论授课分为必修课与选修课\\r\\n必修课是指指由教育处、继续教育处制定的课表中所含或认定的理论必修课课程，包括我院内部及承担由医学部安排的北医系统内其他学院的正规理论课程。涵盖院校教育、毕业后教育、继续医学教育以及护理专业教育课程。\\r\\n选修课是指由教育处、继续教育处制定的选修课课程&quot;}', `del_flag`='0', `type`='1' WHERE (`id`='ebb235c8fc6f4930a0f4817434bd7212');
