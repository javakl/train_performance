/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50717
Source Host           : 127.0.0.1:3306
Source Database       : train_performance

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2019-12-21 21:20:16
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tp_act_proc_his
-- ----------------------------
DROP TABLE IF EXISTS `tp_act_proc_his`;
CREATE TABLE `tp_act_proc_his` (
  `proc_ins_id` varchar(64) COLLATE utf8_bin NOT NULL COMMENT '流程任务id',
  `f_id` varchar(64) COLLATE utf8_bin NOT NULL COMMENT '流程任务业务表id',
  PRIMARY KEY (`proc_ins_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='业务表id对应的历史流程任务表';

-- ----------------------------
-- Records of tp_act_proc_his
-- ----------------------------
INSERT INTO `tp_act_proc_his` VALUES ('025721eb86a545ada1746c53829893e8', '53863fe8f13741ee8bd9ea942aeb5972');
INSERT INTO `tp_act_proc_his` VALUES ('0b8e14e4006a441d858b130663ce024e', '0b0fdb1ef2ab4f3a9f6212299c6b49b7');
INSERT INTO `tp_act_proc_his` VALUES ('11311277ffaf4cda89d5201e81f9f19d', '729031d07f24408f8613c1bb35b9696d');
INSERT INTO `tp_act_proc_his` VALUES ('2094d11176df4f3ab8f143553339eeb5', '389469a4fd274609a40d8dc77ea5490c');
INSERT INTO `tp_act_proc_his` VALUES ('2285149d4ce641268e3ade7d51bd9bae', '9150b47a2add4f53ba3020f3a248661b');
INSERT INTO `tp_act_proc_his` VALUES ('30c90b71861d46a6a2067a1601725494', '7dd305a23b6e4d678b676fe944279c83');
INSERT INTO `tp_act_proc_his` VALUES ('312a6e51cf314c6382c3e2e945ea561a', 'b4030934d93d4aa891d7a58ec4a29220');
INSERT INTO `tp_act_proc_his` VALUES ('332f22252ba24f28aa3e6871c12b68c5', '53863fe8f13741ee8bd9ea942aeb5972');
INSERT INTO `tp_act_proc_his` VALUES ('3b0ad3eeb21a4eccbb73258ff13aba03', '4e1eed9be03849519d096e03baf3ebea');
INSERT INTO `tp_act_proc_his` VALUES ('3bac59eda8084ac89dbb17904bf72da4', 'dd0a385675f944e0a77ffd1516a4a581');
INSERT INTO `tp_act_proc_his` VALUES ('3c274f2f2b194cdab08b0742c669ffd8', '09be6a6c82f14ca4812e98711c286a3e');
INSERT INTO `tp_act_proc_his` VALUES ('3e05facde4dc4fbabecb7970f1883bd4', '457ae5c0f79e4474ad4d70eaff84bbe4');
INSERT INTO `tp_act_proc_his` VALUES ('406f72f81b2245079b78348544595c21', '6b2daf86118a43e39f94184c32e5330d');
INSERT INTO `tp_act_proc_his` VALUES ('41a8602426e346998ab84544ce2598bf', '5447e8d876f340bba9071cc3ea4bc6f4');
INSERT INTO `tp_act_proc_his` VALUES ('461289e687694820ba31752e4f556e29', '362d27d5484f4841ac1ca25034e9d877');
INSERT INTO `tp_act_proc_his` VALUES ('5235ad17826f44ea9db9353c13567243', '76a4b7117f844242b9aad97272b5039a');
INSERT INTO `tp_act_proc_his` VALUES ('54e08f87e2894f7189f1e7cfb59477ca', 'f8c82f9a39cb414eb980e06f1ed145a1');
INSERT INTO `tp_act_proc_his` VALUES ('5cc625a366fd413b949f5cdbafe6a244', '002f145c4117423e98ebbd28767d82c0');
INSERT INTO `tp_act_proc_his` VALUES ('5dcff21daff84022a4fd6d62f08482fa', '77ac16a2e997478e9b13ea16b95b1ea0');
INSERT INTO `tp_act_proc_his` VALUES ('5e6405e87c444aa2b2aee45112eacdc0', 'ba2d5d41ac534564a2f7690a3c2d8d82');
INSERT INTO `tp_act_proc_his` VALUES ('6eb63f36d64a4252aec9f1fe1369a9b7', '65c5aa7d525041119cc0ae68df8ccff9');
INSERT INTO `tp_act_proc_his` VALUES ('86a41918198d475c9b9f9487d3477f87', 'd6c95d3a73104eeab73e8a9b58e6c305');
INSERT INTO `tp_act_proc_his` VALUES ('890dce1567f94b22a23c0bb55c333b56', '702fd8512a9a4f7eae2425b757d28e0a');
INSERT INTO `tp_act_proc_his` VALUES ('8cc0eb7057844c4a81875a867bea3e4a', '4d7697bf42114ee9bc888516b0687ce0');
INSERT INTO `tp_act_proc_his` VALUES ('8df17f8932074dda8ae43a586719d582', '71af3cd5d4bc44c19e02e08165e619fb');
INSERT INTO `tp_act_proc_his` VALUES ('af6d1b1c387f42f5858d246d82d6a5d3', '14c8dee4de724c30af3ac5eee3a33679');
INSERT INTO `tp_act_proc_his` VALUES ('b16b4c7b4aa74086afa64fa5da580a8d', '0a47aae5e6644d60b07bca9c347a355a');
INSERT INTO `tp_act_proc_his` VALUES ('b34cadb4749c4e49b2d32f1ee8429e96', '2b789ebbb5ab4a93aa8e54266ec4d48c');
INSERT INTO `tp_act_proc_his` VALUES ('d26a9367544b495e844f48c0921edefd', '6580af7be48b43fa9e2f722f9addfbc4');
INSERT INTO `tp_act_proc_his` VALUES ('e04fc5f5006c444a8a4db37d9c7c4d55', 'cb248a46b1d84491a0c7b6ab2060dc62');
INSERT INTO `tp_act_proc_his` VALUES ('e478038577374c2cb5ee2202cf850638', '6c03cb238b8c47658095a45a11ba111d');
INSERT INTO `tp_act_proc_his` VALUES ('e6867ee5f0cf40b48d94b194cc2080f1', 'ef60150bff0f497f9d00327f8144add7');
INSERT INTO `tp_act_proc_his` VALUES ('ed09088e3fc84299bdf574dadcfe7c5f', '87af228fb7334ed1a3744c630ab9fdee');
INSERT INTO `tp_act_proc_his` VALUES ('ed5f4e7a51a748e4872e83c91c1b24ce', '02da9ed860b5408daf6b29a9af76e9ce');

-- ----------------------------
-- Table structure for tp_article_classify
-- ----------------------------
DROP TABLE IF EXISTS `tp_article_classify`;
CREATE TABLE `tp_article_classify` (
  `id` varchar(64) COLLATE utf8_bin NOT NULL,
  `name` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '期刊名称',
  `type` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '期刊分类',
  `category` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '期刊类别',
  `year` varchar(10) COLLATE utf8_bin DEFAULT NULL COMMENT '年限',
  `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
  `create_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
  `update_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标识',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='文章期刊分类表';

-- ----------------------------
-- Records of tp_article_classify
-- ----------------------------
INSERT INTO `tp_article_classify` VALUES ('0e461a96a02148799f202708bda22b0b', '低温工程', '2', '导航与控制', '2014', '1', '2018-07-19 10:30:51', '1', '2018-07-19 10:30:51', '0');
INSERT INTO `tp_article_classify` VALUES ('2fb1ad70531743b0af75b9175419ea4f', '中国生物工程杂志', '3', '生物工程类', '2013', '1', '2018-07-19 10:30:56', '1', '2018-07-19 10:30:56', '0');
INSERT INTO `tp_article_classify` VALUES ('371e057798d149b7b79ddf9f246ab6c3', '低温工程信息', '3', '导航与控制', '2018', '1', '2018-07-19 10:33:07', '1', '2018-07-19 10:33:07', '0');
INSERT INTO `tp_article_classify` VALUES ('3be09d0a65374d8697c864351d31964d', '低温工程身上', '2', '导航与控制', '2017', '1', '2018-06-15 14:59:56', '1', '2018-06-15 14:59:56', '0');
INSERT INTO `tp_article_classify` VALUES ('5060b3119444418c85f8609ae0fd825b', '低温工程擦', '2', '导航与控制', '2016', '1', '2018-06-15 15:00:00', '1', '2018-06-15 15:00:00', '0');
INSERT INTO `tp_article_classify` VALUES ('8a2f68da14244b26885a4910e46e671c', '社会啊', '3', '社会', '2018', '1', '2018-07-19 10:30:43', '1', '2018-07-19 10:30:43', '0');
INSERT INTO `tp_article_classify` VALUES ('8af8b2575adb4e53b65a142d3dd73d7f', '中国生物工程杂志', '3', '生物工程类', '2019-09', '1', '2019-11-10 20:33:38', '1', '2019-11-10 20:33:38', '0');
INSERT INTO `tp_article_classify` VALUES ('91dc899653c64eb0b260a70cfbc13317', '低温工程啊', '1', '导航与控制', '2015', '1', '2018-07-19 10:33:29', '1', '2018-07-19 10:33:29', '0');
INSERT INTO `tp_article_classify` VALUES ('a1f04e99a22a4cd69fa53f67197c8d25', '生物学研究期刊', '3', '生物工程类', null, '1', '2019-11-10 20:23:27', '1', '2019-11-10 20:23:27', '0');

-- ----------------------------
-- Table structure for tp_class_group
-- ----------------------------
DROP TABLE IF EXISTS `tp_class_group`;
CREATE TABLE `tp_class_group` (
  `id` varchar(64) COLLATE utf8_bin NOT NULL COMMENT '主键',
  `name` varchar(64) COLLATE utf8_bin NOT NULL COMMENT '名称',
  `description` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '描述',
  `principal_id` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '负责人id(第一级为班主任、第二级为小组长)',
  `parent_id` varchar(64) COLLATE utf8_bin DEFAULT '-1' COMMENT '父节点id(第一级为-1)',
  `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
  `create_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
  `update_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `remarks` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) COLLATE utf8_bin DEFAULT NULL COMMENT '删除标记',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='学生班级-小组表';

-- ----------------------------
-- Records of tp_class_group
-- ----------------------------

-- ----------------------------
-- Table structure for tp_class_student_group
-- ----------------------------
DROP TABLE IF EXISTS `tp_class_student_group`;
CREATE TABLE `tp_class_student_group` (
  `student_id` varchar(64) COLLATE utf8_bin NOT NULL COMMENT '学生id(来源于用户表主键)',
  `class_group_id` varchar(64) COLLATE utf8_bin NOT NULL COMMENT '班级小组id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='学生所属班级小组表';

-- ----------------------------
-- Records of tp_class_student_group
-- ----------------------------

-- ----------------------------
-- Table structure for tp_custom_title
-- ----------------------------
DROP TABLE IF EXISTS `tp_custom_title`;
CREATE TABLE `tp_custom_title` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` varchar(64) COLLATE utf8_bin DEFAULT '' COMMENT '所属菜单id',
  `column_name` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '字段属性名称',
  `label_name` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '字段显示名称',
  `show` tinyint(1) unsigned DEFAULT '1' COMMENT '是否显示：0=否  1=是',
  `required` tinyint(1) unsigned DEFAULT '0' COMMENT '是否必填：0=否 1=是',
  `description` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique` (`menu_id`,`column_name`) USING BTREE COMMENT '联合主键'
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of tp_custom_title
-- ----------------------------
INSERT INTO `tp_custom_title` VALUES ('1', 'ebb235c8fc6f4930a0f4817434bd7212', 'name', '课程名称', '1', '1', '');
INSERT INTO `tp_custom_title` VALUES ('3', 'ebb235c8fc6f4930a0f4817434bd7212', 'user.id', '授课老师', '1', '1', null);
INSERT INTO `tp_custom_title` VALUES ('4', 'ebb235c8fc6f4930a0f4817434bd7212', 'user.loginName', '工号', '1', '0', null);
INSERT INTO `tp_custom_title` VALUES ('5', 'ebb235c8fc6f4930a0f4817434bd7212', 'user.name', '老师', '1', '0', null);
INSERT INTO `tp_custom_title` VALUES ('6', 'ebb235c8fc6f4930a0f4817434bd7212', 'teachType', '类型', '1', '1', null);
INSERT INTO `tp_custom_title` VALUES ('7', 'ebb235c8fc6f4930a0f4817434bd7212', 'stuNum', '工作量', '1', '0', null);
INSERT INTO `tp_custom_title` VALUES ('8', 'ebb235c8fc6f4930a0f4817434bd7212', 'startTime', '开始时间', '1', '1', null);
INSERT INTO `tp_custom_title` VALUES ('9', 'ebb235c8fc6f4930a0f4817434bd7212', 'endTime', '结束时间', '1', '1', null);
INSERT INTO `tp_custom_title` VALUES ('10', 'ebb235c8fc6f4930a0f4817434bd7212', 'stuType', '授课学员类型', '1', '0', null);
INSERT INTO `tp_custom_title` VALUES ('11', 'ebb235c8fc6f4930a0f4817434bd7212', 'stuGrade', '学员年级', '1', '0', null);
INSERT INTO `tp_custom_title` VALUES ('12', 'ebb235c8fc6f4930a0f4817434bd7212', 'department.id', '所属科室', '1', '1', null);
INSERT INTO `tp_custom_title` VALUES ('13', 'ebb235c8fc6f4930a0f4817434bd7212', 'trc.id', '所属教研室', '1', '0', null);
INSERT INTO `tp_custom_title` VALUES ('14', 'ebb235c8fc6f4930a0f4817434bd7212', 'administrative', '管理部门', '0', '0', null);
INSERT INTO `tp_custom_title` VALUES ('15', 'ebb235c8fc6f4930a0f4817434bd7212', 'activePlace', '地点', '1', '0', null);
INSERT INTO `tp_custom_title` VALUES ('16', 'ebb235c8fc6f4930a0f4817434bd7212', 'activeIntroduce', '内容介绍', '1', '0', null);
INSERT INTO `tp_custom_title` VALUES ('17', 'ebb235c8fc6f4930a0f4817434bd7212', 'remarks', '备注', '1', '0', null);
INSERT INTO `tp_custom_title` VALUES ('18', 'ebb235c8fc6f4930a0f4817434bd7212', 'status', '审核状态', '1', '0', null);
INSERT INTO `tp_custom_title` VALUES ('19', '4cb0b0a67cad448a9145d3c281b26a52', 'name', '名称', '1', '0', null);
INSERT INTO `tp_custom_title` VALUES ('20', '4cb0b0a67cad448a9145d3c281b26a52', 'user.id', '教师', '1', '0', null);
INSERT INTO `tp_custom_title` VALUES ('21', '4cb0b0a67cad448a9145d3c281b26a52', 'user.loginName', '工号', '1', '0', null);
INSERT INTO `tp_custom_title` VALUES ('22', '4cb0b0a67cad448a9145d3c281b26a52', 'user.name', '教师', '1', '0', null);
INSERT INTO `tp_custom_title` VALUES ('23', '4cb0b0a67cad448a9145d3c281b26a52', 'teachType', '类型', '1', '0', null);
INSERT INTO `tp_custom_title` VALUES ('24', '4cb0b0a67cad448a9145d3c281b26a52', 'stuNum', '工作量', '1', '0', null);
INSERT INTO `tp_custom_title` VALUES ('25', '4cb0b0a67cad448a9145d3c281b26a52', 'startTime', '开始时间', '1', '0', null);
INSERT INTO `tp_custom_title` VALUES ('26', '4cb0b0a67cad448a9145d3c281b26a52', 'endTime', '结束时间', '1', '0', null);
INSERT INTO `tp_custom_title` VALUES ('27', '4cb0b0a67cad448a9145d3c281b26a52', 'stuType', '', '1', '0', null);
INSERT INTO `tp_custom_title` VALUES ('28', '4cb0b0a67cad448a9145d3c281b26a52', 'stuGrade', '', '1', '0', null);
INSERT INTO `tp_custom_title` VALUES ('29', '4cb0b0a67cad448a9145d3c281b26a52', 'department.id', '', '1', '0', null);
INSERT INTO `tp_custom_title` VALUES ('30', '4cb0b0a67cad448a9145d3c281b26a52', 'trc.id', '', '1', '0', null);
INSERT INTO `tp_custom_title` VALUES ('31', '4cb0b0a67cad448a9145d3c281b26a52', 'administrative', '', '0', '0', null);
INSERT INTO `tp_custom_title` VALUES ('32', '4cb0b0a67cad448a9145d3c281b26a52', 'activePlace', '', '0', '0', null);
INSERT INTO `tp_custom_title` VALUES ('33', '4cb0b0a67cad448a9145d3c281b26a52', 'activeIntroduce', '内容描述', '1', '0', null);
INSERT INTO `tp_custom_title` VALUES ('34', '4cb0b0a67cad448a9145d3c281b26a52', 'remarks', '备注信息', '1', '0', null);
INSERT INTO `tp_custom_title` VALUES ('35', '4cb0b0a67cad448a9145d3c281b26a52', 'status', '审核状态', '1', '0', null);
INSERT INTO `tp_custom_title` VALUES ('36', '7005cc22a69946da9c1b53746d8eae92', 'name', '内容名称', '1', '0', null);
INSERT INTO `tp_custom_title` VALUES ('37', '7005cc22a69946da9c1b53746d8eae92', 'user.id', '所属老师', '1', '0', null);
INSERT INTO `tp_custom_title` VALUES ('38', '7005cc22a69946da9c1b53746d8eae92', 'user.loginName', '工号', '1', '0', null);
INSERT INTO `tp_custom_title` VALUES ('39', '7005cc22a69946da9c1b53746d8eae92', 'user.name', '所属老师', '1', '1', null);
INSERT INTO `tp_custom_title` VALUES ('40', '7005cc22a69946da9c1b53746d8eae92', 'teachType', '类型', '1', '1', null);
INSERT INTO `tp_custom_title` VALUES ('41', '7005cc22a69946da9c1b53746d8eae92', 'stuNum', '工作量', '1', '0', null);
INSERT INTO `tp_custom_title` VALUES ('42', '7005cc22a69946da9c1b53746d8eae92', 'startTime', '开始时间', '1', '0', null);
INSERT INTO `tp_custom_title` VALUES ('43', '7005cc22a69946da9c1b53746d8eae92', 'endTime', '结束时间', '1', '0', null);
INSERT INTO `tp_custom_title` VALUES ('44', '7005cc22a69946da9c1b53746d8eae92', 'stuType', '学员类型', '1', '1', null);
INSERT INTO `tp_custom_title` VALUES ('45', '7005cc22a69946da9c1b53746d8eae92', 'stuGrade', '', '1', '0', null);
INSERT INTO `tp_custom_title` VALUES ('46', '7005cc22a69946da9c1b53746d8eae92', 'department.id', '所属科室', '1', '0', null);
INSERT INTO `tp_custom_title` VALUES ('47', '7005cc22a69946da9c1b53746d8eae92', 'trc.id', '所属教研室', '1', '0', null);
INSERT INTO `tp_custom_title` VALUES ('48', '7005cc22a69946da9c1b53746d8eae92', 'administrative', '', '0', '0', null);
INSERT INTO `tp_custom_title` VALUES ('49', '7005cc22a69946da9c1b53746d8eae92', 'activePlace', '', '0', '0', null);
INSERT INTO `tp_custom_title` VALUES ('50', '7005cc22a69946da9c1b53746d8eae92', 'activeIntroduce', '', '0', '0', null);
INSERT INTO `tp_custom_title` VALUES ('51', '7005cc22a69946da9c1b53746d8eae92', 'remarks', '备注信息', '1', '0', null);
INSERT INTO `tp_custom_title` VALUES ('52', '7005cc22a69946da9c1b53746d8eae92', 'status', '', '1', '0', null);
INSERT INTO `tp_custom_title` VALUES ('53', 'd7c9e959894e43abbb349b0ab2cb8d30', 'name', '内容名称', '1', '0', null);
INSERT INTO `tp_custom_title` VALUES ('54', 'd7c9e959894e43abbb349b0ab2cb8d30', 'user.id', '老师', '1', '1', null);
INSERT INTO `tp_custom_title` VALUES ('55', 'd7c9e959894e43abbb349b0ab2cb8d30', 'user.loginName', '工号', '1', '0', null);
INSERT INTO `tp_custom_title` VALUES ('56', 'd7c9e959894e43abbb349b0ab2cb8d30', 'user.name', '老师', '1', '0', null);
INSERT INTO `tp_custom_title` VALUES ('57', 'd7c9e959894e43abbb349b0ab2cb8d30', 'teachType', '类型', '1', '1', null);
INSERT INTO `tp_custom_title` VALUES ('58', 'd7c9e959894e43abbb349b0ab2cb8d30', 'stuNum', '', '1', '0', null);
INSERT INTO `tp_custom_title` VALUES ('59', 'd7c9e959894e43abbb349b0ab2cb8d30', 'startTime', '', '1', '0', null);
INSERT INTO `tp_custom_title` VALUES ('60', 'd7c9e959894e43abbb349b0ab2cb8d30', 'endTime', '', '1', '0', null);
INSERT INTO `tp_custom_title` VALUES ('61', 'd7c9e959894e43abbb349b0ab2cb8d30', 'stuType', '', '1', '0', null);
INSERT INTO `tp_custom_title` VALUES ('62', 'd7c9e959894e43abbb349b0ab2cb8d30', 'stuGrade', '', '1', '0', null);
INSERT INTO `tp_custom_title` VALUES ('63', 'd7c9e959894e43abbb349b0ab2cb8d30', 'department.id', '', '1', '1', null);
INSERT INTO `tp_custom_title` VALUES ('64', 'd7c9e959894e43abbb349b0ab2cb8d30', 'trc.id', '', '1', '0', null);
INSERT INTO `tp_custom_title` VALUES ('65', 'd7c9e959894e43abbb349b0ab2cb8d30', 'administrative', '', '0', '0', null);
INSERT INTO `tp_custom_title` VALUES ('66', 'd7c9e959894e43abbb349b0ab2cb8d30', 'activePlace', '', '0', '0', null);
INSERT INTO `tp_custom_title` VALUES ('67', 'd7c9e959894e43abbb349b0ab2cb8d30', 'activeIntroduce', '内容描述', '1', '0', null);
INSERT INTO `tp_custom_title` VALUES ('68', 'd7c9e959894e43abbb349b0ab2cb8d30', 'remarks', '', '1', '0', null);
INSERT INTO `tp_custom_title` VALUES ('69', 'd7c9e959894e43abbb349b0ab2cb8d30', 'status', '', '1', '0', null);
INSERT INTO `tp_custom_title` VALUES ('70', '154835c75c1d4cc383de20e50373b6cf', 'name', '名称', '1', '0', null);
INSERT INTO `tp_custom_title` VALUES ('71', '154835c75c1d4cc383de20e50373b6cf', 'user.id', '老师', '1', '1', null);
INSERT INTO `tp_custom_title` VALUES ('72', '154835c75c1d4cc383de20e50373b6cf', 'user.loginName', '工号', '1', '0', null);
INSERT INTO `tp_custom_title` VALUES ('73', '154835c75c1d4cc383de20e50373b6cf', 'user.name', '老师', '1', '0', null);
INSERT INTO `tp_custom_title` VALUES ('74', '154835c75c1d4cc383de20e50373b6cf', 'teachType', '类型', '1', '1', null);
INSERT INTO `tp_custom_title` VALUES ('75', '154835c75c1d4cc383de20e50373b6cf', 'stuNum', '', '1', '0', null);
INSERT INTO `tp_custom_title` VALUES ('76', '154835c75c1d4cc383de20e50373b6cf', 'startTime', '', '1', '0', null);
INSERT INTO `tp_custom_title` VALUES ('77', '154835c75c1d4cc383de20e50373b6cf', 'endTime', '', '1', '0', null);
INSERT INTO `tp_custom_title` VALUES ('78', '154835c75c1d4cc383de20e50373b6cf', 'stuType', '', '0', '0', null);
INSERT INTO `tp_custom_title` VALUES ('79', '154835c75c1d4cc383de20e50373b6cf', 'stuGrade', '', '0', '0', null);
INSERT INTO `tp_custom_title` VALUES ('80', '154835c75c1d4cc383de20e50373b6cf', 'department.id', '', '1', '0', null);
INSERT INTO `tp_custom_title` VALUES ('81', '154835c75c1d4cc383de20e50373b6cf', 'trc.id', '', '1', '0', null);
INSERT INTO `tp_custom_title` VALUES ('82', '154835c75c1d4cc383de20e50373b6cf', 'administrative', '', '0', '0', null);
INSERT INTO `tp_custom_title` VALUES ('83', '154835c75c1d4cc383de20e50373b6cf', 'activePlace', '', '0', '0', null);
INSERT INTO `tp_custom_title` VALUES ('84', '154835c75c1d4cc383de20e50373b6cf', 'activeIntroduce', '', '0', '0', null);
INSERT INTO `tp_custom_title` VALUES ('85', '154835c75c1d4cc383de20e50373b6cf', 'remarks', '备注信息', '1', '0', null);
INSERT INTO `tp_custom_title` VALUES ('86', '154835c75c1d4cc383de20e50373b6cf', 'status', '审核状态', '1', '0', null);

-- ----------------------------
-- Table structure for tp_dic_title
-- ----------------------------
DROP TABLE IF EXISTS `tp_dic_title`;
CREATE TABLE `tp_dic_title` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='职称表';

-- ----------------------------
-- Records of tp_dic_title
-- ----------------------------

-- ----------------------------
-- Table structure for tp_fileupload_info
-- ----------------------------
DROP TABLE IF EXISTS `tp_fileupload_info`;
CREATE TABLE `tp_fileupload_info` (
  `id` varchar(64) COLLATE utf8_bin NOT NULL COMMENT '主键id',
  `file_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '文件名',
  `size` int(10) DEFAULT NULL COMMENT '文件大小',
  `suffix` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '文件后缀',
  `file_path` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '文件路径',
  `download_num` int(10) DEFAULT '0' COMMENT '下载次数',
  `file_type` smallint(3) DEFAULT NULL COMMENT '文件类型',
  `model` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '文件所属模块',
  `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
  `create_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
  `update_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `remarks` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) COLLATE utf8_bin DEFAULT NULL COMMENT '删除标记',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='文件表';

-- ----------------------------
-- Records of tp_fileupload_info
-- ----------------------------

-- ----------------------------
-- Table structure for tp_p_student
-- ----------------------------
DROP TABLE IF EXISTS `tp_p_student`;
CREATE TABLE `tp_p_student` (
  `user_id` varchar(64) COLLATE utf8_bin NOT NULL,
  `is_master` int(11) DEFAULT '0' COMMENT '是否研究生（0否 1是）',
  `teacher_user_id` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '带教老师userId',
  `tutor_user_id` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '导师userId',
  `grade` varchar(20) COLLATE utf8_bin DEFAULT '' COMMENT '年级',
  `stu_type` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '学生类别，数据来源于字典表',
  `political_outlook` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '政治面貌，数据来源于字典表',
  `nation` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '民族，数据来源于字典',
  `master_type` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '研究生类型，数据来源于字典表',
  `degree` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '学位（数据来源于字典表）',
  `in_school` date DEFAULT NULL COMMENT '入学时间',
  `out_school` date DEFAULT NULL COMMENT '毕业时间',
  `birth_place` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '生源地',
  `enrolment_way` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '入学方式（数据来源于字典表）',
  `root_in` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '来源(字典类型=tp_stu_root_in)',
  `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
  `create_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
  `update_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `remarks` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) COLLATE utf8_bin DEFAULT NULL COMMENT '删除标记',
  `student_no` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='学生信息表';

-- ----------------------------
-- Records of tp_p_student
-- ----------------------------

-- ----------------------------
-- Table structure for tp_p_teacher
-- ----------------------------
DROP TABLE IF EXISTS `tp_p_teacher`;
CREATE TABLE `tp_p_teacher` (
  `user_id` varchar(64) COLLATE utf8_bin NOT NULL,
  `title_id` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '职称id(来源于字典表，类型=tp_title)',
  `tutor_type` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '导师类型(来源于字典表，类型=tp_tutor_type)',
  `political_outlook` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '政治面貌(来源于字典表，类型=tp_political_outlook)',
  `is_master_tutor` int(11) DEFAULT '0' COMMENT '是否硕导（0否 1是）',
  `is_undergraduate_tutor` int(11) DEFAULT NULL,
  `is_doctor_tutor` int(11) DEFAULT '0' COMMENT '是否博导（0否 1是）',
  `trc_id` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '所属教研室Id',
  `department_id` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '科室Id',
  `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
  `create_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
  `update_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `remarks` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) COLLATE utf8_bin DEFAULT NULL COMMENT '删除标记',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='老师信息表';

-- ----------------------------
-- Records of tp_p_teacher
-- ----------------------------

-- ----------------------------
-- Table structure for tp_student_exam
-- ----------------------------
DROP TABLE IF EXISTS `tp_student_exam`;
CREATE TABLE `tp_student_exam` (
  `id` varchar(64) COLLATE utf8_bin NOT NULL,
  `user_id` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '参考人',
  `department_id` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '所属科室',
  `full_name` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '参考人姓名',
  `exam_name` varchar(20) COLLATE utf8_bin DEFAULT NULL COMMENT '考试名称',
  `exam_result` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '考试结果',
  `no_pass_num` int(11) DEFAULT NULL COMMENT '未通过门数',
  `exam_date` datetime DEFAULT NULL COMMENT '考试时间',
  `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
  `create_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
  `update_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `remarks` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) COLLATE utf8_bin DEFAULT NULL COMMENT '删除标记',
  `not_pass_course` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '未通过科目',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='考试记录';

-- ----------------------------
-- Records of tp_student_exam
-- ----------------------------

-- ----------------------------
-- Table structure for tp_student_graduate
-- ----------------------------
DROP TABLE IF EXISTS `tp_student_graduate`;
CREATE TABLE `tp_student_graduate` (
  `id` varchar(64) COLLATE utf8_bin NOT NULL,
  `user_id` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '提交人',
  `full_name` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '提交人姓名',
  `review_opinions` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '匿名评阅意见情况',
  `question_num` int(11) DEFAULT NULL COMMENT '出问题份数',
  `result` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '结果',
  `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
  `create_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
  `update_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `remarks` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) COLLATE utf8_bin DEFAULT NULL COMMENT '删除标记',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='毕业论文情况';

-- ----------------------------
-- Records of tp_student_graduate
-- ----------------------------

-- ----------------------------
-- Table structure for tp_student_meeting
-- ----------------------------
DROP TABLE IF EXISTS `tp_student_meeting`;
CREATE TABLE `tp_student_meeting` (
  `id` varchar(64) COLLATE utf8_bin NOT NULL,
  `user_id` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `full_name` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '参会人姓名',
  `meeting_start` date DEFAULT NULL COMMENT '开始时间',
  `meeting_end` date DEFAULT NULL COMMENT '结束时间',
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '会议名',
  `level` int(11) DEFAULT NULL COMMENT '学术会议级别',
  `communicate_type` int(11) DEFAULT NULL COMMENT '交流类型',
  `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
  `create_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
  `update_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `remarks` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) COLLATE utf8_bin DEFAULT NULL COMMENT '删除标记',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='参加学术会议记录表';

-- ----------------------------
-- Records of tp_student_meeting
-- ----------------------------

-- ----------------------------
-- Table structure for tp_student_punish
-- ----------------------------
DROP TABLE IF EXISTS `tp_student_punish`;
CREATE TABLE `tp_student_punish` (
  `id` varchar(64) COLLATE utf8_bin NOT NULL,
  `user_id` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `full_name` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '处分标题',
  `level` int(11) DEFAULT NULL COMMENT '1.通报批评、2.警告、3.严重警告、4.留校察看、5.开除',
  `punish_time` date DEFAULT NULL COMMENT '处分时间',
  `reason` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '处分理由',
  `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
  `create_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
  `update_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `remarks` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) COLLATE utf8_bin DEFAULT NULL COMMENT '删除标记',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='处分情况表';

-- ----------------------------
-- Records of tp_student_punish
-- ----------------------------

-- ----------------------------
-- Table structure for tp_student_teacher
-- ----------------------------
DROP TABLE IF EXISTS `tp_student_teacher`;
CREATE TABLE `tp_student_teacher` (
  `id` varchar(64) COLLATE utf8_bin NOT NULL COMMENT '主键',
  `teacher_id` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '带教/辅导教师',
  `stu_id` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '学生id',
  `start_date` varchar(24) COLLATE utf8_bin DEFAULT NULL COMMENT '开始日期，格式=yyyy-MM-dd',
  `end_date` varchar(24) COLLATE utf8_bin DEFAULT NULL COMMENT '结束日期，格式=yyyy-MM-dd',
  `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=辅导老师、2=带教老师',
  `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='学生的辅导老师表';

-- ----------------------------
-- Records of tp_student_teacher
-- ----------------------------

-- ----------------------------
-- Table structure for tp_task
-- ----------------------------
DROP TABLE IF EXISTS `tp_task`;
CREATE TABLE `tp_task` (
  `id` varchar(64) NOT NULL,
  `msg_type` int(11) DEFAULT '0' COMMENT '消息类型 0：邮件消息 1：微信消息 2：短信消息',
  `name` varchar(255) DEFAULT NULL COMMENT '任务名称',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `dispatch_time` datetime DEFAULT NULL COMMENT '调度时间',
  `url` varchar(255) DEFAULT NULL COMMENT '发送链接',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `content` text COMMENT '发送内容',
  `remark` varchar(300) DEFAULT NULL COMMENT '备注',
  `state` int(11) DEFAULT '0' COMMENT '调度状态  0:未调度 1:准备调度2:即时调度 -2:进行中 3:部分成功  9：调度成功 -9：调度失败  -1:失效',
  `result_msg` text COMMENT '发送结果',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
  `create_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
  `update_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '删除标记',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='消息推送表';

-- ----------------------------
-- Records of tp_task
-- ----------------------------

-- ----------------------------
-- Table structure for tp_task_dispatch
-- ----------------------------
DROP TABLE IF EXISTS `tp_task_dispatch`;
CREATE TABLE `tp_task_dispatch` (
  `task_id` varchar(64) NOT NULL DEFAULT '0',
  `role_id` varchar(64) DEFAULT NULL,
  `user_ids` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='消息推送人员中间表';

-- ----------------------------
-- Records of tp_task_dispatch
-- ----------------------------

-- ----------------------------
-- Table structure for tp_teach_achievement
-- ----------------------------
DROP TABLE IF EXISTS `tp_teach_achievement`;
CREATE TABLE `tp_teach_achievement` (
  `id` varchar(64) COLLATE utf8_bin NOT NULL,
  `teach_id` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '课程建设人',
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '成果名称',
  `level` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '选择：医学部级/北京大学级/省部级/国家级/其他',
  `complete_order_person` int(1) DEFAULT NULL COMMENT '第几完成人',
  `department_id` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '所属科室',
  `trc_id` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '教研室Id',
  `status` int(255) DEFAULT '0',
  `PROC_INS_ID` varchar(64) COLLATE utf8_bin DEFAULT '' COMMENT '流程实例ID',
  `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
  `create_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
  `update_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `remarks` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) COLLATE utf8_bin DEFAULT NULL COMMENT '删除标记',
  `winning_time` datetime DEFAULT NULL COMMENT '获奖时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='教学成果表';

-- ----------------------------
-- Records of tp_teach_achievement
-- ----------------------------

-- ----------------------------
-- Table structure for tp_teach_active
-- ----------------------------
DROP TABLE IF EXISTS `tp_teach_active`;
CREATE TABLE `tp_teach_active` (
  `id` varchar(64) COLLATE utf8_bin NOT NULL,
  `teach_id` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '对应于老师的userId',
  `teach_type` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '教学活动类型（来源于数据字典）',
  `name` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `stu_num` double(11,2) DEFAULT NULL COMMENT '带教量',
  `start_time` varchar(24) COLLATE utf8_bin DEFAULT NULL COMMENT '上课开始时间',
  `end_time` varchar(24) COLLATE utf8_bin DEFAULT NULL COMMENT '上课结束时间',
  `stu_type` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '上课学生类别，数据来源于字典表',
  `stu_grade` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '上课学员年级',
  `department_id` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '所属科室',
  `trc_id` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '教研室Id',
  `administrative` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '管理部门',
  `active_place` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `active_introduce` text COLLATE utf8_bin COMMENT '活动介绍',
  `status` int(1) DEFAULT '0' COMMENT '审核状态',
  `PROC_INS_ID` varchar(64) COLLATE utf8_bin DEFAULT '' COMMENT '流程实例ID',
  `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
  `create_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
  `update_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `remarks` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) COLLATE utf8_bin DEFAULT NULL COMMENT '删除标记',
  `score` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '得分',
  `files` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='教学活动表';

-- ----------------------------
-- Records of tp_teach_active
-- ----------------------------
INSERT INTO `tp_teach_active` VALUES ('0c1b6e7559514a2f87fcb1631a6d3529', '4b174b574c2440b392804373b7e86358', '15', '月考1', '1.00', '2019-07-18 14:00:00', '2019-07-18 23:00:00', '1', '2017', '3b74e44a2c8042f7801972b9fa11bf73', 'b1832f57f0624091ba4ce67ca2f787dc', null, null, 0xE681A9E681A9, '2', '', 'a3884e19a3074f6fac83862f3c1e406e', '2019-07-18 23:58:00', 'a3884e19a3074f6fac83862f3c1e406e', '2019-07-18 23:58:00', null, '0', null, null);
INSERT INTO `tp_teach_active` VALUES ('1c85db66f18b4c32815287a1af0f42ca', '99a97b57567a4e3793bc36d90d67fa4a', '15', '2013级诊断学多站考核', '1.00', '2019-08-03 13:00:00', '2019-08-04 13:00:00', null, null, '3b74e44a2c8042f7801972b9fa11bf73', 'b1832f57f0624091ba4ce67ca2f787dc', null, null, null, '2', '', '1', '2019-08-03 13:47:52', '1', '2019-08-03 13:47:52', null, '0', null, null);

-- ----------------------------
-- Table structure for tp_teach_articles
-- ----------------------------
DROP TABLE IF EXISTS `tp_teach_articles`;
CREATE TABLE `tp_teach_articles` (
  `id` varchar(64) COLLATE utf8_bin NOT NULL,
  `author_id` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT 'userId',
  `author_name` varchar(20) COLLATE utf8_bin DEFAULT NULL COMMENT '第一作者或通讯作者',
  `article_type` varchar(64) COLLATE utf8_bin DEFAULT '' COMMENT '文章类型',
  `article_title` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '论文名',
  `push_journal` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '发表文章期刊',
  `journal_type` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '期刊类型',
  `push_year` varchar(10) COLLATE utf8_bin DEFAULT NULL COMMENT '发表年份',
  `volume` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `pages` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '页数',
  `OnlTime` date DEFAULT NULL COMMENT 'online收录时间',
  `OnlElement` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT 'online影响因子',
  `OnlJcr` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT 'onlineJCR分区',
  `onl_jcr_url` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `auth_content` varchar(200) COLLATE utf8_bin DEFAULT NULL COMMENT '作者情况',
  `is_apply_degree` int(255) DEFAULT '0' COMMENT '是否以该篇论文申请学位（0否 1是）',
  `department_id` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '所属科室',
  `trc_id` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '教研室Id',
  `PROC_INS_ID` varchar(64) COLLATE utf8_bin DEFAULT '' COMMENT '流程实例ID',
  `status` int(1) DEFAULT '0' COMMENT '审核状态',
  `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
  `create_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
  `update_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `remarks` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) COLLATE utf8_bin DEFAULT NULL COMMENT '删除标记',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='教学文章表';

-- ----------------------------
-- Records of tp_teach_articles
-- ----------------------------

-- ----------------------------
-- Table structure for tp_teach_award
-- ----------------------------
DROP TABLE IF EXISTS `tp_teach_award`;
CREATE TABLE `tp_teach_award` (
  `id` varchar(64) COLLATE utf8_bin NOT NULL,
  `user_id` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '课程建设人',
  `title` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '课程名称',
  `winning_time` datetime DEFAULT NULL COMMENT '获奖时间',
  `level` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '选择：院级/医学部级/北京大学级/省部级/国家级/其他',
  `rank` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '名次（特等奖/一等奖/二等奖/三等奖/优秀奖）',
  `other_award` varchar(200) COLLATE utf8_bin DEFAULT NULL COMMENT '其他奖项',
  `department_id` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '所属科室',
  `trc_id` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '教研室Id',
  `status` int(1) DEFAULT '0' COMMENT '审核状态',
  `PROC_INS_ID` varchar(64) COLLATE utf8_bin DEFAULT '' COMMENT '流程实例ID',
  `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
  `create_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
  `update_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `remarks` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) COLLATE utf8_bin DEFAULT NULL COMMENT '删除标记',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='讲课比赛表';

-- ----------------------------
-- Records of tp_teach_award
-- ----------------------------

-- ----------------------------
-- Table structure for tp_teach_excellent_award
-- ----------------------------
DROP TABLE IF EXISTS `tp_teach_excellent_award`;
CREATE TABLE `tp_teach_excellent_award` (
  `id` varchar(64) COLLATE utf8_bin NOT NULL,
  `teach_id` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '课程建设人',
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '奖项名称',
  `level` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '选择：院级/医学部级/北京大学级/省部级/国家级/其他',
  `complete_order_person` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '第几完成人',
  `department_id` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '所属科室',
  `trc_id` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '教研室Id',
  `status` int(1) DEFAULT '0',
  `PROC_INS_ID` varchar(64) COLLATE utf8_bin DEFAULT '' COMMENT '流程实例ID',
  `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
  `create_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
  `update_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `remarks` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) COLLATE utf8_bin DEFAULT NULL COMMENT '删除标记',
  `winning_time` datetime DEFAULT NULL COMMENT '获奖时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='优秀教学奖';

-- ----------------------------
-- Records of tp_teach_excellent_award
-- ----------------------------
INSERT INTO `tp_teach_excellent_award` VALUES ('cade72229f944118a893588f58410cff', '1eaa799a36a94b1bbfee6539aa4c182a', 'xxxx', '4', '1', 'd30909a48df34509a8be6181c3435aed', '1985f500363a4327bceef63726234fee', '2', '', '1', '2019-11-16 22:02:19', '1', '2019-11-16 22:02:19', null, '0', '2019-11-16 00:00:00');

-- ----------------------------
-- Table structure for tp_teach_keep_project
-- ----------------------------
DROP TABLE IF EXISTS `tp_teach_keep_project`;
CREATE TABLE `tp_teach_keep_project` (
  `id` varchar(64) COLLATE utf8_bin NOT NULL,
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `level` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '省部级/国家级',
  `hold_time` datetime DEFAULT NULL,
  `join_num` int(11) DEFAULT NULL COMMENT '参加人数',
  `teacher_id` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `department_id` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '所属科室',
  `trc_id` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '教研室Id',
  `status` int(1) DEFAULT '0',
  `PROC_INS_ID` varchar(64) COLLATE utf8_bin DEFAULT '' COMMENT '流程实例ID',
  `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
  `create_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
  `update_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `remarks` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) COLLATE utf8_bin DEFAULT NULL COMMENT '删除标记',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='继续教育项目';

-- ----------------------------
-- Records of tp_teach_keep_project
-- ----------------------------

-- ----------------------------
-- Table structure for tp_teach_lesson_build
-- ----------------------------
DROP TABLE IF EXISTS `tp_teach_lesson_build`;
CREATE TABLE `tp_teach_lesson_build` (
  `id` varchar(64) COLLATE utf8_bin NOT NULL,
  `teach_id` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '课程建设人',
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '课程名称',
  `level` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '选择：医学部级/北京大学级/省部级/国家级',
  `build_time` datetime DEFAULT NULL COMMENT '获得时间',
  `department_id` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '所属科室',
  `trc_id` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '教研室Id',
  `status` int(1) DEFAULT '0',
  `PROC_INS_ID` varchar(64) COLLATE utf8_bin DEFAULT '' COMMENT '流程实例ID',
  `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
  `create_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
  `update_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `remarks` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) COLLATE utf8_bin DEFAULT NULL COMMENT '删除标记',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='课程建设表';

-- ----------------------------
-- Records of tp_teach_lesson_build
-- ----------------------------

-- ----------------------------
-- Table structure for tp_teach_material
-- ----------------------------
DROP TABLE IF EXISTS `tp_teach_material`;
CREATE TABLE `tp_teach_material` (
  `id` varchar(64) COLLATE utf8_bin NOT NULL,
  `teach_id` varchar(64) COLLATE utf8_bin NOT NULL COMMENT 'userId',
  `department_id` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '所属科室',
  `trc_id` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '教研室Id',
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `push_time` datetime DEFAULT NULL COMMENT '出版时间',
  `press` varchar(255) COLLATE utf8_bin DEFAULT '' COMMENT '出版社',
  `identity` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '参编情况（选择：主编/副主编/编者/主译/副主译/译者）',
  `status` tinyint(1) DEFAULT '0',
  `PROC_INS_ID` varchar(64) COLLATE utf8_bin DEFAULT '' COMMENT '流程实例ID',
  `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
  `create_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
  `update_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `remarks` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) COLLATE utf8_bin DEFAULT NULL COMMENT '删除标记',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='教材建设';

-- ----------------------------
-- Records of tp_teach_material
-- ----------------------------

-- ----------------------------
-- Table structure for tp_teach_patent
-- ----------------------------
DROP TABLE IF EXISTS `tp_teach_patent`;
CREATE TABLE `tp_teach_patent` (
  `id` varchar(64) COLLATE utf8_bin NOT NULL,
  `teach_id` varchar(64) COLLATE utf8_bin NOT NULL COMMENT 'userId',
  `department_id` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '所属科室',
  `trc_id` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '教研室Id',
  `author_name` varchar(200) COLLATE utf8_bin DEFAULT NULL COMMENT '专利权人',
  `inventioner` varchar(200) COLLATE utf8_bin DEFAULT NULL COMMENT '发明人',
  `patent_type` varchar(64) COLLATE utf8_bin DEFAULT '' COMMENT '专利类型',
  `patent_name` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '专利名称',
  `complete_order_person` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '第几完成人',
  `apply_time` date DEFAULT NULL COMMENT '申请日期',
  `grant_time` date DEFAULT NULL COMMENT '授权时间',
  `auth_content` varchar(200) COLLATE utf8_bin DEFAULT NULL COMMENT '作者情况',
  `PROC_INS_ID` varchar(64) COLLATE utf8_bin DEFAULT '' COMMENT '流程实例ID',
  `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
  `create_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
  `update_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `remarks` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '备注信息',
  `status` int(1) DEFAULT '0' COMMENT '审核状态',
  `del_flag` char(1) COLLATE utf8_bin DEFAULT NULL COMMENT '删除标记',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='研究专利表';

-- ----------------------------
-- Records of tp_teach_patent
-- ----------------------------

-- ----------------------------
-- Table structure for tp_teach_research
-- ----------------------------
DROP TABLE IF EXISTS `tp_teach_research`;
CREATE TABLE `tp_teach_research` (
  `id` varchar(64) COLLATE utf8_bin NOT NULL,
  `author_id` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT 'userId',
  `author_name` varchar(20) COLLATE utf8_bin DEFAULT NULL COMMENT '第一作者或通讯作者',
  `article_type` varchar(64) COLLATE utf8_bin DEFAULT '' COMMENT '文章类型',
  `article_title` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '论文名',
  `push_journal` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '发表文章期刊',
  `journal_type` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '期刊类型',
  `push_year` year(4) DEFAULT NULL COMMENT '发表年份',
  `volume` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `pages` int(11) DEFAULT NULL COMMENT '页数',
  `online_time` datetime DEFAULT NULL COMMENT 'online收录时间',
  `online_
IF` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT 'online影响因子',
  `online_JCR` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT 'onlineJCR分区',
  `auth_content` varchar(200) COLLATE utf8_bin DEFAULT NULL COMMENT '作者情况',
  `is_apply_degree` int(255) DEFAULT '0' COMMENT '是否以该篇论文申请学位（0否 1是）',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `department_id` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '所属科室',
  `trc_id` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '教研室Id',
  `create_id` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='教学研究表';

-- ----------------------------
-- Records of tp_teach_research
-- ----------------------------

-- ----------------------------
-- Table structure for tp_teach_subject
-- ----------------------------
DROP TABLE IF EXISTS `tp_teach_subject`;
CREATE TABLE `tp_teach_subject` (
  `id` varchar(64) COLLATE utf8_bin NOT NULL,
  `teacher_id` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `level` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '选择：院级/医学部级/北京大学级/省部级/国家级/其他',
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `department_id` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '所属科室',
  `trc_id` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '教研室Id',
  `status` tinyint(1) DEFAULT '0' COMMENT '审核状态',
  `PROC_INS_ID` varchar(64) COLLATE utf8_bin DEFAULT '' COMMENT '流程实例ID',
  `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
  `create_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '更新者',
  `update_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `remarks` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) COLLATE utf8_bin DEFAULT NULL COMMENT '删除标记',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='教学课题表';

-- ----------------------------
-- Records of tp_teach_subject
-- ----------------------------
