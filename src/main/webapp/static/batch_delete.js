/**
 * 批量删除
 * @author yr
 * @DATE 2018/6/12
 */
$(function () {
    //判断是否有修改的权限
    if($(".nav.nav-tabs").find("li").length <= 1){
        return false;
    }

    var $table = $("#contentTable");
    $table.find("thead>tr").prepend('<th><input type="checkbox" id="select_all"></th>');
    $table.find("tbody>tr").each(function () {
        var $id = $(this).prop("id");
        if (!!$id) {
            $(this).prepend('<td><input type="checkbox" class="sub_checkbox" value="' + $id + '" onclick="selectChange(this)"></td>');
        }
    });

    $("#select_all").click(function () {
        var selectFlag = this.checked;
        $(".sub_checkbox").each(function () {
            this.checked = selectFlag;
        });
    });

    $(".btns").append('<input id="btnBatchDelete" class="btn btn-primary" type="button" value="批量删除"/>');

    $("body").append('<form id="hid_batch_detele_form" method="POST" style="display:none"><input id="idArr" name="idArr" type="hidden"></form>')

    var url = $("#searchForm").prop("action");
    if (!!url) {
        var lastIndex = 0;
        if((lastIndex = url.lastIndexOf('?')) > -1){
            var param = url.substring(url.lastIndexOf('?'),url.length);
        }
        var end = url.lastIndexOf('/');
        url = url.substring(0, end + 1);
        url += "batchDelete";
        url += (lastIndex > -1 ? param : '');
        $("#hid_batch_detele_form").prop("action", url);
    }

    $("#btnBatchDelete").click(function () {
        var arrIds = [];
        $(".sub_checkbox:checked").each(function () {
            arrIds.push(this.value);
        });
        $("#idArr").val(arrIds.join());
        $("#hid_batch_detele_form").submit();
    });
});

function selectChange(obj) {
    var $subSelectCount = $(".sub_checkbox");
    if ($subSelectCount.length == $(".sub_checkbox:checked").length) {
        document.getElementById("select_all").checked = true;
    } else {
        document.getElementById("select_all").checked = false;
    }
}

function getAllCheckedItem() {
    var arrIds = [];
    $(".sub_checkbox:checked").each(function () {
        arrIds.push(this.value);
    });
    unSelectAll();
    return arrIds.join();
}

function unSelectAll(){
    document.getElementById("select_all").checked = false;
    var selectFlag = this.checked;
    $(".sub_checkbox").each(function () {
        this.checked = selectFlag;
    });
}
