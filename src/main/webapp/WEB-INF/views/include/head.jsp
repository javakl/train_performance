<%@ page contentType="text/html;charset=UTF-8" %><meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="renderer" content="webkit"><meta http-equiv="X-UA-Compatible" content="IE=8,IE=9,IE=10" />
<meta http-equiv="Expires" content="0"><meta http-equiv="Cache-Control" content="no-cache"><meta http-equiv="Cache-Control" content="no-store">
<script src="${ctxStatic}/jquery/jquery-1.8.3.min.js" type="text/javascript"></script>
<script type="text/javascript" src="${ctxStatic}/layer/layui.all.js"></script>
<link href="${ctxStatic}/layer/css/layui.css" rel="stylesheet" />
<link href="${ctxStatic}/bootstrap/2.3.1/css_${not empty cookie.theme.value ? cookie.theme.value : 'cerulean'}/bootstrap.min.css" type="text/css" rel="stylesheet" />
<script src="${ctxStatic}/bootstrap/2.3.1/js/bootstrap.min.js" type="text/javascript"></script>
<link href="${ctxStatic}/bootstrap/2.3.1/awesome/font-awesome.min.css" type="text/css" rel="stylesheet" />
<!--[if lte IE 7]><link href="${ctxStatic}/bootstrap/2.3.1/awesome/font-awesome-ie7.min.css" type="text/css" rel="stylesheet" /><![endif]-->
<!--[if lte IE 6]><link href="${ctxStatic}/bootstrap/bsie/css/bootstrap-ie6.min.css" type="text/css" rel="stylesheet" />
<script src="${ctxStatic}/bootstrap/bsie/js/bootstrap-ie.min.js" type="text/javascript"></script><![endif]-->
<link href="${ctxStatic}/jquery-select2/3.4/select2.min.css" rel="stylesheet" />
<script src="${ctxStatic}/jquery-select2/3.4/select2.min.js" type="text/javascript"></script>
<link href="${ctxStatic}/jquery-validation/1.11.0/jquery.validate.min.css" type="text/css" rel="stylesheet" />
<script src="${ctxStatic}/jquery-validation/1.11.0/jquery.validate.min.js" type="text/javascript"></script>
<link href="${ctxStatic}/jquery-jbox/2.3/Skins/Bootstrap/jbox.min.css" rel="stylesheet" />
<script src="${ctxStatic}/jquery-jbox/2.3/jquery.jBox-2.3.min.js" type="text/javascript"></script>
<script src="${ctxStatic}/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
<script src="${ctxStatic}/common/mustache.min.js" type="text/javascript"></script>
<script src="${ctxStatic}/common/echarts.min.js" type="text/javascript"></script>
<link href="${ctxStatic}/common/jeesite.css" type="text/css" rel="stylesheet" />
<script src="${ctxStatic}/common/jeesite.js?ver=1.0" type="text/javascript"></script>
<link rel="stylesheet" href="${ctxStatic}/mobile/font/iconfont.css">
<link rel="stylesheet" href="${ctxStatic}/mobile/css/home.css">
<script type="text/javascript" src="http://www.sunsean.com/idTabs/jquery.idTabs.min.js"></script>
<script type="text/javascript">
	var ctx = '${ctx}', ctxStatic='${ctxStatic}';

	//根据用户给指定属性绑定部门和上级部门
	 function bindOfficesByUserId(userIdObj,officeObj,parentOfficeObj){
			$('#'+userIdObj+'Id').off().on("change",function(){
				console.log($('#'+userIdObj+'Id').val());
				var v = $(this).val();//用户id
				console.log(v);
				if(v){
					$.getJSON("${ctx}/sys/user/get?id="+v, function(udata){//获取用户信息
						console.log("user Data: " + udata);
						if(udata.office && udata.office.id){
							$.getJSON("${ctx}/sys/office/get?id="+udata.office.id, function(odata){//根据部门id获取部门信息
								console.log("user office Data: " + odata);
								if(officeObj){
									$('#'+officeObj+'Id').val(odata.id);
									$('#'+officeObj+'Name').val(odata.name);
								}
								if(parentOfficeObj){
									$('#'+parentOfficeObj+'Id').val(odata.parent.id);
									$('#'+parentOfficeObj+'Name').val(odata.parent.name);
								}
							});
						}
					});
				}else{//userId为空，清空部门
					if(officeObj){
						$('#'+officeObj+'Id').val();
						$('#'+officeObj+'Name').val();
					}
					if(parentOfficeObj){
						$('#'+parentOfficeObj+'Id').val();
						$('#'+parentOfficeObj+'Name').val();
					}
				}
			});
		} 
	
	 /**
	     * 标题替换
	     * @param customTitle 自定义标题对象
	     * @param formId 表单id
	     * @param theadId 表头id
	     * @param element label的父级元素，默认是 <li>
	     */
	    function replaceTitle(customTitle,formId,theadId,element){
	        if(!customTitle) return;
	        var form = $("#" + formId);
	        var arr = form.serializeArray();
	        var newTitle;
	        for(var i = 0;i<arr.length;i++){
	            if(!!(newTitle = customTitle[arr[i].name])){
	                if(!!element){
	                    form.find('[name="' + arr[i].name + '"]').parents('div[class="control-group"]').find('label').html(newTitle + ":");
	                }else {
	                    form.find('[name="' + arr[i].name + '"]').parents('li:eq(0)').find('label').html(newTitle+":");
	                }
	            }
	        }
	        if(!!theadId){
	            $("#"+theadId).find("th").each(function(){
	                if(!!$(this).data('title')){
	                    $(this).html(customTitle[$(this).data('title')]);
	                }
	            })
	        }
	    }
		//标题定制-表格
	     function initCustomTitleTable(menuId,tableSelector){
	    	 //异步根据menuId获取标题数据
	    	$.getJSON("${ctx}/sys/menu/getCustomTitleByMenuId"+menuId, function(data){
	    		$(tableSelector+' th').each(function(idx){
	    			var columnName = $(this).attr('data-title');
	    			console.log(columnName+':'+JSON.stringify(data[columnName]));
	    			if(!columnName || !data[columnName])return true;
	    			//根据标题的labelName和show 进行操作
	    			//隐藏 or 显示
	    			var cTitle = data[columnName];
	    			console.log('display is '+cTitle.show);
	    			if(!cTitle.show){//隐藏列
	    				$(this).hide();
	    				$(tableSelector + " tr td:nth-child("+idx+")").hide();
	    			}else{//显示，修改表头
	    				console.log(cTitle.labelName)
	    				if(cTitle.labelName)$(this).html(cTitle.labelName);
	    			}
	    		});
	    	});
	     }
		//标题定制-表单
	     function initCustomTitleForm(menuId,formSelector){
	    	 //异步根据menuId获取标题数据
	    	$.getJSON("${ctx}/sys/menu/getCustomTitleByMenuId"+menuId, function(data){
	    		$(formSelector).find('.control-group[data-title]').each(function(){
	    			var columnName = $(this).attr('data-title');
	    			//console.log(columnName+':'+JSON.stringify(data[columnName]));
	    			if(!columnName || !data[columnName])return true;
	    			//根据标题的labelName和show 进行操作
	    			//隐藏 or 显示
	    			var cTitle = data[columnName];
	    			//console.log('display is '+cTitle.show);
	    			if(!cTitle.show){//隐藏列
	    				$(this).remove();//直接删除元素
	    			}else{//显示，修改字段名称，
	    				if(cTitle.labelName)$(this).find('.control-label').html(cTitle.labelName+'：');
	    				//判断是否必填
	    				if(!cTitle.required){//非必填
	    					$(this).find('[name="'+columnName+'"]').removeClass('required');
	    					$(this).find('span[class=help-inline]').hide();
	    				}else{
	    					$(this).find('[name="'+columnName+'"]').addClass('required');
	    					$(this).find('span[class=help-inline]').length > 0 ? '' : $(this).find('.controls').append('<span class="help-inline"><font color="red">*</font> </span>')  ;
	    				}
	    			}
	    		});
	    	});
	     }
</script>
