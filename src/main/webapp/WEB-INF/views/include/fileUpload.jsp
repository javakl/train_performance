
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="fns" uri="/WEB-INF/tlds/fns.tld" %>
<script type="text/javascript">
$(document).ready(function() {
    $(".btnBox").click(function() {
        fileIFrameInit($(this).data('modelid'));
    });
    $("#fileUpload").click(function () {
		var id=$("#fileUpload").attr("data-modelId");
        if(id!=''){
            fileIFrameInit(id,refreshFileNum);
		}else{
            if($("#inputForm").validate().form()){
                $.post("${pageContext.request.contextPath}${fns:getAdminPath()}/teach/tpTeachActive/async_save?teachTypes=${teachTypes}&menuId=${param.menuId}&delFlag=1",$("#inputForm").serializeObject(),function(data){
                    if(data.code==200){
						$("#id").val(data.id);
                        $("#fileUpload").attr("data-modelId",data.id);
                        fileIFrameInit(data.id,refreshFileNum);
					}else{
                       alert("当前数据有误，请先保存后再进行修改！");
					}
                })
			}else{
                alert("请先补充完整必填信息！");
			}
		}
    });
    refreshFileNum();
});
function fileIFrameInit(id,callback){
	var opType=$("#fileUpload").attr("data-opType");
	if(!opType)opType = '';
    $("#fileIframe").attr("src", "${pageContext.request.contextPath}${fns:getAdminPath()}/file/tpFileuploadInfo/list?model=" + id+"&opType="+opType);
    $.jBox($("#fileBox").html(), {
        title: "我的文件管理",
        height: 400,
        width: 800,
        buttons: {"关闭": true},
        showScrolling: false,
        iframeScrolling: 'no',
        closed: function () { /* 窗口关闭后执行的函数 */
        	if(callback)callback();
        }, 
        bottomText: "上传文件不能超过5M！"
    });
}
//刷新上传文件数
function refreshFileNum(){
	var id=$("#fileUpload").attr("data-modelId");
	$.get('${pageContext.request.contextPath}${fns:getAdminPath()}/file/tpFileuploadInfo/getFileList?model=' + id,function(data){
		if(data)$('#fileNum').text('文件数量:'+data.length);
	});
}
</script>
<div id="fileBox" class="hide">
    <iframe id="fileIframe" src="" width="100%" height="100%" scrolling="hidden">

    </iframe>
</div>
