<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>学位论文匿名评阅管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
            $("#btnImport").click(function(){
                $.jBox($("#importBox").html(), {title:"导入数据", buttons:{"关闭":true},
                    bottomText:"导入文件不能超过5M，仅允许导入“xls”或“xlsx”格式文件！"});
            });
            $("#btnExport").click(function(){
                top.$.jBox.confirm("确认要导出数据吗？","系统提示",function(v,h,f){
                    if(v=="ok"){
                        $("#searchForm").attr("action","${ctx}/student/graduate/tpStudentGraduate/export?ids="+getAllCheckedItem());
                        $("#searchForm").submit();
                    }
                },{buttonsFocus:1});
                top.$('.jbox-body .jbox-icon').css('top','55px');
            });
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
	<%@ include file="/WEB-INF/views/include/include_only_delete_js.jsp"%>
</head>
<body>
	<div id="importBox" class="hide">
		<form id="importForm" action="${ctx}/student/graduate/tpStudentGraduate/import" method="post" enctype="multipart/form-data"
			  class="form-search" style="padding-left:20px;text-align:center;" onsubmit="loading('正在导入，请稍等...');"><br/>
			<input id="uploadFile" name="file" type="file" style="width:330px"/><br/><br/>　　
			<input id="btnImportSubmit" class="btn btn-primary" type="submit" value="   导    入   "/>
			<a href="${ctx}/student/graduate/tpStudentGraduate/import/template">下载模板</a>
		</form>
	</div>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/student/graduate/tpStudentGraduate/">学位论文匿名评阅列表</a></li>
		<shiro:hasPermission name="student:graduate:tpStudentGraduate:edit"><li><a href="${ctx}/student/graduate/tpStudentGraduate/form">学位论文匿名评阅添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="tpStudentGraduate" action="${ctx}/student/graduate/tpStudentGraduate/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>提交人：</label>
				<sys:treeselect id="user" name="user.id" value="${tpStudentGraduate.user.id}" labelName="user.name" labelValue="${tpStudentGraduate.user.name}"
					title="用户" url="/sys/office/treeData?type=3&uType=2" cssClass="input-small" allowClear="true" notAllowSelectParent="true"/>
			</li>
			<li><label>提交人姓名：</label>
				<form:input path="fullName" htmlEscape="false" maxlength="50" class="input-medium"/>
			</li>
			<li><label>导师：</label>
				<sys:treeselect id="" name="stuInfo.tutor.id" value="${tpStudentExam.stuInfo.tutor.id}" labelName="stuInfo.tutor.name" labelValue="${tpStudentExam.stuInfo.tutor.name}"
					title="用户" url="/sys/office/treeData?type=3&uType=1" cssClass="input-small" allowClear="true" notAllowSelectParent="true"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/>
				<input id="btnExport" class="btn btn-primary" type="button" value="导出"/>
				<input id="btnImport" class="btn btn-primary" type="button" value="导入"/>
			</li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>毕业生</th>
				<th>年级</th>
				<th>类型</th>
				<th>来源</th>
				<th>导师</th>
				<th>所属部门</th>
				<th>评阅意见情况</th>
				<th>出问题份数</th>
				<th>结果</th>
				<!-- <th>更新者</th>
				<th>更新时间</th> -->
				<th>备注信息</th>
				<shiro:hasPermission name="student:graduate:tpStudentGraduate:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="tpStudentGraduate">
			<tr id="${tpStudentGraduate.id}">
				<td><a href="${ctx}/student/graduate/tpStudentGraduate/form?id=${tpStudentGraduate.id}">
					${tpStudentGraduate.user.name}
				</a></td>
				<td>
					${fns:getDictLabel(tpStudentGraduate.stuInfo.grade, 'stu_grade', '')}
				</td>
				<td>
					${fns:getDictLabel(tpStudentGraduate.stuInfo.stuType, 'stu_type', '')}
				</td>
				<td>
					${fns:getDictLabel(tpStudentGraduate.stuInfo.rootIn, 'tp_stu_root_in', '')}
				</td>
				<td>
					${tpStudentGraduate.stuInfo.tutor.name}
				</td>
				<td>
					${tpStudentGraduate.user.office.name}
				</td>
				<td>
					${tpStudentGraduate.reviewOpinions}
				</td>
				<td>
					${tpStudentGraduate.questionNum}
				</td>
				<td>
					${tpStudentGraduate.result}
				</td>
				<%-- <td>
					${tpStudentGraduate.updateBy.id}
				</td>
				<td>
					<fmt:formatDate value="${tpStudentGraduate.updateDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td> --%>
				<td>
					${tpStudentGraduate.remarks}
				</td>
				<shiro:hasPermission name="student:graduate:tpStudentGraduate:edit"><td>
    				<a href="${ctx}/student/graduate/tpStudentGraduate/form?id=${tpStudentGraduate.id}">修改</a>
					<a href="javascript:;"  class="btnBox" data-modelId="${tpStudentGraduate.id}"> 文件</a>
					<a href="${ctx}/student/graduate/tpStudentGraduate/delete?id=${tpStudentGraduate.id}" onclick="return confirmx('确认要删除该毕业模块吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<jsp:include page="/WEB-INF/views/include/fileUpload.jsp"></jsp:include>
	<div class="pagination">${page}</div>
</body>
</html>