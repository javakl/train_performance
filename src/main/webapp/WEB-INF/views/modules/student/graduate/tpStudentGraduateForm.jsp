<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>毕业模块管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			//$("#name").focus();
			$("#inputForm").validate({
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
		});
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li><a href="${ctx}/student/graduate/tpStudentGraduate/">毕业模块列表</a></li>
		<li class="active"><a href="${ctx}/student/graduate/tpStudentGraduate/form?id=${tpStudentGraduate.id}">毕业模块<shiro:hasPermission name="student:graduate:tpStudentGraduate:edit">${not empty tpStudentGraduate.id?'修改':'添加'}</shiro:hasPermission><shiro:lacksPermission name="student:graduate:tpStudentGraduate:edit">查看</shiro:lacksPermission></a></li>
	</ul><br/>
	<form:form id="inputForm" modelAttribute="tpStudentGraduate" action="${ctx}/student/graduate/tpStudentGraduate/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<sys:message content="${message}"/>		
		<div class="control-group">
			<label class="control-label">提交人：</label>
			<div class="controls">
				<sys:treeselect id="user" name="user.id" value="${tpStudentGraduate.user.id}" labelName="user.name" labelValue="${tpStudentGraduate.user.name}"
					title="用户" url="/sys/office/treeData?type=3&uType=2" cssClass="" allowClear="true" notAllowSelectParent="true"/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">评阅意见情况：</label>
			<div class="controls">
				<form:input path="reviewOpinions" htmlEscape="false" maxlength="255" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">出问题份数：</label>
			<div class="controls">
				<form:input path="questionNum" htmlEscape="false" maxlength="11" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">结果：</label>
			<div class="controls">
				<form:input path="result" htmlEscape="false" maxlength="255" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">备注信息：</label>
			<div class="controls">
				<form:textarea path="remarks" htmlEscape="false" rows="4" maxlength="255" class="input-xxlarge "/>
			</div>
		</div>
		<div class="form-actions">
			<shiro:hasPermission name="student:graduate:tpStudentGraduate:edit"><input id="btnSubmit" class="btn btn-primary" type="submit" value="保 存"/>&nbsp;</shiro:hasPermission>
			<input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
		</div>
	</form:form>
</body>
</html>