<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>处分管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
            $("#btnImport").click(function(){
                $.jBox($("#importBox").html(), {title:"导入数据", buttons:{"关闭":true},
                    bottomText:"导入文件不能超过5M，仅允许导入“xls”或“xlsx”格式文件！"});
            });
            $("#btnExport").click(function(){
                top.$.jBox.confirm("确认要导出数据吗？","系统提示",function(v,h,f){
                    if(v=="ok"){
                        $("#searchForm").attr("action","${ctx}/student/punish/tpStudentPunish/export?ids="+getAllCheckedItem());
                        $("#searchForm").submit();
                    }
                },{buttonsFocus:1});
                top.$('.jbox-body .jbox-icon').css('top','55px');
            });
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
	<%@ include file="/WEB-INF/views/include/include_only_delete_js.jsp"%>
</head>
<body>
	<div id="importBox" class="hide">
		<form id="importForm" action="${ctx}/student/punish/tpStudentPunish/import" method="post" enctype="multipart/form-data"
			  class="form-search" style="padding-left:20px;text-align:center;" onsubmit="loading('正在导入，请稍等...');"><br/>
			<input id="uploadFile" name="file" type="file" style="width:330px"/><br/><br/>　　
			<input id="btnImportSubmit" class="btn btn-primary" type="submit" value="   导    入   "/>
			<a href="${ctx}/student/punish/tpStudentPunish/import/template">下载模板</a>
		</form>
	</div>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/student/punish/tpStudentPunish/">处分列表</a></li>
		<shiro:hasPermission name="student:punish:tpStudentPunish:edit"><li><a href="${ctx}/student/punish/tpStudentPunish/form">处分添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="tpStudentPunish" action="${ctx}/student/punish/tpStudentPunish/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>受处分人：</label>
				<sys:treeselect id="user" name="user.id" value="${tpStudentPunish.user.id}" labelName="user.name" labelValue="${tpStudentPunish.user.name}"
					title="用户" url="/sys/office/treeData?type=3&uType=2" cssClass="input-small" allowClear="true" notAllowSelectParent="true"/>
			</li>
			<li><label>导师：</label>
				<sys:treeselect id="" name="stuInfo.tutor.id" value="${tpStudentExam.stuInfo.tutor.id}" labelName="stuInfo.tutor.name" labelValue="${tpStudentExam.stuInfo.tutor.name}"
					title="用户" url="/sys/office/treeData?type=3&uType=1" cssClass="input-small" allowClear="true" notAllowSelectParent="true"/>
			</li>
			<li><label>处分标题：</label>
				<form:input path="name" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li><label>处分等级：</label>
				<form:select path="level" class="input-medium">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('punish_level')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</li>
			<li><label>下达时间：</label>
				<input name="beginPunishTime" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					value="<fmt:formatDate value="${tpStudentPunish.beginPunishTime}" pattern="yyyy-MM-dd HH:mm:ss"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/> - 
				<input name="endPunishTime" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					value="<fmt:formatDate value="${tpStudentPunish.endPunishTime}" pattern="yyyy-MM-dd HH:mm:ss"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/>
				<input id="btnExport" class="btn btn-primary" type="button" value="导出"/>
				<input id="btnImport" class="btn btn-primary" type="button" value="导入"/>
			</li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>受处分人</th>
				<th>年级</th>
				<th>类型</th>
				<th>来源</th>
				<th>导师</th>
				<th>所属部门</th>
				<th>处分标题</th>
				<th>处分等级</th>
				<th>下达时间</th>
				<th>处分事由</th>
				<th>备注信息</th>
				<shiro:hasPermission name="student:punish:tpStudentPunish:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="tpStudentPunish">
			<tr id="${tpStudentPunish.id}">
				<td><a href="${ctx}/student/punish/tpStudentPunish/form?id=${tpStudentPunish.id}">
					${tpStudentPunish.user.name}
				</a></td>
				<td>
					${fns:getDictLabel(tpStudentPunish.stuInfo.grade, 'stu_grade', '')}
				</td>
				<td>
					${fns:getDictLabel(tpStudentPunish.stuInfo.stuType, 'stu_type', '')}
				</td>
				<td>
					${fns:getDictLabel(tpStudentPunish.stuInfo.rootIn, 'tp_stu_root_in', '')}
				</td>
				<td>
					${tpStudentPunish.stuInfo.tutor.name}
				</td>
				<td>
					${tpStudentPunish.user.office.name}
				</td>
				<td>
					${tpStudentPunish.name}
				</td>
				<td>
					${fns:getDictLabel(tpStudentPunish.level, 'punish_level', '')}
				</td>
				<td>
					<fmt:formatDate value="${tpStudentPunish.punishTime}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td>
					${tpStudentPunish.reason}
				</td>
				<td>
					${tpStudentPunish.remarks}
				</td>
				<shiro:hasPermission name="student:punish:tpStudentPunish:edit"><td>
    				<a href="${ctx}/student/punish/tpStudentPunish/form?id=${tpStudentPunish.id}">修改</a>
					<a href="javascript:;"  class="btnBox" data-modelId="${tpStudentPunish.id}"> 文件</a>
					<a href="${ctx}/student/punish/tpStudentPunish/delete?id=${tpStudentPunish.id}" onclick="return confirmx('确认要删除该处分吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<jsp:include page="/WEB-INF/views/include/fileUpload.jsp"></jsp:include>
	<div class="pagination">${page}</div>
</body>
</html>