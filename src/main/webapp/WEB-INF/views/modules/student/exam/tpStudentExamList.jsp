<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>考试记录管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
            $("#btnImport").click(function(){
                $.jBox($("#importBox").html(), {title:"导入数据", buttons:{"关闭":true},
                    bottomText:"导入文件不能超过5M，仅允许导入“xls”或“xlsx”格式文件！"});
            });
            $("#btnExport").click(function(){
                top.$.jBox.confirm("确认要导出数据吗？","系统提示",function(v,h,f){
                    if(v=="ok"){
                        $("#searchForm").attr("action","${ctx}/student/exam/tpStudentExam/export?ids="+getAllCheckedItem());
                        $("#searchForm").submit();
                    }
                },{buttonsFocus:1});
                top.$('.jbox-body .jbox-icon').css('top','55px');
            });
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
	<%@ include file="/WEB-INF/views/include/include_only_delete_js.jsp"%>
</head>
<body>
	<div id="importBox" class="hide">
		<form id="importForm" action="${ctx}/student/exam/tpStudentExam/import" method="post" enctype="multipart/form-data"
			  class="form-search" style="padding-left:20px;text-align:center;" onsubmit="loading('正在导入，请稍等...');"><br/>
			<input id="uploadFile" name="file" type="file" style="width:330px"/><br/><br/>　　
			<input id="btnImportSubmit" class="btn btn-primary" type="submit" value="   导    入   "/>
			<a href="${ctx}/student/exam/tpStudentExam/import/template">下载模板</a>
		</form>
	</div>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/student/exam/tpStudentExam/">考试记录列表</a></li>
		<shiro:hasPermission name="student:exam:tpStudentExam:edit"><li><a href="${ctx}/student/exam/tpStudentExam/form">考试记录添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="tpStudentExam" action="${ctx}/student/exam/tpStudentExam/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>参考人：</label>
				<sys:treeselect id="user" name="user.id" value="${tpStudentExam.user.id}" labelName="user.name" labelValue="${tpStudentExam.user.name}"
					title="用户" url="/sys/office/treeData?type=3&uType=2" cssClass="input-small" allowClear="true" notAllowSelectParent="true"/>
			</li>
			<li><label>所属科室：</label>
				<sys:treeselect id="department" name="department.id" value="${tpStudentExam.department.id}" labelName="department.name" labelValue="${tpStudentExam.department.name}"
					title="科室" url="/sys/office/treeData?type=2" cssClass="input-small" allowClear="true" notAllowSelectParent="true"/>
			</li>
			<li><label>导师：</label>
				<sys:treeselect id="" name="stuInfo.tutor.id" value="${tpStudentExam.stuInfo.tutor.id}" labelName="stuInfo.tutor.name" labelValue="${tpStudentExam.stuInfo.tutor.name}"
					title="用户" url="/sys/office/treeData?type=3&uType=1" cssClass="input-small" allowClear="true" notAllowSelectParent="true"/>
			</li>
			<%--<li><label>参考人姓名：</label>
				<form:input path="fullName" htmlEscape="false" maxlength="50" class="input-medium"/>
			</li>--%>
			<li><label>考试名称：</label>
				<form:input path="examName" htmlEscape="false" maxlength="20" class="input-medium"/>
			</li>
			<li><label>考试时间：</label>
				<input name="beginExamDate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					value="<fmt:formatDate value="${tpStudentExam.beginExamDate}" pattern="yyyy-MM-dd HH:mm:ss"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/> - 
				<input name="endExamDate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					value="<fmt:formatDate value="${tpStudentExam.endExamDate}" pattern="yyyy-MM-dd HH:mm:ss"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/>
			</li>
			<li class="btns">
				<input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/>
				<input id="btnExport" class="btn btn-primary" type="button" value="导出"/>
				<input id="btnImport" class="btn btn-primary" type="button" value="导入"/>
			</li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>参考人</th>
				<th>年级</th>
				<th>类型</th>
				<th>来源</th>
				<th>导师</th>
				<th>所属部门</th>
				<th>所属科室</th>
				<th>考试名称</th>
				<th>考试结果</th>
				<th>未通过门数</th>
				<th>考试时间</th>
				<th>备注信息</th>
				<shiro:hasPermission name="student:exam:tpStudentExam:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="tpStudentExam">
			<tr id="${tpStudentExam.id}">
				<td><a href="${ctx}/student/exam/tpStudentExam/form?id=${tpStudentExam.id}">
					${tpStudentExam.user.name}
				</a></td>
				<td>
					${fns:getDictLabel(tpStudentExam.stuInfo.grade, 'stu_grade', '')}
				</td>
				<td>
					${fns:getDictLabel(tpStudentExam.stuInfo.stuType, 'stu_type', '')}
				</td>
				<td>
					${fns:getDictLabel(tpStudentExam.stuInfo.rootIn, 'tp_stu_root_in', '')}
				</td>
				<td>
					${tpStudentExam.stuInfo.tutor.name}
				</td>
				<td>
					${tpStudentExam.user.office.name}
				</td>
				<td>
					${tpStudentExam.department.name}
				</td>
				<td>
					${tpStudentExam.examName}
				</td>
				<td>
					${tpStudentExam.examResult}
				</td>
				<td>
					${tpStudentExam.noPassNum}
				</td>
				<td>
					<fmt:formatDate value="${tpStudentExam.examDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td>
					${tpStudentExam.remarks}
				</td>
				<shiro:hasPermission name="student:exam:tpStudentExam:edit"><td>
    				<a href="${ctx}/student/exam/tpStudentExam/form?id=${tpStudentExam.id}">修改</a>
					<a href="javascript:;"  class="btnBox" data-modelId="${tpStudentExam.id}"> 文件</a>
					<a href="${ctx}/student/exam/tpStudentExam/delete?id=${tpStudentExam.id}" onclick="return confirmx('确认要删除该考试记录吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<jsp:include page="/WEB-INF/views/include/fileUpload.jsp"></jsp:include>
	<div class="pagination">${page}</div>
</body>
</html>