<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>学术会议管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
            $("#btnImport").click(function(){
                $.jBox($("#importBox").html(), {title:"导入数据", buttons:{"关闭":true},
                    bottomText:"导入文件不能超过5M，仅允许导入“xls”或“xlsx”格式文件！"});
            });
            $("#btnExport").click(function(){
                top.$.jBox.confirm("确认要导出数据吗？","系统提示",function(v,h,f){
                    if(v=="ok"){
                        $("#searchForm").attr("action","${ctx}/student/meeting/tpStudentMeeting/export?ids="+getAllCheckedItem());
                        $("#searchForm").submit();
                    }
                },{buttonsFocus:1});
                top.$('.jbox-body .jbox-icon').css('top','55px');
            });
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
	<%@ include file="/WEB-INF/views/include/include_only_delete_js.jsp"%>
</head>
<body>
	<div id="importBox" class="hide">
		<form id="importForm" action="${ctx}/student/meeting/tpStudentMeeting/import" method="post" enctype="multipart/form-data"
			  class="form-search" style="padding-left:20px;text-align:center;" onsubmit="loading('正在导入，请稍等...');"><br/>
			<input id="uploadFile" name="file" type="file" style="width:330px"/><br/><br/>　　
			<input id="btnImportSubmit" class="btn btn-primary" type="submit" value="   导    入   "/>
			<a href="${ctx}/student/meeting/tpStudentMeeting/import/template">下载模板</a>
		</form>
	</div>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/student/meeting/tpStudentMeeting/">学术会议列表</a></li>
		<shiro:hasPermission name="student:meeting:tpStudentMeeting:edit"><li><a href="${ctx}/student/meeting/tpStudentMeeting/form">学术会议添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="tpStudentMeeting" action="${ctx}/student/meeting/tpStudentMeeting/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>参会人：</label>
				<sys:treeselect id="user" name="user.id" value="${tpStudentMeeting.user.id}" labelName="user.name" labelValue="${tpStudentMeeting.user.name}"
					title="用户" url="/sys/office/treeData?type=3&uType=2" cssClass="input-small" allowClear="true" notAllowSelectParent="true"/>
			</li>
			<li><label>导师：</label>
				<sys:treeselect id="" name="stuInfo.tutor.id" value="${tpStudentExam.stuInfo.tutor.id}" labelName="stuInfo.tutor.name" labelValue="${tpStudentExam.stuInfo.tutor.name}"
					title="用户" url="/sys/office/treeData?type=3&uType=1" cssClass="input-small" allowClear="true" notAllowSelectParent="true"/>
			</li>
			<li><label>会议名称：</label>
				<form:input path="name" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li><label>学术会议级别：</label>
				<form:select path="level" class="input-medium">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('meeting_level')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</li>
			<li><label>交流类型：</label>
				<form:radiobuttons path="communicateType" items="${fns:getDictList('communicate_type')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/>
				<input id="btnExport" class="btn btn-primary" type="button" value="导出"/>
				<input id="btnImport" class="btn btn-primary" type="button" value="导入"/>
			</li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>参会人</th>
				<th>年级</th>
				<th>类型</th>
				<th>来源</th>
				<th>导师</th>
				<th>所属部门</th>
				<th>开始时间</th>
				<th>结束时间</th>
				<th>会议名称</th>
				<th>学术会议级别</th>
				<th>交流类型</th>
				<th>备注信息</th>
				<shiro:hasPermission name="student:meeting:tpStudentMeeting:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="tpStudentMeeting">
			<tr id="${tpStudentMeeting.id}">
				<td><a href="${ctx}/student/meeting/tpStudentMeeting/form?id=${tpStudentMeeting.id}">
					${tpStudentMeeting.user.name}
				</a></td>
				<td>
					${fns:getDictLabel(tpStudentMeeting.stuInfo.grade, 'stu_grade', '')}
				</td>
				<td>
					${fns:getDictLabel(tpStudentMeeting.stuInfo.stuType, 'stu_type', '')}
				</td>
				<td>
					${fns:getDictLabel(tpStudentMeeting.stuInfo.rootIn, 'tp_stu_root_in', '')}
				</td>
				<td>
					${tpStudentMeeting.stuInfo.tutor.name}
				</td>
				<td>
					${tpStudentMeeting.user.office.name}
				</td>
				<td>
					<fmt:formatDate value="${tpStudentMeeting.meetingStart}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td>
					<fmt:formatDate value="${tpStudentMeeting.meetingEnd}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td>
					${tpStudentMeeting.name}
				</td>
				<td>
					${fns:getDictLabel(tpStudentMeeting.level, 'meeting_level', '')}
				</td>
				<td>
					${fns:getDictLabel(tpStudentMeeting.communicateType, 'communicate_type', '')}
				</td>
				<td>
					${tpStudentMeeting.remarks}
				</td>
				<shiro:hasPermission name="student:meeting:tpStudentMeeting:edit"><td>
    				<a href="${ctx}/student/meeting/tpStudentMeeting/form?id=${tpStudentMeeting.id}">修改</a>
					<a href="javascript:;"  class="btnBox" data-modelId="${tpStudentMeeting.id}"> 文件</a>
					<a href="${ctx}/student/meeting/tpStudentMeeting/delete?id=${tpStudentMeeting.id}" onclick="return confirmx('确认要删除该学术会议吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<jsp:include page="/WEB-INF/views/include/fileUpload.jsp"></jsp:include>
	<div class="pagination">${page}</div>
</body>
</html>