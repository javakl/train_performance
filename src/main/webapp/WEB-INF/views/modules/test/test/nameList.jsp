<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>哈哈管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/test/test/name/">哈哈列表</a></li>
		<shiro:hasPermission name="test:test:name:edit"><li><a href="${ctx}/test/test/name/form">哈哈添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="name" action="${ctx}/test/test/name/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>名字：</label>
				<form:input path="name" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>名字</th>
				<shiro:hasPermission name="test:test:name:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="name">
			<tr>
				<td><a href="${ctx}/test/test/name/form?id=${name.id}">
					${name.name}
				</a></td>
				<shiro:hasPermission name="test:test:name:edit"><td>
    				<a href="${ctx}/test/test/name/form?id=${name.id}">修改</a>
					<a href="${ctx}/test/test/name/delete?id=${name.id}" onclick="return confirmx('确认要删除该哈哈吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>