<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>教学活动表管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			//$("#name").focus();
			$("#inputForm").validate({
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
			//部门、教研室和用户设置联动
			bindOfficesByUserId('user','department','trc');

			//replaceTitle(${custom_title},"inputForm",undefined,"div");
			//标题定制初始化表单
			initCustomTitleForm('${param.menuId}','#inputForm');
		});
	</script>
</head>
<body>
<ul class="nav nav-tabs">
	<li><a href="${ctx}/teach/tpTeachActive/list?teachTypes=${teachTypes}&menuId=${param.menuId}">列表</a></li>
	<li class="active"><a href="${ctx}/teach/tpTeachActive/form?teachTypes=${teachTypes}&id=${tpTeachActive.id}&menuId=${param.menuId}"><shiro:hasPermission name="teach:tpTeachActive:edit">${not empty tpTeachActive.id?'修改':'添加'}</shiro:hasPermission><shiro:lacksPermission name="teach:tpTeachActive:edit">查看</shiro:lacksPermission></a></li>
</ul><br/>
<form:form id="inputForm" modelAttribute="tpTeachActive" action="${ctx}/teach/tpTeachActive/save?teachTypes=${teachTypes}&menuId=${param.menuId}&delFlag=0" method="post" class="form-horizontal">
	<form:hidden path="id"/>
	<form:hidden path="act.taskId"/>
	<form:hidden path="act.taskName"/>
	<form:hidden path="act.taskDefKey"/>
	<form:hidden path="act.procInsId"/>
	<form:hidden path="act.procDefId"/>
	<form:hidden id="flag" path="act.flag"/>
	<input type="hidden" name="status" value="${not empty tpTeachActive.act.procDefId ? 0 : 2 }"/>
	<sys:message content="${message}"/>
	<div class="control-group" data-title="name">
		<label class="control-label">课程名：</label>
		<div class="controls">
			<form:input path="name" htmlEscape="false" maxlength="50" class="input-xlarge required"/>
			<span class="help-inline"><font color="red">*</font> </span>
		</div>
	</div>
	<div class="control-group" data-title="user.id">
		<label class="control-label">授课老师：</label>
		<div class="controls">
			<c:choose>
				<c:when test="${tpTeachActive.userDataScope eq 0 }">
					<input type="hidden" value="${ tpTeachActive.user.id}" id="userId" name="user.id" />
					<input type="text" value="${tpTeachActive.user.name }" id="userName" name="user.name" readonly="readonly" />
				</c:when>
				<c:otherwise>
					<sys:treeselect id="user" name="user.id" value="${tpTeachActive.user.id}" labelName="user.name" labelValue="${tpTeachActive.user.name}"
									title="用户" url="/sys/office/treeData?type=3" cssClass="required" allowClear="true" notAllowSelectParent="true"/>
				</c:otherwise>
			</c:choose>
			<span class="help-inline"><font color="red">*</font> </span>
		</div>
	</div>
	<div class="control-group" data-title="teachType">
		<label class="control-label">活动类型：</label>
		<div class="controls">
			<form:select path="teachType" class="input-xlarge required">
				<option value=""></option>
				<form:options items="${fns:getDictListByIds('teach_type',teachTypes)}" itemLabel="label"  itemValue="value" htmlEscape="false"/>
			</form:select>
			<span class="help-inline"><font color="red">*</font> </span>
		</div>
	</div>
	<div class="control-group" data-title="stuNum">
		<label class="control-label">带教量：</label>
		<div class="controls">
			<form:input path="stuNum" readonly="readonly" htmlEscape="false" maxlength="11" class="input-xlarge"/>
			<i id="unitLabel"></i>
		</div>
	</div>
	<div class="control-group" data-title="startTime">
		<label class="control-label">开始时间：</label>
		<div class="controls">
			<input id="startTime" name="startTime" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate required"
				   value="${tpTeachActive.startTime}" onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:true});"
			>
			<span class="help-inline"><font color="red">*</font> </span>
		</div>
	</div>

	<div class="control-group" data-title="endTime">
		<label class="control-label">结束时间：</label>
		<div class="controls">
			<input name="endTime" id="endTime" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate required"
				   value="<fmt:formatDate value="${tpTeachActive.endTime}" pattern="yyyy-MM-dd"/>" onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:false});"
			/>
			<span class="help-inline"><font color="red">*</font> </span>
		</div>
	</div>

	<div class="control-group" data-title="stuType">
		<label class="control-label">学生类别：</label>
		<div class="controls">
			<form:select path="stuType" class="input-xlarge required">
				<form:option value="" label=""/>
				<form:options items="${fns:getDictList('stu_type')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
			</form:select>
			<span class="help-inline"><font color="red">*</font> </span>
		</div>
	</div>
	<div class="control-group" data-title="stuGrade">
		<label class="control-label">学员年级：</label>
		<div class="controls">
			<form:select path="stuGrade" class="input-xlarge required">
				<form:option value="" label=""/>
				<form:options items="${fns:getDictList('stu_grade')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
			</form:select>
			<span class="help-inline"><font color="red">*</font> </span>
		</div>
	</div>
	<div class="control-group" data-title="department.id">
		<label class="control-label">所属科室：</label>
		<div class="controls">
			<sys:treeselect id="department" name="department.id" value="${tpTeachActive.department.id}" labelName="department.name" labelValue="${tpTeachActive.department.name}"
							title="科室" url="/sys/office/treeData?type=2" cssClass="required" allowClear="true" notAllowSelectParent="true"/>
			<span class="help-inline"><font color="red">*</font> </span>
		</div>
	</div>
	<div class="control-group" data-title="trc.id">
		<label class="control-label">所属教研室：</label>
		<div class="controls">
			<sys:treeselect id="trc" name="trc.id" value="${tpTeachActive.trc.id}" labelName="trc.name" labelValue="${tpTeachActive.trc.name}"
							title="教研室" url="/sys/office/treeData?type=1" allowClear="true" notAllowSelectParent="true"/>
		</div>
	</div>
	<div class="control-group" data-title="administrative">
		<label class="control-label">管理部门：</label>
		<div class="controls">
			<form:input path="administrative" htmlEscape="false" maxlength="255" class="input-xlarge "/>
		</div>
	</div>
	<div class="control-group" data-title="activePlace">
		<label class="control-label">活动地点：</label>
		<div class="controls">
			<form:input path="activePlace" htmlEscape="false" maxlength="200" class="input-xlarge "/>
		</div>
	</div>
	<div class="control-group" data-title="activeIntroduce">
		<label class="control-label">活动介绍：</label>
		<div class="controls">
			<form:input path="activeIntroduce" htmlEscape="false" class="input-xlarge "/>
		</div>
	</div>
	<div class="control-group" data-title="remarks">
		<label class="control-label">备注信息：</label>
		<div class="controls">
			<form:textarea path="remarks" htmlEscape="false" rows="4" maxlength="255" class="input-xxlarge "/>
			<span class="help-inline" id="deptRemarkRequired"><font color="red">*</font> </span>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label">资料:</label>
		<div class="controls">
			<a href="javascript:;"  id="fileUpload" data-opType="1" data-modelId="${tpTeachActive.id}"> 文件上传&nbsp;<span id="fileNum" style="text-decoration: underline;"></span></a>
		</div>
	</div>
	<div class="form-actions">
		<shiro:hasPermission name="teach:tpTeachActive:edit">
			<input id="btnSubmit" class="btn btn-primary" type="submit" value="提交"  onclick="$('#flag').val('yes')"/>
			<c:if test="${not empty tpTeachActive.id}">
				<input id="btnSubmit2" class="btn btn-primary" type="submit" value="销毁" onclick="$('#flag').val('no')"/>
			</c:if>
			&nbsp;
		</shiro:hasPermission>
		<input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
	</div>
</form:form>
<jsp:include page="/WEB-INF/views/include/fileUpload.jsp"></jsp:include>
<script type="text/javascript">
	$(function(){
		$.fn.serializeObject=function(){
			var obj=new Object();
			$.each(this.serializeArray(),function(index,param){
				if(!(param.name in obj)){
					obj[param.name]=param.value;
				}
			});
			return obj;
		};
		getReamrk();
		$("#teachType").off().on("change",function(e){
			getReamrk();
			//科室xxx的类型时，备注信息必填
			if(e.val == 7 || e.val == 8 || e.val == 9){
				$('#remarks').attr('placeholder','请填写授课学生姓名，且大于五人');
				$('#deptRemarkRequired').show();
			}else{
				$('#remarks').removeAttr('placeholder','请填写授课学生姓名，且大于五人');
				$('#deptRemarkRequired').hide();
			}
		});

	})
	function getReamrk(){
		var data = $("#teachType").val();
		if(data!=null){
			$.post("${ctx}/sys/dict/getRemark",{type:"teach_type",id:data},function(r){
				if(r!=null){
					console.log(r.dateForma);
					$('#startTime').unbind('focus');
					$('#endTime').unbind('focus');
					$('#startTime').bind('focus',function(){WdatePicker({skin:'whyGreen',dateFmt:r.dateForma});});
					$('#endTime').bind('focus',function(){WdatePicker({skin:'whyGreen',dateFmt:r.dateForma});});
					if(r.unit){
						$('#unitLabel').html(r.unit);
					}
				}
			},"json")
		}
	}
</script>
</body>
</html>