<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>教学文章管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			//$("#name").focus();
			$("#inputForm").validate({
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
		});
	</script>
	<style type="text/css">
		.online{
			display: none;
		}
	</style>
</head>
<body>
	<ul class="nav nav-tabs">
		<li><a href="${ctx}/teach/tpTeachArticles/list?isStudent=${isStudent}">${empty isStudent ? '教学文章列表' : '发表文章列表'}</a></li>
		<li class="active"><a href="${ctx}/teach/tpTeachArticles/form?id=${tpTeachArticles.id}&isStudent=${isStudent}">${empty isStudent ? '教学文章' : '发表文章'}<shiro:hasPermission name="teach:tpTeachArticles:edit">${not empty tpTeachArticles.id?'修改':'添加'}</shiro:hasPermission><shiro:lacksPermission name="teach:tpTeachArticles:edit">查看</shiro:lacksPermission></a></li>
	</ul><br/>
	<form:form id="inputForm" modelAttribute="tpTeachArticles" action="${ctx}/teach/tpTeachArticles/save?isStudent=${isStudent}" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<form:hidden path="act.taskId"/>
		<form:hidden path="act.taskName"/>
		<form:hidden path="act.taskDefKey"/>
		<form:hidden path="act.procInsId"/>
		<form:hidden path="act.procDefId"/>
		<form:hidden id="flag" path="act.flag"/>
		<input type="hidden" name="status" value="${not empty tpTeachArticles.act.procDefId ? 0 : 2 }"/>
		<sys:message content="${message}"/>		
		<div class="control-group">
			<label class="control-label">论文名：</label>
			<div class="controls">
				<form:input path="articleTitle" htmlEscape="false" maxlength="100" class="input-xlarge required"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">发表作者：</label>
			<div class="controls">
				<sys:treeselect id="user" name="user.id" value="${tpTeachArticles.user.id}" labelName="user.name" labelValue="${tpTeachArticles.user.name}"
					title="用户" url="/sys/office/treeData?type=3" cssClass="required" allowClear="true" notAllowSelectParent="true"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">第一作者或通讯作者：</label>
			<div class="controls">
				<form:input path="authorName" htmlEscape="false" maxlength="20" class="input-xlarge required"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">文章类型：</label>
			<div class="controls">
				<form:select path="articleType" class="input-xlarge required">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('article_type')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">期刊类型：</label>
			<div class="controls">
				<form:select path="journalType" class="input-xlarge required">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('journal_type')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">发表年份：</label>
			<div class="controls">
				<input name="pushYear" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate required"
					value="${tpTeachArticles.pushYear}"
					onclick="WdatePicker({dateFmt:'yyyy',isShowClear:false});"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">发表期刊：</label>
			<div class="controls">
				<form:input path="pushJournalName" htmlEscape="false" maxlength="255" class="input-xlarge required"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">期卷：</label>
			<div class="controls">
				<form:input path="volume" htmlEscape="false" maxlength="50" class="input-xlarge required"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">页数：</label>
			<div class="controls">
				<form:input path="pages" htmlEscape="false" maxlength="11" class="input-xlarge required"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group online">
			<label class="control-label">online收录时间：</label>
			<div class="controls">
				<input name="onltime" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate required"
					   value="<fmt:formatDate value="${tpTeachArticles.onltime}" pattern="yyyy-MM-dd"/>"
					   onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:false});"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group online">
			<label class="control-label">online影响因子：</label>
			<div class="controls">
				<form:input path="onlelement" htmlEscape="false" maxlength="255" class="input-xlarge "/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group online">
			<label class="control-label">onlineJCR分区：</label>
			<div class="controls">
				<form:input path="onljcr" htmlEscape="false" maxlength="255" class="input-xlarge "/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group online">
			<label class="control-label">onlineJCR分区链接：</label>
			<div class="controls">
				<form:input path="onlJcrUrl" htmlEscape="false" maxlength="255" class="input-xlarge "/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">作者情况：</label>
			<div class="controls">
				<form:input path="authContent" disabled="true" htmlEscape="false" maxlength="255" class="input-xlarge required"/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">所属科室：</label>
			<div class="controls">
				<sys:treeselect id="department" name="department.id" value="${tpTeachArticles.department.id}" labelName="department.name" labelValue="${tpTeachArticles.department.name}"
					title="科室" url="/sys/office/treeData?type=2" cssClass="required" allowClear="true" notAllowSelectParent="true"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">所属教研室：</label>
			<div class="controls">
				<sys:treeselect id="trc" name="trc.id" value="${tpTeachArticles.trc.id}" labelName="trc.name" labelValue="${tpTeachArticles.trc.name}"
					title="教研室" url="/sys/office/treeData?type=1" cssClass="required" allowClear="true" notAllowSelectParent="true"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">备注信息：</label>
			<div class="controls">
				<form:textarea path="remarks" htmlEscape="false" rows="4" maxlength="255" class="input-xxlarge required"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<c:if test="${!empty isStudent}">
		<div class="control-group">
			<label class="control-label">是否以此申学位：</label>
			<div class="controls">
				<form:radiobuttons path="isApplyDegree" items="${fns:getDictList('yes_no')}" itemLabel="label" itemValue="value" htmlEscape="false" class="required"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		</c:if>
		<act:histoicFlow procInsId="${tpTeachArticles.act.procInsId}" />
		<div class="form-actions">
			<input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
		</div>
	</form:form>
	<script type="text/javascript">
        $(function(){
            if("${tpTeachArticles.articleType}"==1){
                $(".online").addClass("onlineShow").removeClass("online");
			}
            $("#articleType").on("change",function(e){
                console.log(e.val)
                if(e.val==1){
                    $(".online").addClass("onlineShow").removeClass("online");
                }else{
                    $(".onlineShow").addClass("online").removeClass("onlineShow");
                }
            })
        })
	</script>
</body>
</html>