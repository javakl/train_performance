<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>教学成果管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
            $(".remark").hover(function(){
                //鼠标指针离开"src/main/webapp/WEB-INF/views/modules/teach/tpTeachAchievementList.jsp"
                top.$.jBox.info(`${remark}`,"Help",function(v,h,f){
                },{buttonsFocus:1});
                top.$('.jbox-body .jbox-icon').css('top','55px');
            },function(){

            });
			$("#btnExport").click(function(){
				top.$.jBox.confirm("确认要导出数据吗？","系统提示",function(v,h,f){
					if(v=="ok"){
						$("#searchForm").attr("action","${ctx}/teach/tpTeachAchievement/export");
						$("#searchForm").submit();
						//$("#searchForm").attr("action","${ctx}/teach/tpTeachAchievement/");
					}
				},{buttonsFocus:1});
				top.$('.jbox-body .jbox-icon').css('top','55px');
			});
			$("#btnImport").click(function(){
				$.jBox($("#importBox").html(), {title:"导入数据", buttons:{"关闭":true}, 
					bottomText:"导入文件不能超过5M，仅允许导入“xls”或“xlsx”格式文件！"});
			});
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
	<%@ include file="/WEB-INF/views/include/include_only_delete_js.jsp"%>
</head>
<body>
	<div id="importBox" class="hide">
		<form id="importForm" action="${ctx}/teach/tpTeachAchievement/import" method="post" enctype="multipart/form-data"
			  class="form-search" style="padding-left:20px;text-align:center;" onsubmit="loading('正在导入，请稍等...');"><br/>
			<input id="uploadFile" name="file" type="file" style="width:330px"/><br/><br/>　　
			<input id="btnImportSubmit" class="btn btn-primary" type="submit" value="   导    入   "/>
			<a href="${ctx}/teach/tpTeachAchievement/import/template">下载模板</a>
		</form>
	</div>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/teach/tpTeachAchievement/">教学成果列表</a></li>
		<shiro:hasPermission name="teach:tpTeachAchievement:edit"><li><a href="${ctx}/teach/tpTeachAchievement/form">教学成果添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="tpTeachAchievement" action="${ctx}/teach/tpTeachAchievement/list" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form" style="position: relative" >
			<div class="remark" style="position: absolute;right: 0px;"> <i class="icon-comment icon-blue"></i> </div>
			<li><label>成果名称：</label>
				<form:input path="name" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li><label>所属人：</label>
				<sys:treeselect id="user" name="user.id" value="${tpTeachAchievement.user.id}" labelName="user.name" labelValue="${tpTeachAchievement.user.name}"
					title="用户" url="/sys/office/treeData?type=3" cssClass="input-small" allowClear="true" notAllowSelectParent="true"/>
			</li>
			<li><label>级别：</label>
				<form:select path="level" class="input-medium">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('level_raward')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</li>
			<li><label>所属科室：</label>
				<sys:treeselect id="department" name="department.id" value="${tpTeachAchievement.department.id}" labelName="department.name" labelValue="${tpTeachAchievement.department.name}"
					title="科室" url="/sys/office/treeData?type=2" cssClass="input-small" allowClear="true" notAllowSelectParent="true"/>
			</li>
			<li><label>审核状态：</label>
				<form:radiobuttons path="status" items="${fns:getDictList('status')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
			</li>
			<li class="btns">
				<input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/>
				<input id="btnExport" class="btn btn-primary" type="button" value="导出"/>
				<c:if test="${tpTeachAchievement.userDataScope gt 0 }">
					<input id="btnImport" class="btn btn-primary" type="button" value="导入"/>
				</c:if>
			</li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>成果名称</th>
				<th>工号</th>
				<th>完成人</th>
				<th>级别</th>
				<th>第几完成人</th>
				<th>所属科室</th>
				<th>审核状态</th>
				<th>更新时间</th>
				<th>备注信息</th>
				<shiro:hasPermission name="teach:tpTeachAchievement:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="tpTeachAchievement">
			<tr id="${tpTeachAchievement.id}">
				<td><a href="${ctx}/teach/tpTeachAchievement/form?id=${tpTeachAchievement.id}">
					${tpTeachAchievement.name}
				</a></td>
				<td>
					${tpTeachAchievement.user.loginName}
				</td>
				<td>
					${tpTeachAchievement.user.name}
				</td>
				<td>
					${fns:getDictLabel(tpTeachAchievement.level, 'level_raward', '')}
				</td>
				<td>
					${tpTeachAchievement.completeOrderPerson}
				</td>
				<td>
					${tpTeachAchievement.department.name}
				</td>
				<td>
					${fns:getDictLabel(tpTeachAchievement.status, 'status', '')}
				</td>
				<td>
					<fmt:formatDate value="${tpTeachAchievement.updateDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td>
					${tpTeachAchievement.remarks}
				</td>
				<shiro:hasPermission name="teach:tpTeachAchievement:edit"><td>
    				<a href="${ctx}/teach/tpTeachAchievement/form?id=${tpTeachAchievement.id}">${tpTeachAchievement.status eq 2 ? '修改' : '查看' }</a>
					<a href="javascript:;"  class="btnBox" data-modelId="${tpTeachAchievement.id}"> 文件</a>
    				<c:if test="${tpTeachAchievement.status eq 0 or empty tpTeachAchievement.procInsId}">
						<a href="${ctx}/teach/tpTeachAchievement/delete?id=${tpTeachAchievement.id}" onclick="return confirmx('确认要删除该教学成果吗？', this.href)">删除</a>
    				</c:if>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		<jsp:include page="/WEB-INF/views/include/fileUpload.jsp"></jsp:include>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>