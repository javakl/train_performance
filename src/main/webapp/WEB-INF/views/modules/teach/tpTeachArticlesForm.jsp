<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>教学文章管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		var pushJournal = "${tpTeachArticles.pushJournal}";
		$(document).ready(function() {
			//$("#name").focus();
			$("#inputForm").validate({
				submitHandler: function(form){
				    var select=$("#authContentSelect").select2("data");
				    var remark=$("#authContentRemark").val();
				    var content="";
				    if(select==1){
                        content+="独立完成";
					}else{
                        content+="合作完成，共"+$("#numPeople").val()+"作,排"+$("#numPeopleOrder").val();
					}
					if(!!remark)content+="（注："+remark+"）";
                    $("#authContent").val(content);
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
			//部门、教研室和用户设置联动
			bindOfficesByUserId('user','department','trc');
		});
	</script>
	<style type="text/css">
		.online{
			display: none;
		}
	</style>
</head>
<body>
	<ul class="nav nav-tabs">
		<li><a href="${ctx}/teach/tpTeachArticles/list?isStudent=${isStudent}">${isStudent != 1 ? '教学文章列表' : '发表文章列表'}</a></li>
		<li class="active"><a href="${ctx}/teach/tpTeachArticles/form?id=${tpTeachArticles.id}&isStudent=${isStudent}">${isStudent != 1 ? '教学文章' : '发表文章'}<shiro:hasPermission name="teach:tpTeachArticles:edit">${not empty tpTeachArticles.id?'修改':'添加'}</shiro:hasPermission><shiro:lacksPermission name="teach:tpTeachArticles:edit">查看</shiro:lacksPermission></a></li>
	</ul><br/>
	<form:form id="inputForm" modelAttribute="tpTeachArticles" action="${ctx}/teach/tpTeachArticles/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<form:hidden path="act.taskId"/>
		<form:hidden path="act.taskName"/>
		<form:hidden path="act.taskDefKey"/>
		<form:hidden path="act.procInsId"/>
		<form:hidden path="act.procDefId"/>
		<form:hidden id="flag" path="act.flag"/>
		<input type="hidden" name="isStudent" value="${isStudent }" />
		<input type="hidden" name="status" value="${not empty tpTeachArticles.act.procDefId ? 0 : 2 }"/>
		<sys:message content="${message}"/>		
		<div class="control-group">
			<label class="control-label">论文名：</label>
			<div class="controls">
				<form:input path="articleTitle" htmlEscape="false" maxlength="100" class="input-xlarge required"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">发表作者：</label>
			<div class="controls">
				<c:choose>
					<c:when test="${tpTeachArticles.userDataScope eq 0 }">
						<input type="hidden" value="${ tpTeachArticles.user.id}" id="userId" name="user.id" />
						<input type="text" value="${tpTeachArticles.user.name }" id="userName" name="user.name" readonly="readonly" />
					</c:when>
					<c:otherwise>
					<sys:treeselect id="user" name="user.id" value="${tpTeachArticles.user.id}" labelName="user.name" labelValue="${tpTeachArticles.user.name}"
						title="用户" url="/sys/office/treeData?type=3${isStudent eq 1 ? '&uType=2' : '' }" cssClass="required" allowClear="true" notAllowSelectParent="true"/>
					</c:otherwise>
				</c:choose>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">第一作者或通讯作者：</label>
			<div class="controls">
				<form:input path="authorName" htmlEscape="false" maxlength="20" class="input-xlarge required"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">文章类型：</label>
			<div class="controls">
				<form:select path="articleType" class="input-xlarge required">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('article_type')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">期刊类型：</label>
			<div class="controls">
				<form:select path="journalType" class="input-xlarge required">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('journal_type')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">发表年份：</label>
			<div class="controls">
				<input name="pushYear" type="text" readonly="readonly" onchange="initpushJournal()" maxlength="20" class="input-medium Wdate required"
					value="${tpTeachArticles.pushYear}"
					onclick="WdatePicker({dateFmt:'yyyy-MM',isShowClear:false});"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">发表期刊：</label>
			<div class="controls">
				<form:select path="pushJournal" class="input-xlarge">
					<form:option value="" label=""/>
				</form:select>
				<%--<form:input path="pushJournal" htmlEscape="false" maxlength="255" class="input-xlarge required"/>--%>
				<!-- <span class="help-inline"><font color="red">*</font> </span> -->
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">期卷：</label>
			<div class="controls">
				<form:input path="volume" htmlEscape="false" maxlength="50" class="input-xlarge required"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">页数：</label>
			<div class="controls">
				<form:input path="pages" htmlEscape="false" maxlength="11" class="input-xlarge required"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group online">
			<label class="control-label">online收录时间：</label>
			<div class="controls">
				<input name="onltime" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate required"
					value="<fmt:formatDate value="${tpTeachArticles.onltime}" pattern="yyyy-MM-dd"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:false});"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group online">
			<label class="control-label">online影响因子：</label>
			<div class="controls">
				<form:input path="onlelement" htmlEscape="false" maxlength="255" class="input-xlarge "/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group online">
			<label class="control-label">onlineJCR分区：</label>
			<div class="controls">
				<form:input path="onljcr" htmlEscape="false" maxlength="255" class="input-xlarge "/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group online">
			<label class="control-label">onlineJCR分区链接：</label>
			<div class="controls">
				<form:input path="onlJcrUrl" htmlEscape="false" maxlength="255" class="input-xlarge "/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">作者情况：</label>
			<div class="controls">
				<input type="hidden" name="authContent" id="authContent">
				<select id="authContentSelect">
					<option value="1">单独第一作者</option>
					<option value="2">共同第一作者</option>
				</select>
				<input placeholder="共几" disabled type="number" id="numPeople" style="width: 60px" class="input-xlarge required">
				<input placeholder="排几" disabled type="number" id="numPeopleOrder" style="width: 60px" class="input-xlarge required">
				<br/>
				<textarea class="input-xlarge required" id="authContentRemark" placeholder="备注。。。"></textarea>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">所属科室：</label>
			<div class="controls">
				<sys:treeselect id="department" name="department.id" value="${tpTeachArticles.department.id}" labelName="department.name" labelValue="${tpTeachArticles.department.name}"
					title="科室" url="/sys/office/treeData?type=2" cssClass="required" allowClear="true" notAllowSelectParent="true"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">所属教研室：</label>
			<div class="controls">
				<sys:treeselect id="trc" name="trc.id" value="${tpTeachArticles.trc.id}" labelName="trc.name" labelValue="${tpTeachArticles.trc.name}"
					title="教研室" url="/sys/office/treeData?type=1" cssClass="required" allowClear="true" notAllowSelectParent="true"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">备注信息：</label>
			<div class="controls">
				<form:textarea path="remarks" htmlEscape="false" rows="4" maxlength="255" class="input-xxlarge"/>
			</div>
		</div>
		<c:if test="${!empty isStudent}">
			<div class="control-group">
				<label class="control-label">是否以此申学位：</label>
				<div class="controls">
					<form:radiobuttons path="isApplyDegree" items="${fns:getDictList('yes_no')}" itemLabel="label" itemValue="value" htmlEscape="false" class="required"/>
					<span class="help-inline"><font color="red">*</font> </span>
				</div>
			</div>
		</c:if>
		<div class="form-actions">
			<shiro:hasPermission name="teach:tpTeachArticles:edit">
				<input id="btnSubmit" class="btn btn-primary" type="submit" value="提交" onclick="$('#flag').val('yes')"/>
				<c:if test="${not empty tpTeachArticles.id}">
					<input id="btnSubmit2" class="btn btn-primary" type="submit" value="销毁" onclick="$('#flag').val('no')"/>
				</c:if>
				&nbsp;</shiro:hasPermission>
			<input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
		</div>
	</form:form>
<script type="text/javascript">
	$(function(){
        $("#journalType").on("change",function(e){
            if(e.val==1){
                $(".online").addClass("onlineShow").removeClass("online");
			}else{
                $(".onlineShow").addClass("online").removeClass("onlineShow");
			}
            initpushJournal();
        })
		$("#authContentSelect").on("change",function (e){
			if(e.val==1){
			    $("#numPeople").attr("disabled","disabled");
                $("#numPeopleOrder").attr("disabled","disabled");
                $("#numPeople").val("");
                $("#numPeopleOrder").val("");
			}else{
                $("#numPeople").removeAttr("disabled");
                $("#numPeopleOrder").removeAttr("disabled");
			}
        })
        initpushJournal();
	})
	//初始化文章类型下拉框
	 var initpushJournal =  function() {
	     var year=$("[name='pushYear']").val();
	     var rst=$("#journalType").select2('val');
	     if(year && rst){
	         $.post("${ctx}/teach/tpArticleClassify/queryData",{"year":year,"type":rst},function(data){
	             var instance = $('#pushJournal').data('select2');
	             if(instance){
	                 $('#pushJournal').empty();
	             }   
	             
	             $('#pushJournal').append('<option value=""></option>');
                 var $option = '';
	             for(var i=0;i<data.length;i++){
                     if(!!pushJournal && pushJournal == data[i].id){
                         $option = '<option value="' + data[i].id + '" selected>' + data[i].name + '</option>';
					 }else {
                         $option = '<option value="' + data[i].id + '">' + data[i].name + '</option>';
					 }
	                 $('#pushJournal').append($option);
				  }
	             $('#pushJournal').select2(
	            	{placeholder:'请选择',
	    			 allowClear:true});
			})
		}
	}
</script>
</body>
</html>