<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>优秀教学奖管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			//$("#name").focus();
			$("#inputForm").validate({
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
			//部门、教研室和用户设置联动
			bindOfficesByUserId('user','department','trc');
		});
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li><a href="${ctx}/teach/tpTeachExcellentAward/">优秀教学奖列表</a></li>
		<li class="active"><a href="${ctx}/teach/tpTeachExcellentAward/form?id=${tpTeachExcellentAward.id}">优秀教学奖<shiro:hasPermission name="teach:tpTeachExcellentAward:edit">${not empty tpTeachExcellentAward.id?'修改':'添加'}</shiro:hasPermission><shiro:lacksPermission name="teach:tpTeachExcellentAward:edit">查看</shiro:lacksPermission></a></li>
	</ul><br/>
	<form:form id="inputForm" modelAttribute="tpTeachExcellentAward" action="${ctx}/teach/tpTeachExcellentAward/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<form:hidden path="act.taskId"/>
		<form:hidden path="act.taskName"/>
		<form:hidden path="act.taskDefKey"/>
		<form:hidden path="act.procInsId"/>
		<form:hidden path="act.procDefId"/>
		<form:hidden id="flag" path="act.flag"/>
		<input type="hidden" name="status" value="${not empty tpTeachExcellentAward.act.procDefId ? 0 : 2 }"/>
		<sys:message content="${message}"/>		
		<div class="control-group">
			<label class="control-label">获奖人：</label>
			<div class="controls">
				<c:choose>
					<c:when test="${tpTeachExcellentAward.userDataScope eq 0 }">
						<input type="hidden" value="${ tpTeachExcellentAward.user.id}" id="userId" name="user.id" />
						<input type="text" value="${tpTeachExcellentAward.user.name }" id="userName" name="user.name" readonly="readonly" />
					</c:when>
					<c:otherwise>
						<sys:treeselect id="user" name="user.id" value="${tpTeachExcellentAward.user.id}" labelName="user.name" labelValue="${tpTeachExcellentAward.user.name}"
							title="用户" url="/sys/office/treeData?type=3" cssClass="required" allowClear="true" notAllowSelectParent="true"/>
					</c:otherwise>
				</c:choose>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">奖项名称：</label>
			<div class="controls">
				<form:input path="name" htmlEscape="false" maxlength="255" class="input-xlarge required"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">获奖级别：</label>
			<div class="controls">
				<form:select path="level" class="input-xlarge required">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('level_raward')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">完成人名次：</label>
			<div class="controls">
				<form:select path="completeOrderPerson" class="input-xlarge required">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('complete_human_role')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">获奖时间：</label>
			<div class="controls">
				<input name="winningTime" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate required"
					value="<fmt:formatDate value="${tpTeachExcellentAward.winningTime}" pattern="yyyy-MM-dd"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:false});"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">所属科室：</label>
			<div class="controls">
				<sys:treeselect id="department" name="department.id" value="${tpTeachExcellentAward.department.id}" labelName="department.name" labelValue="${tpTeachExcellentAward.department.name}"
					title="科室" url="/sys/office/treeData?type=2" cssClass="required" allowClear="true" notAllowSelectParent="true"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">所属教研室：</label>
			<div class="controls">
				<sys:treeselect id="trc" name="trc.id" value="${tpTeachExcellentAward.trc.id}" labelName="trc.name" labelValue="${tpTeachExcellentAward.trc.name}"
					title="教研室" url="/sys/office/treeData?type=1" cssClass="required" allowClear="true" notAllowSelectParent="true"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">备注信息：</label>
			<div class="controls">
				<form:textarea path="remarks" htmlEscape="false" rows="4" maxlength="255" class="input-xxlarge"/>
			</div>
		</div>
		<div class="form-actions">
			<shiro:hasPermission name="teach:tpTeachExcellentAward:edit">
				<input id="btnSubmit" class="btn btn-primary" type="submit" value="提交" onclick="$('#flag').val('yes')"/>
				<c:if test="${not empty tpTeachExcellentAward.id}">
					<input id="btnSubmit2" class="btn btn-primary" type="submit" value="销毁" onclick="$('#flag').val('no')"/>
				</c:if>
				&nbsp;</shiro:hasPermission>
			<input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
		</div>
	</form:form>
</body>
</html>