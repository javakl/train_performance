<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>教学活动统计</title>
	<meta name="decorator" content="default"/>
	<style>
		.empty-content {
			text-align: center;
			margin: 3% 0;
			font-size: 1rem;
			color: #ff00d2;
		}
		#container {
			margin-top: 2rem;
		}
	</style>
</head>
<body>
	<%--<ul class="nav nav-tabs">
		<li class="active"><a href="javascript:void(0);">教学活动统计</a></li>
	</ul>--%>
	<form:form id="searchForm" modelAttribute="tpTeachActive" action="${ctx}/teach/tpTeachActive/statisticSingleTeachType" method="post" class="breadcrumb form-search">
		<ul class="ul-form" style="position: relative">
			<div class="remark" style="position: absolute;right: 0px;"> <i class="icon-comment icon-blue"></i> </div>
			<li>
				<label style="width: 2.5rem;">日期：</label>
				<input name="startTime" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					   value="<fmt:formatDate value="${tpTeachActive.startTime}" pattern="yyyy-MM-dd"/>"
					   onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:true});"/> -
				<input name="endTime" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					   value="<fmt:formatDate value="${tpTeachActive.endTime}" pattern="yyyy-MM-dd"/>"
					   onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:true});"/>
			</li>
			<li><label>活动类型：</label>
				<form:select path="teachType" class="input-medium">
					<form:option value="" label="" title="" />
					<form:options items="${fns:getDictList('teach_type')}" itemLabel="label" itemValue="value" title="description"   htmlEscape="false"/>
				</form:select>
			</li>
			<li class="btns">
				<input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/>
			</li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<c:choose>
		<c:when test="${empty departmentNameList}">
			<div class="empty-content">暂无数据</div>
		</c:when>
		<c:otherwise>
			<table id="contentTable" class="table table-striped table-bordered table-condensed">

						<thead>
						<tr>
							<th>科室</th>
							<c:forEach items="${departmentNameList}" var="departmentName">
								<th>${departmentName}</th>
							</c:forEach>
						</tr>
						</thead>
						<tbody>
							<tr>
								<td>${unit}</td>
								<c:forEach items="${totalList}" var="total">
										<td>
											${total}
										</td>
								</c:forEach>
							</tr>
						</tbody>
			</table>
			<div id="container" style="height: 350px"></div>
		</c:otherwise>
	</c:choose>
<script>
	if(!!"${departmentNameList}"){
        var departmentNameArr = '${departmentNameListStr}'.split(',');
        var totalListArr = '${totalListStr}'.split(',');
        var dom = document.getElementById("container");
        var myChart = echarts.init(dom);
        option = null;
        option = {
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'cross',
                    crossStyle: {
                        color: '#999'
                    }
                }
            },
            toolbox: {
                feature: {
                    dataView: {show: true, readOnly: false},
                    magicType: {show: true, type: ['line', 'bar']},
                    restore: {show: true},
                    saveAsImage: {show: true}
                }
            },
            legend: {
                data:['${unit}']
            },
            xAxis: [
                {
                    type: 'category',
                    data: departmentNameArr,
                    axisPointer: {
                        type: 'shadow'
                    }
                }
            ],
            yAxis: [
                {
                    type: 'value',
                    name: '${unit}',
                    min: 0,
                    axisLabel: {
                        formatter: '{value}'
                    }
                }
            ],
            series: [
                {
                    name:'${unit}',
                    type:'bar',
                    data:totalListArr,
                    itemStyle:{
                        color:'red'
					}
                }
            ]
        };

        if (option && typeof option === "object") {
            myChart.setOption(option, true);
        }
	}
</script>
</body>
</html>