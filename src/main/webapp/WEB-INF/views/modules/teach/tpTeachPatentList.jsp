<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>研究专利表管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
            $(".remark").hover(function(){
                top.$.jBox.info(`${remark}`,"Help",function(v,h,f){
                },{buttonsFocus:1});
                top.$('.jbox-body .jbox-icon').css('top','55px');
            },function(){
                //鼠标指针离开
            });
            $("#btnImport").click(function(){
                $.jBox($("#importBox").html(), {title:"导入数据", buttons:{"关闭":true},
                    bottomText:"导入文件不能超过5M，仅允许导入“xls”或“xlsx”格式文件！"});
            });
            $("#btnExport").click(function(){
                top.$.jBox.confirm("确认要导出数据吗？","系统提示",function(v,h,f){
                    if(v=="ok"){
                        $("#searchForm").attr("action","${ctx}/teach/tpTeachPatent/export?ids="+getAllCheckedItem());
                        $("#searchForm").submit();
                    }
                },{buttonsFocus:1});
                top.$('.jbox-body .jbox-icon').css('top','55px');
            });
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
	<%@ include file="/WEB-INF/views/include/include_only_delete_js.jsp"%>
</head>
<body>
	<div id="importBox" class="hide">
		<form id="importForm" action="${ctx}/teach/tpTeachPatent/import" method="post" enctype="multipart/form-data"
			  class="form-search" style="padding-left:20px;text-align:center;" onsubmit="loading('正在导入，请稍等...');"><br/>
			<input id="uploadFile" name="file" type="file" style="width:330px"/><br/><br/>　　
			<input id="btnImportSubmit" class="btn btn-primary" type="submit" value="   导    入   "/>
			<a href="${ctx}/teach/tpTeachPatent/import/template">下载模板</a>
		</form>
	</div>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/teach/tpTeachPatent/">教学专利列表</a></li>
		<shiro:hasPermission name="teach:tpTeachPatent:edit"><li><a href="${ctx}/teach/tpTeachPatent/form">教学专利添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="tpTeachPatent" action="${ctx}/teach/tpTeachPatent/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form" style="position: relative">
			<div class="remark" style="position: absolute;right: 0px;"> <i class="icon-comment icon-blue"></i> </div>
			<li><label>专利名称：</label>
				<form:input path="patentName" htmlEscape="false" maxlength="100" class="input-medium"/>
			</li>
			<li><label>所属科室：</label>
				<sys:treeselect id="department" name="department.id" value="${tpTeachPatent.department.id}" labelName="department.name" labelValue="${tpTeachPatent.department.name}"
					title="科室" url="/sys/office/treeData?type=2" cssClass="input-small" allowClear="true" notAllowSelectParent="true"/>
			</li>
			<li><label>所属教研室：</label>
				<sys:treeselect id="trc" name="trc.id" value="${tpTeachPatent.trc.id}" labelName="trc.name" labelValue="${tpTeachPatent.trc.name}"
					title="教研室" url="/sys/office/treeData?type=1" cssClass="input-small" allowClear="true" notAllowSelectParent="true"/>
			</li>
			<li><label>所属人：</label>
				<sys:treeselect id="user" name="user.id" value="${tpTeachSubject.user.id}" labelName="user.name" labelValue="${tpTeachSubject.user.name}"
					title="用户" url="/sys/office/treeData?type=3" cssClass="input-small" allowClear="true" notAllowSelectParent="true"/>
			</li>
			<li><label>发明人：</label>
				<form:input path="inventioner" htmlEscape="false" maxlength="200" class="input-medium"/>
			</li>
			<li><label>申请日期：</label>
				<input name="beginApplyTime" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					value="<fmt:formatDate value="${tpTeachPatent.beginApplyTime}" pattern="yyyy-MM-dd HH:mm:ss"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/> - 
				<input name="endApplyTime" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					value="<fmt:formatDate value="${tpTeachPatent.endApplyTime}" pattern="yyyy-MM-dd HH:mm:ss"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/>
			</li>
			<li><label>授权时间：</label>
				<input name="beginGrantTime" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					value="<fmt:formatDate value="${tpTeachPatent.beginGrantTime}" pattern="yyyy-MM-dd HH:mm:ss"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/> - 
				<input name="endGrantTime" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					value="<fmt:formatDate value="${tpTeachPatent.endGrantTime}" pattern="yyyy-MM-dd HH:mm:ss"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/>
			</li>
			<li><label>审核状态：</label>
				<form:select path="status" class="input-medium">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('status')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</li>
			<li class="btns">
				<input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/>
				<input id="btnExport" class="btn btn-primary" type="button" value="导出"/>
				<c:if test="${tpTeachPatent.userDataScope gt 0 }">
					<input id="btnImport" class="btn btn-primary" type="button" value="导入"/>
				</c:if>
			</li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>专利名称</th>
				<th>工号</th>
				<th>所属人</th>
				<th>第几完成人</th>
				<th>所属科室</th>
				<th>所属教研室</th>
				<th>专利权人</th>
				<th>发明人</th>
				<th>专利类型</th>
				<th>申请日期</th>
				<th>授权时间</th>
				<th>作者情况</th>
				<th>备注信息</th>
				<th>审核状态</th>
				<shiro:hasPermission name="teach:tpTeachPatent:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="tpTeachPatent">
			<tr id="${tpTeachPatent.id}">
				<td><a href="${ctx}/teach/tpTeachPatent/form?id=${tpTeachPatent.id}">
					${tpTeachPatent.patentName}
				</a></td>
				<td>
					${tpTeachPatent.user.loginName}
				</td>
				<td>
					${tpTeachPatent.user.name}
				</td>
				<td>
					${tpTeachPatent.completeOrderPerson}
				</td>
				<td>
					${tpTeachPatent.department.name}
				</td>
				<td>
					${tpTeachPatent.trc.name}
				</td>
				<td>
					${tpTeachPatent.authorName}
				</td>
				<td>
					${tpTeachPatent.inventioner}
				</td>
				<td>
						${fns:getDictLabel(tpTeachPatent.patentType, 'patent_type', '')}
				</td>
				<td>
					<fmt:formatDate value="${tpTeachPatent.applyTime}" pattern="yyyy-MM-dd"/>
				</td>
				<td>
					<fmt:formatDate value="${tpTeachPatent.grantTime}" pattern="yyyy-MM-dd"/>
				</td>
				<td>
					${tpTeachPatent.authContent}
				</td>
				<td>
					${tpTeachPatent.remarks}
				</td>
				<td>
					${fns:getDictLabel(tpTeachPatent.status, 'status', '')}
				</td>
				<shiro:hasPermission name="teach:tpTeachPatent:edit"><td>
    				<a href="${ctx}/teach/tpTeachPatent/form?id=${tpTeachPatent.id}">${tpTeachPatent.status eq 2 ? '修改' : '查看' }</a>
					<a href="javascript:;"  class="btnBox" data-modelId="${tpTeachPatent.id}"> 文件</a>
					<a href="${ctx}/teach/tpTeachPatent/delete?id=${tpTeachPatent.id}" onclick="return confirmx('确认要删除该研究专利表吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<jsp:include page="/WEB-INF/views/include/fileUpload.jsp"></jsp:include>
	<div class="pagination">${page}</div>
</body>
</html>