<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>教学课题表管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
            $(".remark").hover(function(){
                top.$.jBox.info(`${remark}`,"Help",function(v,h,f){
                },{buttonsFocus:1});
                top.$('.jbox-body .jbox-icon').css('top','55px');
            },function(){
                //鼠标指针离开
            });
            $("#btnImport").click(function(){
                $.jBox($("#importBox").html(), {title:"导入数据", buttons:{"关闭":true},
                    bottomText:"导入文件不能超过5M，仅允许导入“xls”或“xlsx”格式文件！"});
            });
            $("#btnExport").click(function(){
                top.$.jBox.confirm("确认要导出数据吗？","系统提示",function(v,h,f){
                    if(v=="ok"){
                        $("#searchForm").attr("action","${ctx}/teach/tpTeachSubject/export?ids="+getAllCheckedItem());
                        $("#searchForm").submit();
                    }
                },{buttonsFocus:1});
                top.$('.jbox-body .jbox-icon').css('top','55px');
            });
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
	<%@ include file="/WEB-INF/views/include/include_only_delete_js.jsp"%>
</head>
<body>
	<div id="importBox" class="hide">
		<form id="importForm" action="${ctx}/teach/tpTeachSubject/import" method="post" enctype="multipart/form-data"
			  class="form-search" style="padding-left:20px;text-align:center;" onsubmit="loading('正在导入，请稍等...');"><br/>
			<input id="uploadFile" name="file" type="file" style="width:330px"/><br/><br/>　　
			<input id="btnImportSubmit" class="btn btn-primary" type="submit" value="   导    入   "/>
			<a href="${ctx}/teach/tpTeachSubject/import/template">下载模板</a>
		</form>
	</div>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/teach/tpTeachSubject/">教学课题表列表</a></li>
		<shiro:hasPermission name="teach:tpTeachSubject:edit"><li><a href="${ctx}/teach/tpTeachSubject/form">教学课题表添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="tpTeachSubject" action="${ctx}/teach/tpTeachSubject/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form" style="position: relative">
			<div class="remark" style="position: absolute;right: 0px;"> <i class="icon-comment icon-blue"></i> </div>
			<li><label>课题名称：</label>
				<form:input path="name" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li><label>课题级别：</label>
				<form:select path="level" class="input-medium">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('level_subject')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</li>
			<li><label>负责人：</label>
				<sys:treeselect id="user" name="user.id" value="${tpTeachSubject.user.id}" labelName="user.name" labelValue="${tpTeachSubject.user.name}"
					title="用户" url="/sys/office/treeData?type=3" cssClass="input-small" allowClear="true" notAllowSelectParent="true"/>
			</li>
			<li><label>所属科室：</label>
				<sys:treeselect id="office" name="office.id" value="${tpTeachSubject.office.id}" labelName="office.name" labelValue="${tpTeachSubject.office.name}"
					title="科室" url="/sys/office/treeData?type=2" cssClass="input-small" allowClear="true" notAllowSelectParent="true"/>
			</li>
			<li><label>所属教研室：</label>
				<sys:treeselect id="trc" name="trc.id" value="${tpTeachSubject.trc.id}" labelName="trc.name" labelValue="${tpTeachSubject.trc.name}"
					title="教研室" url="/sys/office/treeData?type=1" cssClass="input-small" allowClear="true" notAllowSelectParent="true"/>
			</li>
			<li><label>审核状态：</label>
				<form:radiobuttons path="status" items="${fns:getDictList('status')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
			</li>
			<li class="btns">
				<input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/>
				<input id="btnExport" class="btn btn-primary" type="button" value="导出"/>
				<c:if test="${tpTeachSubject.userDataScope gt 0 }">
					<input id="btnImport" class="btn btn-primary" type="button" value="导入"/>
				</c:if>
			</li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>课题名称</th>
				<th>课题级别</th>
				<th>工号</th>
				<th>负责人</th>
				<th>起始时间</th>
				<th>结束时间</th>
				<th>所属科室</th>
				<th>所属教研室</th>
				<th>备注信息</th>
				<th>审核状态</th>
				<shiro:hasPermission name="teach:tpTeachSubject:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="tpTeachSubject">
			<tr id="${tpTeachSubject.id}">
				<td><a href="${ctx}/teach/tpTeachSubject/form?id=${tpTeachSubject.id}">
					${tpTeachSubject.name}
				</a></td>
				<td>
					${fns:getDictLabel(tpTeachSubject.level, 'level_subject', '')}
				</td>
				<td>
					${tpTeachSubject.user.loginName}
				</td>
				<td>
					${tpTeachSubject.user.name}
				</td>
				<td>
					<fmt:formatDate value="${tpTeachSubject.startDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td>
					<fmt:formatDate value="${tpTeachSubject.endDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td>
					${tpTeachSubject.office.name}
				</td>
				<td>
					${tpTeachSubject.trc.name}
				</td>
				<td>
					${tpTeachSubject.remarks}
				</td>
				<td>
					${fns:getDictLabel(tpTeachSubject.status, 'status', '')}
				</td>
				<shiro:hasPermission name="teach:tpTeachSubject:edit"><td>
    				<a href="${ctx}/teach/tpTeachSubject/form?id=${tpTeachSubject.id}">${tpTeachSubject.status eq 2 ? '修改' : '查看' }</a>
					<a href="javascript:;"  class="btnBox" data-modelId="${tpTeachSubject.id}"> 文件</a>
					<a href="${ctx}/teach/tpTeachSubject/delete?id=${tpTeachSubject.id}" onclick="return confirmx('确认要删除该教学课题表吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<jsp:include page="/WEB-INF/views/include/fileUpload.jsp"></jsp:include>
	<div class="pagination">${page}</div>
</body>
</html>