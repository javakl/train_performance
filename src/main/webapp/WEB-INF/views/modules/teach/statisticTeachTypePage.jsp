<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>教学活动统计</title>
	<meta name="decorator" content="default"/>
	<script>
		$(function(){
		    $("#btnSubmit").click(function(){
                var teachTypeValues = $("#teachType").select2("data");
                let ttv = [];
                for(let i = 0;i<teachTypeValues.length;i++){
                    if(!!teachTypeValues[i].id)
                    ttv.push(teachTypeValues[i].id);
				}
				if(ttv.length > 0){
                    $("#teachTypeValues").val(ttv.join())
				}else{
                    $("#teachTypeValues").val("");
				}
           })

			if(!!"${selectedTeachTypes}"){
                $("#teachType").val("${selectedTeachTypes}".split(',')).trigger("change");
			}
		})
	</script>
	<style type="text/css">
		.table th.title{
			text-align: center;
		}
		.select2-container.select2-container-multi.input-medium{
			width: 240px;
		}
		.form-search .ul-form li.select2-search-field,.form-search .ul-form li.select2-search-choice{
			height: 20px;
		}
		.select2-container-multi .select2-choices{
			overflow-y: scroll;
			max-height:100px;
		}
		.form-search .ul-form li{
			line-height:105px;
			height:105px;
		}
		.zdi .select2-container-multi .select2-choices {
			height: 95px !important;
		}
		.form-search .ul-form li.select2-search-field, .form-search .ul-form li.select2-search-choice {
			height: 1rem;
		}
	</style>
</head>
<body>
	<form:form id="searchForm" modelAttribute="tpTeachActive" action="${ctx}/teach/tpTeachActive/statisticTeachType" method="post" class="breadcrumb form-search">
		<input id="teachTypeValues" type="hidden" name="teachTypeValues" value="${selectedTeachTypes}">
		<ul class="ul-form" style="position: relative">
			<div class="remark" style="position: absolute;right: 0px;"> <i class="icon-comment icon-blue"></i> </div>
			<li>
				<label style="width: 2.5rem;">日期：</label>
				<input name="startTime" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					   value="<fmt:formatDate value="${tpTeachActive.startTime}" pattern="yyyy-MM-dd"/>"
					   onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:true});"/> -
				<input name="endTime" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					   value="<fmt:formatDate value="${tpTeachActive.endTime}" pattern="yyyy-MM-dd"/>"
					   onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:true});"/>
			</li>
			<li class="zdi"><label>活动类型：</label>
				<form:select path="teachType" class="input-medium" multiple="true">
					<form:options items="${fns:getDictList('teach_type')}" itemLabel="label" itemValue="value" title="description"   htmlEscape="false"/>
				</form:select>
			</li>
			<li class="btns">
				<input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/>
			</li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th class="title">科室</th>
				<c:forEach items="${teachTypes}" var="tt">
					<th class="title">${tt.label}</th>
				</c:forEach>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${listMap}" var="map">
			<tr>
				<td>
					${map.key}
				</td>
				<c:forEach items="${teachTypes}" var="tt">
					<c:set var="k" value="type${tt.value}"></c:set>
					<td>${map.value[k]}</td>
				</c:forEach>
			</tr>
		</c:forEach>
		</tbody>
	</table>
</body>
</html>