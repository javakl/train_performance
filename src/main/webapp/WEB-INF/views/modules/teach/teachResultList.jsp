<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>教学成果统计</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
        var option = {
            tooltip : {
                trigger: 'axis',
                axisPointer : {            // 坐标轴指示器，坐标轴触发有效
                    type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
                }
            },
            legend: {
                data:[]
            },
            toolbox: {
                show : true,
                orient: 'vertical',
                x: 'right',
                y: 'center',
                feature : {
                    mark : {show: true},
                    dataView : {show: true, readOnly: false},
                    magicType : {show: true, type: ['line', 'bar', 'stack', 'tiled']},
                    restore : {show: true},
                    saveAsImage : {show: true}
                }
            },
            calculable : true,
            xAxis : [
                {
                    type : 'category',
                    data : []
                }
            ],
            yAxis : [
                {
                    type : 'value'
                }
            ],
            series : []
        };

        $(document).ready(function() {
			$("#btnExport").click(function(){
				top.$.jBox.confirm("确认要导出数据吗？","系统提示",function(v,h,f){
					if(v=="ok"){
						$("#searchForm").attr("action","${ctx}/teach/count/export");
						$("#searchForm").submit();
						//$("#searchForm").attr("action","${ctx}/teach/tpTeachAchievement/");
					}
				},{buttonsFocus:1});
				top.$('.jbox-body .jbox-icon').css('top','55px');
			});
			$("#btnChart").click(function(){
				$.jBox("<div id='main' style='width:800px;height:400px;'></div>", {title:"图表", width: 800,height:450, buttons:{"关闭":true}});
                var myChart=echarts.init(document.getElementById('main'));
                $.post("${ctx}/teach/count/chartData",{startDate:$("#startDate").val(),endDate:$("#endDate").val()},function(data){
                    option.legend.data=data.legendData;
                    option.series=data.series;
                    option.xAxis[0].data=data.xAxis;
                    myChart.setOption(option);
				},"json")
			});
		});
	</script>
</head>
<body>
	<form id="searchForm" action="${ctx}/teach/count/teachResultList" method="post" class="breadcrumb form-search">
		<ul class="ul-form" style="position: relative" >
			<li><label>所属科室：</label>
				<sys:treeselect id="department" name="department.id" value="${data.department.id}" labelName="department.name" labelValue="${data.department.name}"
					title="科室" url="/sys/office/treeData?type=2" cssClass="input-small" allowClear="true" notAllowSelectParent="true"/>
			</li>
			<li><label>所属教研室：</label>
				<sys:treeselect id="trc" name="trc.id" value="${data.trc.id}" labelName="trc.name" labelValue="${data.trc.name}"
								title="教研室" url="/sys/office/treeData?type=1" cssClass="input-small" allowClear="true" notAllowSelectParent="true"/>
			</li>
			<li><label>日期：</label>
				<input name="startDate" id="startDate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					   value="<fmt:formatDate value="${data.startDate}" pattern="yyyy-MM"/>"
					   onclick="WdatePicker({dateFmt:'yyyy-MM',isShowClear:false});"/> -
				<input name="endDate" id="endDate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					   value="<fmt:formatDate value="${data.endDate}" pattern="yyyy-MM"/>"
					   onclick="WdatePicker({dateFmt:'yyyy-MM',isShowClear:false});"/>
			</li>
			<li class="btns">
				<input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/>
				<input id="btnExport" class="btn btn-primary" type="button" value="导出"/>
				<input id="btnChart" class="btn btn-primary" type="button" value="Chart"/>
			</li>
			<li class="clearfix"></li>
		</ul>
	</form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>科室</th>
				<th>教研室</th>
				<th>获奖时间</th>
				<c:forEach items="${types}" var="type">
					<th>${type.value}</th>
				</c:forEach>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${TpteachCounts}" var="tpteachCount">
			<c:if test="${tpteachCount.department!=null}">
			<tr>
				<td>
					${tpteachCount.department.name}
				</td>
				<td>
					${tpteachCount.trc.name}
				</td>
				<td>
					<fmt:formatDate value="${tpteachCount.startDate}" pattern="yyyy-MM"/>
				</td>
				<td>
					${tpteachCount.articleNum}
				</td>
				<td>
					${tpteachCount.patentNum}
				</td>
				<td>
					${tpteachCount.subjectNum}
				</td>
				<td>
					${tpteachCount.achievementNum}
				</td>
				<td>
					${tpteachCount.excellentAwardNum}
				</td>
				<td>
					${tpteachCount.awardNum}
				</td>
				<td>
					${tpteachCount.articlesNum}
				</td>
			</tr>
			</c:if>
		</c:forEach>
		<jsp:include page="/WEB-INF/views/include/fileUpload.jsp"></jsp:include>
		</tbody>
	</table>


</body>
</html>