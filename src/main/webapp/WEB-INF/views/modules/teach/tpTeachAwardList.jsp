<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>讲课比赛管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
            $(".remark").hover(function(){
                top.$.jBox.info(`${remark}`,"Help",function(v,h,f){
                },{buttonsFocus:1});
                top.$('.jbox-body .jbox-icon').css('top','55px');
            },function(){
                //鼠标指针离开
            });
            $("#btnImport").click(function(){
                $.jBox($("#importBox").html(), {title:"导入数据", buttons:{"关闭":true},
                    bottomText:"导入文件不能超过5M，仅允许导入“xls”或“xlsx”格式文件！"});
            });
            $("#btnExport").click(function(){
                top.$.jBox.confirm("确认要导出数据吗？","系统提示",function(v,h,f){
                    if(v=="ok"){
                    	var url_bak =  $("#searchForm").attr("action");
                        $("#searchForm").attr("action","${ctx}/teach/tpTeachAward/export?isStudent=${isStudent}&ids="+getAllCheckedItem());
                        $("#searchForm").submit();
                        $("#searchForm").attr("action",url_bak);
                    }
                },{buttonsFocus:1});
                top.$('.jbox-body .jbox-icon').css('top','55px');
            });
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
	<%@ include file="/WEB-INF/views/include/include_only_delete_js.jsp"%>
</head>
<body>
	<div id="importBox" class="hide">
		<form id="importForm" action="${ctx}/teach/tpTeachAward/import?isStudent=${isStudent}" method="post" enctype="multipart/form-data"
			  class="form-search" style="padding-left:20px;text-align:center;" onsubmit="loading('正在导入，请稍等...');"><br/>
			<input id="uploadFile" name="file" type="file" style="width:330px"/><br/><br/>　　
			<input id="btnImportSubmit" class="btn btn-primary" type="submit" value="   导    入   "/>
			<a href="${ctx}/teach/tpTeachAward/import/template?isStudent=${isStudent}">下载模板</a>
		</form>
	</div>

	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/teach/tpTeachAward/list?isStudent=${isStudent}">${empty isStudent ? '讲课比赛列表' : '获奖列表'}</a></li>
		<shiro:hasPermission name="teach:tpTeachAward:edit"><li><a href="${ctx}/teach/tpTeachAward/form?isStudent=${isStudent}">${empty isStudent ? '讲课比赛' : '获奖'}添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="tpTeachAward" action="${ctx}/teach/tpTeachAward/list?isStudent=${isStudent}" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form" style="position: relative">
			<div class="remark" style="position: absolute;right: 0px;"> <i class="icon-comment icon-blue"></i> </div>
			<li><label>${empty isStudent ? '课程名称：' : '奖项名：'}</label>
				<form:input path="title" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li><label>${empty isStudent ? '课程建设人：' : '获奖人：'}</label>
				<sys:treeselect id="user" name="user.id" value="${tpTeachAward.user.id}" labelName="user.name" labelValue="${tpTeachAward.user.name}"
					title="用户" url="/sys/office/treeData?type=3${isStudent eq 1 ? '&uType=2' : '' }" cssClass="input-small" allowClear="true" notAllowSelectParent="true"/>
			</li>
			<c:if test="${not empty isStudent and isStudent eq 1}">
				<li><label>导师</label>
					<sys:treeselect id="tutor" name="tutor.id" value="${tpTeachAward.tutor.id}" labelName="tutor.name" labelValue="${tpTeachAward.tutor.name}"
						title="用户" url="/sys/office/treeData?type=3&uType=1" cssClass="input-small" allowClear="true" notAllowSelectParent="true"/>
				</li>
			</c:if>
			<li><label>获奖级别：</label>
				<form:select path="level" class="input-medium">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('level_raward')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</li>
			<li><label>名次：</label>
				<form:select path="rank" class="input-medium">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('raward_rank')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</li>
			<li><label>获奖时间：</label>
				<input name="startWinningTime" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					value="<fmt:formatDate value="${tpTeachAward.startWinningTime}" pattern="yyyy-MM-dd"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:false});"/> - 
				<input name="endWinningTime" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					value="<fmt:formatDate value="${tpTeachAward.endWinningTime}" pattern="yyyy-MM-dd"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:false});"/>
			</li>
			<li><label>所属科室：</label>
				<sys:treeselect id="department" name="department.id" value="${tpTeachAward.department.id}" labelName="department.name" labelValue="${tpTeachAward.department.name}"
					title="科室" url="/sys/office/treeData?type=2" cssClass="input-small" allowClear="true" notAllowSelectParent="true"/>
			</li>
			<li><label>所属教研室：</label>
				<sys:treeselect id="trc" name="trc.id" value="${tpTeachAward.trc.id}" labelName="trc.name" labelValue="${tpTeachAward.trc.name}"
					title="教研室" url="/sys/office/treeData?type=1" cssClass="input-small" allowClear="true" notAllowSelectParent="true"/>
			</li>
			<li class="btns">
				<input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/>
				<input id="btnExport" class="btn btn-primary" type="button" value="导出"/>
				<c:if test="${tpTeachAward.userDataScope gt 0 }">
					<input id="btnImport" class="btn btn-primary" type="button" value="导入"/>
				</c:if>
			</li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>${empty isStudent ? '讲课题目' : '奖项名'}</th>
				<th>工号</th>
				<th>获奖人</th>
				<th>获奖级别</th>
				<th>获奖时间</th>
				<th>名次</th>
				<th>其他奖项</th>
				<th>所属科室</th>
				<th>所属教研室</th>
				<th>备注信息</th>
				<th>审核状态</th>
				<%--<th>更新者</th>
				<th>更新时间</th>--%>
				<shiro:hasPermission name="teach:tpTeachAward:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="tpTeachAward">
			<tr id="${tpTeachAward.id}">
				<td><a href="${ctx}/teach/tpTeachAward/form?isStudent=${isStudent}&id=${tpTeachAward.id}">
					${tpTeachAward.title}
				</a></td>
				<td>
					${tpTeachAward.user.loginName}
				</td>
				<td>
					${tpTeachAward.user.name}
				</td>
				<td>
					${fns:getDictLabel(tpTeachAward.level, 'level_raward', '')}
				</td>
				<td>
					<fmt:formatDate value="${tpTeachAward.winningTime}" pattern="yyyy-MM-dd"/>
				</td>
				<td>
					${fns:getDictLabel(tpTeachAward.rank, 'raward_rank', '')}
				</td>
				<td>
					${tpTeachAward.otherAward}
				</td>
				<td>
					${tpTeachAward.department.name}
				</td>
				<td>
					${tpTeachAward.trc.name}
				</td>
				<td>
					${tpTeachAward.remarks}
				</td>
				<td>
					${fns:getDictLabel(tpTeachAward.status, 'status', '')}
				</td>
			<%--	<td>
					${tpTeachAward.updateBy.id}
				</td>
				<td>
					<fmt:formatDate value="${tpTeachAward.updateDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>--%>
				<shiro:hasPermission name="teach:tpTeachAward:edit"><td>
    				<a href="${ctx}/teach/tpTeachAward/form?isStudent=${isStudent}&id=${tpTeachAward.id}">${tpTeachAward.status eq 2 ? '修改' : '查看' }</a>
    				<c:if test="${tpTeachAward.status eq 0 or empty tpTeachAward.procInsId}">
						<a href="javascript:;"  class="btnBox" data-modelId="${tpTeachAward.id}"> 文件</a>
						<a href="${ctx}/teach/tpTeachAward/delete?isStudent=${isStudent}&id=${tpTeachAward.id}" onclick="return confirmx('确认要删除吗？', this.href)">删除</a>
					</c:if>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		<jsp:include page="/WEB-INF/views/include/fileUpload.jsp"></jsp:include>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>