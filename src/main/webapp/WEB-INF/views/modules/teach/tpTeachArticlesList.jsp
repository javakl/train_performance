<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>教学文章管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
            $(".remark").hover(function(){
                top.$.jBox.info(`${remark}`,"Help",function(v,h,f){
                },{buttonsFocus:1});
                top.$('.jbox-body .jbox-icon').css('top','55px');
            },function(){
                //鼠标指针离开
            });
            $("#btnImport").click(function(){
                $.jBox($("#importBox").html(), {title:"导入数据", buttons:{"关闭":true},
                    bottomText:"导入文件不能超过5M，仅允许导入“xls”或“xlsx”格式文件！"});
            });
            $("#btnExport").click(function(){
                top.$.jBox.confirm("确认要导出用户数据吗？","系统提示",function(v,h,f){
                    if(v=="ok"){
                    	var url_bak =  $("#searchForm").attr("action");
                        $("#searchForm").attr("action","${ctx}/teach/tpTeachArticles/export?isStudent=${isStudent}&ids="+getAllCheckedItem());
                        $("#searchForm").submit();
                        $("#searchForm").attr("action",url_bak);
                    }
                },{buttonsFocus:1});
                top.$('.jbox-body .jbox-icon').css('top','55px');
            });
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
    <%@ include file="/WEB-INF/views/include/include_only_delete_js.jsp"%>
</head>
<body>
	<div id="importBox" class="hide">
		<form id="importForm" action="${ctx}/teach/tpTeachArticles/import?isStudent=${isStudent}" method="post" enctype="multipart/form-data"
			  class="form-search" style="padding-left:20px;text-align:center;" onsubmit="loading('正在导入，请稍等...');"><br/>
			<input id="uploadFile" name="file" type="file" style="width:330px"/><br/><br/>　　
			<input id="btnImportSubmit" class="btn btn-primary" type="submit" value="   导    入   "/>
			<a href="${ctx}/teach/tpTeachAward/import/template">下载模板</a>
		</form>
	</div>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/teach/tpTeachArticles/list?isStudent=${isStudent}">${isStudent != 1  ? '教学文章列表' : '发表文章列表'}</a></li>
		<shiro:hasPermission name="teach:tpTeachArticles:edit"><li><a href="${ctx}/teach/tpTeachArticles/form?isStudent=${isStudent}">${isStudent != 1 ? '教学文章' : '发表文章'}添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="tpTeachArticles" action="${ctx}/teach/tpTeachArticles/list?isStudent=${isStudent}" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form" style="position: relative">
			<div class="remark" style="position: absolute;right: 0px;"> <i class="icon-comment icon-blue"></i> </div>
			<li><label>论文名：</label>
				<form:input path="articleTitle" htmlEscape="false" maxlength="100" class="input-medium"/>
			</li>
			<li><label>发表作者：</label>
				<sys:treeselect id="user" name="user.id" value="${tpTeachArticles.user.id}" labelName="user.name" labelValue="${tpTeachArticles.user.name}"
					title="用户" url="/sys/office/treeData?type=3${isStudent eq 1 ? '&uType=2' : '' }" cssClass="input-small" allowClear="true" notAllowSelectParent="true"/>
			</li>
			<c:if test="${isStudent eq 1 }">
				<li><label>导师</label>
					<sys:treeselect id="tutor" name="tutor.id" value="${tpTeachArticles.tutor.id}" labelName="tutor.name" labelValue="${tpTeachArticles.tutor.name}"
						title="用户" url="/sys/office/treeData?type=3&uType=1" cssClass="input-small" allowClear="true" notAllowSelectParent="true"/>
				</li>
			</c:if>
			<li><label>文章类型：</label>
				<form:select path="articleType" class="input-medium">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('article_type')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</li>
			<li><label>发表期刊：</label>
				<form:input path="pushJournal" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li><label>期刊类型：</label>
				<form:select path="journalType" class="input-medium">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('journal_type')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</li>
			<li><label>发表年份：</label>
				<input id="beginPushYear" name="beginPushYear" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					value="${tpTeachArticles.beginPushYear}"
					onclick="WdatePicker({dateFmt:'yyyy-MM',maxDate:'#F{$dp.$D(\'endPushYear\')}',isShowClear:false});"/> -
				<input id="endPushYear" name="endPushYear" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					value="${tpTeachArticles.endPushYear}"
					onclick="WdatePicker({dateFmt:'yyyy-MM',minDate:'#F{$dp.$D(\'beginPushYear\')}',isShowClear:false});"/>
			</li>
			<li><label>所属科室：</label>
				<sys:treeselect id="department" name="department.id" value="${tpTeachArticles.department.id}" labelName="department.name" labelValue="${tpTeachArticles.department.name}"
					title="科室" url="/sys/office/treeData?type=2" cssClass="input-small" allowClear="true" notAllowSelectParent="true"/>
			</li>
			<li><label>所属教研室：</label>
				<sys:treeselect id="trc" name="trc.id" value="${tpTeachArticles.trc.id}" labelName="trc.name" labelValue="${tpTeachArticles.trc.name}"
					title="教研室" url="/sys/office/treeData?type=1" cssClass="input-small" allowClear="true" notAllowSelectParent="true"/>
			</li>
			<li><label>审核状态：</label>
				<form:select path="status" class="input-medium">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('status')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</li>
			<c:if test="${isStudent eq 1}">
				<li><label>是否以此申学位：</label>
					<form:radiobuttons path="isApplyDegree" items="${fns:getDictList('yes_no')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</li>
			</c:if>
			<li class="btns">
				<input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/>
				<input id="btnExport" class="btn btn-primary" type="button" value="导出"/>
				<c:if test="${tpTeachArticles.userDataScope gt 0 }">
					<input id="btnImport" class="btn btn-primary" type="button" value="导入"/>
				</c:if>
			</li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr style="text-align: center">
				<th>论文名</th>
				<th>工号</th>
				<th style="min-width:80px;">发表作者</th>
				<th>第一作者或通讯作者</th>
				<th>文章类型</th>
				<th>发表期刊</th>
				<th>期刊类型</th>
				<th>发表年份</th>
				<th>期卷</th>
				<th>页数</th>
				<th>online收录时间</th>
				<th>online影响因子</th>
				<th>onlineJCR分区</th>
				<th>作者情况</th>
				<th>所属科室</th>
				<th>所属教研室</th>
				<th>备注信息</th>
				<th>审核状态</th>
				<th>是否以此申学位</th>
				<shiro:hasPermission name="teach:tpTeachArticles:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="tpTeachArticles">
			<tr id="${tpTeachArticles.id}">
				<td style="white-space:nowrap"><a href="${ctx}/teach/tpTeachArticles/form?id=${tpTeachArticles.id}&isStudent=${isStudent}">
					${tpTeachArticles.articleTitle}
				</a></td>
				<td>
					${tpTeachArticles.user.loginName}
				</td>
				<td>
					${tpTeachArticles.user.name}
				</td>
				<td>
					${tpTeachArticles.authorName}
				</td>
				<td>
					${fns:getDictLabel(tpTeachArticles.articleType, 'article_type', '')}
				</td>
				<td>
					${tpTeachArticles.pushJournalName}
				</td>
				<td>
					${fns:getDictLabel(tpTeachArticles.journalType, 'journal_type', '')}
				</td>
				<td>
					${tpTeachArticles.pushYear}
				</td>
				<td>
					${tpTeachArticles.volume}
				</td>
				<td>
					${tpTeachArticles.pages}
				</td>
				<td style="white-space:nowrap">
					<fmt:formatDate value="${tpTeachArticles.onltime}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td>
					${tpTeachArticles.onlelement}
				</td>
				<td>
					<c:choose>
						<c:when test="${!empty tpTeachArticles.onlJcrUrl}">
							<a href="${tpTeachArticles.onlJcrUrl}" target="_blank">${tpTeachArticles.onljcr}</a>
						</c:when>
						<c:otherwise>
							${tpTeachArticles.onljcr}
						</c:otherwise>
					</c:choose>
				</td>
				<td style="white-space:nowrap">
					${tpTeachArticles.authContent}
				</td>
				<td>
					${tpTeachArticles.department.name}
				</td>
				<td>
					${tpTeachArticles.trc.name}
				</td>
				<td>
					${tpTeachArticles.remarks}
				</td>
				<td>
					${fns:getDictLabel(tpTeachArticles.status, 'status', '')}
				</td>
				<td>
					${fns:getDictLabel(tpTeachArticles.isApplyDegree, 'yes_no', '')}
				</td>
				<shiro:hasPermission name="teach:tpTeachArticles:edit"><td>
    				<a href="${ctx}/teach/tpTeachArticles/form?id=${tpTeachArticles.id}&isStudent=${isStudent}">${tpTeachArticles.status eq 2 ? '修改' : '查看' }</a>
					<a href="javascript:;"  class="btnBox" data-modelId="${tpTeachArticles.id}"> 文件</a>
					<a href="${ctx}/teach/tpTeachArticles/delete?id=${tpTeachArticles.id}&isStudent=${isStudent}" onclick="return confirmx('确认要删除该教学文章吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		<jsp:include page="/WEB-INF/views/include/fileUpload.jsp"></jsp:include>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>