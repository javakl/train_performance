<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>教学活动表管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
            $(".remark").hover(function(){
                top.$.jBox.info(`${remark}`,"Help",function(v,h,f){
                },{buttonsFocus:1});
                top.$('.jbox-body .jbox-icon').css('top','55px');
            },function(){
                //鼠标指针离开
            });
            $("#btnExport").click(function(){
                top.$.jBox.confirm("确认要导出数据吗？","系统提示",function(v,h,f){
                    if(v=="ok"){
                    	var url_bak =  $("#searchForm").attr("action");
                        $("#searchForm").attr("action","${ctx}/teach/tpTeachActive/export?teachTypes=${teachTypes}&menuId=${param.menuId}&ids="+getAllCheckedItem());
                        $("#searchForm").submit();
                        $("#searchForm").attr("action",url_bak);
                    }
                },{buttonsFocus:1});
                top.$('.jbox-body .jbox-icon').css('top','55px');
            });
            $("#btnImport").click(function(){
                $.jBox($("#importBox").html(), {title:"导入数据", buttons:{"关闭":true},
                    bottomText:"导入文件不能超过5M，仅允许导入“xls”或“xlsx”格式文件！"});
            });
            $("#btnRefresh").click(function(){
                top.$.jBox.confirm("确认要刷新得分吗？","系统提示",function(v,h,f){
                    if(v=="ok"){
                    	
                    }
                },{buttonsFocus:1});
            });
            //replaceTitle( ${custom_title},"searchForm","ths",undefined);
            //标题定制初始化表格
            initCustomTitleTable(`${param.menuId}`,'#contentTable');
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
	<style type="text/css">
		#contentTable  td,#contentTable  th{
			white-space: nowrap;
			text-align: center;
		}
	</style>
	<%@ include file="/WEB-INF/views/include/include_only_delete_js.jsp"%>
</head>
<body>
	<div id="importBox" class="hide">
		<form id="importForm" action="${ctx}/teach/tpTeachActive/import?teachTypes=${teachTypes}&menuId=${param.menuId}" method="post" enctype="multipart/form-data"
			  class="form-search" style="padding-left:20px;text-align:center;" onsubmit="loading('正在导入，请稍等...');"><br/>
			<input id="uploadFile" name="file" type="file" style="width:330px"/><br/><br/>　　
			<input id="btnImportSubmit" class="btn btn-primary" type="submit" value="   导    入   "/>
			<a href="${ctx}/teach/tpTeachActive/import/template?">下载模板</a>
		</form>
	</div>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/teach/tpTeachActive/list?teachTypes=${teachTypes}&menuId=${param.menuId}">列表</a></li>
		<shiro:hasPermission name="teach:tpTeachActive:edit"><li><a href="${ctx}/teach/tpTeachActive/form?teachTypes=${teachTypes}&menuId=${param.menuId}">添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="tpTeachActive" action="${ctx}/teach/tpTeachActive/?teachTypes=${teachTypes}&menuId=${param.menuId}" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form" style="position: relative">
			<div class="remark" style="position: absolute;right: 0px;"> <i class="icon-comment icon-blue"></i> </div>
			<li><label>名称：</label>
				<form:input path="name" htmlEscape="false" maxlength="50" class="input-medium"/>
			</li>
			<li><label>老师工号：</label>
				<form:input path="user.loginName" htmlEscape="false" maxlength="50" class="input-medium"/>
			</li>
			<li><label>老师：</label>
				<sys:treeselect id="user" name="user.id" value="${tpTeachActive.user.id}" labelName="user.name" labelValue="${tpTeachActive.user.name}"
					title="用户" url="/sys/office/treeData?type=3" cssClass="input-small" allowClear="true" notAllowSelectParent="true"/>
			</li>
			<li><label>类型：</label>
				<form:select path="teachType" class="input-medium">
					<form:option value="" label="" title="" />
					<form:options items="${fns:getDictListByIds('teach_type',teachTypes)}" itemLabel="label" itemValue="value" title="description"   htmlEscape="false"/>
				</form:select>
			</li>
			<li><label>学生类别：</label>
				<form:select path="stuType" class="input-medium">
					<form:option value="" label="" />
					<form:options items="${fns:getDictList('stu_type')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</li>
			<li><label>学员年级：</label>
				<form:select path="stuGrade" class="input-medium">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('stu_grade')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</li>
			<li><label>所属科室：</label>
				<sys:treeselect id="department" name="department.id" value="${tpTeachActive.department.id}" labelName="department.name" labelValue="${tpTeachActive.department.name}"
					title="科室" url="/sys/office/treeData?type=2" cssClass="input-small" allowClear="true" notAllowSelectParent="true"/>
			</li>
			<li><label>所属教研室：</label>
				<sys:treeselect id="trc" name="trc.id" value="${tpTeachActive.trc.id}" labelName="trc.name" labelValue="${tpTeachActive.trc.name}"
					title="教研室" url="/sys/office/treeData?type=1" cssClass="input-small" allowClear="true" notAllowSelectParent="true"/>
			</li>
			<li><label>地点：</label>
				<form:input path="activePlace" htmlEscape="false" maxlength="200" class="input-medium"/>
			</li>
			<li><label>开始日期：</label>
				<input id="startTime" name="startTime" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					value="<fmt:formatDate value="${tpTeachActive.startTime}" pattern="yyyy-MM-dd" />"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:false});"/>
				<c:if test="${fn:contains('1,14,15,16',teachTypes)}">
				-
				<input id="endTime" name="endTime" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					value="<fmt:formatDate value="${tpTeachActive.endTime}" pattern="yyyy-MM-dd" />"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:false});"/>
				</c:if>
			</li>
			<li><label>审核状态：</label>
				<form:select path="status" class="input-medium">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('status')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</li>
			<li class="btns">
				<input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/>
				<input id="btnExport" class="btn btn-primary" type="button" value="导出"/>
				<c:if test="${tpTeachActive.userDataScope gt 0 }">
					<input id="btnImport" class="btn btn-primary" type="button" value="导入"/>
				</c:if>
				<shiro:hasPermission name="teach:tpTeachActive:edit">
					<input id="btnRefresh" class="btn btn-info" type="button" value="刷新得分"/>
				</shiro:hasPermission>
				
			</li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr id="ths">
				<th data-title="name">课程名</th>
				<th data-title="user.loginName">工号</th>
				<th data-title="user.name">授课老师</th>
				<th data-title="teachType">活动类型</th>
				<th data-title="stuNum">带教量</th>
				<th data-title="startTime">开始时间</th>
				<th data-title="endTime">结束时间</th>
				<th data-title="stuType">学生类别</th>
				<th data-title="stuGrade">学员年级</th>
				<th data-title="department.id">所属科室</th>
				<th data-title="trc.id">所属教研室</th>
				<th data-title="administrative">管理部门</th>
				<th data-title="activePlace">活动地点</th>
				<th data-title="activeIntroduce">活动介绍</th>
				<th data-title="remarks">备注信息</th>
				<th data-title="status">审核状态</th>
				<shiro:hasPermission name="teach:tpTeachActive:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="tpTeachActive">
			<tr id="${tpTeachActive.id}">
				<td><a href="${ctx}/teach/tpTeachActive/form?id=${tpTeachActive.id}&teachTypes=${teachTypes}&menuId=${param.menuId}">
					${tpTeachActive.name}
				</a></td>
				<td>
					${tpTeachActive.user.loginName}
				</td>
				<td>
					${tpTeachActive.user.name}
				</td>
				<td>
					${fns:getDictLabel(tpTeachActive.teachType, 'teach_type', '')}
				</td>
				<td>
					${tpTeachActive.stuNum}
					<c:if test="${not empty tpTeachActive.stuNum}">
						${fns:getDictUnit(tpTeachActive.teachType,'teach_type')}
					</c:if>
				</td>
				<td>
					<fmt:formatDate value="${tpTeachActive.startTime}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td>
					<fmt:formatDate value="${tpTeachActive.endTime}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td>
					${fns:getDictLabel(tpTeachActive.stuType, 'stu_type', '')}
				</td>
				<td>
					${fns:getDictLabel(tpTeachActive.stuGrade, 'stu_grade', '')}
				</td>
				<td>
					${tpTeachActive.department.name}
				</td>
				<td>
					${tpTeachActive.trc.name}
				</td>
				<td>
					${tpTeachActive.administrative}
				</td>
				<td>
					${tpTeachActive.activePlace}
				</td>
				<td>
					${tpTeachActive.activeIntroduce}
				</td>
				<td>
					${tpTeachActive.remarks}
				</td>
				<td>
					${fns:getDictLabel(tpTeachActive.status, 'status', '')}
				</td>
				<shiro:hasPermission name="teach:tpTeachActive:edit"><td>
    				<a href="${ctx}/teach/tpTeachActive/form?id=${tpTeachActive.id}&teachTypes=${teachTypes}&menuId=${param.menuId}">${tpTeachActive.status eq 2 ? '修改' : '查看' }</a>
					<a href="javascript:;"  class="btnBox" data-modelId="${tpTeachActive.id}"> 文件</a>
					<a href="${ctx}/teach/tpTeachActive/delete?id=${tpTeachActive.id}&teachTypes=${teachTypes}&menuId=${param.menuId}" onclick="return confirmx('确认要删除该教学活动表吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		<jsp:include page="/WEB-INF/views/include/fileUpload.jsp"></jsp:include>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>