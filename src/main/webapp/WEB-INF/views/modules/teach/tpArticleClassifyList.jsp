<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>文章期刊管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
	$(document).ready(function() {
        $("#btnImport").click(function(){
            $.jBox($("#importBox").html(), {title:"导入数据", buttons:{"关闭":true},
                bottomText:"导入文件不能超过5M，仅允许导入“xls”或“xlsx”格式文件！"});
        });
        $("#btnExport").click(function(){
            top.$.jBox.confirm("确认要导出数据吗？","系统提示",function(v,h,f){
                if(v=="ok"){
                    $("#searchForm").attr("action","${ctx}/teach/tpArticleClassify/export?ids="+getAllCheckedItem());
                    $("#searchForm").submit();
                }
            },{buttonsFocus:1});
            top.$('.jbox-body .jbox-icon').css('top','55px');
        });

	});

    function page(n,s){
        $("#pageNo").val(n);
        $("#pageSize").val(s);
        $("#searchForm").submit();
        return false;
    }
	</script>
    <%@ include file="/WEB-INF/views/include/include_only_delete_js.jsp"%>
</head>
<body>
	<div id="importBox" class="hide">
		<form id="importForm" action="${ctx}/teach/tpArticleClassify/import" method="post" enctype="multipart/form-data"
			  class="form-search" style="padding-left:20px;text-align:center;" onsubmit="loading('正在导入，请稍等...');"><br/>
			<input id="uploadFile" name="file" type="file" style="width:330px"/><br/><br/>　　
			<input id="btnImportSubmit" class="btn btn-primary" type="submit" value="   导    入   "/>
			<a href="${ctx}/teach/tpArticleClassify/import/template">下载模板</a>
		</form>
	</div>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/teach/tpArticleClassify/">文章期刊列表</a></li>
		<shiro:hasPermission name="teach:tpArticleClassify:edit"><li><a href="${ctx}/teach/tpArticleClassify/form">文章期刊添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="tpArticleClassify" action="${ctx}/teach/tpArticleClassify/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>期刊名称：</label>
				<form:input path="name" htmlEscape="false" maxlength="100" class="input-medium"/>
			</li>
			<li><label>期刊分类：</label>
				<form:radiobuttons path="type" items="${fns:getDictList('journal_type')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
			</li>
			<li><label>期刊类别：</label>
				<form:input path="category" htmlEscape="false" maxlength="50" class="input-medium"/>
			</li>
			<li><label>年限：</label>
				<form:input path="year" htmlEscape="false" maxlength="10" class="input-medium"/>
			</li>
			<li class="btns">
				<input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/>
				<input id="btnExport" class="btn btn-primary" type="button" value="导出"/>
				<shiro:hasPermission name="teach:tpArticleClassify:edit">
					<input id="btnImport" class="btn btn-primary" type="button" value="导入"/>
				</shiro:hasPermission>
			</li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>期刊名称</th>
				<th>期刊分类</th>
				<th>期刊类别</th>
				<th>年限</th>
				<th>更新时间</th>
				<shiro:hasPermission name="teach:tpArticleClassify:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="tpArticleClassify">
			<tr id="${tpArticleClassify.id}">
				<td><a href="${ctx}/teach/tpArticleClassify/form?id=${tpArticleClassify.id}">
					${tpArticleClassify.name}
				</a></td>
				<td>
					${fns:getDictLabel(tpArticleClassify.type, 'journal_type', '')}
				</td>
				<td>
					${tpArticleClassify.category}
				</td>
				<td>
					${tpArticleClassify.year}
				</td>
				<td>
					<fmt:formatDate value="${tpArticleClassify.updateDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<shiro:hasPermission name="teach:tpArticleClassify:edit"><td>
    				<a href="${ctx}/teach/tpArticleClassify/form?id=${tpArticleClassify.id}">修改</a>
					<a href="${ctx}/teach/tpArticleClassify/delete?id=${tpArticleClassify.id}" onclick="return confirmx('确认要删除该文章期刊吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>