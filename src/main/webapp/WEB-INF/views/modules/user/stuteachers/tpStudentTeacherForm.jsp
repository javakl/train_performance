<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>学生辅导老师管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			//$("#name").focus();
			$("#inputForm").validate({
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
		});
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li><a href="${ctx}/user/stuteachers/tpStudentTeacher/">列表</a></li>
		<li class="active"><a href="${ctx}/user/stuteachers/tpStudentTeacher/form?id=${tpStudentTeacher.id}"><shiro:hasPermission name="user:stuteachers:tpStudentTeacher:edit">${not empty tpStudentTeacher.id?'修改':'添加'}</shiro:hasPermission><shiro:lacksPermission name="user:stuteachers:tpStudentTeacher:edit">查看</shiro:lacksPermission></a></li>
	</ul><br/>
	<form:form id="inputForm" modelAttribute="tpStudentTeacher" action="${ctx}/user/stuteachers/tpStudentTeacher/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<sys:message content="${message}"/>		
		<div class="control-group">
			<label class="control-label">教师：</label>
			<div class="controls">
				<sys:treeselect id="teacher" name="teacher.id" value="${tpStudentTeacher.teacher.id}" labelName="teacher.name" labelValue="${tpStudentTeacher.teacher.name}"
					title="用户" url="/sys/office/treeData?type=3&uType=1" cssClass="" allowClear="true" notAllowSelectParent="true"/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">学生：</label>
			<div class="controls">
				<sys:treeselect id="stu" name="stu.id" value="${tpStudentTeacher.stu.id}" labelName="stu.name" labelValue="${tpStudentTeacher.stu.name}"
					title="用户" url="/sys/office/treeData?type=3&uType=2" cssClass="" allowClear="true" notAllowSelectParent="true"/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">开始日期：</label>
			<div class="controls">
				<input name="startDate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate "
					value="${tpStudentTeacher.startDate}"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:false});"/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">结束日期：</label>
			<div class="controls">
				<input name="endDate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate "
					value="${tpStudentTeacher.endDate}"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:false});"/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">类型：</label>
			<div class="controls">
				<form:select path="type" class="input-medium required">
					<form:option value="" label="" title="" />
					<form:options items="${fns:getDictList('tutor_type')}" itemLabel="label" itemValue="value" title="description"   htmlEscape="false"/>
				</form:select>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="form-actions">
			<shiro:hasPermission name="user:stuteachers:tpStudentTeacher:edit"><input id="btnSubmit" class="btn btn-primary" type="submit" value="保 存"/>&nbsp;</shiro:hasPermission>
			<input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
		</div>
	</form:form>
</body>
</html>