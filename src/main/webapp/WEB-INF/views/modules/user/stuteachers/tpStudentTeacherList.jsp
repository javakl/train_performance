<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>学生辅导老师管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
		function getTabHtml(setting){
        	var liHtml='';
            var divHtml = '';
            for(var i = 0;i<setting.length;i++){
                if(i==0){
                    liHtml+='<li><a href="#tab'+i+'" class="selected">'+setting[i].title+'</a></li>';
                    divHtml+='<div id="tab'+i+'" class="tabson" style="margin:0px;height: 90%;">'
					divHtml+='	<iframe src="${ctx}/'+setting[i].url+'" width="100%" height="100%"></iframe>'
					divHtml+='</div>';
                }
                else{
                    liHtml+='<li><a href="#tab'+i+'" class="" onclick="openTab(\''+setting[i].url+'\',\'tab'+i+'\')" >'+setting[i].title+'</a></li>';
                    divHtml+='<div id="tab'+i+'" class="tabson" style="margin:0px;height: 90%;">'
					divHtml+='	<iframe src="" width="100%" height="100%"></iframe>'
					divHtml+='</div>';
                }
            }
            var rtnHtml='<div id="usual1" class="usual" style="height:100%;"><div class="itab" ><ul>'+liHtml+'</ul></div>'+divHtml+'</div>';
            return rtnHtml;
        }
        function openTab(toUrl,tabId){
            console.log(toUrl+'   '+tabId);
            console.log($('#'+tabId).find('iframe').attr('src')=='')
            if($('#'+tabId).find('iframe').attr('src')==''){
                $('#'+tabId).find('iframe').attr('src','${ctx}/'+toUrl);
            }
        }
		//学员资料管理
        function openStuInformationDd(stuId){
            layer.open({
                type: 1,
                title:'资料管理',
                skin: 'layui-layer-lan',
                closeBtn:2,
                fix: false, //不固定
                maxmin: true,
                area: ['96%', '95%'], //宽高
                content: getTabHtml([{title:'获奖情况',url:'/teach/tpTeachAward/list?isStudent=1&user.id='+stuId},
                                     {title:'发表文章',url:'/teach/tpTeachArticles/list?isStudent=1&user.id='+stuId},
                                     {title:'考试记录',url:'/student/exam/tpStudentExam/list?user.id='+stuId},
                                     {title:'毕业论文',url:'/student/graduate/tpStudentGraduate/list?user.id='+stuId},
                                     {title:'学术会议',url:'/student/meeting/tpStudentMeeting/list?user.id='+stuId},
                                     {title:'处分情况',url:'/student/punish/tpStudentPunish/list?user.id='+stuId}]),
                success : function(){
                    $("#usual1 ul").idTabs();
                    $('.tablelist tbody tr:odd').addClass('odd');
                },
                end : function(){//关闭事件

                }
            });
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/user/stuteachers/tpStudentTeacher/">列表</a></li>
		<shiro:hasPermission name="user:stuteachers:tpStudentTeacher:edit"><li><a href="${ctx}/user/stuteachers/tpStudentTeacher/form">添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="tpStudentTeacher" action="${ctx}/user/stuteachers/tpStudentTeacher/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>教师：</label>
				<sys:treeselect id="teacher" name="teacher.id" value="${tpStudentTeacher.teacher.id}" labelName="teacher.name" labelValue="${tpStudentTeacher.teacher.name}"
					title="用户" url="/sys/office/treeData?type=3&uType=1" cssClass="input-small" allowClear="true" notAllowSelectParent="true"/>
			</li>
			<li><label>学生：</label>
				<sys:treeselect id="stu" name="stu.id" value="${tpStudentTeacher.stu.id}" labelName="stu.name" labelValue="${tpStudentTeacher.stu.name}"
					title="用户" url="/sys/office/treeData?type=3&uType=2" cssClass="input-small" allowClear="true" notAllowSelectParent="true"/>
			</li>
			<li><label>开始日期：</label>
				<input name="beginStartDate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					value="${tpStudentTeacher.beginStartDate}"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:false});"/> - 
				<input name="endStartDate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					value="${tpStudentTeacher.endStartDate}"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:false});"/>
			</li>
			<li><label>结束日期：</label>
				<input name="beginEndDate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					value="${tpStudentTeacher.beginEndDate}"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:false});"/> - 
				<input name="endEndDate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					value="${tpStudentTeacher.endEndDate}"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:false});"/>
			</li>
			<li><label>类型：</label>
				<form:select path="type" class="input-medium">
					<form:option value="" label="" title="" />
					<form:options items="${fns:getDictList('tutor_type')}" itemLabel="label" itemValue="value" title="description"   htmlEscape="false"/>
				</form:select>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>教师</th>
				<th>职称</th>
				<th>学生</th>
				<th>年级</th>
				<th>学员类型</th>
				<th>是否研究生</th>
				<th>开始日期</th>
				<th>结束日期</th>
				<th>类型</th>
				<th>学员资料</th>
				<th>创建时间</th>
				<shiro:hasPermission name="user:stuteachers:tpStudentTeacher:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="tpStudentTeacher">
			<tr>
				<td><a href="${ctx}/user/stuteachers/tpStudentTeacher/form?id=${tpStudentTeacher.id}">
					${tpStudentTeacher.teacher.name}
				</a></td>
				<td>
					${fns:getDictLabel(tpStudentTeacher.teacherInfo.titleId, 'title', '')}
				</td>
				<td>
					${tpStudentTeacher.stu.name}
				</td>
				<td>
					${tpStudentTeacher.studentInfo.grade}
				</td>
				<td>
					${fns:getDictLabel(tpStudentTeacher.studentInfo.stuType, 'stu_type', '')}
				</td>
				<td>
					<c:if test="${empty tpStudentTeacher.studentInfo.isMaster}">
						${tpStudentTeacher.studentInfo.isMaster==1?"是":"否"}
					</c:if>
				</td>
				<td>
					${tpStudentTeacher.startDate}
				</td>
				<td>
					${tpStudentTeacher.endDate}
				</td>
				<td>
					${fns:getDictLabel(tpPTeacher.tutorType, 'tutor_type', '')}
				</td>
				<td>
					<a href="#" onclick="openStuInformationDd('${tpPStudent.user.id}')" >资料管理</a>
				</td>
				<td>
					<fmt:formatDate value="${tpStudentTeacher.createDate}" pattern="yyyy-MM-dd"/>
				</td>
				<shiro:hasPermission name="user:stuteachers:tpStudentTeacher:edit"><td>
    				<a href="${ctx}/user/stuteachers/tpStudentTeacher/form?id=${tpStudentTeacher.id}">修改</a>
					<a href="${ctx}/user/stuteachers/tpStudentTeacher/delete?id=${tpStudentTeacher.id}" onclick="return confirmx('确认要删除该学生辅导老师吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>