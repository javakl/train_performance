<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>学生管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
            $("#btnImport").click(function(){
                $.jBox($("#importBox").html(), {title:"导入数据", buttons:{"关闭":true},
                    bottomText:"导入文件不能超过5M，仅允许导入“xls”或“xlsx”格式文件！"});
            });
            $("#btnExport").click(function(){
                top.$.jBox.confirm("确认要导出数据吗？","系统提示",function(v,h,f){
                    if(v=="ok"){
                        $("#searchForm").attr("action","${ctx}/user/student/tpPStudent/export");
                        $("#searchForm").submit();
                    }
                },{buttonsFocus:1});
                top.$('.jbox-body .jbox-icon').css('top','55px');
            });
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
		
		function getTabHtml(setting){
        	var liHtml='';
            var divHtml = '';
            for(var i = 0;i<setting.length;i++){
                if(i==0){
                    liHtml+='<li><a href="#tab'+i+'" class="selected">'+setting[i].title+'</a></li>';
                    divHtml+='<div id="tab'+i+'" class="tabson" style="margin:0px;height: 90%;">'
					divHtml+='	<iframe src="${ctx}/'+setting[i].url+'" width="100%" height="100%"></iframe>'
					divHtml+='</div>';
                }
                else{
                    liHtml+='<li><a href="#tab'+i+'" class="" onclick="openTab(\''+setting[i].url+'\',\'tab'+i+'\')" >'+setting[i].title+'</a></li>';
                    divHtml+='<div id="tab'+i+'" class="tabson" style="margin:0px;height: 90%;">'
					divHtml+='	<iframe src="" width="100%" height="100%"></iframe>'
					divHtml+='</div>';
                }
            }
            var rtnHtml='<div id="usual1" class="usual" style="height:100%;"><div class="itab" ><ul>'+liHtml+'</ul></div>'+divHtml+'</div>';
            return rtnHtml;
        }
        function openTab(toUrl,tabId){
            console.log(toUrl+'   '+tabId);
            console.log($('#'+tabId).find('iframe').attr('src')=='')
            if($('#'+tabId).find('iframe').attr('src')==''){
                $('#'+tabId).find('iframe').attr('src','${ctx}/'+toUrl);
            }
        }
      //资料管理
        function openStuInformationDd(stuId){
            layer.open({
                type: 1,
                title:'资料管理',
                skin: 'layui-layer-lan',
                closeBtn:2,
                fix: false, //不固定
                maxmin: true,
                area: ['96%', '95%'], //宽高
                content: getTabHtml([{title:'获奖情况',url:'/teach/tpTeachAward/list?isStudent=1&user.id='+stuId},
                                     {title:'发表文章',url:'/teach/tpTeachArticles/list?isStudent=1&user.id='+stuId},
                                     {title:'考试记录',url:'/student/exam/tpStudentExam/list?user.id='+stuId},
                                     {title:'毕业论文',url:'/student/graduate/tpStudentGraduate/list?user.id='+stuId},
                                     {title:'学术会议',url:'/student/meeting/tpStudentMeeting/list?user.id='+stuId},
                                     {title:'处分情况',url:'/student/punish/tpStudentPunish/list?user.id='+stuId}]),
                success : function(){
                    $("#usual1 ul").idTabs();
                    $('.tablelist tbody tr:odd').addClass('odd');
                },
                end : function(){//关闭事件

                }
            });
        }
	</script>
</head>
<body>
	<div id="importBox" class="hide">
		<form id="importForm" action="${ctx}/user/student/tpPStudent/import?isMaster=${tpPStudent.isMaster}" method="post" enctype="multipart/form-data"
			  class="form-search" style="padding-left:20px;text-align:center;" onsubmit="loading('正在导入，请稍等...');"><br/>
			<input id="uploadFile" name="file" type="file" style="width:330px"/><br/><br/>　　
			<input id="btnImportSubmit" class="btn btn-primary" type="submit" value="   导    入   "/>
			<a href="${ctx}/user/student/tpPStudent/import/template">下载模板</a>
		</form>
	</div>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/user/student/tpPStudent?isMaster=${tpPStudent.isMaster}">学生列表</a></li>
		<shiro:hasPermission name="user:student:tpPStudent:edit"><li><a href="${ctx}/user/student/tpPStudent/userform?isMaster=${tpPStudent.isMaster}">学生添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="tpPStudent" action="${ctx}/user/student/tpPStudent/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<input  name="isMaster" type="hidden" value="${tpPStudent.isMaster}"/>

		<ul class="ul-form">
			<li><label>姓名：</label>
				<form:input path="user.name" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li><label>带教老师：</label>
				<sys:treeselect id="teacher" name="teacher.id" value="${tpPStudent.teacher.id}" labelName="teacher.name" labelValue="${tpPStudent.teacher.name}"
					title="用户" url="/sys/office/treeData?type=3" cssClass="input-small" allowClear="true" notAllowSelectParent="true"/>
			</li>
			<li><label>导师：</label>
				<sys:treeselect id="tutor" name="tutor.id" value="${tpPStudent.tutor.id}" labelName="tutor.name" labelValue="${tpPStudent.tutor.name}"
					title="用户" url="/sys/office/treeData?type=3" cssClass="input-small" allowClear="true" notAllowSelectParent="true"/>
			</li>
			<li><label>年级：</label>
				<form:select path="grade" class="input-medium">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('stu_grade')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</li>
			<li><label>学生类别：</label>
				<form:select path="stuType" class="input-medium">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('stu_type')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</li>
			<li><label>政治面貌：</label>
				<form:select path="politicalOutlook" class="input-medium">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('political_outlook')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</li>
			<li><label>民族：</label>
				<form:select path="nation" class="input-medium">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('nation')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</li>
			<li><label>研究生类型：</label>
				<form:select path="masterType" class="input-medium">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('master_type')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</li>
			<li><label>学位：</label>
				<form:select path="degree" class="input-medium">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('degree')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</li>
			<li><label>入学方式：</label>
				<form:select path="enrolmentWay" class="input-medium">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('enrolment_way')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</li>
			<li><label>所属科室：</label>
				<sys:treeselect id="" name="user.office.id" value="${tpPStudent.user.office.id}" labelName="user.office.name" labelValue="${tpPStudent.user.office.name}"
					title="部门" url="/sys/office/treeData?type=2" cssClass="input-small" allowClear="true" notAllowSelectParent="true"/>
			</li>
			<c:if test="${empty tpPStudent.isMaster}">
				<li><label>是否研究生：</label>
					<form:radiobuttons path="isMaster" items="${fns:getDictList('yes_no')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</li>
			</c:if>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/>
				<input id="btnExport" class="btn btn-primary" type="button" value="导出"/>
				<input id="btnImport" class="btn btn-primary" type="button" value="导入"/>
			</li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<c:set value="${tpPStudent.isMaster}" var="isMaster"></c:set>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th class="sort-column login_name">登录名</th>
				<th class="sort-column name">姓名</th>
				<th class="">学号</th>
				<%--<th>电话</th>--%>
				<th>手机</th>
				<c:if test="${empty isMaster}"><th>是否研究生</th></c:if>
				<th>带教老师</th>
				<th>导师</th>
				<th>年级</th>
				<th>所属科室</th>
				<th>学生类别</th>
				<th>政治面貌</th>
				<th>民族</th>
				<th>研究生类型</th>
				<th>学位</th>
				<th width="80px">入学时间</th>
				<th width="80px">毕业时间</th>
				<th>生源地</th>
				<th>入学方式</th>
				<th>来源</th>
				<th>备注信息</th>
				<th>资料管理</th>
				<shiro:hasPermission name="user:student:tpPStudent:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="tpPStudent">
			<tr>
				<td>
					<a href="${ctx}/user/student/tpPStudent/form?id=${tpPStudent.id}">
							${tpPStudent.user.loginName}
					</a>
				</td>
				<td>${tpPStudent.user.name}</td>
				<td>${tpPStudent.studentNo}</td>
				<%--<td>${tpPStudent.user.phone}</td>--%>
				<td>${tpPStudent.user.mobile}</td>
				<%-- <td>${tpPStudent.user.company.name}</td>
				<td>${tpPStudent.user.office.name}</td> --%>
				<c:if test="${empty isMaster}">
					<td>
						${tpPStudent.isMaster==1?"是":"否"}
					</td>
				</c:if>
				<td>
					${tpPStudent.teacher.name}
				</td>
				<td>
					${tpPStudent.tutor.name}
				</td>
				<td>
					${fns:getDictLabel(tpPStudent.grade, 'stu_grade', '')}
				</td>
				<td>${tpPStudent.user.office.name}</td>
				<td>
					${fns:getDictLabel(tpPStudent.stuType, 'stu_type', '')}
				</td>
				<td>
					${fns:getDictLabel(tpPStudent.politicalOutlook, 'political_outlook', '')}
				</td>
				<td>
					${fns:getDictLabel(tpPStudent.nation, 'nation', '')}
				</td>
				<td>
					${fns:getDictLabel(tpPStudent.masterType, 'master_type', '')}
				</td>
				<td>
					${fns:getDictLabel(tpPStudent.degree, 'degree', '')}
				</td>
				<td>
					<fmt:formatDate value="${tpPStudent.inSchool}" pattern="yyyy-MM-dd"/>
				</td>
				<td>
					<fmt:formatDate value="${tpPStudent.outSchool}" pattern="yyyy-MM-dd"/>
				</td>
				<td>
					${tpPStudent.birthPlace}
				</td>
				<td>
					${fns:getDictLabel(tpPStudent.enrolmentWay, 'enrolment_way', '')}
				</td>
				<td>
					${fns:getDictLabel(tpPStudent.rootIn, 'tp_stu_root_in', '')}
				</td>
				<td>
					${tpPStudent.remarks}
				</td>
				<td>
					<a href="#" onclick="openStuInformationDd('${tpPStudent.user.id}')" >资料管理</a>
				</td>
				<shiro:hasPermission name="user:student:tpPStudent:edit"><td>
    				<a href="${ctx}/user/student/tpPStudent/userform?id=${tpPStudent.user.id}">修改</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>