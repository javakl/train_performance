<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>老师管理</title>
	<meta name="decorator" content="default"/>
	<style type="text/css">
	</style>
	<script type="text/javascript">
	//被选中用户id列表
	var userIdList = new Array();
		$(document).ready(function() {
			$("#btnExport").click(function(){
				top.$.jBox.confirm("确认要导出用户数据吗？","系统提示",function(v,h,f){
					if(v=="ok"){
						$("#searchForm").attr("action","${ctx}/user/teacher/tpPTeacher/export?flag=${flag}&ids="+getAllCheckedItem());
						$("#searchForm").submit();
					}
				},{buttonsFocus:1});
				top.$('.jbox-body .jbox-icon').css('top','55px');
			});
			$("#btnImport").click(function(){
				$.jBox($("#importBox").html(), {title:"导入数据", buttons:{"关闭":true}, 
					bottomText:"导入文件不能超过5M，仅允许导入“xls”或“xlsx”格式文件！"});
			});
			//选中事件
			$('input[name=userIdList]').click(function(){
				if($(this).is(':checked')){
					if(userIdList.indexOf($(this).val())<0)userIdList.push($(this).val());
				}else{
					if(userIdList.indexOf($(this).val())>=0)userIdList.splice(userIdList.indexOf($(this).val()),1);
				}
				 
			});
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
		//指导学生列表弹出框
		var stuListDdIdx;
		function openStuListDd(teacherId,colName,title){
			stuListDdIdx = layer.open({
				  type: 2,
				  title:title+'列表',
				  skin: 'layui-layer-lan',
				  closeBtn:1,
				  area: ['95%', '95%'], //宽高
				  btn: ['关闭'],//按钮组
				  btnAlign: 'l',
				  yes: function(){
					  //关闭
					  layer.close(stuListDdIdx);
				  },
				  content: '${ctx}/user/stuteachers/tpStudentTeacher/list?'+colName+'='+teacherId,
				  success : function(){
				  },
				  end : function(){//关闭事件
				  }
				});
			
		}
		
		//教学工作弹出框
				//获得tab页html代码
        //setting [{title:'',url:''},{}]
        function getTabHtml(setting){
        	var liHtml='';
            var divHtml = '';
            for(var i = 0;i<setting.length;i++){
                if(i==0){
                    liHtml+='<li><a href="#tab'+i+'" class="selected">'+setting[i].title+'</a></li>';
                    divHtml+='<div id="tab'+i+'" class="tabson" style="margin:0px;height: 90%;">'
					divHtml+='	<iframe src="${ctx}/'+setting[i].url+'" width="100%" height="100%"></iframe>'
					divHtml+='</div>';
                }
                else{
                    liHtml+='<li><a href="#tab'+i+'" class="" onclick="openTab(\''+setting[i].url+'\',\'tab'+i+'\')" >'+setting[i].title+'</a></li>';
                    divHtml+='<div id="tab'+i+'" class="tabson" style="margin:0px;height: 90%;">'
					divHtml+='	<iframe src="" width="100%" height="100%"></iframe>'
					divHtml+='</div>';
                }
            }
            var rtnHtml='<div id="usual1" class="usual" style="height:100%;"><div class="itab" ><ul>'+liHtml+'</ul></div>'+divHtml+'</div>';
            return rtnHtml;
        }
        function openTab(toUrl,tabId){
            console.log(toUrl+'   '+tabId);
            console.log($('#'+tabId).find('iframe').attr('src')=='')
            if($('#'+tabId).find('iframe').attr('src')==''){
                $('#'+tabId).find('iframe').attr('src','${ctx}/'+toUrl);
            }
        }
      //教学工作
        function openTeachJobDd(teacherId){
            layer.open({
                type: 1,
                title:'教学工作列表',
                skin: 'layui-layer-lan',
                closeBtn:2,
                fix: false, //不固定
                maxmin: true,
                area: ['96%', '95%'], //宽高
                content: getTabHtml([{title:'教学活动',url:'/teach/tpTeachActive/list?&user.id='+teacherId},
                                     {title:'教学成果',url:'/teach/tpTeachAchievement/list?&user.id='+teacherId},
                                     {title:'教学文章',url:'/teach/tpTeachArticles/list?&user.id='+teacherId},
                                     {title:'讲课比赛',url:'/teach/tpTeachAward/list?&user.id='+teacherId},
                                     {title:'优秀教学奖',url:'/teach/tpTeachExcellentAward/list?&user.id='+teacherId},
                                     {title:'继教项目',url:'/teach/tpTeachKeepProject/list?&user.id='+teacherId},
                                     {title:'课程建设',url:'/teach/tpTeachLessonBuild/list?&user.id='+teacherId},
                                     {title:'教材建设',url:'/teach/tpTeachMaterial/list?&user.id='+teacherId},
                                     {title:'研究专利',url:'/teach/tpTeachPatent/list?&user.id='+teacherId},
                                     /* {title:'教学研究',url:'/teach/tpTeachResearch/list?&user.id='+teacherId}, */
                                     {title:'科研课题',url:'/teach/tpTeachSubject/list?&user.id='+teacherId}]),
                success : function(){
                    $("#usual1 ul").idTabs();
                    $('.tablelist tbody tr:odd').addClass('odd');
                },
                end : function(){//关闭事件

                }
            });
        }
      //全选
		function checkAll(){
			userIdList = [];
			if($('#checkAll').is(':checked')){//全选
				$('input[name=userIdList]').each(function(index){
					$(this).attr("checked","checked");
					userIdList.push($(this).val());
				});
			}else{
				$('input[name=userIdList]').each(function(index){
					$(this).removeAttr("checked");
				});
			}
		}
      	//教师用户批量删除
        function deleteBatch(){
      		console.log(userIdList.length);
      		console.log(userIdList);
			if(userIdList.length == 0)return false;
			top.$.jBox.confirm("删除后无法恢复，确认要删除指定教师用户吗？","系统提示",function(v,h,f){
				if(v=="ok"){
					window.location.href = "${ctx}/user/teacher/tpPTeacher/deleteBatch?flag=${flag}&userIds="+userIdList.join(',');
				}
			},{buttonsFocus:1});
		}

	function getAllCheckedItem() {
		var result = userIdList.join();
		userIdList = [];
		document.getElementById("checkAll").checked = false;
		$('input[name=userIdList]').each(function(index){
			$(this).removeAttr("checked");
		});
		return result;
	}
	</script>
</head>
<body>
	<div id="importBox" class="hide">
		<form id="importForm" action="${ctx}/user/teacher/tpPTeacher/import?flag=${flag}" method="post" enctype="multipart/form-data"
			class="form-search" style="padding-left:20px;text-align:center;" onsubmit="loading('正在导入，请稍等...');"><br/>
			<input id="uploadFile" name="file" type="file" style="width:330px"/><br/><br/>　　
			<input id="btnImportSubmit" class="btn btn-primary" type="submit" value="导入   "/>
			<a href="${ctx}/user/teacher/tpPTeacher/import/template?flag=${flag}">下载模板</a>
		</form>
	</div>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/user/teacher/tpPTeacher/list?flag=${flag}">老师列表</a></li>
		<shiro:hasPermission name="user:teacher:tpPTeacher:edit"><li><a href="${ctx}/user/teacher/tpPTeacher/form?flag=${flag}">老师添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="tpPTeacher" action="${ctx}/user/teacher/tpPTeacher/list?flag=${flag}" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<c:if test="${!empty flag}">
			<input type="hidden" name="isTutor" value="1">
		</c:if>
		<sys:tableSort id="orderBy" name="orderBy" value="${page.orderBy}" callback="page();"/>
		<ul class="ul-form">
			<li><label>姓名：</label>
				<form:input path="user.name" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li><label>职称：</label>
				<form:select path="titleId" class="input-medium">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('title')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</li>
			<c:if test="${empty flag}">
				<li><label>是否硕导：</label>
					<form:radiobuttons path="isMasterTutor" items="${fns:getDictList('yes_no')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</li>
				<li><label>是否博导：</label>
					<form:radiobuttons path="isDoctorTutor" items="${fns:getDictList('yes_no')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</li>
				<li><label>本科生导师：</label>
					<form:radiobuttons path="isUndergraduateTutor" items="${fns:getDictList('yes_no')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</li>
			</c:if>
			<li class="clearfix"></li>
			<li><label>所属教研室：</label>
				<sys:treeselect id="trc" name="trc.id" value="${tpPTeacher.trc.id}" labelName="trc.name" labelValue="${tpPTeacher.trc.name}"
					title="教研室" url="/sys/office/treeData?type=1" cssClass="input-small" allowClear="true" notAllowSelectParent="true"/>
			</li>
			<li><label>所属科室：</label>
				<sys:treeselect id="office" name="office.id" value="${tpPTeacher.office.id}" labelName="office.name" labelValue="${tpPTeacher.office.name}"
					title="部门" url="/sys/office/treeData?type=2" cssClass="input-small" allowClear="true" notAllowSelectParent="true"/>
			</li>
			<li class="btns">
				<input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/>
				<input id="btnExport" class="btn btn-primary" type="button" value="导出"/>
				<shiro:hasPermission name="user:teacher:tpPTeacher:edit">
					<input id="btnImport" class="btn btn-primary" type="button" value="导入"/>
				</shiro:hasPermission>
				<input id="" class="btn btn-primary" onclick="deleteBatch()" type="button" value="批量删除"/>
			</li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th><input id="checkAll" onclick="checkAll()" class="required" type="checkbox"> </th>
				<th class="sort-column u1.login_name">登录名</th>
				<th class="sort-column u1.name">姓名</th>
				<th>电话</th>
				<th>手机</th>
				<th>职称</th>
				<th>导师类型</th>
				<th>政治面貌</th>
				<th>是否硕导</th>
				<th>是否博导</th>
				<th>本科生导师</th>
				<th>所属教研室</th>
				<th>所属科室</th>
				<th>备注信息</th>
				<th>教学工作</th>
				<th>指导教师工作</th>
				<shiro:hasPermission name="user:teacher:tpPTeacher:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="tpPTeacher">
			<tr>
				<td><input name="userIdList" class="required" value="${tpPTeacher.user.id }" type="checkbox"></td>
				<td>${tpPTeacher.user.loginName}</td>
				<td><a href="${ctx}/user/teacher/tpPTeacher/form?userId=${tpPTeacher.user.id}">${tpPTeacher.user.name}</a></td>
				<td>${tpPTeacher.user.phone}</td>
				<td>${tpPTeacher.user.mobile}</td>
				<td>
					${fns:getDictLabel(tpPTeacher.titleId, 'title', '')}
				</td>
				<td>
					${fns:getDictLabel(tpPTeacher.tutorType, 'tutor_type', '')}
				</td>
				<td>
					${fns:getDictLabel(tpPTeacher.politicalOutlook, 'political_outlook', '')}
				</td>
				<td>
					${fns:getDictLabel(tpPTeacher.isMasterTutor, 'yes_no', '')}
				</td>
				<td>
					${fns:getDictLabel(tpPTeacher.isDoctorTutor, 'yes_no', '')}
				</td>
				<td>
					${fns:getDictLabel(tpPTeacher.isUndergraduateTutor, 'yes_no', '')}
				</td>
				<td>
					${tpPTeacher.trc.name}
				</td>
				<td>
					${tpPTeacher.office.name}
				</td>
				<td>
					${tpPTeacher.remarks}
				</td>
				<td>
					<a href="#" onclick="openTeachJobDd('${tpPTeacher.user.id}')" >教学工作</a>
				</td>
				<td>
					<a href="#" onclick="openStuListDd('${tpPTeacher.user.id}','type=2&teacher.id','带教学生')" >带教学生</a>
					<a href="#" onclick="openStuListDd('${tpPTeacher.user.id}','type=1&teacher.id','指导学生')" >指导学生</a>
				</td>
				<shiro:hasPermission name="user:teacher:tpPTeacher:edit"><td>
    				<a href="${ctx}/user/teacher/tpPTeacher/form?flag=${flag}&userId=${tpPTeacher.user.id}">修改</a>
					<a href="${ctx}/user/teacher/tpPTeacher/delete?flag=${flag}&userId=${tpPTeacher.user.id}" onclick="return confirmx('确认要删除该老师吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
<script type="text/javascript">
</script>
</html>