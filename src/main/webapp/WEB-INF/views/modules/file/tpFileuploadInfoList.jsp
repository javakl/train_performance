<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>文件管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }

        //预览图片
        function previewImg(id){
            layer.open({
                type: 1,
                title: '预览',
                area: ['80%', '80%'],
                fix: false, //不固定
                maxmin: true,
                //content: '${sr}/uploadfile/reviewImg/'+id
                content: '<img src="${ctx}/file/tpFileuploadInfo/reviewImg/'+id+'" height="100%" style="padding-left: 30px; padding-right: 30px;padding-top: 16px;padding-bottom: 16px;"></img>'
            });
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/file/tpFileuploadInfo/">文件列表</a></li>
		<c:if test="${empty param.opType or param.opType eq '1'}">
			<shiro:hasPermission name="file:tpFileuploadInfo:edit"><li><a href="${ctx}/file/tpFileuploadInfo/form?model=${model}&fileType=${fileType}">文件添加</a></li></shiro:hasPermission>
		</c:if>
	</ul>
	<form:form id="searchForm" modelAttribute="tpFileuploadInfo" action="${ctx}/file/tpFileuploadInfo/" method="post" class="breadcrumb form-search">
		<input id="model" name="model" type="hidden" value="${model}"/>
		<input id="model" name="fileType" type="hidden" value="${fileType}"/>
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>文件名：</label>
				<form:input path="fileName" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<%--<li><label>文件类型：</label>
				<form:select path="fileType" class="input-medium">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('file_type')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</li>--%>
			<%--<li><label>文件所属模块：</label>
				<form:select path="model" class="input-medium">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('model')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</li>--%>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>文件名</th>
				<th>文件大小</th>
				<th>文件后缀</th>
				<%--<th>文件类型</th>
				<th>文件所属模块</th>--%>
				<th>备注信息</th>
				<shiro:hasPermission name="file:tpFileuploadInfo:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="tpFileuploadInfo">
			<tr>
				<td><a href="${ctx}/file/tpFileuploadInfo/download?id=${tpFileuploadInfo.id}">
					${tpFileuploadInfo.fileName}
				</a></td>
				<td>
					${tpFileuploadInfo.size}
				</td>
				<td>
					${tpFileuploadInfo.suffix}
				</td>
				<%--<td>
					${fns:getDictLabel(tpFileuploadInfo.fileType, 'file_type', '')}
				</td>
				<td>
					${fns:getDictLabel(tpFileuploadInfo.model, 'model', '')}
				</td>--%>
				<td>
					<c:choose>
						<c:when test="${!empty tpFileuploadInfo.remarks && fn:length(tpFileuploadInfo.remarks) > 40 }">
							<span title="${tpFileuploadInfo.remarks }">${fn:substring(tpFileuploadInfo.remarks, 0, 40)  }……</span>
						</c:when>
						<c:when test="${!empty tpFileuploadInfo.remarks && fn:length(tpFileuploadInfo.remarks) < 40 }">
							${tpFileuploadInfo.remarks }
						</c:when>
						<c:otherwise></c:otherwise>
					</c:choose>
				</td>
				<shiro:hasPermission name="file:tpFileuploadInfo:edit">
					<td>
						<a href="${ctx}/file/tpFileuploadInfo/form?id=${tpFileuploadInfo.id}">修改</a>
						<a href="${ctx}/file/tpFileuploadInfo/delete?id=${tpFileuploadInfo.id}" onclick="return confirmx('确认要删除该文件吗？', this.href)">删除</a>
						<c:if test="${fn:contains('jpg,jpeg,gif,bmp,png', tpFileuploadInfo.suffix)}">
							<a href="javascript:void(0);" onclick="previewImg('${tpFileuploadInfo.id}')">预览</a>
						</c:if>
					</td>
				</shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>