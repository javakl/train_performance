<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>文件管理</title>
	<meta name="decorator" content="default"/>
	<!-- layer相关 -->
	<script type="text/javascript">
        var arr = [],
		    uploadedFiles = [];
		$(document).ready(function() {
			//$("#name").focus();
			$("#inputForm").validate({
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});

            layui.use('upload', function(){
                var $ = layui.jquery
                    ,upload = layui.upload;
                var files;
                var demoListView = $('#demoList')
                    ,uploadListIns = upload.render({
                    elem: '#testList'
                    ,url: '${ctx}/file/tpFileuploadInfo/preUpload'
                    ,accept: 'file'
                    ,multiple: true
                    ,auto: false
                    ,bindAction: '#testListAction'
                    ,choose: function(obj){
                        files = obj.pushFile(); //将每次选择的文件追加到文件队列
                        //读取本地文件
                        obj.preview(function(index, file, result){
                            var tr = $(['<tr id="upload-'+ index +'">'
                                ,'<td>'+ file.name +'</td>'
                                ,'<td>'+ (file.size/1014).toFixed(1) +'kb</td>'
                                ,'<td>等待上传</td>'
                                ,'<td>'
                                ,'<button class="layui-btn layui-btn-mini demo-reload layui-hide">重传</button>'
                                ,'<button class="layui-btn layui-btn-mini layui-btn-danger demo-delete">删除</button>'
                                ,'</td>'
                                ,'</tr>'].join(''));

                            //单个重传
                            tr.find('.demo-reload').on('click', function(){
                                obj.upload(index, file);
                            });

                            //删除
                            tr.find('.demo-delete').on('click', function(){
                                delete files[index]; //删除对应的文件
                                tr.remove();
                            });

                            if(!!$(".demo-reload").length){
                                $("#testListAction").attr('disabled',false);
                                $("#testListAction").css({ cursor: ''});
                            }

                            demoListView.append(tr);
                        });
                    },
                    before: function(obj){
                        //console.log(obj)//obj参数包含的信息，跟 choose回调完全一致，可参见上文。
                        if(!!$("tr[id^='upload-']").length){
                            $("#testListAction").attr('disabled',true);
                            $("#testListAction").css({ cursor: 'not-allowed'});
                            layer.load(); //上传loading
                        }
                    }
                    ,done: function(res, index, upload){
                        layer.closeAll('loading'); //关闭loading
                        if(res.code == 0){ //上传成功
                            flag = true;
                            arr.push(res.file);
                            uploadedFiles.push(res.file.fileName);
                            //console.log(arr);
                            var tr = demoListView.find('tr#upload-'+ index)
                                ,tds = tr.children();
                            tds.eq(2).html('<span style="color: #5FB878;">上传成功</span>');
                            tds.eq(3).html(''); //清空操作
                            //delete files[index]; //删除文件队列已经上传成功的文件
                            //tr.remove();
                            //arr = [];
                            $("#files").val(JSON.stringify(arr));
                            return;
                        }
                        this.error(index, upload);
                        //$("#testListAction").bind('click');
                    }
                    ,error: function(index, upload){
                        layer.closeAll('loading'); //关闭loading
                        var tr = demoListView.find('tr#upload-'+ index)
                            ,tds = tr.children();
                        tds.eq(2).html('<span style="color: #FF5722;">上传失败</span>');
                        tds.eq(3).find('.demo-reload').removeClass('layui-hide'); //显示重传
                    }
                });
            });

            $("#open_upload_file_dialog").click(function(){
                $(".submit").fadeIn(200);
                $("#testListAction").css({ cursor: 'default'});
                bg_model_show();
            });

            $("#submit").click(function(){
                if(uploadedFiles.length > 0){
                   $("#showUploadedFile>div").html(uploadedFiles.join());
                   $("#showUploadedFile").show();
				}else{
                    $("#showUploadedFile").hide();
                    $("#showUploadedFile>div").html("");
				}
                bg_model_hidden();
                uploadedFiles = [];
            });
		});

        function bg_model_show(){
            $(".bg-model").fadeTo(300,1)
            //隐藏窗体的滚动条
            $("body").css({ "overflow": "hidden" });
        }

        function bg_model_hidden(){
            $(".bg-model").hide();
            //显示窗体的滚动条
            $("body").css({ "overflow": "visible" });

            $("#demoList").html("");
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li><a href="${ctx}/file/tpFileuploadInfo/list?model=${model}">文件列表</a></li>
		<li class="active"><a href="${ctx}/file/tpFileuploadInfo/form?id=${tpFileuploadInfo.id}">文件<shiro:hasPermission name="file:tpFileuploadInfo:edit">${not empty tpFileuploadInfo.id?'修改':'添加'}</shiro:hasPermission><shiro:lacksPermission name="file:tpFileuploadInfo:edit">查看</shiro:lacksPermission></a></li>
	</ul><br/>
	<form:form id="inputForm" modelAttribute="tpFileuploadInfo" action="${ctx}/file/tpFileuploadInfo/save?model=${model}&fileType=${fileType}" method="post" class="form-horizontal">
		<form:hidden path="id"/>

		<sys:message content="${message}"/>
		<%--<div class="control-group">
			<label class="control-label">文件名：</label>
			<div class="controls">
				<form:input path="fileName" htmlEscape="false" maxlength="255" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">文件大小：</label>
			<div class="controls">
				<form:input path="size" htmlEscape="false" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">文件后缀：</label>
			<div class="controls">
				<form:input path="suffix" htmlEscape="false" maxlength="50" class="input-xlarge "/>
			</div>
		</div>--%>
		<c:if test="${empty tpFileuploadInfo.id}">
		<div class="control-group">
			<label class="control-label">文件上传：</label>
			<div class="controls">
				<input id="files" type="hidden" name="files">
				<button type="button" class="btn" id="open_upload_file_dialog">选择文件</button>
			</div>
		</div>
		<div id="showUploadedFile" class="control-group" style="display: none">
			<label class="control-label">上传成功文件：</label>
			<div class="controls" >

			</div>
		</div>
		</c:if>
		<div class="control-group">
			<label class="control-label">备注信息：</label>
			<div class="controls">
				<form:textarea path="remarks" htmlEscape="false" rows="4" maxlength="255" class="input-xxlarge "/>
			</div>
		</div>
		<div class="form-actions">
			<shiro:hasPermission name="file:tpFileuploadInfo:edit"><input id="btnSubmit" class="btn btn-primary" type="submit" value="保 存"/>&nbsp;</shiro:hasPermission>
			<input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
		</div>
	</form:form>

	<div class = "bg-model" style="top: 5%;left: 10%;background: white;width: 80%;height: 80%;position: fixed;z-index: 9999;border-color: black;opacity: 1;/* border-left-width: 3px; *//* border-style: solid; *//* border-width: 5px; */box-shadow: -3px -5px 12px 10px #aaa;border: 1px solid #BFBFBF;padding-left: 4px;padding-top: 4px;padding-bottom: 4px;padding-right: 4px;">
		<div class="submit tip">
			<%--<div class="tiptop" style="background: #38A8E8"><span>上传文件</span><a></a></div>--%>
			<form id="submitForm">
				<div class="tipcontent">
					<div class="left_info">
						<div class="layui-upload">
							<div class="upload_table">
								<table class="layui-table">
									<thead>
									<tr><th>文件名</th>
										<th>大小</th>
										<th>状态</th>
										<th>操作</th>
									</tr></thead>
									<tbody id="demoList"></tbody>
								</table>
							</div>
							<div class="button">
								<button type="button" class="layui-btn layui-btn-normal" id="testList">选择多文件</button>
								<button type="button" class="layui-btn" id="testListAction">开始上传</button>
								<button type="button" class="layui-btn" class="sure" id="submit" style="background-color: #4fa043;">&nbsp;&nbsp;&nbsp;关  闭&nbsp;&nbsp;&nbsp;</button>
							</div>
						</div>
					</div>
					<%--<div class="right_info">
						<label class="tips">上传说明：</label><textarea id="remark"  name="remark"  class="form-control input-sm scinput1"></textarea>
					</div>--%>
				</div>
			</form>
		</div>
	</div>
</body>
</html>