<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<%@ attribute name="id" type="java.lang.String" required="true" description="编号"%>
<%@ attribute name="name" type="java.lang.String" required="true" description="输入框名称"%>
<%@ attribute name="value" type="java.lang.String" required="true" description="输入框值"%>
<i id="${id}Icon" class="icon-${not empty value?value:' hide'}"></i>&nbsp;<label id="${id}IconLabel">${not empty value?value:'无'}</label>&nbsp;
<input id="${id}" name="${name}" type="hidden" value="${value}"/><a id="${id}Button" href="javascript:" class="btn">选择</a>&nbsp;&nbsp;
<script type="text/javascript">
	$(function(){
		if($("#${id}Icon").attr('class').substr(5).indexOf('iconfont') == 0){//图标字体不需要增加icon-
			$("#${id}Icon").attr('class',$("#${id}Icon").attr('class').substr(5));
		}
	});
	$("#${id}Button").click(function(){
		top.$.jBox.open("iframe:${ctx}/tag/iconselect?value="+$("#${id}").val(), "选择图标", 700, $(top.document).height()-180, {
            buttons:{"确定":"ok", "清除":"clear", "关闭":true}, submit:function(v, h, f){
                if (v=="ok"){
                	var icon = h.find("iframe")[0].contentWindow.$("#icon").val();
                	console.log('icon is '+icon);
                	console.log($.trim(icon));
                	if($.trim(icon).indexOf('iconfont') == 0){//图标字体不需要截取
	                	icon = $.trim(icon);
	                	$("#${id}Icon").attr("class", icon);
                	}else{
	                	icon = $.trim(icon).substr(5);
	                	$("#${id}Icon").attr("class", "icon-"+icon);
                	}
	                $("#${id}IconLabel").text(icon);
	                $("#${id}").val(icon);
                }else if (v=="clear"){
	                $("#${id}Icon").attr("class", "icon- hide");
	                $("#${id}IconLabel").text("无");
	                $("#${id}").val("");
                }
            }, loaded:function(h){
                $(".jbox-content", top.document).css("overflow-y","hidden");
            }
        });
	});
</script>
