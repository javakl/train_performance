/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.act.dao;

import org.apache.ibatis.annotations.Param;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.act.entity.Act;

/**
 * 审批DAO接口
 * @author thinkgem
 * @version 2014-05-16
 */
@MyBatisDao
public interface ActDao extends CrudDao<Act> {

	public int updateProcInsIdByBusinessId(Act act);
	
	
	/**
	 * 根据流程名称 获得最新版本在允许的流程定义id
	 * @return
	 */
	public String getprocDefIdByProcDefKey(@Param("procDefKey")String procDefKey);
	
	/**
	 * 向业务表id对应的历史流程任务表 插入数据
	 * @param procInsId
	 * @param fId
	 * @return
	 */
	public int insertProcHis(@Param("procInsId")String procInsId,@Param("fId")String fId);
}
