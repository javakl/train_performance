/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.student.dao.exam;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.student.entity.exam.TpStudentExam;

/**
 * 考试记录DAO接口
 * @author 康力
 * @version 2018-03-11
 */
@MyBatisDao
public interface TpStudentExamDao extends CrudDao<TpStudentExam> {
	
}