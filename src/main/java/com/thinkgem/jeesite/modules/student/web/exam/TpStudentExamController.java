/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.student.web.exam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.common.web.ExcelController;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.student.entity.exam.TpStudentExam;
import com.thinkgem.jeesite.modules.student.service.exam.TpStudentExamService;
import com.thinkgem.jeesite.modules.sys.service.SystemService;
import com.thinkgem.jeesite.modules.sys.utils.UserUtils;

/**
 * 考试记录Controller
 * @author 康力
 * @version 2018-03-11
 */
@Controller
@RequestMapping(value = "${adminPath}/student/exam/tpStudentExam")
public class TpStudentExamController extends ExcelController<TpStudentExam> {

	@Autowired
	private TpStudentExamService tpStudentExamService;
	@Autowired
	private SystemService systemService;
	
	@ModelAttribute
	public TpStudentExam get(@RequestParam(required=false) String id) {
		TpStudentExam entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = tpStudentExamService.get(id);
		}
		if (entity == null){
			entity = new TpStudentExam();
		}
		return entity;
	}
	
	@RequiresPermissions("student:exam:tpStudentExam:view")
	@RequestMapping(value = {"list", ""})
	public String list(TpStudentExam tpStudentExam, HttpServletRequest request, HttpServletResponse response, Model model) {
		//根据拥有当前菜单角色的最大的数据范围 判断是否是获取本人数据
		tpStudentExam.setUserDataScope(systemService.getCurUserDataScopeByPermission("student:exam:tpStudentExam:view"));
		if(tpStudentExam.getUserDataScope() == 0){
			tpStudentExam.setUser(UserUtils.getUser());
		}
		Page<TpStudentExam> page = tpStudentExamService.findPage(new Page<TpStudentExam>(request, response), tpStudentExam); 
		model.addAttribute("page", page);
		return "modules/student/exam/tpStudentExamList";
	}

	@RequiresPermissions("student:exam:tpStudentExam:view")
	@RequestMapping(value = "form")
	public String form(TpStudentExam tpStudentExam, Model model) {
		model.addAttribute("tpStudentExam", tpStudentExam);
		return "modules/student/exam/tpStudentExamForm";
	}

	@RequiresPermissions("student:exam:tpStudentExam:edit")
	@RequestMapping(value = "save")
	public String save(TpStudentExam tpStudentExam, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, tpStudentExam)){
			return form(tpStudentExam, model);
		}
		tpStudentExamService.save(tpStudentExam);
		addMessage(redirectAttributes, "保存考试记录成功");
		return "redirect:"+Global.getAdminPath()+"/student/exam/tpStudentExam/?repage";
	}
	
	@RequiresPermissions("student:exam:tpStudentExam:edit")
	@RequestMapping(value = "delete")
	public String delete(TpStudentExam tpStudentExam, RedirectAttributes redirectAttributes) {
		tpStudentExamService.delete(tpStudentExam);
		addMessage(redirectAttributes, "删除考试记录成功");
		return "redirect:"+Global.getAdminPath()+"/student/exam/tpStudentExam/?repage";
	}

	@Override
	public String getTitle() {
		return "考试记录";
	}

	@Override
	public String defaultStr() {
		return "/student/exam/tpStudentExam/?repage";
	}

	@Override
	public CrudService baseService() {
		return tpStudentExamService;
	}

	@Override
	@RequestMapping(value = "import/template")
	public String importFileTemplate(HttpServletResponse response, RedirectAttributes redirectAttributes, TpStudentExam tpStudentExam) {
		return super.importFileTemplate(response, redirectAttributes, tpStudentExam);
	}

	@Override
	@RequestMapping(value = "import")
	public String importFile(MultipartFile file, RedirectAttributes redirectAttributes, TpStudentExam data) {
		return super.importFile(file, redirectAttributes, data,false);
	}

	@Override
	@RequestMapping(value = "export")
	public String exportFile(TpStudentExam tpStudentExam, HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) {
		return super.exportFile(tpStudentExam, request, response, redirectAttributes);
	}
}