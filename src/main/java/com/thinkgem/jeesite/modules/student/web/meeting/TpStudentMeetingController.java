/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.student.web.meeting;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.common.web.ExcelController;
import com.thinkgem.jeesite.modules.student.entity.graduate.TpStudentGraduate;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.student.entity.meeting.TpStudentMeeting;
import com.thinkgem.jeesite.modules.student.service.meeting.TpStudentMeetingService;
import com.thinkgem.jeesite.modules.sys.service.SystemService;
import com.thinkgem.jeesite.modules.sys.utils.UserUtils;

/**
 * 学术会议Controller
 * @author 康力
 * @version 2018-03-08
 */
@Controller
@RequestMapping(value = "${adminPath}/student/meeting/tpStudentMeeting")
public class TpStudentMeetingController extends ExcelController<TpStudentMeeting> {

	@Autowired
	private TpStudentMeetingService tpStudentMeetingService;
	@Autowired
	private SystemService systemService;
	@ModelAttribute
	public TpStudentMeeting get(@RequestParam(required=false) String id) {
		TpStudentMeeting entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = tpStudentMeetingService.get(id);
		}
		if (entity == null){
			entity = new TpStudentMeeting();
		}
		return entity;
	}
	
	@RequiresPermissions("student:meeting:tpStudentMeeting:view")
	@RequestMapping(value = {"list", ""})
	public String list(TpStudentMeeting tpStudentMeeting, HttpServletRequest request, HttpServletResponse response, Model model) {
		//根据拥有当前菜单角色的最大的数据范围 判断是否是获取本人数据
		tpStudentMeeting.setUserDataScope(systemService.getCurUserDataScopeByPermission("student:meeting:tpStudentMeeting:view"));
		if (tpStudentMeeting.getUserDataScope() == 0) {
			tpStudentMeeting.setUser(UserUtils.getUser());
		}		
		Page<TpStudentMeeting> page = tpStudentMeetingService.findPage(new Page<TpStudentMeeting>(request, response), tpStudentMeeting); 
		model.addAttribute("page", page);
		return "modules/student/meeting/tpStudentMeetingList";
	}

	@RequiresPermissions("student:meeting:tpStudentMeeting:view")
	@RequestMapping(value = "form")
	public String form(TpStudentMeeting tpStudentMeeting, Model model) {
		model.addAttribute("tpStudentMeeting", tpStudentMeeting);
		return "modules/student/meeting/tpStudentMeetingForm";
	}

	@RequiresPermissions("student:meeting:tpStudentMeeting:edit")
	@RequestMapping(value = "save")
	public String save(TpStudentMeeting tpStudentMeeting, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, tpStudentMeeting)){
			return form(tpStudentMeeting, model);
		}
		tpStudentMeetingService.save(tpStudentMeeting);
		addMessage(redirectAttributes, "保存学术会议成功");
		return "redirect:"+Global.getAdminPath()+"/student/meeting/tpStudentMeeting/?repage";
	}
	
	@RequiresPermissions("student:meeting:tpStudentMeeting:edit")
	@RequestMapping(value = "delete")
	public String delete(TpStudentMeeting tpStudentMeeting, RedirectAttributes redirectAttributes) {
		tpStudentMeetingService.delete(tpStudentMeeting);
		addMessage(redirectAttributes, "删除学术会议成功");
		return "redirect:"+Global.getAdminPath()+"/student/meeting/tpStudentMeeting/?repage";
	}

	@Override
	public String getTitle() {
		return "学术会议";
	}

	@Override
	public String defaultStr() {
		return "/student/meeting/tpStudentMeeting/?repage";
	}

	@Override
	public CrudService baseService() {
		return tpStudentMeetingService;
	}

	@Override
	@RequestMapping(value = "import/template")
	public String importFileTemplate(HttpServletResponse response, RedirectAttributes redirectAttributes, TpStudentMeeting tpStudentExam) {
		return super.importFileTemplate(response, redirectAttributes, tpStudentExam);
	}

	@Override
	@RequestMapping(value = "import")
	public String importFile(MultipartFile file, RedirectAttributes redirectAttributes, TpStudentMeeting data) {
		return super.importFile(file, redirectAttributes, data,false);
	}

	@Override
	@RequestMapping(value = "export")
	public String exportFile(TpStudentMeeting tpStudentExam, HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) {
		return super.exportFile(tpStudentExam, request, response, redirectAttributes);
	}
}