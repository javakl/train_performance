/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.student.web.punish;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.common.web.ExcelController;
import com.thinkgem.jeesite.modules.student.entity.meeting.TpStudentMeeting;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.student.entity.punish.TpStudentPunish;
import com.thinkgem.jeesite.modules.student.service.punish.TpStudentPunishService;
import com.thinkgem.jeesite.modules.sys.service.SystemService;
import com.thinkgem.jeesite.modules.sys.utils.UserUtils;

/**
 * 处分情况Controller
 * @author 康力
 * @version 2018-03-08
 */
@Controller
@RequestMapping(value = "${adminPath}/student/punish/tpStudentPunish")
public class TpStudentPunishController extends ExcelController<TpStudentPunish> {

	@Autowired
	private TpStudentPunishService tpStudentPunishService;
	@Autowired
	private SystemService systemService;
	@ModelAttribute
	public TpStudentPunish get(@RequestParam(required=false) String id) {
		TpStudentPunish entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = tpStudentPunishService.get(id);
		}
		if (entity == null){
			entity = new TpStudentPunish();
		}
		return entity;
	}
	
	@RequiresPermissions("student:punish:tpStudentPunish:view")
	@RequestMapping(value = {"list", ""})
	public String list(TpStudentPunish tpStudentPunish, HttpServletRequest request, HttpServletResponse response, Model model) {
		// 根据拥有当前菜单角色的最大的数据范围 判断是否是获取本人数据
		tpStudentPunish.setUserDataScope(systemService.getCurUserDataScopeByPermission("student:punish:tpStudentPunish:view"));
		if (tpStudentPunish.getUserDataScope() == 0) {
			tpStudentPunish.setUser(UserUtils.getUser());
		}
		Page<TpStudentPunish> page = tpStudentPunishService.findPage(new Page<TpStudentPunish>(request, response), tpStudentPunish); 
		model.addAttribute("page", page);
		return "modules/student/punish/tpStudentPunishList";
	}

	@RequiresPermissions("student:punish:tpStudentPunish:view")
	@RequestMapping(value = "form")
	public String form(TpStudentPunish tpStudentPunish, Model model) {
		model.addAttribute("tpStudentPunish", tpStudentPunish);
		return "modules/student/punish/tpStudentPunishForm";
	}

	@RequiresPermissions("student:punish:tpStudentPunish:edit")
	@RequestMapping(value = "save")
	public String save(TpStudentPunish tpStudentPunish, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, tpStudentPunish)){
			return form(tpStudentPunish, model);
		}
		tpStudentPunishService.save(tpStudentPunish);
		addMessage(redirectAttributes, "保存处分成功");
		return "redirect:"+Global.getAdminPath()+"/student/punish/tpStudentPunish/?repage";
	}
	
	@RequiresPermissions("student:punish:tpStudentPunish:edit")
	@RequestMapping(value = "delete")
	public String delete(TpStudentPunish tpStudentPunish, RedirectAttributes redirectAttributes) {
		tpStudentPunishService.delete(tpStudentPunish);
		addMessage(redirectAttributes, "删除处分成功");
		return "redirect:"+Global.getAdminPath()+"/student/punish/tpStudentPunish/?repage";
	}

	@Override
	public String getTitle() {
		return "学生处分";
	}

	@Override
	public String defaultStr() {
		return "/student/punish/tpStudentPunish/?repage";
	}

	@Override
	public CrudService baseService() {
		return tpStudentPunishService;
	}

	@Override
	@RequestMapping(value = "import/template")
	public String importFileTemplate(HttpServletResponse response, RedirectAttributes redirectAttributes, TpStudentPunish tpStudentExam) {
		return super.importFileTemplate(response, redirectAttributes, tpStudentExam);
	}

	@Override
	@RequestMapping(value = "import")
	public String importFile(MultipartFile file, RedirectAttributes redirectAttributes, TpStudentPunish data) {
		return super.importFile(file, redirectAttributes, data,false);
	}

	@Override
	@RequestMapping(value = "export")
	public String exportFile(TpStudentPunish tpStudentExam, HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) {
		return super.exportFile(tpStudentExam, request, response, redirectAttributes);
	}
}