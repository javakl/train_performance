/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.student.web.graduate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.common.web.ExcelController;
import com.thinkgem.jeesite.modules.student.entity.exam.TpStudentExam;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.student.entity.graduate.TpStudentGraduate;
import com.thinkgem.jeesite.modules.student.service.graduate.TpStudentGraduateService;
import com.thinkgem.jeesite.modules.sys.service.SystemService;
import com.thinkgem.jeesite.modules.sys.utils.UserUtils;

/**
 * 毕业模块Controller
 * @author 康力
 * @version 2018-03-08
 */
@Controller
@RequestMapping(value = "${adminPath}/student/graduate/tpStudentGraduate")
public class TpStudentGraduateController extends ExcelController<TpStudentGraduate> {

	@Autowired
	private TpStudentGraduateService tpStudentGraduateService;
	@Autowired
	private SystemService systemService;
	@ModelAttribute
	public TpStudentGraduate get(@RequestParam(required=false) String id) {
		TpStudentGraduate entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = tpStudentGraduateService.get(id);
		}
		if (entity == null){
			entity = new TpStudentGraduate();
		}
		return entity;
	}
	
	@RequiresPermissions("student:graduate:tpStudentGraduate:view")
	@RequestMapping(value = {"list", ""})
	public String list(TpStudentGraduate tpStudentGraduate, HttpServletRequest request, HttpServletResponse response, Model model) {
		//根据拥有当前菜单角色的最大的数据范围 判断是否是获取本人数据
		tpStudentGraduate.setUserDataScope(systemService.getCurUserDataScopeByPermission("student:graduate:tpStudentGraduate:view"));
		if (tpStudentGraduate.getUserDataScope() == 0) {
			tpStudentGraduate.setUser(UserUtils.getUser());
		}
		Page<TpStudentGraduate> page = tpStudentGraduateService.findPage(new Page<TpStudentGraduate>(request, response), tpStudentGraduate); 
		model.addAttribute("page", page);
		return "modules/student/graduate/tpStudentGraduateList";
	}

	@RequiresPermissions("student:graduate:tpStudentGraduate:view")
	@RequestMapping(value = "form")
	public String form(TpStudentGraduate tpStudentGraduate, Model model) {
		model.addAttribute("tpStudentGraduate", tpStudentGraduate);
		return "modules/student/graduate/tpStudentGraduateForm";
	}

	@RequiresPermissions("student:graduate:tpStudentGraduate:edit")
	@RequestMapping(value = "save")
	public String save(TpStudentGraduate tpStudentGraduate, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, tpStudentGraduate)){
			return form(tpStudentGraduate, model);
		}
		tpStudentGraduateService.save(tpStudentGraduate);
		addMessage(redirectAttributes, "保存毕业模块成功");
		return "redirect:"+Global.getAdminPath()+"/student/graduate/tpStudentGraduate/?repage";
	}
	
	@RequiresPermissions("student:graduate:tpStudentGraduate:edit")
	@RequestMapping(value = "delete")
	public String delete(TpStudentGraduate tpStudentGraduate, RedirectAttributes redirectAttributes) {
		tpStudentGraduateService.delete(tpStudentGraduate);
		addMessage(redirectAttributes, "删除毕业模块成功");
		return "redirect:"+Global.getAdminPath()+"/student/graduate/tpStudentGraduate/?repage";
	}

	@Override
	public String getTitle() {
		return "毕业信息";
	}

	@Override
	public String defaultStr() {
		return "/student/graduate/tpStudentGraduate/?repage";
	}

	@Override
	public CrudService baseService() {
		return tpStudentGraduateService;
	}

	@Override
	@RequestMapping(value = "import/template")
	public String importFileTemplate(HttpServletResponse response, RedirectAttributes redirectAttributes, TpStudentGraduate tpStudentExam) {
		return super.importFileTemplate(response, redirectAttributes, tpStudentExam);
	}

	@Override
	@RequestMapping(value = "import")
	public String importFile(MultipartFile file, RedirectAttributes redirectAttributes, TpStudentGraduate data) {
		return super.importFile(file, redirectAttributes, data,false);
	}

	@Override
	@RequestMapping(value = "export")
	public String exportFile(TpStudentGraduate tpStudentExam, HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) {
		return super.exportFile(tpStudentExam, request, response, redirectAttributes);
	}
}