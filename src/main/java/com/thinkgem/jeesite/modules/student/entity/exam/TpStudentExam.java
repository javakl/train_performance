/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.student.entity.exam;

import com.thinkgem.jeesite.common.persistence.ActEntity;
import com.thinkgem.jeesite.common.utils.excel.annotation.ExcelField;
import com.thinkgem.jeesite.modules.sys.entity.User;
import com.thinkgem.jeesite.modules.user.entity.student.TpPStudent;
import com.thinkgem.jeesite.modules.sys.entity.Office;
import org.hibernate.validator.constraints.Length;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 考试记录Entity
 * @author 康力
 * @version 2018-03-11
 */
public class TpStudentExam extends ActEntity<TpStudentExam> {
	
	private static final long serialVersionUID = 1L;
	private User user;		// 参考人
	private TpPStudent stuInfo;//学生信息
	private Office department;		// 所属科室
	private String fullName;		// 参考人姓名
	private String examName;		// 考试名称
	private String examResult;		// 考试结果
	private String noPassNum;		// 未通过门数
	private Date examDate;		// 考试时间
	private Date beginExamDate;		// 开始 考试时间
	private Date endExamDate;		// 结束 考试时间
	//未通过科目
	private String notPassCourse;
	public TpStudentExam() {
		super();
	}

	public TpStudentExam(String id){
		super(id);
	}
	@ExcelField(value="user.loginName",title="参考人登录号", align=2, sort=5)
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@ExcelField(value="department.name",title="所属科室", align=2, sort=10)
	public Office getDepartment() {
		return department;
	}

	public void setDepartment(Office department) {
		this.department = department;
	}

	@ExcelField(value="fullName",title="参考人姓名", align=2, sort=15)
	@Length(min=0, max=50, message="参考人姓名长度必须介于 0 和 50 之间")
	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	
	@ExcelField(value="examName",title="考试名称", align=2, sort=20)
	@Length(min=0, max=20, message="考试名称长度必须介于 0 和 20 之间")
	public String getExamName() {
		return examName;
	}

	public void setExamName(String examName) {
		this.examName = examName;
	}

	@ExcelField(value="examResult",title="考试结果", align=2, sort=25)
	@Length(min=0, max=255, message="考试结果长度必须介于 0 和 255 之间")
	public String getExamResult() {
		return examResult;
	}

	public void setExamResult(String examResult) {
		this.examResult = examResult;
	}

	@ExcelField(value="noPassNum",title="未通过门数", align=2, sort=30)
	@Length(min=0, max=11, message="未通过门数长度必须介于 0 和 11 之间")
	public String getNoPassNum() {
		return noPassNum;
	}

	public void setNoPassNum(String noPassNum) {
		this.noPassNum = noPassNum;
	}

	@ExcelField(value="examDate",title="考试时间", align=2, sort=35)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getExamDate() {
		return examDate;
	}

	public void setExamDate(Date examDate) {
		this.examDate = examDate;
	}
	public Date getBeginExamDate() {
		return beginExamDate;
	}

	public void setBeginExamDate(Date beginExamDate) {
		this.beginExamDate = beginExamDate;
	}
	public Date getEndExamDate() {
		return endExamDate;
	}

	public void setEndExamDate(Date endExamDate) {
		this.endExamDate = endExamDate;
	}

	public String getNotPassCourse() {
		return notPassCourse;
	}

	public void setNotPassCourse(String notPassCourse) {
		this.notPassCourse = notPassCourse;
	}
	@ExcelField(value="stuInfo.tutor.name",title="导师", align=2, sort=16,type=1)
	public TpPStudent getStuInfo() {
		return stuInfo;
	}

	public void setStuInfo(TpPStudent stuInfo) {
		this.stuInfo = stuInfo;
	}
		
}