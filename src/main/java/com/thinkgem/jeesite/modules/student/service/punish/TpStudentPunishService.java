/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.student.service.punish;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.student.entity.punish.TpStudentPunish;
import com.thinkgem.jeesite.modules.student.dao.punish.TpStudentPunishDao;

/**
 * 处分情况Service
 * @author 康力
 * @version 2018-03-08
 */
@Service
@Transactional(readOnly = true)
public class TpStudentPunishService extends CrudService<TpStudentPunishDao, TpStudentPunish> {

	public TpStudentPunish get(String id) {
		return super.get(id);
	}
	
	public List<TpStudentPunish> findList(TpStudentPunish tpStudentPunish) {
		return super.findList(tpStudentPunish);
	}
	
	public Page<TpStudentPunish> findPage(Page<TpStudentPunish> page, TpStudentPunish tpStudentPunish) {
		return super.findPage(page, tpStudentPunish);
	}
	
	@Transactional(readOnly = false)
	public void save(TpStudentPunish tpStudentPunish) {
		super.save(tpStudentPunish);
	}
	
	@Transactional(readOnly = false)
	public void delete(TpStudentPunish tpStudentPunish) {
		super.delete(tpStudentPunish);
	}
	
}