/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.student.service.meeting;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.student.entity.meeting.TpStudentMeeting;
import com.thinkgem.jeesite.modules.student.dao.meeting.TpStudentMeetingDao;

/**
 * 学术会议Service
 * @author 康力
 * @version 2018-03-08
 */
@Service
@Transactional(readOnly = true)
public class TpStudentMeetingService extends CrudService<TpStudentMeetingDao, TpStudentMeeting> {

	public TpStudentMeeting get(String id) {
		return super.get(id);
	}
	
	public List<TpStudentMeeting> findList(TpStudentMeeting tpStudentMeeting) {
		return super.findList(tpStudentMeeting);
	}
	
	public Page<TpStudentMeeting> findPage(Page<TpStudentMeeting> page, TpStudentMeeting tpStudentMeeting) {
		return super.findPage(page, tpStudentMeeting);
	}
	
	@Transactional(readOnly = false)
	public void save(TpStudentMeeting tpStudentMeeting) {
		super.save(tpStudentMeeting);
	}
	
	@Transactional(readOnly = false)
	public void delete(TpStudentMeeting tpStudentMeeting) {
		super.delete(tpStudentMeeting);
	}
	
}