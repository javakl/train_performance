/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.student.service.graduate;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.student.entity.graduate.TpStudentGraduate;
import com.thinkgem.jeesite.modules.student.dao.graduate.TpStudentGraduateDao;

/**
 * 毕业模块Service
 * @author 康力
 * @version 2018-03-08
 */
@Service
@Transactional(readOnly = true)
public class TpStudentGraduateService extends CrudService<TpStudentGraduateDao, TpStudentGraduate> {

	public TpStudentGraduate get(String id) {
		return super.get(id);
	}
	
	public List<TpStudentGraduate> findList(TpStudentGraduate tpStudentGraduate) {
		return super.findList(tpStudentGraduate);
	}
	
	public Page<TpStudentGraduate> findPage(Page<TpStudentGraduate> page, TpStudentGraduate tpStudentGraduate) {
		return super.findPage(page, tpStudentGraduate);
	}
	
	@Transactional(readOnly = false)
	public void save(TpStudentGraduate tpStudentGraduate) {
		super.save(tpStudentGraduate);
	}
	
	@Transactional(readOnly = false)
	public void delete(TpStudentGraduate tpStudentGraduate) {
		super.delete(tpStudentGraduate);
	}
	
}