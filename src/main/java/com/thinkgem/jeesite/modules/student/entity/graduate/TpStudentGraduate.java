/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.student.entity.graduate;

import com.thinkgem.jeesite.common.persistence.ActEntity;
import com.thinkgem.jeesite.common.utils.excel.annotation.ExcelField;
import com.thinkgem.jeesite.modules.sys.entity.User;
import com.thinkgem.jeesite.modules.user.entity.student.TpPStudent;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 毕业模块Entity
 * @author 康力
 * @version 2018-03-08
 */
public class TpStudentGraduate extends ActEntity<TpStudentGraduate> {
	
	private static final long serialVersionUID = 1L;
	private User user;		// 提交人
	private TpPStudent stuInfo;//学生信息
	private String fullName;		// 提交人姓名
	private String reviewOpinions;		// 评阅意见情况
	private String questionNum;		// 出问题份数
	private String result;//结果
	public TpStudentGraduate() {
		super();
	}

	public TpStudentGraduate(String id){
		super(id);
	}

	@ExcelField(value="user.name",title="毕业人", align=2, sort=5)
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@ExcelField(value="fullName",title="提交人姓名", align=2, sort=10)
	@Length(min=0, max=50, message="提交人姓名长度必须介于 0 和 50 之间")
	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	@ExcelField(value="reviewOpinions",title="评阅意见", align=2, sort=15)
	@Length(min=0, max=255, message="评阅意见情况长度必须介于 0 和 255 之间")
	public String getReviewOpinions() {
		return reviewOpinions;
	}

	public void setReviewOpinions(String reviewOpinions) {
		this.reviewOpinions = reviewOpinions;
	}

	@ExcelField(value="questionNum",title="出问题份数", align=2, sort=20)
	@Length(min=0, max=11, message="出问题份数长度必须介于 0 和 11 之间")
	public String getQuestionNum() {
		return questionNum;
	}

	public void setQuestionNum(String questionNum) {
		this.questionNum = questionNum;
	}
	@ExcelField(value="result",title="结果", align=2, sort=25)
	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}
	@ExcelField(value="stuInfo.tutor.name",title="导师", align=2, sort=16,type=1)
	public TpPStudent getStuInfo() {
		return stuInfo;
	}

	public void setStuInfo(TpPStudent stuInfo) {
		this.stuInfo = stuInfo;
	}
	
}