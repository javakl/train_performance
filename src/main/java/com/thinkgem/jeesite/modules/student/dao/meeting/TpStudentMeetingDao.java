/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.student.dao.meeting;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.student.entity.meeting.TpStudentMeeting;

/**
 * 学术会议DAO接口
 * @author 康力
 * @version 2018-03-08
 */
@MyBatisDao
public interface TpStudentMeetingDao extends CrudDao<TpStudentMeeting> {
	
}