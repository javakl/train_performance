/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.student.entity.meeting;

import com.thinkgem.jeesite.common.persistence.ActEntity;
import com.thinkgem.jeesite.common.utils.excel.annotation.ExcelField;
import com.thinkgem.jeesite.modules.sys.entity.User;
import com.thinkgem.jeesite.modules.user.entity.student.TpPStudent;

import org.hibernate.validator.constraints.Length;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 学术会议Entity
 * @author 康力
 * @version 2018-03-08
 */
public class TpStudentMeeting extends ActEntity<TpStudentMeeting> {
	
	private static final long serialVersionUID = 1L;
	private User user;		// 申请人
	private TpPStudent stuInfo;//学生信息
	private String fullName;		// 参会人姓名
	private Date meetingStart;		// 开始时间
	private Date meetingEnd;		// 结束时间
	private String name;		// 会议名称
	private String level;		// 学术会议级别
	private String communicateType;		// 交流类型
	
	public TpStudentMeeting() {
		super();
	}

	public TpStudentMeeting(String id){
		super(id);
	}

	@ExcelField(value="user.name",title="申请人", align=2, sort=5)
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@ExcelField(value="fullName",title="参会人姓名", align=2, sort=10)
	@Length(min=0, max=50, message="参会人姓名长度必须介于 0 和 50 之间")
	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	@ExcelField(value="meetingStart",title="会议开始时间", align=2, sort=15)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getMeetingStart() {
		return meetingStart;
	}

	public void setMeetingStart(Date meetingStart) {
		this.meetingStart = meetingStart;
	}
	@ExcelField(value="meetingEnd",title="会议结束时间", align=2, sort=20)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getMeetingEnd() {
		return meetingEnd;
	}

	public void setMeetingEnd(Date meetingEnd) {
		this.meetingEnd = meetingEnd;
	}
	@ExcelField(value="meetingEnd",title="会议名称", align=2, sort=25)
	@Length(min=0, max=255, message="会议名称长度必须介于 0 和 255 之间")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	@ExcelField(value="meetingEnd",title="会议级别", align=2, sort=30,dictType = "meeting_level")
	@Length(min=0, max=11, message="学术会议级别长度必须介于 0 和 11 之间")
	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}
	@ExcelField(value="communicateType",title="交流类型", align=2, sort=35,dictType = "communicate_type")
	@Length(min=0, max=11, message="交流类型长度必须介于 0 和 11 之间")
	public String getCommunicateType() {
		return communicateType;
	}

	public void setCommunicateType(String communicateType) {
		this.communicateType = communicateType;
	}
	@ExcelField(value="stuInfo.tutor.name",title="导师", align=2, sort=16,type=1)
	public TpPStudent getStuInfo() {
		return stuInfo;
	}

	public void setStuInfo(TpPStudent stuInfo) {
		this.stuInfo = stuInfo;
	}
	
}