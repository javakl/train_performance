/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.student.service.exam;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.student.entity.exam.TpStudentExam;
import com.thinkgem.jeesite.modules.student.dao.exam.TpStudentExamDao;

/**
 * 考试记录Service
 * @author 康力
 * @version 2018-03-11
 */
@Service
@Transactional(readOnly = true)
public class TpStudentExamService extends CrudService<TpStudentExamDao, TpStudentExam> {

	public TpStudentExam get(String id) {
		return super.get(id);
	}
	
	public List<TpStudentExam> findList(TpStudentExam tpStudentExam) {
		return super.findList(tpStudentExam);
	}
	
	public Page<TpStudentExam> findPage(Page<TpStudentExam> page, TpStudentExam tpStudentExam) {
		return super.findPage(page, tpStudentExam);
	}
	
	@Transactional(readOnly = false)
	public void save(TpStudentExam tpStudentExam) {
		super.save(tpStudentExam);
	}
	
	@Transactional(readOnly = false)
	public void delete(TpStudentExam tpStudentExam) {
		super.delete(tpStudentExam);
	}
	
}