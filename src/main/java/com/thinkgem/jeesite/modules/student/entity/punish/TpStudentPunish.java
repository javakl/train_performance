/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.student.entity.punish;

import com.thinkgem.jeesite.common.persistence.ActEntity;
import com.thinkgem.jeesite.common.utils.excel.annotation.ExcelField;
import com.thinkgem.jeesite.modules.sys.entity.User;
import com.thinkgem.jeesite.modules.user.entity.student.TpPStudent;

import org.hibernate.validator.constraints.Length;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 处分情况Entity
 * @author 康力
 * @version 2018-03-08
 */
public class TpStudentPunish extends ActEntity<TpStudentPunish> {
	
	private static final long serialVersionUID = 1L;
	private User user;		// 受处分人
	private TpPStudent stuInfo;//学生信息
	private String fullName;		// 处分人姓名
	private String name;		// 处分标题
	private String level;		// 处分等级
	private Date punishTime;		// 下达时间
	private String reason;		// 处分原因
	private Date beginPunishTime;		// 开始 下达时间
	private Date endPunishTime;		// 结束 下达时间
	
	public TpStudentPunish() {
		super();
	}

	public TpStudentPunish(String id){
		super(id);
	}
	@ExcelField(value="user.name",title="受处分人", align=2, sort=5)
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	@Length(min=0, max=255, message="处分人姓名长度必须介于 0 和 255 之间")
	@ExcelField(value="fullName",title="下达人", align=2, sort=10)
	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	@ExcelField(value="name",title="标题", align=2, sort=15)
	@Length(min=0, max=255, message="处分标题长度必须介于 0 和 255 之间")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@ExcelField(value="level",title="等级", align=2, sort=20,dictType = "punish_level")
	@Length(min=0, max=11, message="处分等级长度必须介于 0 和 11 之间")
	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	@ExcelField(value="punishTime",title="下达时间", align=2, sort=25)
	@JsonFormat(pattern = "yyyy-MM")
	public Date getPunishTime() {
		return punishTime;
	}

	public void setPunishTime(Date punishTime) {
		this.punishTime = punishTime;
	}

	@ExcelField(value="reason",title="原因", align=2, sort=30)
	@Length(min=0, max=255, message="处分原因长度必须介于 0 和 255 之间")
	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}
	public Date getBeginPunishTime() {
		return beginPunishTime;
	}

	public void setBeginPunishTime(Date beginPunishTime) {
		this.beginPunishTime = beginPunishTime;
	}
	public Date getEndPunishTime() {
		return endPunishTime;
	}

	public void setEndPunishTime(Date endPunishTime) {
		this.endPunishTime = endPunishTime;
	}
	@ExcelField(value="stuInfo.tutor.name",title="导师", align=2, sort=16,type=1)
	public TpPStudent getStuInfo() {
		return stuInfo;
	}

	public void setStuInfo(TpPStudent stuInfo) {
		this.stuInfo = stuInfo;
	}
		
}