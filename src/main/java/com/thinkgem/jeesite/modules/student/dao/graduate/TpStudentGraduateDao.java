/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.student.dao.graduate;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.student.entity.graduate.TpStudentGraduate;

/**
 * 毕业模块DAO接口
 * @author 康力
 * @version 2018-03-08
 */
@MyBatisDao
public interface TpStudentGraduateDao extends CrudDao<TpStudentGraduate> {
	
}