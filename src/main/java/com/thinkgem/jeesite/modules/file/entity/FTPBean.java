package com.thinkgem.jeesite.modules.file.entity;

import org.springframework.beans.factory.annotation.Value;

import java.util.UUID;

/**
 * Created by yangrui on 2018/3/10.
 */
public class FTPBean {
    /**
     * 端口号
     */
    private int port;
    /**
     * 主机地址
     */
    private String host;
    /**
     * 登录名
     */
    private String loginName;
    /**
     * 密码
     */
    private String password;
    /**
     * ftp根目录
     */
    private String rootPath;
    /**
     *文件存储的目录
     */
    private String storePath;
    /**
     *文件原始名字
     */
    private String originalFileName;
    /**
     * 上传到ftp上的名字
     */
    private String ftpFileName;

    public FTPBean() {
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRootPath() {
        return rootPath;
    }

    public void setRootPath(String rootPath) {
        this.rootPath = rootPath;
    }

    public String getStorePath() {
        return storePath;
    }

    public void setStorePath(String storePath) {
        this.storePath = storePath;
    }

    public String getOriginalFileName() {
        return originalFileName;
    }

    public void setOriginalFileName(String originalFileName) {
        this.originalFileName = originalFileName;
    }

    public String getFtpFileName() {
        return ftpFileName;
    }

    public void setFtpFileName(String ftpFileName) {
        this.ftpFileName = ftpFileName;
    }

    /**
     * 生成新的文件名字：uuid.extend
     * @param extension 文件扩展名
     */
    public void modifyFtpFileName(String extension) {
        this.ftpFileName = new StringBuilder().append(UUID.randomUUID().toString().replaceAll("-", "")).append(".").append(extension).toString();
    }

    @Override
    public String toString() {
        return "FTPBean{" +
                "port=" + port +
                ", host='" + host + '\'' +
                ", loginName='" + loginName + '\'' +
                ", password='" + password + '\'' +
                ", rootPath='" + rootPath + '\'' +
                ", storePath='" + storePath + '\'' +
                ", originalFileName='" + originalFileName + '\'' +
                ", ftpFileName='" + ftpFileName + '\'' +
                '}';
    }
}
