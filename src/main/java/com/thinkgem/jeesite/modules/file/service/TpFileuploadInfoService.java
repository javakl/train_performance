/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.file.service;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.common.utils.FTPUtils;
import com.thinkgem.jeesite.common.utils.Msg;
import com.thinkgem.jeesite.modules.file.dao.TpFileuploadInfoDao;
import com.thinkgem.jeesite.modules.file.entity.FTPBean;
import com.thinkgem.jeesite.modules.file.entity.TpFileuploadInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 文件上传Service
 * @author 康力
 * @version 2018-03-07
 */
@Service
@Transactional(readOnly = true)
public class TpFileuploadInfoService extends CrudService<TpFileuploadInfoDao, TpFileuploadInfo> {

	@Autowired
	private FTPBean ftpBean;

	@Override
	public TpFileuploadInfo get(String id) {
		return super.get(id);
	}
	
	@Override
	public List<TpFileuploadInfo> findList(TpFileuploadInfo tpFileuploadInfo) {
		return super.findList(tpFileuploadInfo);
	}
	
	@Override
	public Page<TpFileuploadInfo> findPage(Page<TpFileuploadInfo> page, TpFileuploadInfo tpFileuploadInfo) {
		return super.findPage(page, tpFileuploadInfo);
	}
	
	@Override
	@Transactional(readOnly = false)
	public void save(TpFileuploadInfo tpFileuploadInfo) {
		super.save(tpFileuploadInfo);
	}
	
	@Override
	@Transactional(readOnly = false)
	public void delete(TpFileuploadInfo tpFileuploadInfo) {
		ftpBean.setStorePath(ftpBean.getRootPath());
		ftpBean.setFtpFileName(tpFileuploadInfo.getFilePath());
		FTPUtils.deleteFile(ftpBean);
		super.delete(tpFileuploadInfo);
	}

	@Transactional(readOnly = false,rollbackFor = Exception.class)
	public Msg batchInsert(List<TpFileuploadInfo> files) {
		try {
			for(TpFileuploadInfo file:files){
                save(file);
            }
			return new Msg(Msg.SUCCESS, "上传文件成功！");
		} catch (Exception e) {
			e.printStackTrace();
			return new Msg(Msg.FAIL, "上传文件失败！");
		}
	}
}