/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.file.entity;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 文件上传Entity
 * @author 康力
 * @version 2018-03-07
 */
public class TpFileuploadInfo extends DataEntity<TpFileuploadInfo> {
	
	private static final long serialVersionUID = 1L;
	private String fileName;		// 文件名
	private Long size;		// 文件大小
	private String suffix;		// 文件后缀
	private String filePath;		// 文件路径
	private Integer downloadNum;		// 下载次数
	private String fileType;		// 文件类型
	private String model;		// 文件所属模块
	
	public TpFileuploadInfo() {
		super();
	}

	public TpFileuploadInfo(String id){
		super(id);
	}

	@Length(min=0, max=255, message="文件名长度必须介于 0 和 255 之间")
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Long getSize() {
		return size;
	}

	public void setSize(Long size) {
		this.size = size;
	}
	
	@Length(min=0, max=50, message="文件后缀长度必须介于 0 和 50 之间")
	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}
	
	@Length(min=0, max=255, message="文件路径长度必须介于 0 和 255 之间")
	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public Integer getDownloadNum() {
		return downloadNum;
	}

	public void setDownloadNum(Integer downloadNum) {
		this.downloadNum = downloadNum;
	}
	
	@Length(min=0, max=3, message="文件类型长度必须介于 0 和 3 之间")
	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	
	@Length(min=0, max=11, message="文件所属模块长度必须介于 0 和 11 之间")
	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}
	
}