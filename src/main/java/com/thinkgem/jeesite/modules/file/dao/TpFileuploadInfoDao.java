/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.file.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.file.entity.TpFileuploadInfo;

/**
 * 文件上传DAO接口
 * @author 康力
 * @version 2018-03-07
 */
@MyBatisDao
public interface TpFileuploadInfoDao extends CrudDao<TpFileuploadInfo> {
	
}