/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.classgroup.dao;

import com.thinkgem.jeesite.common.persistence.TreeDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.classgroup.entity.ClassGroup;

import java.util.List;
import java.util.Map;

/**
 * 学员班级小组DAO接口
 * @author 洪
 * @version 2018-01-29
 */
@MyBatisDao
public interface ClassGroupDao extends TreeDao<ClassGroup> {

    List<Map<String,String>> querygroup();
}