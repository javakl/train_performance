/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.classgroup.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.classgroup.entity.ClassGroup;
import com.thinkgem.jeesite.modules.classgroup.service.ClassGroupService;

/**
 * 学员班级小组Controller
 * @author 洪
 * @version 2018-01-29
 */
@Controller
@RequestMapping(value = "${adminPath}/classgroup/classGroup")
public class ClassGroupController extends BaseController {

	@Autowired
	private ClassGroupService classGroupService;
	
	@ModelAttribute
	public ClassGroup get(@RequestParam(required=false) String id) {
		ClassGroup entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = classGroupService.get(id);
		}
		if (entity == null){
			entity = new ClassGroup();
		}
		return entity;
	}
	
	@RequiresPermissions("classgroup:classGroup:view")
	@RequestMapping(value = {"list", ""})
	public String list(ClassGroup classGroup, HttpServletRequest request, HttpServletResponse response, Model model) {
		List<ClassGroup> list = classGroupService.findList(classGroup); 
		model.addAttribute("list", list);
		return "modules/classgroup/classGroupList";
	}

	@RequiresPermissions("classgroup:classGroup:view")
	@RequestMapping(value = "form")
	public String form(ClassGroup classGroup, Model model) {
		if (classGroup.getParent()!=null && StringUtils.isNotBlank(classGroup.getParent().getId())){
			classGroup.setParent(classGroupService.get(classGroup.getParent().getId()));
			// 获取排序号，最末节点排序号+30
			if (StringUtils.isBlank(classGroup.getId())){
				ClassGroup classGroupChild = new ClassGroup();
				classGroupChild.setParent(new ClassGroup(classGroup.getParent().getId()));
				List<ClassGroup> list = classGroupService.findList(classGroup); 
				if (list.size() > 0){
					classGroup.setSort(list.get(list.size()-1).getSort());
					if (classGroup.getSort() != null){
						classGroup.setSort(classGroup.getSort() + 30);
					}
				}
			}
		}
		if (classGroup.getSort() == null){
			classGroup.setSort(30);
		}
		model.addAttribute("classGroup", classGroup);
		return "modules/classgroup/classGroupForm";
	}

	@RequiresPermissions("classgroup:classGroup:edit")
	@RequestMapping(value = "save")
	public String save(ClassGroup classGroup, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, classGroup)){
			return form(classGroup, model);
		}
		classGroupService.save(classGroup);
		addMessage(redirectAttributes, "保存班级或小组成功");
		return "redirect:"+Global.getAdminPath()+"/classgroup/classGroup/?repage";
	}
	
	@RequiresPermissions("classgroup:classGroup:edit")
	@RequestMapping(value = "delete")
	public String delete(ClassGroup classGroup, RedirectAttributes redirectAttributes) {
		classGroupService.delete(classGroup);
		addMessage(redirectAttributes, "删除班级或小组成功");
		return "redirect:"+Global.getAdminPath()+"/classgroup/classGroup/?repage";
	}

	@RequiresPermissions("user")
	@ResponseBody
	@RequestMapping(value = "treeData")
	public List<Map<String, Object>> treeData(@RequestParam(required=false) String extId, HttpServletResponse response) {
		List<Map<String, Object>> mapList = Lists.newArrayList();
		List<ClassGroup> list = classGroupService.findList(new ClassGroup());
		for (int i=0; i<list.size(); i++){
			ClassGroup e = list.get(i);
			if (StringUtils.isBlank(extId) || (extId!=null && !extId.equals(e.getId()) && e.getParentIds().indexOf(","+extId+",")==-1)){
				Map<String, Object> map = Maps.newHashMap();
				map.put("id", e.getId());
				map.put("pId", e.getParentId());
				map.put("name", e.getName());
				mapList.add(map);
			}
		}
		return mapList;
	}
	
}