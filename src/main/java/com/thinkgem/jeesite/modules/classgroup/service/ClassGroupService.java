/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.classgroup.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.service.TreeService;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.classgroup.entity.ClassGroup;
import com.thinkgem.jeesite.modules.classgroup.dao.ClassGroupDao;

/**
 * 学员班级小组Service
 * @author 洪
 * @version 2018-01-29
 */
@Service
@Transactional(readOnly = true)
public class ClassGroupService extends TreeService<ClassGroupDao, ClassGroup> {

	public ClassGroup get(String id) {
		return super.get(id);
	}
	
	public List<ClassGroup> findList(ClassGroup classGroup) {
		if (StringUtils.isNotBlank(classGroup.getParentIds())){
			classGroup.setParentIds(","+classGroup.getParentIds()+",");
		}
		return super.findList(classGroup);
	}
	
	@Transactional(readOnly = false)
	public void save(ClassGroup classGroup) {
		super.save(classGroup);
	}
	
	@Transactional(readOnly = false)
	public void delete(ClassGroup classGroup) {
		super.delete(classGroup);
	}
	
}