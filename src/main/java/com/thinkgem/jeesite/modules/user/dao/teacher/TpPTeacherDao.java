/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.user.dao.teacher;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.user.entity.teacher.TpPTeacher;

/**
 * 老师DAO接口
 * @author 康力
 * @version 2018-03-10
 */
@MyBatisDao
public interface TpPTeacherDao extends CrudDao<TpPTeacher> {

	/**
	 * 教师数据彻底删除，（包含用户表数据、角色）
	 * @param model
	 * @return
	 */
	int completelyDelete(TpPTeacher model);
}