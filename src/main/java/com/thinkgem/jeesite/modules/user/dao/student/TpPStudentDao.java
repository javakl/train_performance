/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.user.dao.student;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.user.entity.student.TpPStudent;

/**
 * 学生模块DAO接口
 * @author 康力
 * @version 2018-03-08
 */
@MyBatisDao
public interface TpPStudentDao extends CrudDao<TpPStudent> {
	
}