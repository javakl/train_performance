/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.user.service.stuteachers;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.common.utils.DateUtils;
import com.thinkgem.jeesite.modules.user.entity.student.TpPStudent;
import com.thinkgem.jeesite.modules.user.entity.stuteachers.TpStudentTeacher;
import com.thinkgem.jeesite.modules.user.dao.student.TpPStudentDao;
import com.thinkgem.jeesite.modules.user.dao.stuteachers.TpStudentTeacherDao;

/**
 * 学生的辅导老师信息Service
 * @author hhm
 * @version 2018-04-19
 */
@Service
@Transactional(readOnly = true)
public class TpStudentTeacherService extends CrudService<TpStudentTeacherDao, TpStudentTeacher> {
	
	@Autowired
	private TpPStudentDao studentDao;

	public TpStudentTeacher get(String id) {
		return super.get(id);
	}
	
	public List<TpStudentTeacher> findList(TpStudentTeacher tpStudentTeacher) {
		return super.findList(tpStudentTeacher);
	}
	
	public Page<TpStudentTeacher> findPage(Page<TpStudentTeacher> page, TpStudentTeacher tpStudentTeacher) {
		return super.findPage(page, tpStudentTeacher);
	}
	
	@Transactional(readOnly = false)
	public void save(TpStudentTeacher tpStudentTeacher) {
		//检查同学员同类型与库中有无时间交叉
		TpStudentTeacher query = new TpStudentTeacher();
		query.setStartDate(tpStudentTeacher.getStartDate());
		query.setEndDate(tpStudentTeacher.getEndDate());
		query.setStu(tpStudentTeacher.getStu());
		query.setType(tpStudentTeacher.getType());
		List<TpStudentTeacher> stuTeachers = super.findList(query);
		if(stuTeachers.size()>0){
			boolean repeat = true;//是否有交叉
			//有一条交叉，刚好是本次更新的数据，则不冲突
			if(stuTeachers.size()==1 
					&& (StringUtils.isNotBlank(tpStudentTeacher.getId())&&stuTeachers.get(0).getId().equals(tpStudentTeacher.getId()))){
				repeat = false;
			}
			if(repeat)throw new RuntimeException("同学员同类型起止日期和数据库中有交叉");
		}
		super.save(tpStudentTeacher);
		if(tpStudentTeacher.getStu()!= null  && tpStudentTeacher.getStu().getId() != null){
			//起止日期和当前日期有交叉
			long startDateTime = DateUtils.parseDate(tpStudentTeacher.getStartDate()).getTime();
			long endDateTime = DateUtils.parseDate(tpStudentTeacher.getEndDate(),"yyyy-MM-dd").getTime()+24*3600*1000;
			long curDateTime = new Date().getTime();
			if(startDateTime<=curDateTime && curDateTime<=endDateTime){
				//更新学生表当前指导老师/带教老师信息
				TpPStudent student = studentDao.get(tpStudentTeacher.getStu().getId());
				//if(tpStudentTeacher.getType() == 1){//辅导老师
					student.setTutor(tpStudentTeacher.getTeacher());
				/*}else if(tpStudentTeacher.getType() == 2){//带教老师
					student.setTeacher(tpStudentTeacher.getTeacher());
				}*/
				studentDao.update(student);
			}
		}
	}
	
	@Transactional(readOnly = false)
	public void delete(TpStudentTeacher tpStudentTeacher) {
		super.delete(tpStudentTeacher);
	}
	
}