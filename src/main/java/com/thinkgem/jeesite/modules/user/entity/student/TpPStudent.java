/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.user.entity.student;

import com.thinkgem.jeesite.common.utils.excel.annotation.ExcelField;
import com.thinkgem.jeesite.modules.classgroup.entity.ClassGroup;
import com.thinkgem.jeesite.modules.sys.entity.Office;
import com.thinkgem.jeesite.modules.sys.entity.User;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.Length;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 学生模块Entity
 * @author 康力
 * @version 2018-03-08
 */
public class TpPStudent extends DataEntity<TpPStudent> {
	
	private static final long serialVersionUID = 1L;
	private User user;		// 学生基本信息
	private String isMaster;		// 是否研究生
	private User teacher;		// 当前带教老师
	private User tutor;		// 当前导师
	private String grade;		// 年级
	private String stuType;		// 学生类别
	private String politicalOutlook;		// 政治面貌
	private String nation;		// 民族
	private String masterType;		// 研究生类型
	private String degree;		// 学位
	private Date inSchool;		// 入学时间
	private Date outSchool;		// 毕业时间
	private String birthPlace;		// 生源地
	private String enrolmentWay;		// 入学方式
	private ClassGroup classGroup;  //所属教学小组
	private String studentNo;//学生学号
	private String rootIn;//来源（统招、在职） dictType = tp_stu_root_in
	private Office department;//所属部门
	public TpPStudent() {
		super();
	}

	public TpPStudent(String id){
		super(id);
	}

	@ExcelField(title="小组", align=2, sort=40)
	public ClassGroup getClassGroup() {
		return classGroup;
	}

	public void setClassGroup(ClassGroup classGroup) {
		this.classGroup = classGroup;
	}

	@NotNull(message="学生基本信息不能为空")
	@ExcelField(value="",title="", align=2, sort=1,fieldType=User.class)
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	@Length(min=0, max=11, message="是否研究生长度必须介于 0 和 11 之间")
	@ExcelField(title="是否研究生", align=2, sort=48,dictType = "yes_no")
	public String getIsMaster() {
		return isMaster;
	}

	public void setIsMaster(String isMaster) {
		this.isMaster = isMaster;
	}

	@ExcelField(title="带教老师", align=2, sort=50)
	public User getTeacher() {
		return teacher;
	}

	public void setTeacher(User teacher) {
		this.teacher = teacher;
	}

	@ExcelField(title="导师", align=2, sort=55)
	public User getTutor() {
		return tutor;
	}

	public void setTutor(User tutor) {
		this.tutor = tutor;
	}
	
	@Length(min=0, max=20, message="年级长度必须介于 0 和 20 之间")
	@ExcelField(title="年级",  align=2, sort=43,dictType = "stu_grade")
	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}
	
	@Length(min=0, max=64, message="学生类别长度必须介于 0 和 64 之间")
	@ExcelField(title="学生类别",  align=2, sort=45 ,dictType = "stu_type")
	public String getStuType() {
		return stuType;
	}

	public void setStuType(String stuType) {
		this.stuType = stuType;
	}
	
	@Length(min=0, max=64, message="政治面貌长度必须介于 0 和 64 之间")
	@ExcelField(title="政治面貌",  align=2, sort=60,dictType= "political_outlook")
	public String getPoliticalOutlook() {
		return politicalOutlook;
	}

	public void setPoliticalOutlook(String politicalOutlook) {
		this.politicalOutlook = politicalOutlook;
	}
	
	@Length(min=0, max=64, message="民族长度必须介于 0 和 64 之间")
	@ExcelField(title="民族", align=2, sort=65,dictType= "nation")
	public String getNation() {
		return nation;
	}

	public void setNation(String nation) {
		this.nation = nation;
	}
	
	@Length(min=0, max=64, message="研究生类型长度必须介于 0 和 64 之间")
	@ExcelField(title="研究生类型", align=2, sort=70,dictType= "master_type")
	public String getMasterType() {
		return masterType;
	}

	public void setMasterType(String masterType) {
		this.masterType = masterType;
	}
	
	@Length(min=0, max=64, message="学位长度必须介于 0 和 64 之间")
	@ExcelField(title="学位",  align=2, sort=75,dictType= "degree")
	public String getDegree() {
		return degree;
	}

	public void setDegree(String degree) {
		this.degree = degree;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd")
	@ExcelField(title="入学时间",  align=2, sort=80)
	public Date getInSchool() {
		return inSchool;
	}

	public void setInSchool(Date inSchool) {
		this.inSchool = inSchool;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd")
	@ExcelField(title="毕业时间",  align=2, sort=85)
	public Date getOutSchool() {
		return outSchool;
	}

	public void setOutSchool(Date outSchool) {
		this.outSchool = outSchool;
	}

	@Length(min=0, max=255, message="生源地长度必须介于 0 和 255 之间")
	@ExcelField(title="生源地",  align=2, sort=90)
	public String getBirthPlace() {
		return birthPlace;
	}

	public void setBirthPlace(String birthPlace) {
		this.birthPlace = birthPlace;
	}
	
	@Length(min=0, max=64, message="入学方式长度必须介于 0 和 64 之间")
	@ExcelField(title="入学方式",  align=2, sort=95,dictType = "enrolment_way")
	public String getEnrolmentWay() {
		return enrolmentWay;
	}

	public void setEnrolmentWay(String enrolmentWay) {
		this.enrolmentWay = enrolmentWay;
	}

	@ExcelField(title="学号",  align=2, sort=8)
	public String getStudentNo() {
		return studentNo;
	}

	public void setStudentNo(String studentNo) {
		this.studentNo = studentNo;
	}
	@ExcelField(title="来源",  align=2, sort=46,dictType = "tp_stu_root_in")
	public String getRootIn() {
		return rootIn;
	}
	public void setRootIn(String rootIn) {
		this.rootIn = rootIn;
	}
	@ExcelField(title="所属部门", align=2, sort=72)
	public Office getDepartment() {
		return department;
	}

	public void setDepartment(Office department) {
		this.department = department;
	}
	
}