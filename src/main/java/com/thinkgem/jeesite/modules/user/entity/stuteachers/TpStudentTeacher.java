/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.user.entity.stuteachers;

import com.thinkgem.jeesite.modules.sys.entity.User;
import com.thinkgem.jeesite.modules.user.entity.student.TpPStudent;
import com.thinkgem.jeesite.modules.user.entity.teacher.TpPTeacher;

import org.hibernate.validator.constraints.Length;
import javax.validation.constraints.NotNull;

import com.thinkgem.jeesite.common.persistence.DataEntity;
import com.thinkgem.jeesite.common.utils.excel.annotation.ExcelField;

/**
 * 学生的辅导老师信息Entity
 * @author hhm
 * @version 2018-04-19
 */
public class TpStudentTeacher extends DataEntity<TpStudentTeacher> {
	
	private static final long serialVersionUID = 1L;
	private User teacher;		// 带教/辅导教师
	private User stu;		// 学生id
	private String startDate;		// 开始日期，格式=yyyy-MM-dd
	private String endDate;		// 结束日期，格式=yyyy-MM-dd
	private String type;		// 辅导类型
	private String beginStartDate;		// 开始 开始日期，格式=yyyy-MM-dd
	private String endStartDate;		// 结束 开始日期，格式=yyyy-MM-dd
	private String beginEndDate;		// 开始 结束日期，格式=yyyy-MM-dd
	private String endEndDate;		// 结束 结束日期，格式=yyyy-MM-dd
	private TpPStudent studentInfo;//学生信息
	private TpPTeacher teacherInfo;//老师信息
	public TpStudentTeacher() {
		super();
	}

	public TpStudentTeacher(String id){
		super(id);
	}
	@ExcelField(title="教师", align=2, sort=1)
	public User getTeacher() {
		return teacher;
	}

	public void setTeacher(User teacher) {
		this.teacher = teacher;
	}
	@ExcelField(title="", align=2, sort=2,fieldType=User.class)
	public User getStu() {
		return stu;
	}

	public void setStu(User stu) {
		this.stu = stu;
	}
	
	@Length(min=0, max=24, message="开始日期，格式=yyyy-MM-dd长度必须介于 0 和 24 之间")
	@ExcelField(title="开始日期", align=10, sort=1)
	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	
	@Length(min=0, max=24, message="结束日期，格式=yyyy-MM-dd长度必须介于 0 和 24 之间")
	@ExcelField(title="结束日期", align=11, sort=1)
	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
	@NotNull(message="辅导类型不能为空")
	@ExcelField(title="类型", align=12, sort=1,dictType="tutor_type")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public String getBeginStartDate() {
		return beginStartDate;
	}

	public void setBeginStartDate(String beginStartDate) {
		this.beginStartDate = beginStartDate;
	}
	
	public String getEndStartDate() {
		return endStartDate;
	}

	public void setEndStartDate(String endStartDate) {
		this.endStartDate = endStartDate;
	}
		
	public String getBeginEndDate() {
		return beginEndDate;
	}

	public void setBeginEndDate(String beginEndDate) {
		this.beginEndDate = beginEndDate;
	}
	
	public String getEndEndDate() {
		return endEndDate;
	}

	public void setEndEndDate(String endEndDate) {
		this.endEndDate = endEndDate;
	}

	public TpPStudent getStudentInfo() {
		return studentInfo;
	}

	public void setStudentInfo(TpPStudent studentInfo) {
		this.studentInfo = studentInfo;
	}

	public TpPTeacher getTeacherInfo() {
		return teacherInfo;
	}

	public void setTeacherInfo(TpPTeacher teacherInfo) {
		this.teacherInfo = teacherInfo;
	}

	@Override
	public String toString() {
		return " [教师=" + teacher.getName() + ", 学员=" + stu.getName() + ", 开始日期=" + startDate + ", 结束日期="
				+ endDate + "]";
	}
		
}