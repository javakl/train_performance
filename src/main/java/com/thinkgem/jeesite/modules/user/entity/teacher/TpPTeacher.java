/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.user.entity.teacher;

import com.thinkgem.jeesite.common.persistence.DataEntity;
import com.thinkgem.jeesite.common.utils.excel.annotation.ExcelField;
import com.thinkgem.jeesite.modules.sys.entity.Office;
import com.thinkgem.jeesite.modules.sys.entity.User;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 老师Entity
 *
 * @author 康力
 * @version 2018-03-10
 */
public class TpPTeacher extends DataEntity<TpPTeacher> {

    private static final long serialVersionUID = 1L;
    private User user;        // 老师基本信息
    private String titleId;        // 职称id(来源于字典表，类型=title)
    private String tutorType;        // 导师类型(来源于字典表，类型=tutor_type)
    private String politicalOutlook;        // 政治面貌(来源于字典表，类型=political_outlook)
    private String isMasterTutor;        // 是否硕导
    private String isDoctorTutor;        // 是否博导
    private String isUndergraduateTutor;        // 是否本科生导师
    private Office trc;        // 所属教研室
    private Office office;        // 所属科室
    private boolean isTutor;//是否过滤导师 用于查询
	private List<String> ids;
    public TpPTeacher() {
        super();
    }

    public TpPTeacher(String id) {
        super(id);
    }

    @NotNull(message = "老师基本信息不能为空")
    @ExcelField(title = "", align = 2, sort = 1, fieldType = User.class)
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Length(min = 0, max = 64, message = "职称id(来源于字典表，类型=title)长度必须介于 0 和 64 之间")
    @ExcelField(title = "职称", align = 2, sort = 40, dictType = "title")
    public String getTitleId() {
        return titleId;
    }

    public void setTitleId(String titleId) {
        this.titleId = titleId;
    }

    @Length(min = 0, max = 64, message = "导师类型(来源于字典表，类型=tutor_type)长度必须介于 0 和 64 之间")
    @ExcelField(title = "导师类型", align = 2, sort = 45, dictType = "tutor_type")
    public String getTutorType() {
        return tutorType;
    }

    public void setTutorType(String tutorType) {
        this.tutorType = tutorType;
    }

    @Length(min = 0, max = 64, message = "政治面貌(来源于字典表，类型=political_outlook)长度必须介于 0 和 64 之间")
    @ExcelField(title = "政治面貌", align = 2, sort = 50, dictType = "political_outlook")
    public String getPoliticalOutlook() {
        return politicalOutlook;
    }

    public void setPoliticalOutlook(String politicalOutlook) {
        this.politicalOutlook = politicalOutlook;
    }

    @Length(min = 0, max = 11, message = "是否硕导状态有误")
    @ExcelField(title = "是否硕导", align = 2, sort = 55, dictType = "yes_no")
    public String getIsMasterTutor() {
        return isMasterTutor;
    }

    public void setIsMasterTutor(String isMasterTutor) {
        this.isMasterTutor = isMasterTutor;
    }

    @Length(min = 0, max = 11, message = "是否博导状态有误")
    @ExcelField(title = "是否博导", align = 2, sort = 60, dictType = "yes_no")
    public String getIsDoctorTutor() {
        return isDoctorTutor;
    }

    public void setIsDoctorTutor(String isDoctorTutor) {
        this.isDoctorTutor = isDoctorTutor;
    }

    @Length(min = 0, max = 11, message = "是否本科生导师长度必须介于 0 和 11 之间")
    @ExcelField(title = "是否本科生导师", align = 2, sort = 65, dictType = "yes_no")
    public String getIsUndergraduateTutor() {
        return isUndergraduateTutor;
    }

    public void setIsUndergraduateTutor(String isUndergraduateTutor) {
        this.isUndergraduateTutor = isUndergraduateTutor;
    }

    @ExcelField(title = "所属教研室", align = 2, sort = 75)
    public Office getTrc() {
        return trc;
    }

    public void setTrc(Office trc) {
        this.trc = trc;
    }

    @ExcelField(title = "所属科室", align = 2, sort = 70)
    public Office getOffice() {
        return office;
    }

    public void setOffice(Office office) {
        this.office = office;
    }

	public boolean getIsTutor() {
		return isTutor;
	}

	public void setIsTutor(boolean isTutor) {
		this.isTutor = isTutor;
	}

	public List<String> getIds() {
		return ids;
	}

	public void setIds(List<String> ids) {
		this.ids = ids;
	}
}