/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.user.service.teacher;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.user.entity.teacher.TpPTeacher;
import com.thinkgem.jeesite.modules.sys.entity.Office;
import com.thinkgem.jeesite.modules.sys.entity.User;
import com.thinkgem.jeesite.modules.sys.service.SystemService;
import com.thinkgem.jeesite.modules.user.dao.teacher.TpPTeacherDao;

/**
 * 老师Service
 * @author 康力
 * @version 2018-03-10
 */
@Service
@Transactional
public class TpPTeacherService extends CrudService<TpPTeacherDao, TpPTeacher> {
	@Autowired
	private SystemService systemService;
	public TpPTeacher get(String id) {
		return super.get(id);
	}
	
	public List<TpPTeacher> findList(TpPTeacher tpPTeacher) {
		return super.findList(tpPTeacher);
	}
	
	public Page<TpPTeacher> findPage(Page<TpPTeacher> page, TpPTeacher tpPTeacher) {
		return super.findPage(page, tpPTeacher);
	}
	
	@Transactional(readOnly = false)
	public void save(TpPTeacher tpPTeacher) {
		if(tpPTeacher.getUser()!=null){
			User tuser = tpPTeacher.getUser();
			if(StringUtils.isEmpty(tuser.getPassword()))tuser.setPassword("123");
			tuser.setPassword(SystemService.entryptPassword(tuser.getPassword()));
			tuser.setOffice(new Office(tpPTeacher.getOffice().getId()));
			systemService.saveUser(tuser);
		}
		super.save(tpPTeacher);
	}
	
	@Transactional(readOnly = false)
	public void delete(TpPTeacher tpPTeacher) {
		super.delete(tpPTeacher);
	}
	
	/**
	 * 教师数据彻底删除，（包含用户表数据、角色）
	 * @param model
	 * @return
	 */
	public int completelyDelete(TpPTeacher model){
		return dao.completelyDelete(model);
	}
}