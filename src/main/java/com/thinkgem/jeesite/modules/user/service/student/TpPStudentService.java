/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.user.service.student;

import java.util.List;

import com.thinkgem.jeesite.common.utils.DateUtils;
import com.thinkgem.jeesite.common.utils.IdGen;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.sys.entity.User;
import com.thinkgem.jeesite.modules.sys.service.SystemService;
import com.thinkgem.jeesite.modules.sys.utils.DictUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.user.entity.student.TpPStudent;
import com.thinkgem.jeesite.modules.user.entity.stuteachers.TpStudentTeacher;
import com.thinkgem.jeesite.modules.user.dao.student.TpPStudentDao;
import com.thinkgem.jeesite.modules.user.dao.stuteachers.TpStudentTeacherDao;

/**
 * 学生模块Service
 * @author 康力
 * @version 2018-03-08
 */
@Service
@Transactional(readOnly = true)
public class TpPStudentService extends CrudService<TpPStudentDao, TpPStudent> {
	@Autowired
	private SystemService systemService;
	
	@Autowired
	private TpStudentTeacherDao studentTeacherDao;

	public TpPStudent get(String id) {
		return super.get(id);
	}
	
	public List<TpPStudent> findList(TpPStudent tpPStudent) {
		return super.findList(tpPStudent);
	}
	
	public Page<TpPStudent> findPage(Page<TpPStudent> page, TpPStudent tpPStudent) {
		return super.findPage(page, tpPStudent);
	}
	
	@Transactional(readOnly = false)
	public void save(TpPStudent tpPStudent) {
		User user=tpPStudent.getUser();
//		if (StringUtils.isBlank(user.getId())){
			systemService.saveUser(user);
//		}
		tpPStudent.setUser(user);
//		super.save(tpPStudent);
		//保存学生表
		if (tpPStudent.getIsNewRecord()){
			tpPStudent.preInsert();
			dao.insert(tpPStudent);
			//向学员当前指导、带教老师中间表()插入数据，默认起止范围为入学时间和毕业时间
			if(tpPStudent.getInSchool() != null && tpPStudent.getOutSchool() != null){
				TpStudentTeacher  st = new TpStudentTeacher();
				st.setId(IdGen.uuid());
				//st.setType(DictUtils.getDictValue("", "", null));
				st.setTeacher(tpPStudent.getTeacher());
				st.setStartDate(DateUtils.formatDate(tpPStudent.getInSchool()));
				st.setEndDate(DateUtils.formatDate(tpPStudent.getOutSchool()));
				st.setStu(tpPStudent.getUser());
				studentTeacherDao.insert(st);
				/*st = new TpStudentTeacher();
				st.setId(IdGen.uuid());
				st.setType(1);
				st.setTeacher(tpPStudent.getTutor());
				st.setStartDate(DateUtils.formatDate(tpPStudent.getInSchool()));
				st.setEndDate(DateUtils.formatDate(tpPStudent.getOutSchool()));
				st.setStu(tpPStudent.getUser());
				studentTeacherDao.insert(st);*/
			}
		}else{
			tpPStudent.preUpdate();
			dao.update(tpPStudent);
		}
	}
	
	@Transactional(readOnly = false)
	public void delete(TpPStudent tpPStudent) {
		super.delete(tpPStudent);
	}
	
}