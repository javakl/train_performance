/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.user.dao.stuteachers;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.user.entity.stuteachers.TpStudentTeacher;

/**
 * 学生的辅导老师信息DAO接口
 * @author hhm
 * @version 2018-04-19
 */
@MyBatisDao
public interface TpStudentTeacherDao extends CrudDao<TpStudentTeacher> {
	
}