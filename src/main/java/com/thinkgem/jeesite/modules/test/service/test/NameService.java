/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.test.service.test;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.test.entity.test.Name;
import com.thinkgem.jeesite.modules.test.dao.test.NameDao;

/**
 * testService
 * @author 康力
 * @version 2018-01-18
 */
@Service
@Transactional(readOnly = true)
public class NameService extends CrudService<NameDao, Name> {

	public Name get(String id) {
		return super.get(id);
	}
	
	public List<Name> findList(Name name) {
		return super.findList(name);
	}
	
	public Page<Name> findPage(Page<Name> page, Name name) {
		return super.findPage(page, name);
	}
	
	@Transactional(readOnly = false)
	public void save(Name name) {
		super.save(name);
	}
	
	@Transactional(readOnly = false)
	public void delete(Name name) {
		super.delete(name);
	}
	
}