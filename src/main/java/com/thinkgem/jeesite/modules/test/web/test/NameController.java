/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.test.web.test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.test.entity.test.Name;
import com.thinkgem.jeesite.modules.test.service.test.NameService;

/**
 * testController
 * @author 康力
 * @version 2018-01-18
 */
@Controller
@RequestMapping(value = "${adminPath}/test/test/name")
public class NameController extends BaseController {

	@Autowired
	private NameService nameService;
	
	@ModelAttribute
	public Name get(@RequestParam(required=false) String id) {
		Name entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = nameService.get(id);
		}
		if (entity == null){
			entity = new Name();
		}
		return entity;
	}
	
	@RequiresPermissions("test:test:name:view")
	@RequestMapping(value = {"list", ""})
	public String list(Name name, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<Name> page = nameService.findPage(new Page<Name>(request, response), name); 
		model.addAttribute("page", page);
		return "modules/test/test/nameList";
	}

	@RequiresPermissions("test:test:name:view")
	@RequestMapping(value = "form")
	public String form(Name name, Model model) {
		model.addAttribute("name", name);
		return "modules/test/test/nameForm";
	}

	@RequiresPermissions("test:test:name:edit")
	@RequestMapping(value = "save")
	public String save(Name name, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, name)){
			return form(name, model);
		}
		nameService.save(name);
		addMessage(redirectAttributes, "保存哈哈成功");
		return "redirect:"+Global.getAdminPath()+"/test/test/name/?repage";
	}
	
	@RequiresPermissions("test:test:name:edit")
	@RequestMapping(value = "delete")
	public String delete(Name name, RedirectAttributes redirectAttributes) {
		nameService.delete(name);
		addMessage(redirectAttributes, "删除哈哈成功");
		return "redirect:"+Global.getAdminPath()+"/test/test/name/?repage";
	}

}