/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.test.entity.test;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * testEntity
 * @author 康力
 * @version 2018-01-18
 */
public class Name extends DataEntity<Name> {
	
	private static final long serialVersionUID = 1L;
	private String name;		// 名字
	private String remarkA;		// 备注1
	
	public Name() {
		super();
	}

	public Name(String id){
		super(id);
	}

	@Length(min=0, max=255, message="名字长度必须介于 0 和 255 之间")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Length(min=0, max=255, message="备注1长度必须介于 0 和 255 之间")
	public String getRemarkA() {
		return remarkA;
	}

	public void setRemarkA(String remarkA) {
		this.remarkA = remarkA;
	}
	
}