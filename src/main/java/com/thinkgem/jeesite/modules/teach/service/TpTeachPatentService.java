/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.teach.service;

import com.google.common.collect.Maps;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.ActAuditService;
import com.thinkgem.jeesite.modules.act.service.ActTaskService;
import com.thinkgem.jeesite.modules.act.utils.ActUtils;
import com.thinkgem.jeesite.modules.teach.dao.TpTeachPatentDao;
import com.thinkgem.jeesite.modules.teach.entity.TpTeachPatent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 研究专利表Service
 * @author 康力
 * @version 2018-03-11
 */
@Service
@Transactional(readOnly = true)
public class TpTeachPatentService extends ActAuditService<TpTeachPatentDao, TpTeachPatent> {

	@Autowired
	private ActTaskService actTaskService;

	@Override
	public TpTeachPatent get(String id) {
		return super.get(id);
	}
	
	@Override
	public List<TpTeachPatent> findList(TpTeachPatent tpTeachPatent) {
		return super.findList(tpTeachPatent);
	}
	
	@Override
	public Page<TpTeachPatent> findPage(Page<TpTeachPatent> page, TpTeachPatent tpTeachPatent) {
		return super.findPage(page, tpTeachPatent);
	}
	
	@Override
	@Transactional(readOnly = false)
	public void save(TpTeachPatent tpTeachPatent) {
		boolean isNewRecord = tpTeachPatent.getIsNewRecord();
		super.save(tpTeachPatent);
		//根据实体类状态值判断是否启用申请流程
		if(tpTeachPatent.getStatus() == 0/* && isNewRecord*/){//状态为0启动流程
			// 启动流程
			actTaskService.startProcess(ActUtils.TP_TEACH_PATENT[0], ActUtils.TP_TEACH_PATENT[1], tpTeachPatent.getId(), tpTeachPatent.getPatentName());
			tpTeachPatent.setStatus(0);
			dao.update(tpTeachPatent);
		}else if(tpTeachPatent.getStatus()==2){//状态2为审核通过，不进入审核流程

		}else if(tpTeachPatent.getAct().getProcInsId() != null){//重申或销毁
			tpTeachPatent.preUpdate();
			dao.update(tpTeachPatent);
			tpTeachPatent.getAct().setComment(("yes".equals(tpTeachPatent.getAct().getFlag())?"[重申] ":"[销毁] ")+tpTeachPatent.getAct().getComment());
			// 完成流程任务
			Map<String, Object> vars = Maps.newHashMap();
			vars.put("pass", "yes".equals(tpTeachPatent.getAct().getFlag())? "1" : "0");
			actTaskService.complete(tpTeachPatent.getAct().getTaskId(), tpTeachPatent.getAct().getProcInsId(), tpTeachPatent.getAct().getComment(), tpTeachPatent.getRemarks(), vars);
		}
	}
	
	@Override
	@Transactional(readOnly = false)
	public void delete(TpTeachPatent tpTeachPatent) {
		super.delete(tpTeachPatent);
	}
	
}