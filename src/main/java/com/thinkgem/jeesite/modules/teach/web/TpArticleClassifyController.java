/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.teach.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.beanvalidator.BeanValidators;
import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.common.utils.excel.ImportExcel;
import com.thinkgem.jeesite.common.web.ExcelController;
import com.thinkgem.jeesite.modules.sys.service.SystemService;
import com.thinkgem.jeesite.modules.teach.entity.TpArticleClassify;
import com.thinkgem.jeesite.modules.teach.service.TpArticleClassifyService;

/**
 * 文章期刊分类Controller
 * @author hhm
 * @version 2018-03-28
 */
@Controller
@RequestMapping(value = "${adminPath}/teach/tpArticleClassify")
public class TpArticleClassifyController extends ExcelController<TpArticleClassify> {

	@Autowired
	private SystemService systemService;
	@Autowired
	private TpArticleClassifyService tpArticleClassifyService;

	@ModelAttribute
	public TpArticleClassify get(@RequestParam(required=false) String id) {
		TpArticleClassify entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = tpArticleClassifyService.get(id);
		}
		if (entity == null){
			entity = new TpArticleClassify();
		}
		return entity;
	}
	
	@RequiresPermissions("teach:tpArticleClassify:view")
	@RequestMapping(value = {"list", ""})
	public String list(TpArticleClassify tpArticleClassify, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<TpArticleClassify> page = tpArticleClassifyService.findPage(new Page<TpArticleClassify>(request, response), tpArticleClassify);
		model.addAttribute("page", page);
		return "modules/teach/tpArticleClassifyList";
	}

	@RequiresPermissions("teach:tpArticleClassify:view")
	@RequestMapping(value = "form")
	public String form(TpArticleClassify tpArticleClassify, Model model) {
		model.addAttribute("tpArticleClassify", tpArticleClassify);
		return "modules/teach/tpArticleClassifyForm";
	}

	@RequiresPermissions("teach:tpArticleClassify:edit")
	@RequestMapping(value = "save")
	public String save(TpArticleClassify tpArticleClassify, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, tpArticleClassify)){
			return form(tpArticleClassify, model);
		}
		tpArticleClassifyService.save(tpArticleClassify);
		addMessage(redirectAttributes, "保存文章期刊成功");
		return "redirect:"+Global.getAdminPath()+"/teach/tpArticleClassify/?repage";
	}
	@Override
	public String getTitle() {
		return "文章期刊分类";
	}

	@Override
	public String defaultStr() {
		return "/teach/tpArticleClassify/?repage";
	}

	@Override
	public CrudService baseService() {
		return tpArticleClassifyService;
	}
	@RequiresPermissions("teach:tpArticleClassify:edit")
	@RequestMapping(value = "delete")
	public String delete(TpArticleClassify tpArticleClassify, RedirectAttributes redirectAttributes) {
		tpArticleClassifyService.delete(tpArticleClassify);
		addMessage(redirectAttributes, "删除文章期刊成功");
		return "redirect:"+Global.getAdminPath()+"/teach/tpArticleClassify/?repage";
	}
	
	@Override
	@RequiresPermissions("teach:tpTeachExcellentAward:view")
	@RequestMapping(value = "import/template")
	public String importFileTemplate(HttpServletResponse response, RedirectAttributes redirectAttributes,TpArticleClassify t) {
		return super.importFileTemplate(response,redirectAttributes,t);
	}

	@Override
	@RequiresPermissions("teach:tpTeachExcellentAward:edit")
	@RequestMapping(value = "import", method= RequestMethod.POST)
	public String importFile(MultipartFile file, RedirectAttributes redirectAttributes, TpArticleClassify data) {
		 try {
	            int successNum = 0;
	            int failureNum = 0;
	            StringBuilder failureMsg = new StringBuilder();
	            ImportExcel ei = new ImportExcel(file, 1, 0);
	            List<TpArticleClassify> list = (List<TpArticleClassify>) ei.getDataList(data.getClass());
	            int n=0;
	            systemService.setFieldId(list);
	            for (TpArticleClassify t : list){
	                n++;
	                try{
	                    baseService().save(t);
	                    successNum++;
	                }catch(ConstraintViolationException ex){
	                    failureMsg.append("<br/>第： "+n+" 行数据导入失败：");
	                    List<String> messageList = BeanValidators.extractPropertyAndMessageAsList(ex, ": ");
	                    for (String message : messageList){
	                        failureMsg.append(message+"; ");
	                        failureNum++;
	                    }
	                }catch (Exception ex) {
	                    failureMsg.append("<br/>第： "+n+" 行数据导入失败："+ex.getMessage());
	                }
	            }
	            if (failureNum>0){
	                failureMsg.insert(0, "，失败 "+failureNum+" 条记录，导入信息如下：");
	            }
	            addMessage(redirectAttributes, "已成功导入 "+successNum+" 条记录"+failureMsg);
	        } catch (Exception e) {
	            addMessage(redirectAttributes, "导入记录失败！失败信息："+e.getMessage());
	        }
	        return "redirect:" + adminPath + defaultStr();
	}

	@Override
	@RequiresPermissions("teach:tpTeachExcellentAward:view")
	@RequestMapping(value = "export", method=RequestMethod.POST)
	public String exportFile(TpArticleClassify t, HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) {
		return super.exportFile(t,request,response,redirectAttributes);
	}

	@RequestMapping("/queryData")
	@RequiresPermissions("teach:tpArticleClassify:view")
	@ResponseBody
	public List<TpArticleClassify> queryData(TpArticleClassify tpArticleClassify){
		List<TpArticleClassify> list = tpArticleClassifyService.findList(tpArticleClassify);
		tpArticleClassify.setYear(null);
		tpArticleClassify.setPubData(true);
		list.addAll(tpArticleClassifyService.findList(tpArticleClassify));
		return list;
	}
}