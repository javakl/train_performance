/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.teach.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.web.util.WebUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.ConstantEnum;
import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.persistence.ReturnObject;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.common.utils.excel.ExportExcel;
import com.thinkgem.jeesite.common.web.ExcelController;
import com.thinkgem.jeesite.modules.act.entity.Act;
import com.thinkgem.jeesite.modules.act.utils.ActUtils;
import com.thinkgem.jeesite.modules.sys.security.FormAuthenticationFilter;
import com.thinkgem.jeesite.modules.sys.service.SystemService;
import com.thinkgem.jeesite.modules.sys.utils.UserUtils;
import com.thinkgem.jeesite.modules.teach.entity.TpTeachAchievement;
import com.thinkgem.jeesite.modules.teach.entity.TpTeachLessonBuild;
import com.thinkgem.jeesite.modules.teach.service.TpTeachLessonBuildService;

/**
 * 课程建设Controller
 * @author 康力
 * @version 2018-03-11
 */
@Controller
@RequestMapping(value = "${adminPath}/teach/tpTeachLessonBuild")
public class TpTeachLessonBuildController extends ExcelController<TpTeachLessonBuild> {

	@Autowired
	private TpTeachLessonBuildService tpTeachLessonBuildService;

	@Autowired
	private SystemService systemService;

	@ModelAttribute
	public TpTeachLessonBuild get(@RequestParam(required=false) String id) {
		TpTeachLessonBuild entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = tpTeachLessonBuildService.get(id);
		}
		if (entity == null){
			entity = new TpTeachLessonBuild();
		}
		//根据拥有当前菜单角色的最大的数据范围 判断是否是获取本人数据
		entity.setUserDataScope(systemService.getCurUserDataScopeByPermission("teach:tpTeachLessonBuild:view"));
		if (entity.getIsNewRecord() && entity.getUserDataScope() == 0) {// 个人数据权限，获取个人信息
			entity.setUser(UserUtils.getUser());// 设置人员
			if (entity.getUser() != null) {
				entity.setDepartment(entity.getUser().getOffice());// 设置科室
				if (entity.getDepartment() != null) {
					// 设置教研室
					entity.setTrc(systemService.findParentByOfficeId(entity.getDepartment().getId()));
				}
			}
		}
		return entity;
	}
	
	@RequiresPermissions("teach:tpTeachLessonBuild:view")
	@RequestMapping(value = {"list", ""})
	public String list(TpTeachLessonBuild tpTeachLessonBuild, HttpServletRequest request, HttpServletResponse response, Model model) {
		String str = request.getRequestURI().replaceFirst(request.getContextPath()+Global.getConfig("adminPath"), "");
		model.addAttribute("remark",systemService.getRemark(str));
		//根据拥有当前菜单角色的最大的数据范围 判断是否是获取本人数据
		tpTeachLessonBuild.setUserDataScope(systemService.getCurUserDataScopeByPermission("teach:tpTeachLessonBuild:view"));
		if(tpTeachLessonBuild.getUserDataScope() == 0){
			tpTeachLessonBuild.setUser(UserUtils.getUser());
		}else{//获得当前登录人部门下的数据
			if(tpTeachLessonBuild.getDepartment() == null)tpTeachLessonBuild.setDepartment(UserUtils.getUser().getOffice());
		}
		Page<TpTeachLessonBuild> page = tpTeachLessonBuildService.findPage(new Page<TpTeachLessonBuild>(request, response), tpTeachLessonBuild);
		if(WebUtils.isTrue(request, FormAuthenticationFilter.DEFAULT_MOBILE_PARAM)){//移动端请求，封装数据
			ReturnObject rtobj = new ReturnObject();
			rtobj.setResult(ConstantEnum.success.getCode());
			rtobj.setMsg(ConstantEnum.success.getName());
			rtobj.setData(page);
			return renderString(response, rtobj);
		}
		model.addAttribute("page", page);
		return "modules/teach/tpTeachLessonBuildList";
	}

	@RequiresPermissions("teach:tpTeachLessonBuild:view")
	@RequestMapping(value = "form")
	public String form(TpTeachLessonBuild tpTeachLessonBuild, Model model, HttpServletRequest request, HttpServletResponse response) {
		//如果为新增数据并且数据权限为个人数据的情况下，启动审批流程
		String viewName = "";
		if(tpTeachLessonBuild == null ||
				(tpTeachLessonBuild.getStatus()!= null && tpTeachLessonBuild.getStatus() == 2)||
				(StringUtils.isBlank(tpTeachLessonBuild.getId())&&StringUtils.isBlank(tpTeachLessonBuild.getAct().getProcInsId()))){
			//根据流程名称获取最新的流程实例名称
			String procDefId = null;
			if(systemService.getCurUserDataScopeByPermission("teach:tpTeachLessonBuild:view") == 0){
				procDefId =  systemService.getprocDefIdByProcDefKey(ActUtils.TP_TEACH_LESSON_BUILD[0]);
			}
			Act act = new Act();
			act.setProcDefId(procDefId);
			tpTeachLessonBuild.setAct(act);
			viewName="tpTeachLessonBuildForm";
		} else if (tpTeachLessonBuild.getAct() != null && tpTeachLessonBuild.getAct().getTaskDefKey() != null
				&& tpTeachLessonBuild.getAct().getTaskDefKey().startsWith("audit")
				&&tpTeachLessonBuild.getAct().isTodoTask()) {
			viewName="tpTeachLessonBuildAudit";
		}else{
			viewName="tpTeachLessonBuildView";
			//如果实体类内容未查出，使用ProcInsId查询
			Act act = tpTeachLessonBuild.getAct();
			if(StringUtils.isBlank(tpTeachLessonBuild.getId()) && act != null
					&& StringUtils.isNotBlank(act.getProcInsId())){
				tpTeachLessonBuild.setProcInsId(act.getProcInsId());
				List<TpTeachLessonBuild> list = tpTeachLessonBuildService.findList(tpTeachLessonBuild);
				if(list.size() == 1){
					tpTeachLessonBuild = list.get(0);
					tpTeachLessonBuild.setAct(act);
				}
			}
		}
		if(WebUtils.isTrue(request, FormAuthenticationFilter.DEFAULT_MOBILE_PARAM)){//移动端请求，封装数据
			ReturnObject rtobj = new ReturnObject();
			rtobj.setResult(ConstantEnum.success.getCode());
			rtobj.setMsg(ConstantEnum.success.getName());
			ExportExcel ex = new ExportExcel("", TpTeachLessonBuild.class);
			tpTeachLessonBuild = ex.getMobileDataList(tpTeachLessonBuild);
			tpTeachLessonBuild.setPropertyNameMap(ex.getPropertyNameMap());
			rtobj.setData(tpTeachLessonBuild);
			return renderString(response, rtobj);
		}
		model.addAttribute("tpTeachLessonBuild", tpTeachLessonBuild);
		return "modules/teach/"+viewName;
	}

	@RequiresPermissions("teach:tpTeachLessonBuild:edit")
	@RequestMapping(value = "save")
	public String save(TpTeachLessonBuild tpTeachLessonBuild, Model model, RedirectAttributes redirectAttributes, HttpServletRequest request, HttpServletResponse response) {
		if (!beanValidator(model, tpTeachLessonBuild)){
			return form(tpTeachLessonBuild, model,request,response);
		}
		tpTeachLessonBuildService.save(tpTeachLessonBuild);
		addMessage(redirectAttributes, "保存课程建设成功");
		return "redirect:"+Global.getAdminPath()+"/teach/tpTeachLessonBuild/?repage";
	}
	
	@RequiresPermissions("teach:tpTeachLessonBuild:edit")
	@RequestMapping(value = "delete")
	public String delete(TpTeachLessonBuild tpTeachLessonBuild, RedirectAttributes redirectAttributes) {
		tpTeachLessonBuildService.delete(tpTeachLessonBuild);
		addMessage(redirectAttributes, "删除课程建设成功");
		return "redirect:"+Global.getAdminPath()+"/teach/tpTeachLessonBuild/?repage";
	}

	@Override
	protected TpTeachLessonBuildService getService() {
		return tpTeachLessonBuildService;
	}
	@Override
	public String getTitle() {
		return "课程建设";
	}

	@Override
	public String defaultStr() {
		return "/teach/tpTeachLessonBuild/?repage";
	}

	@Override
	public CrudService baseService() {
		return tpTeachLessonBuildService;
	}
	@Override
	@RequiresPermissions("teach:tpTeachExcellentAward:view")
	@RequestMapping(value = "import/template")
	public String importFileTemplate(HttpServletResponse response, RedirectAttributes redirectAttributes,TpTeachLessonBuild t) {
		return super.importFileTemplate(response,redirectAttributes,t);
	}

	@Override
	@RequiresPermissions("teach:tpTeachExcellentAward:edit")
	@RequestMapping(value = "import", method= RequestMethod.POST)
	public String importFile(MultipartFile file, RedirectAttributes redirectAttributes, TpTeachLessonBuild data) {
		return super.importFile(file,redirectAttributes,data);
	}

	@Override
	@RequiresPermissions("teach:tpTeachExcellentAward:view")
	@RequestMapping(value = "export", method=RequestMethod.POST)
	public String exportFile(TpTeachLessonBuild t, HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) {
		return super.exportFile(t,request,response,redirectAttributes);
	}
	
}