/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.teach.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.teach.entity.TpTeachAward;

/**
 * 讲课比赛DAO接口
 * @author 康力
 * @version 2018-03-11
 */
@MyBatisDao
public interface TpTeachAwardDao extends CrudDao<TpTeachAward> {
	
}