/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.teach.service;

import java.util.List;
import java.util.Map;

import org.activiti.engine.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Maps;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.ActAuditService;
import com.thinkgem.jeesite.modules.act.service.ActTaskService;
import com.thinkgem.jeesite.modules.act.utils.ActUtils;
import com.thinkgem.jeesite.modules.teach.dao.TpTeachAchievementDao;
import com.thinkgem.jeesite.modules.teach.entity.TpTeachAchievement;

/**
 * 教学成果的增删改查Service
 * @author 康力
 * @version 2018-03-11
 */
@Service
@Transactional(readOnly = true)
public class TpTeachAchievementService extends ActAuditService<TpTeachAchievementDao, TpTeachAchievement> {
	
	@Autowired
	private ActTaskService actTaskService;
	@Override
	public TpTeachAchievement get(String id) {
		return super.get(id);
	}
	
	@Override
	public List<TpTeachAchievement> findList(TpTeachAchievement tpTeachAchievement) {
		return super.findList(tpTeachAchievement);
	}
	
	@Override
	public Page<TpTeachAchievement> findPage(Page<TpTeachAchievement> page, TpTeachAchievement tpTeachAchievement) {
		return super.findPage(page, tpTeachAchievement);
	}
	
	@Override
	@Transactional(readOnly = false)
	public void save(TpTeachAchievement tpTeachAchievement) {
		boolean isNewRecord = tpTeachAchievement.getIsNewRecord();
		super.save(tpTeachAchievement);
		//根据实体类状态值判断是否启用申请流程
		if(tpTeachAchievement.getStatus() == 0){//状态为0启动流程
			// 启动流程
			actTaskService.startProcess(ActUtils.TP_TEACH_ACHIEVEMENT[0], ActUtils.TP_TEACH_ACHIEVEMENT[1], tpTeachAchievement.getId(), tpTeachAchievement.getName());
			tpTeachAchievement.setStatus(0);
			dao.update(tpTeachAchievement);
		}else if(tpTeachAchievement.getStatus()==2){//状态2为审核通过，不进入审核流程
			
		}else if(tpTeachAchievement.getAct().getProcInsId() != null){//重申或销毁
			tpTeachAchievement.preUpdate();
			dao.update(tpTeachAchievement);
			tpTeachAchievement.getAct().setComment(("yes".equals(tpTeachAchievement.getAct().getFlag())?"[重申] ":"[销毁] ")+tpTeachAchievement.getAct().getComment());
			// 完成流程任务
			Map<String, Object> vars = Maps.newHashMap();
			vars.put("pass", "yes".equals(tpTeachAchievement.getAct().getFlag())? "1" : "0");
			actTaskService.complete(tpTeachAchievement.getAct().getTaskId(), tpTeachAchievement.getAct().getProcInsId(), tpTeachAchievement.getAct().getComment(), tpTeachAchievement.getRemarks(), vars);
		}
	}
	
	@Override
	@Transactional(readOnly = false)
	public void delete(TpTeachAchievement tpTeachAchievement) {
		super.delete(tpTeachAchievement);
	}
	
}