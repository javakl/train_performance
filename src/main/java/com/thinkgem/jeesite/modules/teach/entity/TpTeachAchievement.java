/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.teach.entity;

import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.ActEntity;
import com.thinkgem.jeesite.common.utils.excel.annotation.ExcelField;
import com.thinkgem.jeesite.modules.sys.entity.Office;
import com.thinkgem.jeesite.modules.sys.entity.User;

import java.util.Date;

/**
 * 教学成果的增删改查Entity
 * @author 康力
 * @version 2018-03-11
 */
public class TpTeachAchievement extends ActEntity<TpTeachAchievement> {
	
	private static final long serialVersionUID = 1L;
	private String name;		// 成果名称
	private User user;		// 课程建设人
	private String userFullName;//姓名（冗余字段用于导入导出列）
	private String level;		// 级别
	private String completeOrderPerson;		// 第几完成人
	private Date winningTime;//成果时间
	private Office department;		// 所属科室
	private Office trc;		// 所属教研室
	private Integer status;		// 审核状态
	private String procInsId;		// 流程实例ID
	private String startWinningTime;//开始-成果时间
	private String endWinningTime;//结束-成果时间
	public TpTeachAchievement() {
		super();
	}

	public TpTeachAchievement(String id){
		super(id);
	}

	@Length(min=1, max=255, message="成果名称长度必须介于 1 和 255 之间")
	@ExcelField(value="name",title="成果名称", align=2, sort=1)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@NotNull(message="工号不能为空")
	@ExcelField(value="user.loginName",title="工号", align=2, sort=5)
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	@ExcelField(value="userFullName",title="课程建设人", align=2, sort=8)
	public String getUserFullName() {
		if(user != null && StringUtils.isNotBlank(user.getName())){
			return user.getName();
		}else
			return userFullName;
	}

	public void setUserFullName(String userFullName) {
		this.userFullName = userFullName;
	}

	@Length(min=1, max=64, message="级别长度必须介于 1 和 64 之间")
	@ExcelField(value="level",title="级别", align=2, sort=10,dictType="level_raward")
	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}
	
	@Length(min=1, max=11, message="第几完成人长度必须介于 1 和 11 之间")
	@ExcelField(value="completeOrderPerson",title="第几完成人", align=2, sort=15,dictType = "complete_human_role")
	public String getCompleteOrderPerson() {
		return completeOrderPerson;
	}

	public void setCompleteOrderPerson(String completeOrderPerson) {
		this.completeOrderPerson = completeOrderPerson;
	}
	
	@NotNull(message="所属科室不能为空")
	@ExcelField(value="department.name",title="所属科室", align=2, sort=20)
	public Office getDepartment() {
		return department;
	}

	public void setDepartment(Office department) {
		this.department = department;
	}
	
	@NotNull(message="所属教研室不能为空")
	@ExcelField(value="trc.name",title="所属教研室", align=2, sort=25)
	public Office getTrc() {
		return trc;
	}

	public void setTrc(Office trc) {
		this.trc = trc;
	}
	@ExcelField(value="status",title="审核状态", align=2, sort=30,dictType="status")
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	@Length(min=0, max=64, message="流程实例ID长度必须介于 0 和 64 之间")
	public String getProcInsId() {
		return procInsId;
	}

	public void setProcInsId(String procInsId) {
		this.procInsId = procInsId;
	}
	@NotNull(message="成果时间不能为空")
	@ExcelField(value="winningTime",title="成果时间", align=2, sort=32)
	public Date getWinningTime() {
		return winningTime;
	}

	public void setWinningTime(Date winningTime) {
		this.winningTime = winningTime;
	}

	public String getStartWinningTime() {
		return startWinningTime;
	}

	public void setStartWinningTime(String startWinningTime) {
		this.startWinningTime = startWinningTime;
	}

	public String getEndWinningTime() {
		return endWinningTime;
	}

	public void setEndWinningTime(String endWinningTime) {
		this.endWinningTime = endWinningTime;
	}
	
}