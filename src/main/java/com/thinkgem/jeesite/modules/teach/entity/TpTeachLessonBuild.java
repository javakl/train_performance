/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.teach.entity;

import java.util.Date;

import javax.validation.constraints.NotNull;

import com.thinkgem.jeesite.common.utils.excel.annotation.ExcelField;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.thinkgem.jeesite.common.persistence.ActEntity;
import com.thinkgem.jeesite.modules.sys.entity.Area;
import com.thinkgem.jeesite.modules.sys.entity.Office;
import com.thinkgem.jeesite.modules.sys.entity.User;

/**
 * 课程建设Entity
 * @author 康力
 * @version 2018-03-11
 */
public class TpTeachLessonBuild extends ActEntity<TpTeachLessonBuild> {
	
	private static final long serialVersionUID = 1L;
	private String name;		// 课程名称
	private User user;		// 课程建设人
	private String userFullName;//姓名（冗余字段用于导入导出列）
	private String level;		// 课题级别
	private Date buildTime;		// 获得时间
	private Office department;		// 所属科室
	private Office trc;		// 教研室
	private Integer status;		// 审核状态
	private String procInsId;		// 流程实例ID

	private Date startDate;

	private Date endDate;

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public TpTeachLessonBuild() {
		super();
	}

	public TpTeachLessonBuild(String id){
		super(id);
	}

	@Length(min=1, max=255, message="课程名称长度必须介于 1 和 255 之间")
	@ExcelField(value="name",title="课程名称", align=2, sort=5)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@NotNull(message="课程建设人工号不能为空")
	@ExcelField(value="user.loginName",title="课程建设人工号", align=2, sort=10)
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	@ExcelField(value="userFullName",title="建设人姓名", align=2, sort=12)
	public String getUserFullName() {
		if(user != null && StringUtils.isNotBlank(user.getName())){
			return user.getName();
		}else
			return userFullName;
	}

	public void setUserFullName(String userFullName) {
		this.userFullName = userFullName;
	}
	@Length(min=1, max=255, message="课题级别长度必须介于 1 和 255 之间")
	@ExcelField(value="level",title="课题级别", align=2, sort=15,dictType = "level_subject")
	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@NotNull(message="获得时间不能为空")
	@ExcelField(value="buildTime",title="获得时间", align=2, sort=20)
	public Date getBuildTime() {
		return buildTime;
	}

	public void setBuildTime(Date buildTime) {
		this.buildTime = buildTime;
	}
	
	@NotNull(message="所属科室不能为空")
	@ExcelField(value="department.name",title="科室", align=2, sort=25)
	public Office getDepartment() {
		return department;
	}

	public void setDepartment(Office department) {
		this.department = department;
	}
	
	@NotNull(message="教研室不能为空")
	@ExcelField(value="rec.name",title="教研室", align=2, sort=30)
	public Office getTrc() {
		return trc;
	}

	public void setTrc(Office trc) {
		this.trc = trc;
	}

	@ExcelField(title="审核状态", align=2, sort=35,dictType = "status")
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
	
	@Length(min=0, max=64, message="流程实例ID长度必须介于 0 和 64 之间")
	public String getProcInsId() {
		return procInsId;
	}

	public void setProcInsId(String procInsId) {
		this.procInsId = procInsId;
	}
	
}