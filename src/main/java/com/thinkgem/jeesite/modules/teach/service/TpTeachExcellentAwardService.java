/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.teach.service;

import com.google.common.collect.Maps;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.ActAuditService;
import com.thinkgem.jeesite.modules.act.service.ActTaskService;
import com.thinkgem.jeesite.modules.act.utils.ActUtils;
import com.thinkgem.jeesite.modules.teach.dao.TpTeachExcellentAwardDao;
import com.thinkgem.jeesite.modules.teach.entity.TpTeachExcellentAward;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 优秀教学奖Service
 * @author 康力
 * @version 2018-03-11
 */
@Service
@Transactional(readOnly = true)
public class TpTeachExcellentAwardService extends ActAuditService<TpTeachExcellentAwardDao, TpTeachExcellentAward> {

	@Autowired
	private ActTaskService actTaskService;

	@Override
	public TpTeachExcellentAward get(String id) {
		return super.get(id);
	}
	
	@Override
	public List<TpTeachExcellentAward> findList(TpTeachExcellentAward tpTeachExcellentAward) {
		return super.findList(tpTeachExcellentAward);
	}
	
	@Override
	public Page<TpTeachExcellentAward> findPage(Page<TpTeachExcellentAward> page, TpTeachExcellentAward tpTeachExcellentAward) {
		return super.findPage(page, tpTeachExcellentAward);
	}
	
	@Override
	@Transactional(readOnly = false)
	public void save(TpTeachExcellentAward tpTeachExcellentAward) {
		boolean isNewRecord = tpTeachExcellentAward.getIsNewRecord();
		super.save(tpTeachExcellentAward);
		//根据实体类状态值判断是否启用申请流程
		if(tpTeachExcellentAward.getStatus() == 0/* && isNewRecord*/){//状态为0启动流程
			// 启动流程
			actTaskService.startProcess( ActUtils.TP_TEACH_EXCELLENT_AWARD[0], ActUtils.TP_TEACH_EXCELLENT_AWARD[1], tpTeachExcellentAward.getId(), tpTeachExcellentAward.getName());
			tpTeachExcellentAward.setStatus(0);
			dao.update(tpTeachExcellentAward);
		}else if(tpTeachExcellentAward.getStatus()==2){//状态2为审核通过，不进入审核流程

		}else if(tpTeachExcellentAward.getAct().getProcInsId() != null){//重申或销毁
			tpTeachExcellentAward.preUpdate();
			dao.update(tpTeachExcellentAward);
			tpTeachExcellentAward.getAct().setComment(("yes".equals(tpTeachExcellentAward.getAct().getFlag())?"[重申] ":"[销毁] ")+tpTeachExcellentAward.getAct().getComment());
			// 完成流程任务
			Map<String, Object> vars = Maps.newHashMap();
			vars.put("pass", "yes".equals(tpTeachExcellentAward.getAct().getFlag())? "1" : "0");
			actTaskService.complete(tpTeachExcellentAward.getAct().getTaskId(), tpTeachExcellentAward.getAct().getProcInsId(), tpTeachExcellentAward.getAct().getComment(), tpTeachExcellentAward.getRemarks(), vars);
		}
	}
	
	@Override
	@Transactional(readOnly = false)
	public void delete(TpTeachExcellentAward tpTeachExcellentAward) {
		super.delete(tpTeachExcellentAward);
	}
	
}