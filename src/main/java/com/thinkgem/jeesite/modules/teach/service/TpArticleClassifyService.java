/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.teach.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.teach.entity.TpArticleClassify;
import com.thinkgem.jeesite.modules.teach.dao.TpArticleClassifyDao;

/**
 * 文章期刊分类Service
 * @author hhm
 * @version 2018-03-28
 */
@Service
@Transactional(readOnly = true)
public class TpArticleClassifyService extends CrudService<TpArticleClassifyDao, TpArticleClassify> {

	public TpArticleClassify get(String id) {
		return super.get(id);
	}
	
	public List<TpArticleClassify> findList(TpArticleClassify tpArticleClassify) {
		return super.findList(tpArticleClassify);
	}
	
	public Page<TpArticleClassify> findPage(Page<TpArticleClassify> page, TpArticleClassify tpArticleClassify) {
		return super.findPage(page, tpArticleClassify);
	}
	
	@Transactional(readOnly = false)
	public void save(TpArticleClassify tpArticleClassify) {
		super.save(tpArticleClassify);
	}
	
	@Transactional(readOnly = false)
	public void delete(TpArticleClassify tpArticleClassify) {
		super.delete(tpArticleClassify);
	}
	
}