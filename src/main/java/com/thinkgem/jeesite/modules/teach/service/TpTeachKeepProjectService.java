/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.teach.service;

import java.util.List;
import java.util.Map;

import com.google.common.collect.Maps;
import com.thinkgem.jeesite.modules.act.service.ActTaskService;
import com.thinkgem.jeesite.modules.act.utils.ActUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.ActAuditService;
import com.thinkgem.jeesite.modules.teach.dao.TpTeachKeepProjectDao;
import com.thinkgem.jeesite.modules.teach.entity.TpTeachKeepProject;

/**
 * 继续教育项目Service
 * @author 康力
 * @version 2018-03-11
 */
@Service
@Transactional(readOnly = true)
public class TpTeachKeepProjectService extends ActAuditService<TpTeachKeepProjectDao, TpTeachKeepProject> {

	@Autowired
	private ActTaskService actTaskService;

	@Override
	public TpTeachKeepProject get(String id) {
		return super.get(id);
	}
	
	@Override
	public List<TpTeachKeepProject> findList(TpTeachKeepProject tpTeachKeepProject) {
		return super.findList(tpTeachKeepProject);
	}
	
	@Override
	public Page<TpTeachKeepProject> findPage(Page<TpTeachKeepProject> page, TpTeachKeepProject tpTeachKeepProject) {
		return super.findPage(page, tpTeachKeepProject);
	}
	
	@Override
	@Transactional(readOnly = false)
	public void save(TpTeachKeepProject tpTeachKeepProject) {
		boolean isNewRecord = tpTeachKeepProject.getIsNewRecord();
		super.save(tpTeachKeepProject);
		//根据实体类状态值判断是否启用申请流程
		if(tpTeachKeepProject.getStatus() == 0/* && isNewRecord*/){//状态为0启动流程
			// 启动流程
			actTaskService.startProcess(ActUtils.TP_TEACH_KEEP_PROJECT[0], ActUtils.TP_TEACH_KEEP_PROJECT[1], tpTeachKeepProject.getId(), tpTeachKeepProject.getName());
			tpTeachKeepProject.setStatus(0);
			dao.update(tpTeachKeepProject);
		}else if(tpTeachKeepProject.getStatus()==2){//状态2为审核通过，不进入审核流程

		}else if(tpTeachKeepProject.getAct().getProcInsId() != null){//重申或销毁
			tpTeachKeepProject.preUpdate();
			dao.update(tpTeachKeepProject);
			tpTeachKeepProject.getAct().setComment(("yes".equals(tpTeachKeepProject.getAct().getFlag())?"[重申] ":"[销毁] ")+tpTeachKeepProject.getAct().getComment());
			// 完成流程任务
			Map<String, Object> vars = Maps.newHashMap();
			vars.put("pass", "yes".equals(tpTeachKeepProject.getAct().getFlag())? "1" : "0");
			actTaskService.complete(tpTeachKeepProject.getAct().getTaskId(), tpTeachKeepProject.getAct().getProcInsId(), tpTeachKeepProject.getAct().getComment(), tpTeachKeepProject.getRemarks(), vars);
		}
	}
	
	@Override
	@Transactional(readOnly = false)
	public void delete(TpTeachKeepProject tpTeachKeepProject) {
		super.delete(tpTeachKeepProject);
	}
	
}