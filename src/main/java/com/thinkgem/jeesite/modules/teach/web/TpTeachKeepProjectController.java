/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.teach.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.web.util.WebUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.ConstantEnum;
import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.persistence.ReturnObject;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.common.utils.excel.ExportExcel;
import com.thinkgem.jeesite.common.web.ExcelController;
import com.thinkgem.jeesite.modules.act.entity.Act;
import com.thinkgem.jeesite.modules.act.utils.ActUtils;
import com.thinkgem.jeesite.modules.sys.security.FormAuthenticationFilter;
import com.thinkgem.jeesite.modules.sys.service.SystemService;
import com.thinkgem.jeesite.modules.sys.utils.UserUtils;
import com.thinkgem.jeesite.modules.teach.entity.TpTeachAchievement;
import com.thinkgem.jeesite.modules.teach.entity.TpTeachKeepProject;
import com.thinkgem.jeesite.modules.teach.service.TpTeachKeepProjectService;

/**
 * 继续教育项目Controller
 * @author 康力
 * @version 2018-03-11
 */
@Controller
@RequestMapping(value = "${adminPath}/teach/tpTeachKeepProject")
public class TpTeachKeepProjectController extends ExcelController<TpTeachKeepProject> {

	@Autowired
	private TpTeachKeepProjectService tpTeachKeepProjectService;

	@Autowired
	private SystemService systemService;

	@ModelAttribute
	public TpTeachKeepProject get(@RequestParam(required=false) String id) {
		TpTeachKeepProject entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = tpTeachKeepProjectService.get(id);
		}
		if (entity == null){
			entity = new TpTeachKeepProject();
		}
		//根据拥有当前菜单角色的最大的数据范围 判断是否是获取本人数据
		entity.setUserDataScope(systemService.getCurUserDataScopeByPermission("teach:tpTeachKeepProject:view"));
		if (entity.getIsNewRecord() && entity.getUserDataScope() == 0) {// 个人数据权限，获取个人信息
			entity.setUser(UserUtils.getUser());// 设置人员
			if (entity.getUser() != null) {
				entity.setDepartment(entity.getUser().getOffice());// 设置科室
				if (entity.getDepartment() != null) {
					// 设置教研室
					entity.setTrc(systemService.findParentByOfficeId(entity.getDepartment().getId()));
				}
			}
		}
		return entity;
	}
	
	@RequiresPermissions("teach:tpTeachKeepProject:view")
	@RequestMapping(value = {"list", ""})
	public String list(TpTeachKeepProject tpTeachKeepProject, HttpServletRequest request, HttpServletResponse response, Model model) {
		String str = request.getRequestURI().replaceFirst(request.getContextPath()+Global.getConfig("adminPath"), "");
		model.addAttribute("remark",systemService.getRemark(str));
		//根据拥有当前菜单角色的最大的数据范围 判断是否是获取本人数据
		tpTeachKeepProject.setUserDataScope(systemService.getCurUserDataScopeByPermission("teach:tpTeachKeepProject:view"));
		if(tpTeachKeepProject.getUserDataScope() == 0){
			tpTeachKeepProject.setUser(UserUtils.getUser());
		}else{//获得当前登录人部门下的数据
			if(tpTeachKeepProject.getDepartment() == null)tpTeachKeepProject.setDepartment(UserUtils.getUser().getOffice());
		}

		Page<TpTeachKeepProject> page = tpTeachKeepProjectService.findPage(new Page<TpTeachKeepProject>(request, response), tpTeachKeepProject);
		if(WebUtils.isTrue(request, FormAuthenticationFilter.DEFAULT_MOBILE_PARAM)){//移动端请求，封装数据
			ReturnObject rtobj = new ReturnObject();
			rtobj.setResult(ConstantEnum.success.getCode());
			rtobj.setMsg(ConstantEnum.success.getName());
			rtobj.setData(page);
			return renderString(response, rtobj);
		}
		model.addAttribute("page", page);
		return "modules/teach/tpTeachKeepProjectList";
	}

	@RequiresPermissions("teach:tpTeachKeepProject:view")
	@RequestMapping(value = "form")
	public String form(TpTeachKeepProject tpTeachKeepProject, Model model, HttpServletRequest request, HttpServletResponse response) {
		//如果为新增数据并且数据权限为个人数据的情况下，启动审批流程
		String viewName = "";
		if(tpTeachKeepProject == null ||
				(tpTeachKeepProject.getStatus()!= null && tpTeachKeepProject.getStatus() == 2)||
				(StringUtils.isBlank(tpTeachKeepProject.getId())&&StringUtils.isBlank(tpTeachKeepProject.getAct().getProcInsId()))){
			//根据流程名称获取最新的流程实例名称
			String procDefId = null;
			if(systemService.getCurUserDataScopeByPermission("teach:tpTeachKeepProject:view") == 0){
				procDefId =  systemService.getprocDefIdByProcDefKey(ActUtils.TP_TEACH_KEEP_PROJECT[0]);
			}
			Act act = new Act();
			act.setProcDefId(procDefId);
			tpTeachKeepProject.setAct(act);
			viewName="tpTeachKeepProjectForm";
		} else if (tpTeachKeepProject.getAct() != null && tpTeachKeepProject.getAct().getTaskDefKey() != null
				&& tpTeachKeepProject.getAct().getTaskDefKey().startsWith("audit")
				&&tpTeachKeepProject.getAct().isTodoTask()) {
			viewName="tpTeachKeepProjectAudit";
		}else{
			viewName="tpTeachKeepProjectView";
			//如果实体类内容未查出，使用ProcInsId查询
			Act act = tpTeachKeepProject.getAct();
			if(StringUtils.isBlank(tpTeachKeepProject.getId()) && act != null
					&& StringUtils.isNotBlank(act.getProcInsId())){
				tpTeachKeepProject.setProcInsId(act.getProcInsId());
				List<TpTeachKeepProject> list = tpTeachKeepProjectService.findList(tpTeachKeepProject);
				if(list.size() == 1){
					tpTeachKeepProject = list.get(0);
					tpTeachKeepProject.setAct(act);
				}
			}
		}
		if(WebUtils.isTrue(request, FormAuthenticationFilter.DEFAULT_MOBILE_PARAM)){//移动端请求，封装数据
			ReturnObject rtobj = new ReturnObject();
			rtobj.setResult(ConstantEnum.success.getCode());
			rtobj.setMsg(ConstantEnum.success.getName());
			ExportExcel ex = new ExportExcel("", TpTeachKeepProject.class);
			tpTeachKeepProject = ex.getMobileDataList(tpTeachKeepProject);
			tpTeachKeepProject.setPropertyNameMap(ex.getPropertyNameMap());
			rtobj.setData(tpTeachKeepProject);
			return renderString(response, rtobj);
		}
		model.addAttribute("tpTeachKeepProject", tpTeachKeepProject);
		return "modules/teach/"+viewName;
	}

	@RequiresPermissions("teach:tpTeachKeepProject:edit")
	@RequestMapping(value = "save")
	public String save(TpTeachKeepProject tpTeachKeepProject, Model model, RedirectAttributes redirectAttributes, HttpServletRequest request, HttpServletResponse response) {
		if (!beanValidator(model, tpTeachKeepProject)){
			return form(tpTeachKeepProject, model,request,response);
		}
		tpTeachKeepProjectService.save(tpTeachKeepProject);
		addMessage(redirectAttributes, "保存继续教育项目成功");
		return "redirect:"+Global.getAdminPath()+"/teach/tpTeachKeepProject/?repage";
	}
	
	@RequiresPermissions("teach:tpTeachKeepProject:edit")
	@RequestMapping(value = "delete")
	public String delete(TpTeachKeepProject tpTeachKeepProject, RedirectAttributes redirectAttributes) {
		tpTeachKeepProjectService.delete(tpTeachKeepProject);
		addMessage(redirectAttributes, "删除继续教育项目成功");
		return "redirect:"+Global.getAdminPath()+"/teach/tpTeachKeepProject/?repage";
	}

	@Override
	protected TpTeachKeepProjectService getService() {
		return tpTeachKeepProjectService;
	}
	@Override
	public String getTitle() {
		return "继续教育项目";
	}

	@Override
	public String defaultStr() {
		return "/teach/tpTeachKeepProject/?repage";
	}

	@Override
	public CrudService baseService() {
		return tpTeachKeepProjectService;
	}
	@Override
	@RequiresPermissions("teach:tpTeachExcellentAward:view")
	@RequestMapping(value = "import/template")
	public String importFileTemplate(HttpServletResponse response, RedirectAttributes redirectAttributes,TpTeachKeepProject t) {
		return super.importFileTemplate(response,redirectAttributes,t);
	}

	@Override
	@RequiresPermissions("teach:tpTeachExcellentAward:edit")
	@RequestMapping(value = "import", method= RequestMethod.POST)
	public String importFile(MultipartFile file, RedirectAttributes redirectAttributes, TpTeachKeepProject data) {
		return super.importFile(file,redirectAttributes,data);
	}

	@Override
	@RequiresPermissions("teach:tpTeachExcellentAward:view")
	@RequestMapping(value = "export", method=RequestMethod.POST)
	public String exportFile(TpTeachKeepProject t, HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) {
		return super.exportFile(t,request,response,redirectAttributes);
	}
	
}