/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.teach.service;

import com.google.common.collect.Maps;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.ActAuditService;
import com.thinkgem.jeesite.modules.act.service.ActTaskService;
import com.thinkgem.jeesite.modules.act.utils.ActUtils;
import com.thinkgem.jeesite.modules.teach.dao.TpTeachLessonBuildDao;
import com.thinkgem.jeesite.modules.teach.entity.TpTeachLessonBuild;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 课程建设Service
 * @author 康力
 * @version 2018-03-11
 */
@Service
@Transactional(readOnly = true)
public class TpTeachLessonBuildService extends ActAuditService<TpTeachLessonBuildDao, TpTeachLessonBuild> {

	@Autowired
	private ActTaskService actTaskService;
	@Override
	public TpTeachLessonBuild get(String id) {
		return super.get(id);
	}
	
	@Override
	public List<TpTeachLessonBuild> findList(TpTeachLessonBuild tpTeachLessonBuild) {
		return super.findList(tpTeachLessonBuild);
	}
	
	@Override
	public Page<TpTeachLessonBuild> findPage(Page<TpTeachLessonBuild> page, TpTeachLessonBuild tpTeachLessonBuild) {
		return super.findPage(page, tpTeachLessonBuild);
	}
	
	@Override
	@Transactional(readOnly = false)
	public void save(TpTeachLessonBuild tpTeachLessonBuild) {
		boolean isNewRecord = tpTeachLessonBuild.getIsNewRecord();
		super.save(tpTeachLessonBuild);
		//根据实体类状态值判断是否启用申请流程
		if(tpTeachLessonBuild.getStatus() == 0/* && isNewRecord*/){//状态为0启动流程
			// 启动流程
			actTaskService.startProcess(ActUtils.TP_TEACH_LESSON_BUILD[0], ActUtils.TP_TEACH_LESSON_BUILD[1], tpTeachLessonBuild.getId(), tpTeachLessonBuild.getName());
			tpTeachLessonBuild.setStatus(0);
			dao.update(tpTeachLessonBuild);
		}else if(tpTeachLessonBuild.getStatus()==2){//状态2为审核通过，不进入审核流程

		}else if(tpTeachLessonBuild.getAct().getProcInsId() != null){//重申或销毁
			tpTeachLessonBuild.preUpdate();
			dao.update(tpTeachLessonBuild);
			tpTeachLessonBuild.getAct().setComment(("yes".equals(tpTeachLessonBuild.getAct().getFlag())?"[重申] ":"[销毁] ")+tpTeachLessonBuild.getAct().getComment());
			// 完成流程任务
			Map<String, Object> vars = Maps.newHashMap();
			vars.put("pass", "yes".equals(tpTeachLessonBuild.getAct().getFlag())? "1" : "0");
			actTaskService.complete(tpTeachLessonBuild.getAct().getTaskId(), tpTeachLessonBuild.getAct().getProcInsId(), tpTeachLessonBuild.getAct().getComment(), tpTeachLessonBuild.getRemarks(), vars);
		}
	}
	
	@Override
	@Transactional(readOnly = false)
	public void delete(TpTeachLessonBuild tpTeachLessonBuild) {
		super.delete(tpTeachLessonBuild);
	}
	
}