/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.teach.entity;

import com.thinkgem.jeesite.common.persistence.ActEntity;
import com.thinkgem.jeesite.common.utils.excel.annotation.ExcelField;
import com.thinkgem.jeesite.modules.sys.entity.Office;
import com.thinkgem.jeesite.modules.sys.entity.User;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * 讲课比赛Entity
 *
 * @author 康力
 * @version 2018-03-11
 */
public class TpTeachAward extends ActEntity<TpTeachAward> {

    private static final long serialVersionUID = 1L;
    private String title;        // 课程名称
    private String name;        // 课程名称(用于移动端展示）
    private User user;        // 获奖人
    private String userFullName;//姓名（冗余字段用于导入导出列）
    private String level;        // 获奖级别
    private String rank;        // 名次
    private Date winningTime;//获奖时间
    private String otherAward;        // 其他奖项
    private Office department;        // 所属科室
    private Office trc;        // 所属教研室
    private Integer status;        // 审核状态
    private String procInsId;        // 流程实例ID
    private boolean isStudent;//是否学员 数据
    private String startWinningTime;//开始-获奖时间
    private String endWinningTime;//结束-获奖时间

    /****学员获奖记录字段**/
    @ExcelField(value = "tutor.name", title = "导师", align = 2, sort = 13,type=3)
    private User tutor;
    /****学员获奖记录字段**/
    public TpTeachAward() {
        super();
    }

    public TpTeachAward(String id) {
        super(id);
    }

    @Length(min = 1, max = 255, message = "名称长度必须介于 1 和 255 之间")
    @ExcelField(value = "title", title = "名称", align = 2, sort = 5)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @NotNull(message = "获奖人登录号不能为空")
    @ExcelField(value = "user.loginName", title = "获奖人工号(登录号)", align = 2, sort = 10)
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @ExcelField(value = "userFullName", title = "获奖人姓名", align = 2, sort = 12)
    public String getUserFullName() {
        if (user != null && StringUtils.isNotBlank(user.getName())) {
            return user.getName();
        } else
            return userFullName;
    }

    public void setUserFullName(String userFullName) {
        this.userFullName = userFullName;
    }

    @Length(min = 1, max = 64, message = "获奖级别长度必须介于 1 和 64 之间")
    @ExcelField(value = "level", title = "获奖级别", align = 2, sort = 15, dictType = "level_raward")
    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    @Length(min = 1, max = 64, message = "名次长度必须介于 1 和 64 之间")
    @ExcelField(value = "rank", title = "名次", align = 2, sort = 20, dictType = "raward_rank")
    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    //@Length(min=1, max=200, message="其他奖项长度必须介于 1 和 200 之间")
    @ExcelField(value = "otherAward", title = "其他奖项", align = 2, sort = 25)
    public String getOtherAward() {
        return otherAward;
    }

    public void setOtherAward(String otherAward) {
        this.otherAward = otherAward;
    }

    @NotNull(message = "所属科室不能为空")
    @ExcelField(value = "department.name", title = "科室", align = 2, sort = 30)
    public Office getDepartment() {
        return department;
    }

    public void setDepartment(Office department) {
        this.department = department;
    }

    @NotNull(message = "所属教研室不能为空")
    @ExcelField(value = "trc.name", title = "教研室", align = 2, sort = 35)
    public Office getTrc() {
        return trc;
    }

    public void setTrc(Office trc) {
        this.trc = trc;
    }

    @ExcelField(title = "审核状态", align = 2, sort = 40, dictType = "status")
    public Integer getStatus() {
        return status;
    }

    @NotNull(message = "获奖时间不能为空")
    @ExcelField(value = "winningTime", title = "获奖时间", align = 2, sort = 32)
    public Date getWinningTime() {
        return winningTime;
    }

    public void setWinningTime(Date winningTime) {
        this.winningTime = winningTime;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Length(min = 0, max = 64, message = "流程实例ID长度必须介于 0 和 64 之间")
    public String getProcInsId() {
        return procInsId;
    }

    public void setProcInsId(String procInsId) {
        this.procInsId = procInsId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean getIsStudent() {
        return isStudent;
    }

    public void setIsStudent(boolean isStudent) {
        this.isStudent = isStudent;
    }

    public String getStartWinningTime() {
        return startWinningTime;
    }

    public void setStartWinningTime(String startWinningTime) {
        this.startWinningTime = startWinningTime;
    }

    public String getEndWinningTime() {
        return endWinningTime;
    }

    public void setEndWinningTime(String endWinningTime) {
        this.endWinningTime = endWinningTime;
    }
    
	public User getTutor() {
		return tutor;
	}

	public void setTutor(User tutor) {
		this.tutor = tutor;
	}

}