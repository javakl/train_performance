package com.thinkgem.jeesite.modules.teach.service;

import com.thinkgem.jeesite.modules.teach.dao.TpTeachCountDao;
import com.thinkgem.jeesite.modules.teach.entity.TpTeachCount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class
TpTeachCountService {
    @Autowired
    private TpTeachCountDao tpTeachCountDao;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");

    public List<TpTeachCount> TpteachCounts(TpTeachCount data) {
        return tpTeachCountDao.queryList(data);
    }

    public Map queryChartData(TpTeachCount data) {
        Map dataMap=new HashMap();String[] names=getNames();
        dataMap.put("legendData",names);
        String[] dateData=getDateData(data.getStartDate(),data.getEndDate());
        dataMap.put("xAxis",dateData);
        List<TpTeachCount> dataList=tpTeachCountDao.queryList(data);
        Integer[] data1=new Integer[dateData.length];
        Integer[] data2=new Integer[dateData.length];
        Integer[] data3=new Integer[dateData.length];
        Integer[] data4=new Integer[dateData.length];
        Integer[] data5=new Integer[dateData.length];
        Integer[] data6=new Integer[dateData.length];
        Integer[] data7=new Integer[dateData.length];
        int i=0;
        for(String str:dateData){
            boolean flag=false;
            for(TpTeachCount tc:dataList){
                if(str.equals(sdf.format(tc.getStartDate()))){
                    data1[i]=tc.getArticleNum();
                    data2[i]=tc.getPatentNum();
                    data3[i]=tc.getSubjectNum();
                    data4[i]=tc.getAchievementNum();
                    data5[i]=tc.getExcellentAwardNum();
                    data6[i]=tc.getAwardNum();
                    data7[i]=tc.getArticlesNum();
                    flag=true;
                }
            }
            if(!flag){
                data1[i]=0;
                data2[i]=0;
                data3[i]=0;
                data4[i]=0;
                data5[i]=0;
                data6[i]=0;
                data7[i]=0;
            }
            i++;
        }
        Map series=null;
        Map[] seriesData=new HashMap[names.length];
        i=1;
        for (String name:names){
            series=new HashMap();
            series.put("name",name);
            series.put("type","bar");
            switch (i){
                case 1:series.put("data",data1);break;
                case 2:series.put("data",data2);break;
                case 3:series.put("data",data3);break;
                case 4:series.put("data",data4);break;
                case 5:series.put("data",data5);break;
                case 6:series.put("data",data6);break;
                case 7:series.put("data",data7);break;
                default:;
            }
            seriesData[(i-1)]=series;
            i++;
        }
        dataMap.put("series",seriesData);
        return dataMap;
    }


    public String[] getDateData(Date start, Date end){
        Calendar bef = Calendar.getInstance();
        Calendar aft = Calendar.getInstance();
        bef.setTime(start);
        aft.setTime(end);
        int mulData=aft.get(Calendar.MONTH)-bef.get(Calendar.MONTH)+12*(aft.get(Calendar.YEAR)-bef.get(Calendar.YEAR));
        String[] datas=new  String[mulData];
        for(int i=0;i<mulData;i++){
            datas[i]=sdf.format(bef.getTime());
            bef.add(Calendar.MONTH, 1);
        }
        return datas;
    }

    public String[] getNames(){
        Map map=TpTeachCount.getTypeMap();
        Set set=map.keySet();
        String names[]=new String[set.size()];
        int i=0;
        for(Object e:set){
            names[i++]=String.valueOf(map.get(e));
        }
        return names;
    }
}
