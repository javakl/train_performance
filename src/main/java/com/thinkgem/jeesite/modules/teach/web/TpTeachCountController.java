package com.thinkgem.jeesite.modules.teach.web;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.utils.DateUtils;
import com.thinkgem.jeesite.common.utils.excel.ExportExcel;
import com.thinkgem.jeesite.modules.teach.entity.TpTeachCount;
import com.thinkgem.jeesite.modules.teach.entity.TpTeachExcellentAward;
import com.thinkgem.jeesite.modules.teach.service.TpTeachCountService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;


/**
 * 统计Controller
 * @author 康力
 * @version 2018-03-11
 */
@Controller
@RequestMapping(value = "${adminPath}/teach/count")
    public class TpTeachCountController {
    @Autowired
    private TpTeachCountService tpTeachCountService;

    @RequestMapping("teachResultList")
    public String list(TpTeachCount data, Model model) {

        if(data.getStartDate()==null){
            Calendar calendar = Calendar.getInstance();
            Date date = new Date(System.currentTimeMillis());
            data.setEndDate(date);
            calendar.setTime(date);
            calendar.add(Calendar.YEAR, -1);
            data.setStartDate(calendar.getTime());
        }
        model.addAttribute("data",data);
        model.addAttribute("types",TpTeachCount.getTypeMap());
        model.addAttribute("TpteachCounts",tpTeachCountService.TpteachCounts(data));
        return "modules/teach/teachResultList";
    }

    @RequestMapping("chartData")
    public Map getChartMap(TpTeachCount data){
        return tpTeachCountService.queryChartData(data);
    }

    @RequestMapping(value = "export", method=RequestMethod.POST)
    public String exportFile(TpTeachCount t, HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) {
        try {
            String fileName = "教学奖励"+ DateUtils.getDate("yyyy-MM-dd")+".xlsx";
            new ExportExcel("教学奖励统计数据",t.getClass()).setDataList(tpTeachCountService.TpteachCounts(t)).write(response, fileName).dispose();
            return null;
        } catch (Exception e) {
            addMessage(redirectAttributes, "导出记录失败！失败信息："+e.getMessage());
        }
        return "redirect:modules/teach/teachResultList";
    }

    protected void addMessage(Model model, String... messages) {
        StringBuilder sb = new StringBuilder();
        for (String message : messages){
            sb.append(message).append(messages.length>1?"<br/>":"");
        }
        model.addAttribute("message", sb.toString());
    }
}
