/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.teach.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.collect.Maps;
import com.thinkgem.jeesite.modules.act.service.ActTaskService;
import com.thinkgem.jeesite.modules.act.utils.ActUtils;
import com.thinkgem.jeesite.modules.sys.entity.Dict;
import com.thinkgem.jeesite.modules.sys.utils.DictUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.ActAuditService;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.teach.dao.TpTeachActiveDao;
import com.thinkgem.jeesite.modules.teach.entity.TpTeachActive;

/**
 * 教学活动表Service
 * @author 康力
 * @version 2018-03-12
 */
@Service
@Transactional(readOnly = true)
public class TpTeachActiveService extends ActAuditService<TpTeachActiveDao, TpTeachActive> {

	@Autowired
	private ActTaskService actTaskService;

	@Override
	public TpTeachActive get(String id) {
		return super.get(id);
	}
	
	@Override
	public List<TpTeachActive> findList(TpTeachActive tpTeachActive) {
		return super.findList(tpTeachActive);
	}
	
	@Override
	public Page<TpTeachActive> findPage(Page<TpTeachActive> page, TpTeachActive tpTeachActive) {
		return super.findPage(page, tpTeachActive);
	}
	@Override
	@Transactional(readOnly = false)
	public void save(TpTeachActive tpTeachActive) {
		super.save(tpTeachActive);
		//根据实体类状态值判断是否启用申请流程
		commonSave(tpTeachActive);
	}

	private void commonSave(TpTeachActive tpTeachActive) {
		if(tpTeachActive.getStatus() == 0){//状态为0启动流程
			// 启动流程
			String teachTypeLabelName = DictUtils.getDictLabel(tpTeachActive.getTeachType(), "teach_type", "");
			String proDefKey=null;
			String businessTab=null;
			if(StringUtils.isNoneEmpty(tpTeachActive.getTeachType())){
				if(ActUtils.TEACH_ACTIVE_TYPE_DEPT.indexOf(tpTeachActive.getTeachType())>=0){
					proDefKey =  ActUtils.TP_TEACH_ACTIVE_DEPT[0];
					businessTab =  ActUtils.TP_TEACH_ACTIVE_DEPT[1];
				}else if(ActUtils.TEACH_ACTIVE_TYPE_JYS.indexOf(tpTeachActive.getTeachType())>=0){
					proDefKey =  ActUtils.TP_TEACH_ACTIVE_JYS[0];
					businessTab =  ActUtils.TP_TEACH_ACTIVE_JYS[1];
				}
			}
			if(proDefKey == null){
				proDefKey =  ActUtils.TP_TEACH_ACTIVE_ADMIN[0];
				businessTab =  ActUtils.TP_TEACH_ACTIVE_ADMIN[1];
			}
			actTaskService.startProcess(proDefKey, businessTab, tpTeachActive.getId(), tpTeachActive.getName()+"-"+teachTypeLabelName);
			tpTeachActive.setStatus(0);
			dao.update(tpTeachActive);
		}else if(tpTeachActive.getStatus()==2){//状态2为审核通过，不进入审核流程

		}else if(tpTeachActive.getAct().getProcInsId() != null){//重申或销毁
			tpTeachActive.preUpdate();
			dao.update(tpTeachActive);
			tpTeachActive.getAct().setComment(("yes".equals(tpTeachActive.getAct().getFlag())?"[重申] ":"[销毁] ")+tpTeachActive.getAct().getComment());
			// 完成流程任务
			Map<String, Object> vars = Maps.newHashMap();
			vars.put("pass", "yes".equals(tpTeachActive.getAct().getFlag())? "1" : "0");
			actTaskService.complete(tpTeachActive.getAct().getTaskId(), tpTeachActive.getAct().getProcInsId(), tpTeachActive.getAct().getComment(), tpTeachActive.getRemarks(), vars);
		}
	}

	@Transactional(readOnly = false)
	public int saves(TpTeachActive tpTeachActive) {
		int t=0;
		if (tpTeachActive.getIsNewRecord()){
			tpTeachActive.preInsert();
			t=dao.insert(tpTeachActive);
		}else{
			tpTeachActive.preUpdate();
			t=dao.update(tpTeachActive);
		}
		//根据实体类状态值判断是否启用申请流程
		commonSave(tpTeachActive);
		return t;
	}

	@Override
	@Transactional(readOnly = false)
	public void delete(TpTeachActive tpTeachActive) {
		super.delete(tpTeachActive);
	}


	public Map<String, Map<String,Object>> statisticTeachType(TpTeachActive query){
		List<Map<String, Object>> maps = dao.statisticTeachType(query);
		Map<String, Map<String,Object>> hashMaps = new HashMap<>();
		Map<String, Object> hashMap = null;
		for(Map<String,Object> m: maps){
			String dn = ((String) m.get("departmentName"));
			if((hashMap = hashMaps.get(dn)) == null){
				hashMap = new HashMap<>();
				hashMaps.put(dn,hashMap);
			  }
			//构造key,在前台遍历list时，方便对应的教学活动类型的统计数据显示在对应的单元格中，同时速度也有保证
			hashMap.put("type"+m.get("teachType"), m.get("total"));
		}
		return hashMaps;
	}

	public List<Map<String, Object>> statisticSingleTeachType(TpTeachActive query){
		return dao.statisticTeachType(query);
	}
}