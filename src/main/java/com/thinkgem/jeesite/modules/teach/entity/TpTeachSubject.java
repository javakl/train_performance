/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.teach.entity;

import java.util.Date;

import javax.validation.constraints.NotNull;

import com.thinkgem.jeesite.common.utils.excel.annotation.ExcelField;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.thinkgem.jeesite.common.persistence.ActEntity;
import com.thinkgem.jeesite.modules.sys.entity.Office;
import com.thinkgem.jeesite.modules.sys.entity.User;

/**
 * 教学课题表Entity
 * @author 康力
 * @version 2018-03-11
 */
public class TpTeachSubject extends ActEntity<TpTeachSubject> {
	
	private static final long serialVersionUID = 1L;
	private String name;		// 课题名称
	private String level;		// 课题级别
	private User user;		// 负责人
	private String userFullName;//姓名（冗余字段用于导入导出列）
	private Date startDate;		// 起始时间
	private Date endDate;		// 结束时间
	private Office office;		// 所属科室
	private Office trc;		// 所属教研室
	private Integer status;		// 审核状态
	private String procInsId;		// 流程实例ID
	
	public TpTeachSubject() {
		super();
	}

	public TpTeachSubject(String id){
		super(id);
	}

	@Length(min=1, max=255, message="课题名称长度必须介于 1 和 255 之间")
	@ExcelField(value="name",title="课题名称", align=2, sort=5)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Length(min=0, max=255, message="课题级别长度必须介于 0 和 255 之间")
	@ExcelField(value="level",title="课题级别", align=2, sort=10,dictType = "level_subject")
	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}
	@NotNull(message="负责人工号不能为空")
	@ExcelField(value="user.loginName",title="负责人工号", align=2, sort=15)
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	@ExcelField(value="userFullName",title="负责人姓名", align=2, sort=18)
	public String getUserFullName() {
		if(user != null && StringUtils.isNotBlank(user.getName())){
			return user.getName();
		}else
			return userFullName;
	}

	public void setUserFullName(String userFullName) {
		this.userFullName = userFullName;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelField(value="startDate",title="开始时间", align=2, sort=20)
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelField(value="endDate",title="结束时间", align=2, sort=25)
	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	@NotNull(message="所属科室不能为空")
	@ExcelField(value="office.name",title="科室", align=2, sort=30)
	public Office getOffice() {
		return office;
	}

	public void setOffice(Office office) {
		this.office = office;
	}
	
	@NotNull(message="所属教研室不能为空")
	@ExcelField(value="trc.name",title="教研室", align=2, sort=35)
	public Office getTrc() {
		return trc;
	}

	public void setTrc(Office trc) {
		this.trc = trc;
	}
	@ExcelField(title="审核状态", align=2, sort=40,dictType = "status")
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
	
	@Length(min=0, max=64, message="流程实例ID长度必须介于 0 和 64 之间")
	public String getProcInsId() {
		return procInsId;
	}

	public void setProcInsId(String procInsId) {
		this.procInsId = procInsId;
	}

}