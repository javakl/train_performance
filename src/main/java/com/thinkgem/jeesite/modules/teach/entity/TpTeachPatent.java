/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.teach.entity;

import java.util.Date;

import javax.validation.constraints.NotNull;

import com.thinkgem.jeesite.common.utils.excel.annotation.ExcelField;
import com.thinkgem.jeesite.modules.sys.entity.User;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.thinkgem.jeesite.common.persistence.ActEntity;
import com.thinkgem.jeesite.modules.sys.entity.Area;
import com.thinkgem.jeesite.modules.sys.entity.Office;

/**
 * 研究专利表Entity
 * @author 康力
 * @version 2018-03-11
 */
public class TpTeachPatent extends ActEntity<TpTeachPatent> {
	
	private static final long serialVersionUID = 1L;
	private String patentName;		// 专利名称
	private String name;		// 专利名称(用于移动端展示）
	private Integer completeOrderPerson;		// 第几完成人
	private Office department;		// 所属科室
	private Office trc;		// 所属教研室
	private String authorName;		// 专利权人
	private String inventioner;		// 发明人
	private String patentType;		// 专利类型
	private Date applyTime;		// 申请日期
	private Date grantTime;		// 授权时间
	private String authContent;		// 作者情况
	private String procInsId;		// 流程实例ID
	private Integer status;		// 审核状态
	private Date beginApplyTime;		// 开始 申请日期
	private Date endApplyTime;		// 结束 申请日期
	private Date beginGrantTime;		// 开始 授权时间
	private Date endGrantTime;		// 结束 授权时间
	private User user;//专利所属人
	private String userFullName;//姓名（冗余字段用于导入导出列）
	public TpTeachPatent() {
		super();
	}

	public TpTeachPatent(String id){
		super(id);
	}

	@Length(min=1, max=100, message="专利名称长度必须介于 1 和 100 之间")
	@ExcelField(value="patentName",title="专利名称", align=2, sort=5)
	public String getPatentName() {
		return patentName;
	}

	public void setPatentName(String patentName) {
		this.patentName = patentName;
	}
	
	@NotNull(message="第几完成人不能为空")
	@ExcelField(value="completeOrderPerson",title="完成人名次", align=2, sort=10)
	public Integer getCompleteOrderPerson() {
		return completeOrderPerson;
	}

	public void setCompleteOrderPerson(Integer completeOrderPerson) {
		this.completeOrderPerson = completeOrderPerson;
	}
	
	@NotNull(message="所属科室不能为空")
	@ExcelField(value="department.name",title="科室", align=2, sort=15)
	public Office getDepartment() {
		return department;
	}

	public void setDepartment(Office department) {
		this.department = department;
	}
	
	@NotNull(message="所属教研室不能为空")
	@ExcelField(value="trc.name",title="教研室", align=2, sort=20)
	public Office getTrc() {
		return trc;
	}

	public void setTrc(Office trc) {
		this.trc = trc;
	}
	
	@Length(min=1, max=200, message="专利权人长度必须介于 1 和 200 之间")
	@ExcelField(value="authorName",title="专利权人", align=2, sort=25)
	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}
	
	@Length(min=1, max=200, message="发明人长度必须介于 1 和 200 之间")
	@ExcelField(value="inventioner",title="发明人", align=2, sort=30)
	public String getInventioner() {
		return inventioner;
	}

	public void setInventioner(String inventioner) {
		this.inventioner = inventioner;
	}
	
	@Length(min=1, max=64, message="专利类型长度必须介于 1 和 64 之间")
	@ExcelField(value="patentType",title="专利类型", align=2, sort=35,dictType = "patent_type")
	public String getPatentType() {
		return patentType;
	}

	public void setPatentType(String patentType) {
		this.patentType = patentType;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd")
	@NotNull(message="申请日期不能为空")
	@ExcelField(value="applyTime",title="申请日期", align=2, sort=40)
	public Date getApplyTime() {
		return applyTime;
	}

	public void setApplyTime(Date applyTime) {
		this.applyTime = applyTime;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd")
	@NotNull(message="授权时间不能为空")
	@ExcelField(value="grantTime",title="授权时间", align=2, sort=45)
	public Date getGrantTime() {
		return grantTime;
	}

	public void setGrantTime(Date grantTime) {
		this.grantTime = grantTime;
	}
	
	@Length(min=1, max=200, message="作者情况长度必须介于 1 和 200 之间")
	@ExcelField(value="authContent",title="作者情况", align=2, sort=50)
	public String getAuthContent() {
		return authContent;
	}

	public void setAuthContent(String authContent) {
		this.authContent = authContent;
	}
	
	@Length(min=0, max=64, message="流程实例ID长度必须介于 0 和 64 之间")
	public String getProcInsId() {
		return procInsId;
	}

	public void setProcInsId(String procInsId) {
		this.procInsId = procInsId;
	}

	@ExcelField(title="审核状态", align=2, sort=55,dictType = "status")
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
	
	public Date getBeginApplyTime() {
		return beginApplyTime;
	}

	public void setBeginApplyTime(Date beginApplyTime) {
		this.beginApplyTime = beginApplyTime;
	}
	
	public Date getEndApplyTime() {
		return endApplyTime;
	}

	public void setEndApplyTime(Date endApplyTime) {
		this.endApplyTime = endApplyTime;
	}
		
	public Date getBeginGrantTime() {
		return beginGrantTime;
	}

	public void setBeginGrantTime(Date beginGrantTime) {
		this.beginGrantTime = beginGrantTime;
	}
	
	public Date getEndGrantTime() {
		return endGrantTime;
	}

	public void setEndGrantTime(Date endGrantTime) {
		this.endGrantTime = endGrantTime;
	}
	@NotNull(message="所属人工号不能为空")
	@ExcelField(value="user.loginName",title="所属人工号", align=2, sort=8)
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	@ExcelField(value="userFullName",title="所属人姓名", align=2, sort=9)
	public String getUserFullName() {
		if(user != null && StringUtils.isNotBlank(user.getName())){
			return user.getName();
		}else
			return userFullName;
	}

	public void setUserFullName(String userFullName) {
		this.userFullName = userFullName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}