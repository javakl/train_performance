/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.teach.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.teach.entity.TpTeachActive;

import java.util.List;
import java.util.Map;

/**
 * 教学活动表DAO接口
 * @author 康力
 * @version 2018-03-12
 */
@MyBatisDao
public interface TpTeachActiveDao extends CrudDao<TpTeachActive> {

    /**
     * 统计各种教学活动的课时，项或者次数
     * @param query
     * @return
     */
   List<Map<String,Object>> statisticTeachType(TpTeachActive query);

}