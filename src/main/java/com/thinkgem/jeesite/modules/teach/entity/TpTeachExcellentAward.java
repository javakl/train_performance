/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.teach.entity;

import javax.validation.constraints.NotNull;

import com.thinkgem.jeesite.common.utils.excel.annotation.ExcelField;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.ActEntity;
import com.thinkgem.jeesite.modules.sys.entity.Area;
import com.thinkgem.jeesite.modules.sys.entity.Office;
import com.thinkgem.jeesite.modules.sys.entity.User;

import java.util.Date;

/**
 * 优秀教学奖Entity
 * @author 康力
 * @version 2018-03-11
 */
public class TpTeachExcellentAward extends ActEntity<TpTeachExcellentAward> {
	
	private static final long serialVersionUID = 1L;
	private User user;		// 所属人
	private String userFullName;//姓名（冗余字段用于导入导出列）
	private String name;		// 奖项名称
	private String level;		// 获奖级别
	private String completeOrderPerson;		// 完成人名次
	private Office department;		// 所属科室
	private Office trc;		// 所属教研室
	private Date winningTime;//获奖时间
	private Integer status;		// 审核状态
	private String procInsId;		// 流程实例ID


	private Date startDate;

	private Date endDate;

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public TpTeachExcellentAward() {
		super();
	}

	public TpTeachExcellentAward(String id){
		super(id);
	}

	@NotNull(message="所属人工号不能为空")
	@ExcelField(value="user.loginName",title="所属人工号", align=2, sort=5)
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	@ExcelField(value="userFullName",title="所属人姓名", align=2, sort=8)
	public String getUserFullName() {
		if(user != null && StringUtils.isNotBlank(user.getName())){
			return user.getName();
		}else
			return userFullName;
	}

	public void setUserFullName(String userFullName) {
		this.userFullName = userFullName;
	}
	@Length(min=1, max=255, message="奖项名称长度必须介于 1 和 255 之间")
	@ExcelField(value="name",title="奖项名称", align=2, sort=10)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Length(min=1, max=64, message="获奖级别长度必须介于 1 和 64 之间")
	@ExcelField(value="level",title="获奖级别", align=2, sort=15,dictType = "level_raward")
	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	@Length(min=1, max=64, message="完成人名次长度必须介于 1 和 64 之间")
	@ExcelField(value="completeOrderPerson",title="完成人名次", align=2, sort=20,dictType = "complete_human_role")
	public String getCompleteOrderPerson() {
		return completeOrderPerson;
	}

	public void setCompleteOrderPerson(String completeOrderPerson) {
		this.completeOrderPerson = completeOrderPerson;
	}
	
	@NotNull(message="所属科室不能为空")
	@ExcelField(value="department.name",title="所属科室", align=2, sort=25)
	public Office getDepartment() {
		return department;
	}

	public void setDepartment(Office department) {
		this.department = department;
	}
	
	@NotNull(message="所属教研室不能为空")
	@ExcelField(value="trc.name",title="所属教研室", align=2, sort=30)
	public Office getTrc() {
		return trc;
	}

	public void setTrc(Office trc) {
		this.trc = trc;
	}

	@NotNull(message="获奖时间不能为空")
	@ExcelField(value="winningTime",title="获奖时间", align=2, sort=32)
	public Date getWinningTime() {
		return winningTime;
	}

	public void setWinningTime(Date winningTime) {
		this.winningTime = winningTime;
	}

	@ExcelField(title="审核状态", align=2, sort=35,dictType = "status")


	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
	
	@Length(min=0, max=64, message="流程实例ID长度必须介于 0 和 64 之间")
	public String getProcInsId() {
		return procInsId;
	}

	public void setProcInsId(String procInsId) {
		this.procInsId = procInsId;
	}
	
}