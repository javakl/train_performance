/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.teach.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.web.util.WebUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.ConstantEnum;
import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.persistence.ReturnObject;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.common.utils.excel.ExportExcel;
import com.thinkgem.jeesite.common.web.ExcelController;
import com.thinkgem.jeesite.modules.act.entity.Act;
import com.thinkgem.jeesite.modules.act.utils.ActUtils;
import com.thinkgem.jeesite.modules.sys.security.FormAuthenticationFilter;
import com.thinkgem.jeesite.modules.sys.service.SystemService;
import com.thinkgem.jeesite.modules.sys.utils.UserUtils;
import com.thinkgem.jeesite.modules.teach.entity.TpTeachAchievement;
import com.thinkgem.jeesite.modules.teach.entity.TpTeachPatent;
import com.thinkgem.jeesite.modules.teach.service.TpTeachPatentService;

/**
 * 教学专利表Controller
 * @author 康力
 * @version 2018-03-11
 */
@Controller
@RequestMapping(value = "${adminPath}/teach/tpTeachPatent")
public class TpTeachPatentController extends ExcelController<TpTeachPatent> {

	@Autowired
	private TpTeachPatentService tpTeachPatentService;
	@Autowired
	private SystemService systemService;

	@ModelAttribute
	public TpTeachPatent get(@RequestParam(required=false) String id) {
		TpTeachPatent entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = tpTeachPatentService.get(id);
		}
		if (entity == null){
			entity = new TpTeachPatent();
		}
		//根据拥有当前菜单角色的最大的数据范围 判断是否是获取本人数据
		entity.setUserDataScope(systemService.getCurUserDataScopeByPermission("teach:tpTeachPatent:view"));
		if (entity.getIsNewRecord() && entity.getUserDataScope() == 0) {// 个人数据权限，获取个人信息
			entity.setUser(UserUtils.getUser());// 设置人员
			if (entity.getUser() != null) {
				entity.setDepartment(entity.getUser().getOffice());// 设置科室
				if (entity.getDepartment() != null) {
					// 设置教研室
					entity.setTrc(systemService.findParentByOfficeId(entity.getDepartment().getId()));
				}
			}
		}
		return entity;
	}
	
	@RequiresPermissions("teach:tpTeachPatent:view")
	@RequestMapping(value = {"list", ""})
	public String list(TpTeachPatent tpTeachPatent, HttpServletRequest request, HttpServletResponse response, Model model) {
		String str = request.getRequestURI().replaceFirst(request.getContextPath()+Global.getConfig("adminPath"), "");
		model.addAttribute("remark",systemService.getRemark(str));
		//根据拥有当前菜单角色的最大的数据范围 判断是否是获取本人数据
		tpTeachPatent.setUserDataScope(systemService.getCurUserDataScopeByPermission("teach:tpTeachPatent:view"));
		if(tpTeachPatent.getUserDataScope() == 0){
			tpTeachPatent.setUser(UserUtils.getUser());
		}else{//获得当前登录人部门下的数据
			if(tpTeachPatent.getDepartment() == null)tpTeachPatent.setDepartment(UserUtils.getUser().getOffice());
		}
		Page<TpTeachPatent> page = tpTeachPatentService.findPage(new Page<TpTeachPatent>(request, response), tpTeachPatent);
		if(WebUtils.isTrue(request, FormAuthenticationFilter.DEFAULT_MOBILE_PARAM)){//移动端请求，封装数据
			ReturnObject rtobj = new ReturnObject();
			rtobj.setResult(ConstantEnum.success.getCode());
			rtobj.setMsg(ConstantEnum.success.getName());
			rtobj.setData(page);
			return renderString(response, rtobj);
		}
		model.addAttribute("page", page);
		return "modules/teach/tpTeachPatentList";
	}

	@RequiresPermissions("teach:tpTeachPatent:view")
	@RequestMapping(value = "form")
	public String form(TpTeachPatent tpTeachPatent, Model model, HttpServletRequest request, HttpServletResponse response) {
		//如果为新增数据并且数据权限为个人数据的情况下，启动审批流程
		String viewName = "";
		if(tpTeachPatent == null ||
				(tpTeachPatent.getStatus()!= null && tpTeachPatent.getStatus() == 2)||
				(StringUtils.isBlank(tpTeachPatent.getId())&&StringUtils.isBlank(tpTeachPatent.getAct().getProcInsId()))){
			//根据流程名称获取最新的流程实例名称
			String procDefId = null;
			if(systemService.getCurUserDataScopeByPermission("teach:tpTeachPatent:view") == 0){
				procDefId =  systemService.getprocDefIdByProcDefKey(ActUtils.TP_TEACH_PATENT[0]);
			}
			Act act = new Act();
			act.setProcDefId(procDefId);
			tpTeachPatent.setAct(act);
			viewName="tpTeachPatentForm";
		} else if (tpTeachPatent.getAct() != null && tpTeachPatent.getAct().getTaskDefKey() != null
				&& tpTeachPatent.getAct().getTaskDefKey().startsWith("audit")
				&&tpTeachPatent.getAct().isTodoTask()) {
			viewName="tpTeachPatentAudit";
		}else{
			viewName="tpTeachPatentView";
			//如果实体类内容未查出，使用ProcInsId查询
			Act act = tpTeachPatent.getAct();
			if(StringUtils.isBlank(tpTeachPatent.getId()) && act != null
					&& StringUtils.isNotBlank(act.getProcInsId())){
				tpTeachPatent.setProcInsId(act.getProcInsId());
				List<TpTeachPatent> list = tpTeachPatentService.findList(tpTeachPatent);
				if(list.size() == 1){
					tpTeachPatent = list.get(0);
					tpTeachPatent.setAct(act);
				}
			}
		}
		if(WebUtils.isTrue(request, FormAuthenticationFilter.DEFAULT_MOBILE_PARAM)){//移动端请求，封装数据
			ReturnObject rtobj = new ReturnObject();
			rtobj.setResult(ConstantEnum.success.getCode());
			rtobj.setMsg(ConstantEnum.success.getName());
			ExportExcel ex = new ExportExcel("教学专利", TpTeachPatent.class);
			tpTeachPatent = ex.getMobileDataList(tpTeachPatent);
			tpTeachPatent.setPropertyNameMap(ex.getPropertyNameMap());
			rtobj.setData(tpTeachPatent);
			return renderString(response, rtobj);
		}
		model.addAttribute("tpTeachPatent", tpTeachPatent);
		return "modules/teach/"+viewName;
	}

	@RequiresPermissions("teach:tpTeachPatent:edit")
	@RequestMapping(value = "save")
	public String save(TpTeachPatent tpTeachPatent, Model model, RedirectAttributes redirectAttributes, HttpServletRequest request, HttpServletResponse response) {
		if (!beanValidator(model, tpTeachPatent)){
			return form(tpTeachPatent, model,request,response);
		}
		tpTeachPatentService.save(tpTeachPatent);
		addMessage(redirectAttributes, "保存教学专利成功");
		return "redirect:"+Global.getAdminPath()+"/teach/tpTeachPatent/?repage";
	}
	
	@RequiresPermissions("teach:tpTeachPatent:edit")
	@RequestMapping(value = "delete")
	public String delete(TpTeachPatent tpTeachPatent, RedirectAttributes redirectAttributes) {
		tpTeachPatentService.delete(tpTeachPatent);
		addMessage(redirectAttributes, "删除教学专利成功");
		return "redirect:"+Global.getAdminPath()+"/teach/tpTeachPatent/?repage";
	}

	@Override
	protected TpTeachPatentService getService() {
		return tpTeachPatentService;
	}
	@Override
	public String getTitle() {
		return "教学专利";
	}

	@Override
	public String defaultStr() {
		return "/teach/tpTeachPatent/?repage";
	}

	@Override
	public CrudService baseService() {
		return tpTeachPatentService;
	}
	@Override
	@RequiresPermissions("teach:tpTeachPatent:view")
	@RequestMapping(value = "import/template")
	public String importFileTemplate(HttpServletResponse response, RedirectAttributes redirectAttributes,TpTeachPatent t) {
		return super.importFileTemplate(response,redirectAttributes,t);
	}

	@Override
	@RequiresPermissions("teach:tpTeachPatent:edit")
	@RequestMapping(value = "import", method= RequestMethod.POST)
	public String importFile(MultipartFile file, RedirectAttributes redirectAttributes, TpTeachPatent data) {
		return super.importFile(file,redirectAttributes,data);
	}

	@Override
	@RequiresPermissions("teach:tpTeachPatent:view")
	@RequestMapping(value = "export", method=RequestMethod.POST)
	public String exportFile(TpTeachPatent t, HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) {
		return super.exportFile(t,request,response,redirectAttributes);
	}
}