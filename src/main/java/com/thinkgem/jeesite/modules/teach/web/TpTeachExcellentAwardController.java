/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.teach.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.web.util.WebUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.ConstantEnum;
import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.persistence.ReturnObject;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.common.utils.excel.ExportExcel;
import com.thinkgem.jeesite.common.web.ExcelController;
import com.thinkgem.jeesite.modules.act.entity.Act;
import com.thinkgem.jeesite.modules.act.utils.ActUtils;
import com.thinkgem.jeesite.modules.sys.security.FormAuthenticationFilter;
import com.thinkgem.jeesite.modules.sys.service.SystemService;
import com.thinkgem.jeesite.modules.sys.utils.UserUtils;
import com.thinkgem.jeesite.modules.teach.entity.TpTeachAchievement;
import com.thinkgem.jeesite.modules.teach.entity.TpTeachExcellentAward;
import com.thinkgem.jeesite.modules.teach.service.TpTeachExcellentAwardService;

/**
 * 优秀教学奖Controller
 * @author 康力
 * @version 2018-03-11
 */
@Controller
@RequestMapping(value = "${adminPath}/teach/tpTeachExcellentAward")
public class TpTeachExcellentAwardController extends ExcelController<TpTeachExcellentAward> {

	@Autowired
	private TpTeachExcellentAwardService tpTeachExcellentAwardService;

	@Autowired
	private SystemService systemService;

	@ModelAttribute
	public TpTeachExcellentAward get(@RequestParam(required=false) String id) {
		TpTeachExcellentAward entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = tpTeachExcellentAwardService.get(id);
		}
		if (entity == null){
			entity = new TpTeachExcellentAward();
		}
		//根据拥有当前菜单角色的最大的数据范围 判断是否是获取本人数据
		entity.setUserDataScope(systemService.getCurUserDataScopeByPermission("teach:tpTeachExcellentAward:view"));
		if (entity.getIsNewRecord() && entity.getUserDataScope() == 0) {// 个人数据权限，获取个人信息
			entity.setUser(UserUtils.getUser());// 设置人员
			if (entity.getUser() != null) {
				entity.setDepartment(entity.getUser().getOffice());// 设置科室
				if (entity.getDepartment() != null) {
					// 设置教研室
					entity.setTrc(systemService.findParentByOfficeId(entity.getDepartment().getId()));
				}
			}
		}
		return entity;
	}
	
	@RequiresPermissions("teach:tpTeachExcellentAward:view")
	@RequestMapping(value = {"list", ""})
	public String list(TpTeachExcellentAward tpTeachExcellentAward, HttpServletRequest request, HttpServletResponse response, Model model) {
		String str = request.getRequestURI().replaceFirst(request.getContextPath()+Global.getConfig("adminPath"), "");
		model.addAttribute("remark",systemService.getRemark(str));
		//根据拥有当前菜单角色的最大的数据范围 判断是否是获取本人数据
		tpTeachExcellentAward.setUserDataScope(systemService.getCurUserDataScopeByPermission("teach:tpTeachExcellentAward:view"));
		if(tpTeachExcellentAward.getUserDataScope() == 0){
			tpTeachExcellentAward.setUser(UserUtils.getUser());
		}else{//获得当前登录人部门下的数据
			if(tpTeachExcellentAward.getDepartment() == null)tpTeachExcellentAward.setDepartment(UserUtils.getUser().getOffice());
		}
		Page<TpTeachExcellentAward> page = tpTeachExcellentAwardService.findPage(new Page<TpTeachExcellentAward>(request, response), tpTeachExcellentAward);
		if(WebUtils.isTrue(request, FormAuthenticationFilter.DEFAULT_MOBILE_PARAM)){//移动端请求，封装数据
			ReturnObject rtobj = new ReturnObject();
			rtobj.setResult(ConstantEnum.success.getCode());
			rtobj.setMsg(ConstantEnum.success.getName());
			rtobj.setData(page);
			return renderString(response, rtobj);
		}
		model.addAttribute("page", page);
		return "modules/teach/tpTeachExcellentAwardList";
	}

	@RequiresPermissions("teach:tpTeachExcellentAward:view")
	@RequestMapping(value = "form")
	public String form(TpTeachExcellentAward tpTeachExcellentAward, Model model, HttpServletRequest request, HttpServletResponse response) {
		//如果为新增数据并且数据权限为个人数据的情况下，启动审批流程
		String viewName = "";
		if(tpTeachExcellentAward == null ||
				(tpTeachExcellentAward.getStatus()!= null && tpTeachExcellentAward.getStatus() == 2)||
				(StringUtils.isBlank(tpTeachExcellentAward.getId())&&StringUtils.isBlank(tpTeachExcellentAward.getAct().getProcInsId()))){
			//根据流程名称获取最新的流程实例名称
			String procDefId = null;
			if(systemService.getCurUserDataScopeByPermission("teach:tpTeachExcellentAward:view") == 0){
				procDefId =  systemService.getprocDefIdByProcDefKey(ActUtils.TP_TEACH_EXCELLENT_AWARD[0]);
			}
			Act act = new Act();
			act.setProcDefId(procDefId);
			tpTeachExcellentAward.setAct(act);
			viewName="tpTeachExcellentAwardForm";
		} else if (tpTeachExcellentAward.getAct() != null && tpTeachExcellentAward.getAct().getTaskDefKey() != null
				&& tpTeachExcellentAward.getAct().getTaskDefKey().startsWith("audit")
				&&tpTeachExcellentAward.getAct().isTodoTask()) {
			viewName="tpTeachExcellentAwardAudit";
		}else{
			viewName="tpTeachExcellentAwardView";
			//如果实体类内容未查出，使用ProcInsId查询
			Act act = tpTeachExcellentAward.getAct();
			if(StringUtils.isBlank(tpTeachExcellentAward.getId()) && act != null
					&& StringUtils.isNotBlank(act.getProcInsId())){
				tpTeachExcellentAward.setProcInsId(act.getProcInsId());
				List<TpTeachExcellentAward> list = tpTeachExcellentAwardService.findList(tpTeachExcellentAward);
				if(list.size() == 1){
					tpTeachExcellentAward = list.get(0);
					tpTeachExcellentAward.setAct(act);
				}
			}
		}
		if(WebUtils.isTrue(request, FormAuthenticationFilter.DEFAULT_MOBILE_PARAM)){//移动端请求，封装数据
			ReturnObject rtobj = new ReturnObject();
			rtobj.setResult(ConstantEnum.success.getCode());
			rtobj.setMsg(ConstantEnum.success.getName());
			ExportExcel ex = new ExportExcel("", TpTeachExcellentAward.class);
			tpTeachExcellentAward = ex.getMobileDataList(tpTeachExcellentAward);
			tpTeachExcellentAward.setPropertyNameMap(ex.getPropertyNameMap());
			rtobj.setData(tpTeachExcellentAward);
			return renderString(response, rtobj);
		}
		model.addAttribute("tpTeachExcellentAward", tpTeachExcellentAward);
		return "modules/teach/"+viewName;
	}

	@RequiresPermissions("teach:tpTeachExcellentAward:edit")
	@RequestMapping(value = "save")
	public String save(TpTeachExcellentAward tpTeachExcellentAward, Model model, RedirectAttributes redirectAttributes, HttpServletRequest request, HttpServletResponse response) {
		if (!beanValidator(model, tpTeachExcellentAward)){
			return form(tpTeachExcellentAward, model,request,response);
		}
		tpTeachExcellentAwardService.save(tpTeachExcellentAward);
		addMessage(redirectAttributes, "保存优秀教学奖成功");
		return "redirect:"+Global.getAdminPath()+"/teach/tpTeachExcellentAward/?repage";
	}
	
	@RequiresPermissions("teach:tpTeachExcellentAward:edit")
	@RequestMapping(value = "delete")
	public String delete(TpTeachExcellentAward tpTeachExcellentAward, RedirectAttributes redirectAttributes) {
		tpTeachExcellentAwardService.delete(tpTeachExcellentAward);
		addMessage(redirectAttributes, "删除优秀教学奖成功");
		return "redirect:"+ Global.getAdminPath()+"/teach/tpTeachExcellentAward/?repage";
	}

	@Override
	protected TpTeachExcellentAwardService getService() {
		return tpTeachExcellentAwardService;
	}
	@Override
	public String getTitle() {
		return "优秀教学奖";
	}

	@Override
	public String defaultStr() {
		return "/teach/tpTeachExcellentAward/?repage";
	}

	@Override
	public CrudService baseService() {
		return tpTeachExcellentAwardService;
	}

	@Override
	@RequiresPermissions("teach:tpTeachExcellentAward:view")
	@RequestMapping(value = "import/template")
	public String importFileTemplate(HttpServletResponse response, RedirectAttributes redirectAttributes,TpTeachExcellentAward t) {
		return super.importFileTemplate(response,redirectAttributes,t);
	}

	@Override
	@RequiresPermissions("teach:tpTeachExcellentAward:edit")
	@RequestMapping(value = "import", method= RequestMethod.POST)
	public String importFile(MultipartFile file, RedirectAttributes redirectAttributes, TpTeachExcellentAward data) {
		return super.importFile(file,redirectAttributes,data);
	}

	@Override
	@RequiresPermissions("teach:tpTeachExcellentAward:view")
	@RequestMapping(value = "export", method=RequestMethod.POST)
	public String exportFile(TpTeachExcellentAward t, HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) {
		return super.exportFile(t,request,response,redirectAttributes);
	}
}