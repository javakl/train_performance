/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.teach.entity;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.thinkgem.jeesite.common.persistence.ActEntity;
import com.thinkgem.jeesite.common.utils.excel.annotation.ExcelField;
import com.thinkgem.jeesite.modules.sys.entity.Office;
import com.thinkgem.jeesite.modules.sys.entity.User;

/**
 * 教学活动表Entity
 * @author 康力
 * @version 2018-03-12
 */
public class TpTeachActive extends ActEntity<TpTeachActive> {
	private static final long serialVersionUID = 1L;
	private String name;		// 课程名
	private User user;		// 授课老师
	private String userFullName;//姓名（冗余字段用于导入导出列）
	private String teachType;		// 活动类型
	private Double stuNum;		// 带教量
	private Double score;		//分数
	private Date startTime;		// 开始时间
	private Date endTime;		// 结束时间
	private String stuType;		// 学生类别
	private String stuGrade;		// 学员年级
	private Office department;		// 所属科室
	private Office trc;		// 所属教研室
	private String administrative;		// 管理部门
	private String activePlace;		// 活动地点
	private String activeIntroduce;		// 活动介绍
	private Integer status;		// 审核状态
	private String procInsId;		// 流程实例ID
	/**
	 * 教学活动类型values
	 */
	private String teachTypeValues;


	private String  teachTypes;
	private String  files;

	public String getTeachTypes() {
		return teachTypes;
	}

	public void setTeachTypes(String teachTypes) {
		this.teachTypes = teachTypes;
	}

	public TpTeachActive() {
		super();
	}

	public TpTeachActive(String id){
		super(id);
	}

	@Length(min=1, max=50, message="课程名长度必须介于 1 和 50 之间")
	@ExcelField(value="name",title="课程名称", align=2, sort=5)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@NotNull(message="未能匹配对应的老师，请查验数据")
	@ExcelField(value="user.loginName",title="老师工号", align=2, sort=10)
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	@ExcelField(value="userFullName",title="老师姓名", align=2, sort=12)
	public String getUserFullName() {
		if(user != null && StringUtils.isNotBlank(user.getName())){
			return user.getName();
		}else
			return userFullName;
	}

	public void setUserFullName(String userFullName) {
		this.userFullName = userFullName;
	}
	
	@Length(min=1, max=64, message="活动类型不在选项列表中")
	@ExcelField(value="teachType",title="活动类型", align=2, sort=15,dictType = "teach_type")
	public String getTeachType() {
		return teachType;
	}

	public void setTeachType(String teachType) {
		this.teachType = teachType;
	}
	
//	@Length(min=1, max=11, message="带教量长度必须介于 1 和 11 之间")
	@ExcelField(value="stuNum",title="带教量", align=2, sort=20)
	public Double getStuNum() {
		return stuNum;
	}

	public void setStuNum(Double stuNum) {
		this.stuNum = stuNum;
	}

	@ExcelField(value="score",title="分数", align=2, sort=21)
	public Double getScore() {
		return score;
	}

	public void setScore(Double score) {
		this.score = score;
	}

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@NotNull(message="开始时间不能为空")
	@ExcelField(value="startTime",title="开始时间", align=2, sort=25)
	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	//@NotNull(message="结束时间不能为空")
	@ExcelField(value="endTime",title="结束时间", align=2, sort=30)
	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	@Length(min=0, max=64, message="学生类别长度必须介于 1 和 64 之间")
	@ExcelField(value="stuType",title="学生类别", align=2, sort=35,dictType = "stu_type")
	public String getStuType() {
		return stuType;
	}

	public void setStuType(String stuType) {
		this.stuType = stuType;
	}
	
	@Length(min=0, max=20, message="学员年级长度必须介于 1 和 20 之间")
	@ExcelField(value="stuGrade",title="学员年级", align=2, sort=40,dictType = "stu_grade")
	public String getStuGrade() {
		return stuGrade;
	}

	public void setStuGrade(String stuGrade) {
		this.stuGrade = stuGrade;
	}
	
	@NotNull(message="所属科室不能为空")
	@ExcelField(value="department.name",title="科室", align=2, sort=45)
	public Office getDepartment() {
		return department;
	}

	public void setDepartment(Office department) {
		this.department = department;
	}
	
	@NotNull(message="所属教研室不能为空")
	@ExcelField(value="trc.name",title="教研室", align=2, sort=50)
	public Office getTrc() {
		return trc;
	}

	public void setTrc(Office trc) {
		this.trc = trc;
	}
	
	@Length(min=0, max=255, message="管理部门长度必须介于 1 和 255 之间")
	@ExcelField(title="管理部门", align=2, sort=55)
	public String getAdministrative() {
		return administrative;
	}

	public void setAdministrative(String administrative) {
		this.administrative = administrative;
	}
	
	@Length(min=0, max=200, message="活动地点长度必须介于 1 和 200 之间")
	@ExcelField(value="activePlace",title="活动地点", align=2, sort=60)
	public String getActivePlace() {
		return activePlace;
	}


	public void setActivePlace(String activePlace) {
		this.activePlace = activePlace;
	}

	@ExcelField(value="activeIntroduce",title="活动介绍", align=2, sort=65)
	public String getActiveIntroduce() {
		return activeIntroduce;
	}

	public void setActiveIntroduce(String activeIntroduce) {
		this.activeIntroduce = activeIntroduce;
	}

	@ExcelField(title="审核状态", align=2, sort=70,dictType = "status")
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
	
	@Length(min=0, max=64, message="流程实例ID长度必须介于 0 和 64 之间")
	public String getProcInsId() {
		return procInsId;
	}

	public void setProcInsId(String procInsId) {
		this.procInsId = procInsId;
	}

	public String getTeachTypeValues() {
		return teachTypeValues;
	}

	public void setTeachTypeValues(String teachTypeValues) {
		this.teachTypeValues = teachTypeValues;
	}

	public String getFiles() {
		return files;
	}

	public void setFiles(String files) {
		this.files = files;
	}

}