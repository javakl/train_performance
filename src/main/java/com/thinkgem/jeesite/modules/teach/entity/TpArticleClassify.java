/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.teach.entity;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.ActEntity;
import com.thinkgem.jeesite.common.utils.excel.annotation.ExcelField;

/**
 * 文章期刊分类Entity
 * @author hhm
 * @version 2018-03-28
 */
public class TpArticleClassify extends ActEntity<TpArticleClassify> {
	
	private static final long serialVersionUID = 1L;
	private String name;		// 期刊名称
	private String type;		// 期刊分类
	private String category;		// 期刊类别
	private String year;		// 年限
	private boolean pubData;
	public TpArticleClassify() {
		super();
	}

	public TpArticleClassify(String id){
		super(id);
	}

	@Length(min=1, max=100, message="期刊名称长度必须介于 1 和 100 之间")
	@ExcelField(title="期刊名称", align=2, sort=1)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Length(min=1, max=64, message="期刊分类长度必须介于 1 和 64 之间")
	@ExcelField(title="期刊分类", align=2, sort=1,dictType="journal_type")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	@Length(min=0, max=50, message="期刊类别长度必须介于 0 和 50 之间")
	@ExcelField(title="期刊类别", align=2, sort=1)
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
	
	@Length(min=0, max=10, message="年限长度必须介于 0 和 10 之间")
	@ExcelField(title="年限", align=2, sort=1)
	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public boolean isPubData() {
		return pubData;
	}

	public void setPubData(boolean pubData) {
		this.pubData = pubData;
	}
	
}