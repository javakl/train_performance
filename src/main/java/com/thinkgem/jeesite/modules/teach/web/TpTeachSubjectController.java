/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.teach.web;

import com.thinkgem.jeesite.common.ConstantEnum;
import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.persistence.ReturnObject;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.common.utils.excel.ExportExcel;
import com.thinkgem.jeesite.common.web.ExcelController;
import com.thinkgem.jeesite.modules.act.entity.Act;
import com.thinkgem.jeesite.modules.act.utils.ActUtils;
import com.thinkgem.jeesite.modules.sys.security.FormAuthenticationFilter;
import com.thinkgem.jeesite.modules.sys.service.SystemService;
import com.thinkgem.jeesite.modules.sys.utils.UserUtils;
import com.thinkgem.jeesite.modules.teach.entity.TpTeachAchievement;
import com.thinkgem.jeesite.modules.teach.entity.TpTeachSubject;
import com.thinkgem.jeesite.modules.teach.service.TpTeachSubjectService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.web.util.WebUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 教学课题表Controller
 * @author 康力
 * @version 2018-03-11
 */
@Controller
@RequestMapping(value = "${adminPath}/teach/tpTeachSubject")
public class TpTeachSubjectController extends ExcelController<TpTeachSubject> {

	@Autowired
	private TpTeachSubjectService tpTeachSubjectService;
	@Autowired
	private SystemService systemService;

	@ModelAttribute
	public TpTeachSubject get(@RequestParam(required=false) String id) {
		TpTeachSubject entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = tpTeachSubjectService.get(id);
		}
		if (entity == null){
			entity = new TpTeachSubject();
		}
		//根据拥有当前菜单角色的最大的数据范围 判断是否是获取本人数据
		entity.setUserDataScope(systemService.getCurUserDataScopeByPermission("teach:tpTeachSubject:view"));
		if (entity.getIsNewRecord() && entity.getUserDataScope() == 0) {// 个人数据权限，获取个人信息
			entity.setUser(UserUtils.getUser());// 设置人员
			if (entity.getUser() != null) {
				entity.setOffice(entity.getUser().getOffice());// 设置科室
				if (entity.getOffice() != null) {
					// 设置教研室
					entity.setTrc(systemService.findParentByOfficeId(entity.getOffice().getId()));
				}
			}
		}
		return entity;
	}

	@RequiresPermissions("teach:tpTeachSubject:view")
	@RequestMapping(value = {"list", ""})
	public String list(TpTeachSubject tpTeachSubject, HttpServletRequest request, HttpServletResponse response, Model model) {
		String str = request.getRequestURI().replaceFirst(request.getContextPath()+Global.getConfig("adminPath"), "");
		model.addAttribute("remark",systemService.getRemark(str));
		//根据拥有当前菜单角色的最大的数据范围 判断是否是获取本人数据
		tpTeachSubject.setUserDataScope(systemService.getCurUserDataScopeByPermission("teach:tpTeachSubject:view"));
		if(tpTeachSubject.getUserDataScope() == 0){
			tpTeachSubject.setUser(UserUtils.getUser());
		}else{//获得当前登录人部门下的数据
			if(tpTeachSubject.getOffice() == null)tpTeachSubject.setOffice(UserUtils.getUser().getOffice());
		}
		Page<TpTeachSubject> page = tpTeachSubjectService.findPage(new Page<TpTeachSubject>(request, response), tpTeachSubject);
		if(WebUtils.isTrue(request, FormAuthenticationFilter.DEFAULT_MOBILE_PARAM)){//移动端请求，封装数据
			ReturnObject rtobj = new ReturnObject();
			rtobj.setResult(ConstantEnum.success.getCode());
			rtobj.setMsg(ConstantEnum.success.getName());
			rtobj.setData(page);
			return renderString(response, rtobj);
		}
		model.addAttribute("page", page);
		return "modules/teach/tpTeachSubjectList";
	}

	@RequiresPermissions("teach:tpTeachSubject:view")
	@RequestMapping(value = "form")
	public String form(TpTeachSubject tpTeachSubject, Model model, HttpServletRequest request, HttpServletResponse response) {
		String viewName = "";
		if(tpTeachSubject == null ||
				(tpTeachSubject.getStatus()!= null && tpTeachSubject.getStatus() == 2)||
				(StringUtils.isBlank(tpTeachSubject.getId())&&StringUtils.isBlank(tpTeachSubject.getAct().getProcInsId()))){
			//根据流程名称获取最新的流程实例名称
			String procDefId = null;
			if(systemService.getCurUserDataScopeByPermission("teach:tpTeachSubject:view") == 0){
				procDefId =  systemService.getprocDefIdByProcDefKey(ActUtils.TP_TEACH_KEEP_PROJECT[0]);
			}
			Act act = new Act();
			act.setProcDefId(procDefId);
			tpTeachSubject.setAct(act);
			viewName="tpTeachSubjectForm";
		} else if (tpTeachSubject.getAct() != null && tpTeachSubject.getAct().getTaskDefKey() != null
				&& tpTeachSubject.getAct().getTaskDefKey().startsWith("audit")
				&&tpTeachSubject.getAct().isTodoTask()) {
			viewName="tpTeachSubjectAudit";
		}else{
			viewName="tpTeachSubjectView";
			//如果实体类内容未查出，使用ProcInsId查询
			Act act = tpTeachSubject.getAct();
			if(StringUtils.isBlank(tpTeachSubject.getId()) && act != null
					&& StringUtils.isNotBlank(act.getProcInsId())){
				tpTeachSubject.setProcInsId(act.getProcInsId());
				List<TpTeachSubject> list = tpTeachSubjectService.findList(tpTeachSubject);
				if(list.size() == 1){
					tpTeachSubject = list.get(0);
					tpTeachSubject.setAct(act);
				}
			}
		}
		if(WebUtils.isTrue(request, FormAuthenticationFilter.DEFAULT_MOBILE_PARAM)){//移动端请求，封装数据
			ReturnObject rtobj = new ReturnObject();
			rtobj.setResult(ConstantEnum.success.getCode());
			rtobj.setMsg(ConstantEnum.success.getName());
			ExportExcel ex = new ExportExcel("教学成果", TpTeachSubject.class);
			tpTeachSubject = ex.getMobileDataList(tpTeachSubject);
			tpTeachSubject.setPropertyNameMap(ex.getPropertyNameMap());
			rtobj.setData(tpTeachSubject);
			return renderString(response, rtobj);
		}
		model.addAttribute("tpTeachSubject", tpTeachSubject);
		return "modules/teach/"+viewName;
	}

	@RequiresPermissions("teach:tpTeachSubject:edit")
	@RequestMapping(value = "save")
	public String save(TpTeachSubject tpTeachSubject, Model model, RedirectAttributes redirectAttributes, HttpServletRequest request, HttpServletResponse response) {
		if (!beanValidator(model, tpTeachSubject)){
			return form(tpTeachSubject, model,request,response);
		}
		tpTeachSubjectService.save(tpTeachSubject);
		addMessage(redirectAttributes, "保存教学课题表成功");
		return "redirect:"+Global.getAdminPath()+"/teach/tpTeachSubject/?repage";
	}
	
	@RequiresPermissions("teach:tpTeachSubject:edit")
	@RequestMapping(value = "delete")
	public String delete(TpTeachSubject tpTeachSubject, RedirectAttributes redirectAttributes) {
		tpTeachSubjectService.delete(tpTeachSubject);
		addMessage(redirectAttributes, "删除教学课题表成功");
		return "redirect:"+Global.getAdminPath()+"/teach/tpTeachSubject/?repage";
	}

	@Override
	protected TpTeachSubjectService getService() {
		return tpTeachSubjectService;
	}
	@Override
	public String getTitle() {
		return "教学课题";
	}

	@Override
	public String defaultStr() {
		return "/teach/tpTeachSubject/?repage";
	}

	@Override
	public CrudService baseService() {
		return tpTeachSubjectService;
	}
	@Override
	@RequiresPermissions("teach:tpTeachPatent:view")
	@RequestMapping(value = "import/template")
	public String importFileTemplate(HttpServletResponse response, RedirectAttributes redirectAttributes,TpTeachSubject t) {
		return super.importFileTemplate(response,redirectAttributes,t);
	}

	@Override
	@RequiresPermissions("teach:tpTeachPatent:edit")
	@RequestMapping(value = "import", method= RequestMethod.POST)
	public String importFile(MultipartFile file, RedirectAttributes redirectAttributes, TpTeachSubject data) {
		return super.importFile(file,redirectAttributes,data);
	}

	@Override
	@RequiresPermissions("teach:tpTeachPatent:view")
	@RequestMapping(value = "export", method=RequestMethod.POST)
	public String exportFile(TpTeachSubject t, HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) {
		return super.exportFile(t,request,response,redirectAttributes);
	}
}