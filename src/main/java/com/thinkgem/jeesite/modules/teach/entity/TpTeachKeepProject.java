/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.teach.entity;

import java.util.Date;

import javax.validation.constraints.NotNull;

import com.thinkgem.jeesite.common.utils.excel.annotation.ExcelField;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.thinkgem.jeesite.common.persistence.ActEntity;
import com.thinkgem.jeesite.modules.sys.entity.Area;
import com.thinkgem.jeesite.modules.sys.entity.Office;
import com.thinkgem.jeesite.modules.sys.entity.User;

/**
 * 继续教育项目Entity
 * @author 康力
 * @version 2018-03-11
 */
public class TpTeachKeepProject extends ActEntity<TpTeachKeepProject> {
	
	private static final long serialVersionUID = 1L;
	private String name;		// 项目名称
	private String level;		// 项目级别
	private Date holdTime;		// 举办时间
	private Integer joinNum;		// 参加人数
	private User user;		// 项目负责人
	private String userFullName;//姓名（冗余字段用于导入导出列）
	private Office department;		// 所属科室
	private Office trc;		// 所属教研室
	private Integer status;		// 审核状态
	private String procInsId;		// 流程实例ID
	private Date beginHoldTime;		// 开始 举办时间
	private Date endHoldTime;		// 结束 举办时间
	
	public TpTeachKeepProject() {
		super();
	}

	public TpTeachKeepProject(String id){
		super(id);
	}

	@Length(min=1, max=255, message="项目名称长度必须介于 1 和 255 之间")
	@ExcelField(value="name",title="继教项目名称", align=2, sort=5)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Length(min=1, max=255, message="项目级别长度必须介于 1 和 255 之间")
	@ExcelField(value="level",title="项目级别", align=2, sort=10,dictType = "level_project")
	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@NotNull(message="举办时间不能为空")
	@ExcelField(value="holdTime",title="举办时间", align=2, sort=15)
	public Date getHoldTime() {
		return holdTime;
	}

	public void setHoldTime(Date holdTime) {
		this.holdTime = holdTime;
	}
	
	@NotNull(message="参加人数不能为空")
	@ExcelField(value="joinNum",title="参加人数", align=2, sort=20)
	public Integer getJoinNum() {
		return joinNum;
	}

	public void setJoinNum(Integer joinNum) {
		this.joinNum = joinNum;
	}
	
	@NotNull(message="项目负责人工号不能为空")
	@ExcelField(value="user.loginName",title="项目负责人工号", align=2, sort=25)
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	@ExcelField(value="userFullName",title="负责人姓名", align=2, sort=28)
	public String getUserFullName() {
		if(user != null && StringUtils.isNotBlank(user.getName())){
			return user.getName();
		}else
			return userFullName;
	}

	public void setUserFullName(String userFullName) {
		this.userFullName = userFullName;
	}
	
	@NotNull(message="所属科室不能为空")
	@ExcelField(value="department.name",title="科室", align=2, sort=30)
	public Office getDepartment() {
		return department;
	}

	public void setDepartment(Office department) {
		this.department = department;
	}
	
	@NotNull(message="所属教研室不能为空")
	@ExcelField(value="trc.name",title="教研室", align=2, sort=35)
	public Office getTrc() {
		return trc;
	}

	public void setTrc(Office trc) {
		this.trc = trc;
	}

	@ExcelField(title="审核状态", align=2, sort=40,dictType = "status")
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
	
	@Length(min=0, max=64, message="流程实例ID长度必须介于 0 和 64 之间")
	public String getProcInsId() {
		return procInsId;
	}

	public void setProcInsId(String procInsId) {
		this.procInsId = procInsId;
	}
	
	public Date getBeginHoldTime() {
		return beginHoldTime;
	}

	public void setBeginHoldTime(Date beginHoldTime) {
		this.beginHoldTime = beginHoldTime;
	}
	
	public Date getEndHoldTime() {
		return endHoldTime;
	}

	public void setEndHoldTime(Date endHoldTime) {
		this.endHoldTime = endHoldTime;
	}
		
}