/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.teach.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Maps;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.ActAuditService;
import com.thinkgem.jeesite.modules.act.service.ActTaskService;
import com.thinkgem.jeesite.modules.act.utils.ActUtils;
import com.thinkgem.jeesite.modules.teach.dao.TpTeachAwardDao;
import com.thinkgem.jeesite.modules.teach.entity.TpTeachAward;

/**
 * 讲课比赛Service
 * @author 康力
 * @version 2018-03-11
 */
@Service
@Transactional(readOnly = true)
public class TpTeachAwardService extends ActAuditService<TpTeachAwardDao, TpTeachAward> {
	
	@Autowired
	private ActTaskService actTaskService;
	
	@Override
	public TpTeachAward get(String id) {
		return super.get(id);
	}
	
	@Override
	public List<TpTeachAward> findList(TpTeachAward tpTeachAward) {
		return super.findList(tpTeachAward);
	}
	
	@Override
	public Page<TpTeachAward> findPage(Page<TpTeachAward> page, TpTeachAward tpTeachAward) {
		return super.findPage(page, tpTeachAward);
	}
	
	@Override
	@Transactional(readOnly = false)
	public void save(TpTeachAward tpTeachAward) {
		boolean isNewRecord = tpTeachAward.getIsNewRecord();
		super.save(tpTeachAward);
		//根据实体类状态值判断是否启用申请流程
		if(tpTeachAward.getStatus() == 0/* && isNewRecord*/){//状态为0启动流程
			// 启动流程
			if(tpTeachAward.getIsStudent()){//学员数据走学员审批流程
				actTaskService.startProcess(ActUtils.TP_TEACH_AWARD_STUDENT[0], ActUtils.TP_TEACH_AWARD_STUDENT[1], tpTeachAward.getId(), tpTeachAward.getTitle());
			}else{
				actTaskService.startProcess(ActUtils.TP_TEACH_AWARD[0], ActUtils.TP_TEACH_AWARD[1], tpTeachAward.getId(), tpTeachAward.getTitle());
			}
			tpTeachAward.setStatus(0);
			dao.update(tpTeachAward);
		}else if(tpTeachAward.getStatus()==2){//状态2为审核通过，不进入审核流程
			
		}else if(tpTeachAward.getAct().getProcInsId() != null){//重申或销毁
			tpTeachAward.preUpdate();
			dao.update(tpTeachAward);
			tpTeachAward.getAct().setComment(("yes".equals(tpTeachAward.getAct().getFlag())?"[重申] ":"[销毁] ")+tpTeachAward.getAct().getComment());
			// 完成流程任务
			Map<String, Object> vars = Maps.newHashMap();
			vars.put("pass", "yes".equals(tpTeachAward.getAct().getFlag())? "1" : "0");
			actTaskService.complete(tpTeachAward.getAct().getTaskId(), tpTeachAward.getAct().getProcInsId(), tpTeachAward.getAct().getComment(), tpTeachAward.getRemarks(), vars);
		}
	}
	
	@Override
	@Transactional(readOnly = false)
	public void delete(TpTeachAward tpTeachAward) {
		super.delete(tpTeachAward);
	}
	
}