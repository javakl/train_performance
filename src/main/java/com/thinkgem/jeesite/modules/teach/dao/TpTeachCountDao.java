package com.thinkgem.jeesite.modules.teach.dao;

import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.teach.entity.TpTeachCount;

import java.util.List;
@MyBatisDao
public interface TpTeachCountDao {
    List<TpTeachCount> queryList(TpTeachCount data);
}
