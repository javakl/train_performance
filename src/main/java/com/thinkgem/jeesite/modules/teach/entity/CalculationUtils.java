package com.thinkgem.jeesite.modules.teach.entity;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.codehaus.groovy.tools.shell.ParseCode;

import java.text.DecimalFormat;

/**
 * 计算类
 */
public class CalculationUtils {


    private static ScriptEngine engine = new ScriptEngineManager().getEngineByName("js");

    public static Double logicOperation(String OperationalFormula) throws ScriptException {
        Double d = Double.parseDouble(String.valueOf(engine.eval(OperationalFormula)));
        DecimalFormat df   = new DecimalFormat("######0.00");
        return Double.valueOf(df.format(d));
    }

}
