/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.teach.service;

import com.google.common.collect.Maps;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.ActAuditService;
import com.thinkgem.jeesite.modules.act.service.ActTaskService;
import com.thinkgem.jeesite.modules.act.utils.ActUtils;
import com.thinkgem.jeesite.modules.teach.dao.TpTeachSubjectDao;
import com.thinkgem.jeesite.modules.teach.entity.TpTeachSubject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 教学课题表Service
 * @author 康力
 * @version 2018-03-11
 */
@Service
@Transactional(readOnly = true)
public class TpTeachSubjectService extends ActAuditService<TpTeachSubjectDao, TpTeachSubject> {

	@Autowired
	private ActTaskService actTaskService;

	@Override
	public TpTeachSubject get(String id) {
		return super.get(id);
	}
	
	@Override
	public List<TpTeachSubject> findList(TpTeachSubject tpTeachSubject) {
		return super.findList(tpTeachSubject);
	}
	
	@Override
	public Page<TpTeachSubject> findPage(Page<TpTeachSubject> page, TpTeachSubject tpTeachSubject) {
		return super.findPage(page, tpTeachSubject);
	}
	
	@Override
	@Transactional(readOnly = false)
	public void save(TpTeachSubject tpTeachSubject) {
		boolean isNewRecord = tpTeachSubject.getIsNewRecord();
		super.save(tpTeachSubject);
		//根据实体类状态值判断是否启用申请流程
		if(tpTeachSubject.getStatus() == 0/* && isNewRecord*/){//状态为0启动流程
			// 启动流程
			actTaskService.startProcess(ActUtils.TP_TEACH_PATENT[0], ActUtils.TP_TEACH_PATENT[1], tpTeachSubject.getId(), tpTeachSubject.getName());
			tpTeachSubject.setStatus(0);
			dao.update(tpTeachSubject);
		}else if(tpTeachSubject.getStatus()==2){//状态2为审核通过，不进入审核流程

		}else if(tpTeachSubject.getAct().getProcInsId() != null){//重申或销毁
			tpTeachSubject.preUpdate();
			dao.update(tpTeachSubject);
			tpTeachSubject.getAct().setComment(("yes".equals(tpTeachSubject.getAct().getFlag())?"[重申] ":"[销毁] ")+tpTeachSubject.getAct().getComment());
			// 完成流程任务
			Map<String, Object> vars = Maps.newHashMap();
			vars.put("pass", "yes".equals(tpTeachSubject.getAct().getFlag())? "1" : "0");
			actTaskService.complete(tpTeachSubject.getAct().getTaskId(), tpTeachSubject.getAct().getProcInsId(), tpTeachSubject.getAct().getComment(), tpTeachSubject.getRemarks(), vars);
		}
	}
	
	@Override
	@Transactional(readOnly = false)
	public void delete(TpTeachSubject tpTeachSubject) {
		super.delete(tpTeachSubject);
	}
	
}