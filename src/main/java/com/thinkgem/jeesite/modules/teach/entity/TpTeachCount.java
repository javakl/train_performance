package com.thinkgem.jeesite.modules.teach.entity;

import com.thinkgem.jeesite.common.persistence.DataEntity;
import com.thinkgem.jeesite.common.utils.excel.annotation.ExcelField;
import com.thinkgem.jeesite.modules.sys.entity.Office;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class TpTeachCount extends DataEntity {
    public static Map<Integer,String> typeMap=new HashMap<>();
    static {
        typeMap.put(1,"发表文章数");
        typeMap.put(2,"课题立项数");
        typeMap.put(3,"教学专利数");
        typeMap.put(4,"教学成果数");
        typeMap.put(5,"优秀教学奖人次");
        typeMap.put(6,"讲课比赛人次");
        typeMap.put(7,"指导发表文章次数");
    }
    private Office department;		// 所属科室
    private Office trc;		// 所属教研室
    @DateTimeFormat(pattern = "yyyy-MM")
    private Date startDate;
    @DateTimeFormat(pattern = "yyyy-MM")
    private Date endDate;
    /**
     * 发表文章数
     */
    private Integer articleNum;

    /**
     * 教学专利数
     */
    private Integer patentNum;
    /**
     * 课题立项数
     */
    private Integer subjectNum;
    /**
     * 教学成果奖数
     */
    private Integer achievementNum;
    /**
     * 优秀教学奖人次
     */
    private Integer excellentAwardNum;
    /**
     * 讲课比赛人次
     */
    private Integer awardNum;
    /**
     * 指导发表文章数
     */
    private Integer articlesNum;

    public static Map<Integer, String> getTypeMap() {
        return typeMap;
    }

    @Length(min=1, max=255, message="课程名称长度必须介于 1 和 255 之间")
    @ExcelField(value="department.name",title="所属科室", align=2, sort=5)
    public Office getDepartment() {
        return department;
    }

    public void setDepartment(Office department) {
        this.department = department;
    }

    @Length(min=1, max=255, message="教研室长度必须介于 1 和 255 之间")
    @ExcelField(value="trc.name",title="所属教研室", align=2, sort=10)
    public Office getTrc() {
        return trc;
    }

    public void setTrc(Office trc) {
        this.trc = trc;
    }

    @Length(min=1, max=255, message="课程名称长度必须介于 1 和 255 之间")
    @ExcelField(value="startDate",title="获奖时间", align=2, sort=15)
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
    @ExcelField(value="articleNum",title="发表文章数", align=2, sort=20)
    public Integer getArticleNum() {
        return articleNum;
    }

    public void setArticleNum(Integer articleNum) {
        this.articleNum = articleNum;
    }
    @ExcelField(value="patentNum",title="教学专利数", align=2, sort=25)
    public Integer getPatentNum() {
        return patentNum;
    }

    public void setPatentNum(Integer patentNum) {
        this.patentNum = patentNum;
    }
    @ExcelField(value="subjectNum",title="立项课题数", align=2, sort=30)
    public Integer getSubjectNum() {
        return subjectNum;
    }

    public void setSubjectNum(Integer subjectNum) {
        this.subjectNum = subjectNum;
    }
    @ExcelField(value="achievementNum",title="教学成果奖数", align=2, sort=35)
    public Integer getAchievementNum() {
        return achievementNum;
    }

    public void setAchievementNum(Integer achievementNum) {
        this.achievementNum = achievementNum;
    }
    @ExcelField(value="excellentAwardNum",title="优秀教学奖人次", align=2, sort=40)
    public Integer getExcellentAwardNum() {
        return excellentAwardNum;
    }

    public void setExcellentAwardNum(Integer excellentAwardNum) {
        this.excellentAwardNum = excellentAwardNum;
    }
    @ExcelField(value="awardNum",title="讲课比赛人次", align=2, sort=45)
    public Integer getAwardNum() {
        return awardNum;
    }

    public void setAwardNum(Integer awardNum) {
        this.awardNum = awardNum;
    }
    @ExcelField(value="articlesNum",title="指导发表文章数", align=2, sort=50)
    public Integer getArticlesNum() {
        return articlesNum;
    }

    public void setArticlesNum(Integer articlesNum) {
        this.articlesNum = articlesNum;
    }
}
