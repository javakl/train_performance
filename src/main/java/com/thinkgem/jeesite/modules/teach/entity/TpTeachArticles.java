/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.teach.entity;

import com.thinkgem.jeesite.common.persistence.ActEntity;
import com.thinkgem.jeesite.common.utils.excel.annotation.ExcelField;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.Length;
import com.thinkgem.jeesite.modules.sys.entity.User;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.thinkgem.jeesite.modules.sys.entity.Office;
import com.thinkgem.jeesite.modules.sys.entity.Area;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 教学文章Entity
 * @author 康力
 * @version 2018-03-11
 */
public class TpTeachArticles extends ActEntity<TpTeachArticles>{
	
	private static final long serialVersionUID = 1L;
	private String articleTitle;		// 论文名
	private String name;		// 论文名(用于移动端展示)
	private User user;		// 发表作者
	private String userFullName;//姓名（冗余字段用于导入导出列）
	private String authorName;		// 第一作者或通讯作者
	private String articleType;		// 文章类型
	private String pushJournal;		// 发表期刊
	private String journalType;		// 期刊类型
	private String pushYear;		// 发表年份月
	private String volume;		// 期卷
	private String pages;		// 页数
	private Date onltime;		// online收录时间
	private String onlelement;		// online影响因子
	private String onljcr;		// onlineJCR分区
	private String onlJcrUrl;		// onlineJCRUrl
	private String authContent;		// 作者情况
	private Office department;		// 所属科室
	private Office trc;		// 所属教研室
	private String procInsId;		// 流程实例ID
	private Integer status;		// 审核状态
	private String isApplyDegree;		// 是否以此申学位
	private String beginPushYear;		// 开始 发表年份
	private String endPushYear;		// 结束 发表年份
	
	private Integer isStudent;//是否学员数据
    //发表期刊名字
    private String pushJournalName;
    /****学员记录字段**/
    @ExcelField(value = "tutor.name", title = "导师", align = 2, sort = 13,type=3)
    private User tutor;
    /****学员记录字段**/
	public TpTeachArticles() {
		super();
	}

	public TpTeachArticles(String id){
		super(id);
	}

	@Length(min=1, max=100, message="论文名长度必须介于 1 和 100 之间")
	@ExcelField(value="articleTitle",title="论文名称", align=2, sort=5)
	public String getArticleTitle() {
		return articleTitle;
	}

	public void setArticleTitle(String articleTitle) {
		this.articleTitle = articleTitle;
	}
	
	@NotNull(message="发表作者登录号不能为空")
	@ExcelField(value="user.loginName",title="发表作者工号(登录号)", align=2, sort=10)
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	@ExcelField(value="userFullName",title="作者姓名", align=2, sort=12)
	public String getUserFullName() {
		if(user != null && StringUtils.isNotBlank(user.getName())){
			return user.getName();
		}else
			return userFullName;
	}

	public void setUserFullName(String userFullName) {
		this.userFullName = userFullName;
	}
	@Length(min=1, max=20, message="第一作者或通讯作者长度必须介于 1 和 20 之间")
	@ExcelField(value="authorName",title="第一作者或通讯作者", align=2, sort=15)
	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}
	
	@Length(min=1, max=64, message="文章类型长度必须介于 1 和 64 之间")
	@ExcelField(value="articleType",title="文章类型", align=2, sort=20,dictType = "article_type")
	public String getArticleType() {
		return articleType;
	}

	public void setArticleType(String articleType) {
		this.articleType = articleType;
	}
	
	/*@Length(min=1, max=255, message="发表期刊长度必须介于 1 和 255 之间")
	@ExcelField(value="pushJournal",title="发表期刊", align=2, sort=25)*/
	public String getPushJournal() {
		return pushJournal;
	}

	public void setPushJournal(String pushJournal) {
		this.pushJournal = pushJournal;
	}
	
	@Length(min=1, max=64, message="期刊类型长度必须介于 1 和 64 之间")
	@ExcelField(value="journalType",title="期刊类型", align=2, sort=30,dictType = "journal_type")
	public String getJournalType() {
		return journalType;
	}

	public void setJournalType(String journalType) {
		this.journalType = journalType;
	}
	
	@NotNull(message="发表年份不能为空")
	@ExcelField(value="pushYear",title="发表年份", align=2, sort=35)
	public String getPushYear() {
		return pushYear;
	}

	public void setPushYear(String pushYear) {
		this.pushYear = pushYear;
	}
	
	@Length(min=1, max=50, message="期卷长度必须介于 1 和 50 之间")
	@ExcelField(value="volume",title="期卷长度", align=2, sort=40)
	public String getVolume() {
		return volume;
	}

	public void setVolume(String volume) {
		this.volume = volume;
	}
	
	@Length(min=1, max=11, message="页数长度必须介于 1 和 11 之间")
	@ExcelField(value="pages",title="页数", align=2, sort=45)
	public String getPages() {
		return pages;
	}

	public void setPages(String pages) {
		this.pages = pages;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd")
	@ExcelField(value="onltime",title="收录时间", align=2, sort=50)
	public Date getOnltime() {
		return onltime;
	}

	public void setOnltime(Date onltime) {
		this.onltime = onltime;
	}

	@ExcelField(value="onlelement",title="online影响因子", align=2, sort=55)
	public String getOnlelement() {
		return onlelement;
	}

	public void setOnlelement(String onlelement) {
		this.onlelement = onlelement;
	}

	@ExcelField(value="onljcr",title="onlineJCR分区", align=2, sort=60)
	public String getOnljcr() {
		return onljcr;
	}

	public void setOnljcr(String onljcr) {
		this.onljcr = onljcr;
	}

	@Length(min=1, max=200, message="作者情况长度必须介于 1 和 200 之间")
	@ExcelField(value="authContent",title="作者情况", align=2, sort=65)
	public String getAuthContent() {
		return authContent;
	}

	public void setAuthContent(String authContent) {
		this.authContent = authContent;
	}
	
	@NotNull(message="所属科室不能为空")
	@ExcelField(value="department.name",title="所属科室", align=2, sort=70)
	public Office getDepartment() {
		return department;
	}

	public void setDepartment(Office department) {
		this.department = department;
	}
	
	@NotNull(message="所属教研室不能为空")
	@ExcelField(value="trc.name",title="所属教研室", align=2, sort=75)
	public Office getTrc() {
		return trc;
	}

	public void setTrc(Office trc) {
		this.trc = trc;
	}
	
	@Length(min=0, max=64, message="流程实例ID长度必须介于 0 和 64 之间")
	public String getProcInsId() {
		return procInsId;
	}

	public void setProcInsId(String procInsId) {
		this.procInsId = procInsId;
	}
	@ExcelField(title="审核状态", align=2, sort=85,dictType = "status")
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
	
	@Length(min=1, max=255, message="是否以此申学位长度必须介于 1 和 255 之间")
	@ExcelField(value="isApplyDegree",title="是否以此申学位", align=2, sort=80,dictType = "yes_no")
	public String getIsApplyDegree() {
		return isApplyDegree;
	}

	public void setIsApplyDegree(String isApplyDegree) {
		this.isApplyDegree = isApplyDegree;
	}
	
	public String getBeginPushYear() {
		return beginPushYear;
	}

	public void setBeginPushYear(String beginPushYear) {
		this.beginPushYear = beginPushYear;
	}
	
	public String getEndPushYear() {
		return endPushYear;
	}

	public void setEndPushYear(String endPushYear) {
		this.endPushYear = endPushYear;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOnlJcrUrl() {
		return onlJcrUrl;
	}

	public void setOnlJcrUrl(String onlJcrUrl) {
		this.onlJcrUrl = onlJcrUrl;
	}

	public Integer getIsStudent() {
		return isStudent;
	}

	public void setIsStudent(Integer isStudent) {
		this.isStudent = isStudent;
	}
	@ExcelField(value="pushJournalName",title="发表期刊", align=2, sort=27)
    public String getPushJournalName() {
        return pushJournalName;
    }

    public void setPushJournalName(String pushJournalName) {
        this.pushJournalName = pushJournalName;
    }
	public User getTutor() {
		return tutor;
	}

	public void setTutor(User tutor) {
		this.tutor = tutor;
	}
}