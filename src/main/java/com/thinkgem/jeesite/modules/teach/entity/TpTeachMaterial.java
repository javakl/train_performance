/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.teach.entity;

import java.util.Date;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.thinkgem.jeesite.common.utils.excel.annotation.ExcelField;
import com.thinkgem.jeesite.modules.sys.entity.User;
import com.thinkgem.jeesite.modules.sys.entity.User;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.thinkgem.jeesite.common.persistence.ActEntity;
import com.thinkgem.jeesite.modules.sys.entity.Area;
import com.thinkgem.jeesite.modules.sys.entity.Office;

/**
 * 教材建设Entity
 * @author 康力
 * @version 2018-03-11
 */
public class TpTeachMaterial extends ActEntity<TpTeachMaterial> {
	
	private static final long serialVersionUID = 1L;
	private String name;		// 教材名称
	private Office department;		// 所属科室
	private Office trc;		// 所属教研室
	private Date pushTime;		// 出版时间
	private String press;		// 出版社
	private String identity;		// 参编情况
	private Integer status;		// 审核状态
	private String procInsId;		// 流程实例ID
	private Date beginPushTime;		// 开始 出版时间
	private Date endPushTime;		// 结束 出版时间
	private User user;//所属人
	private String userFullName;//姓名（冗余字段用于导入导出列）

	public TpTeachMaterial() {
		super();
	}

	public TpTeachMaterial(String id){
		super(id);
	}

	@Length(min=1, max=255, message="教材名称长度必须介于 1 和 255 之间")
	@ExcelField(value="name",title="教材名称", align=2, sort=5)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@NotNull(message="所属科室不能为空")
	@ExcelField(value="department.name",title="所属科室", align=2, sort=10)
	public Office getDepartment() {
		return department;
	}

	public void setDepartment(Office department) {
		this.department = department;
	}
	
	@NotNull(message="所属教研室不能为空")
	@ExcelField(value="trc.name",title="教研室", align=2, sort=15)
	public Office getTrc() {
		return trc;
	}

	public void setTrc(Office trc) {
		this.trc = trc;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd")
	@NotNull(message="出版时间不能为空")
	@ExcelField(value="pushTime",title="出版时间", align=2, sort=20)
	public Date getPushTime() {
		return pushTime;
	}

	public void setPushTime(Date pushTime) {
		this.pushTime = pushTime;
	}
	
	@Length(min=1, max=255, message="出版社长度必须介于 1 和 255 之间")
	@ExcelField(value="press",title="出版社", align=2, sort=25)
	public String getPress() {
		return press;
	}

	public void setPress(String press) {
		this.press = press;
	}
	
	@Length(min=1, max=64, message="参编情况长度必须介于 1 和 64 之间")
	@ExcelField(value="identity",title="参编情况", align=2, sort=30)
	public String getIdentity() {
		return identity;
	}

	public void setIdentity(String identity) {
		this.identity = identity;
	}
	@ExcelField(title="审核状态", align=2, sort=35,dictType = "status")
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
	
	@Length(min=0, max=64, message="流程实例ID长度必须介于 0 和 64 之间")
	public String getProcInsId() {
		return procInsId;
	}

	public void setProcInsId(String procInsId) {
		this.procInsId = procInsId;
	}
	
	public Date getBeginPushTime() {
		return beginPushTime;
	}

	public void setBeginPushTime(Date beginPushTime) {
		this.beginPushTime = beginPushTime;
	}
	
	public Date getEndPushTime() {
		return endPushTime;
	}

	public void setEndPushTime(Date endPushTime) {
		this.endPushTime = endPushTime;
	}
	@NotNull(message="所属人工号不能为空")
	@ExcelField(value="user.loginName",title="所属人工号", align=2, sort=8)
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	@ExcelField(value="userFullName",title="所属人姓名", align=2, sort=9)
	public String getUserFullName() {
		if(user != null && StringUtils.isNotBlank(user.getName())){
			return user.getName();
		}else
			return userFullName;
	}

	public void setUserFullName(String userFullName) {
		this.userFullName = userFullName;
	}
}