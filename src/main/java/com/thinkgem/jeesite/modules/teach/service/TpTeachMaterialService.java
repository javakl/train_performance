/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.teach.service;

import com.google.common.collect.Maps;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.ActAuditService;
import com.thinkgem.jeesite.modules.act.service.ActTaskService;
import com.thinkgem.jeesite.modules.act.utils.ActUtils;
import com.thinkgem.jeesite.modules.teach.dao.TpTeachMaterialDao;
import com.thinkgem.jeesite.modules.teach.entity.TpTeachMaterial;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 教材建设Service
 * @author 康力
 * @version 2018-03-11
 */
@Service
@Transactional(readOnly = true)
public class TpTeachMaterialService extends ActAuditService<TpTeachMaterialDao, TpTeachMaterial> {

	@Autowired
	private ActTaskService actTaskService;

	@Override
	public TpTeachMaterial get(String id) {
		return super.get(id);
	}
	
	@Override
	public List<TpTeachMaterial> findList(TpTeachMaterial tpTeachMaterial) {
		return super.findList(tpTeachMaterial);
	}
	
	@Override
	public Page<TpTeachMaterial> findPage(Page<TpTeachMaterial> page, TpTeachMaterial tpTeachMaterial) {
		return super.findPage(page, tpTeachMaterial);
	}
	
	@Override
	@Transactional(readOnly = false)
	public void save(TpTeachMaterial tpTeachMaterial) {
		boolean isNewRecord = tpTeachMaterial.getIsNewRecord();
		super.save(tpTeachMaterial);
		//根据实体类状态值判断是否启用申请流程
		if(tpTeachMaterial.getStatus() == 0/* && isNewRecord*/){//状态为0启动流程
			// 启动流程
			actTaskService.startProcess(ActUtils.TP_TEACH_MATERIAL[0], ActUtils.TP_TEACH_MATERIAL[1], tpTeachMaterial.getId(), tpTeachMaterial.getName());
			tpTeachMaterial.setStatus(0);
			dao.update(tpTeachMaterial);
		}else if(tpTeachMaterial.getStatus()==2){//状态2为审核通过，不进入审核流程

		}else if(tpTeachMaterial.getAct().getProcInsId() != null){//重申或销毁
			tpTeachMaterial.preUpdate();
			dao.update(tpTeachMaterial);
			tpTeachMaterial.getAct().setComment(("yes".equals(tpTeachMaterial.getAct().getFlag())?"[重申] ":"[销毁] ")+tpTeachMaterial.getAct().getComment());
			// 完成流程任务
			Map<String, Object> vars = Maps.newHashMap();
			vars.put("pass", "yes".equals(tpTeachMaterial.getAct().getFlag())? "1" : "0");
			actTaskService.complete(tpTeachMaterial.getAct().getTaskId(), tpTeachMaterial.getAct().getProcInsId(), tpTeachMaterial.getAct().getComment(), tpTeachMaterial.getRemarks(), vars);
		}
	}
	
	@Override
	@Transactional(readOnly = false)
	public void delete(TpTeachMaterial tpTeachMaterial) {
		super.delete(tpTeachMaterial);
	}
	
}