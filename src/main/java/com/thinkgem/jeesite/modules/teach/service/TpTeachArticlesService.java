/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.teach.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Maps;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.ActAuditService;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.act.service.ActTaskService;
import com.thinkgem.jeesite.modules.act.utils.ActUtils;
import com.thinkgem.jeesite.modules.teach.dao.TpArticleClassifyDao;
import com.thinkgem.jeesite.modules.teach.dao.TpTeachArticlesDao;
import com.thinkgem.jeesite.modules.teach.entity.TpArticleClassify;
import com.thinkgem.jeesite.modules.teach.entity.TpTeachArticles;

/**
 * 教学文章Service
 * @author 康力
 * @version 2018-03-11
 */
@Service
@Transactional(readOnly = true)
public class TpTeachArticlesService extends ActAuditService<TpTeachArticlesDao, TpTeachArticles> {

	@Autowired
	private ActTaskService actTaskService;
	/**
	 * 文章期刊分类持久层接口
	 */
	@Autowired
	private TpArticleClassifyDao articleClassifyDao;
	@Override
	public TpTeachArticles get(String id) {
		return super.get(id);
	}
	
	@Override
	public List<TpTeachArticles> findList(TpTeachArticles tpTeachArticles) {
		return super.findList(tpTeachArticles);
	}
	
	@Override
	public Page<TpTeachArticles> findPage(Page<TpTeachArticles> page, TpTeachArticles tpTeachArticles) {
		return super.findPage(page, tpTeachArticles);
	}
	
	@Override
	@Transactional(readOnly = false)
	public void save(TpTeachArticles tpTeachArticles) {
		boolean isNewRecord = tpTeachArticles.getIsNewRecord();
		//当发表期刊pushJournalName不为空，但是发表期刊id（pushJournal）为空，则根据期刊名称+期刊类型+发表年月，确定期刊id
		if(isNewRecord && StringUtils.isNotBlank(tpTeachArticles.getPushJournalName()) && StringUtils.isBlank(tpTeachArticles.getPushJournal())){
			TpArticleClassify classifyQuery = new TpArticleClassify();
			classifyQuery.setYear(tpTeachArticles.getPushYear());
			classifyQuery.setName(tpTeachArticles.getPushJournalName());
			classifyQuery.setType(tpTeachArticles.getJournalType());
			List<TpArticleClassify> classifyList = articleClassifyDao.findList(classifyQuery);
			if(classifyList.size()>0){
				tpTeachArticles.setPushJournal(classifyList.get(0).getId());
			}
		}
		super.save(tpTeachArticles);
		//根据实体类状态值判断是否启用申请流程
		if(tpTeachArticles.getStatus() == 0 /*&& isNewRecord*/){//状态为0启动流程
			// 启动流程
			if(tpTeachArticles.getIsStudent() == 1){
				actTaskService.startProcess(ActUtils.TP_TEACH_ARTICLES_STUDENT[0], ActUtils.TP_TEACH_ARTICLES_STUDENT[1], tpTeachArticles.getId(), tpTeachArticles.getArticleTitle());
			}else{
				actTaskService.startProcess(ActUtils.TP_TEACH_ARTICLES[0], ActUtils.TP_TEACH_ARTICLES[1], tpTeachArticles.getId(), tpTeachArticles.getArticleTitle());
			}
			tpTeachArticles.setStatus(0);
			dao.update(tpTeachArticles);
		}else if(tpTeachArticles.getStatus()==2){//状态2为审核通过，不进入审核流程

		}else if(tpTeachArticles.getAct().getProcInsId() != null){//重申或销毁
			tpTeachArticles.preUpdate();
			dao.update(tpTeachArticles);
			tpTeachArticles.getAct().setComment(("yes".equals(tpTeachArticles.getAct().getFlag())?"[重申] ":"[销毁] ")+tpTeachArticles.getAct().getComment());
			// 完成流程任务
			Map<String, Object> vars = Maps.newHashMap();
			vars.put("pass", "yes".equals(tpTeachArticles.getAct().getFlag())? "1" : "0");
			actTaskService.complete(tpTeachArticles.getAct().getTaskId(), tpTeachArticles.getAct().getProcInsId(), tpTeachArticles.getAct().getComment(), tpTeachArticles.getRemarks(), vars);
		}
	}
	
	@Override
	@Transactional(readOnly = false)
	public void delete(TpTeachArticles tpTeachArticles) {
		super.delete(tpTeachArticles);
	}
	
}