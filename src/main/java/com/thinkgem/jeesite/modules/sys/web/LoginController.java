/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.sys.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.thinkgem.jeesite.modules.sys.service.SystemService;
import com.thinkgem.jeesite.modules.weixin.pageauthorization.Auth2UserInfo;
import org.apache.poi.util.StringUtil;
import org.apache.shiro.authz.UnauthorizedException;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.web.util.WebUtils;
import org.apache.xmlbeans.impl.xb.xsdschema.RedefineDocument.Redefine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Maps;
import com.thinkgem.jeesite.common.ConstantEnum;
import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.ReturnObject;
import com.thinkgem.jeesite.common.security.shiro.session.SessionDAO;
import com.thinkgem.jeesite.common.servlet.ValidateCodeServlet;
import com.thinkgem.jeesite.common.utils.CacheUtils;
import com.thinkgem.jeesite.common.utils.CookieUtils;
import com.thinkgem.jeesite.common.utils.IdGen;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.modules.sys.entity.Menu;
import com.thinkgem.jeesite.modules.sys.entity.User;
import com.thinkgem.jeesite.modules.sys.security.FormAuthenticationFilter;
import com.thinkgem.jeesite.modules.sys.security.SystemAuthorizingRealm.Principal;
import com.thinkgem.jeesite.modules.sys.utils.UserUtils;

/**
 * 登录Controller
 * @author ThinkGem
 * @version 2013-5-31
 */
@CrossOrigin(maxAge = 3600)
@Controller
public class LoginController extends BaseController{
	
	@Autowired
	private SessionDAO sessionDAO;

	@Autowired
	private SystemService systemService;
	
	private Logger logger = LoggerFactory.getLogger(LoginController.class);
	/**
	 * 管理登录
	 */
	@RequestMapping(value = "${adminPath}/login", method = RequestMethod.GET)
	public String login(HttpServletRequest request, HttpServletResponse response, Model model) {
		Principal principal = UserUtils.getPrincipal();
		if (logger.isDebugEnabled()){
			logger.debug("login, active session size: {}", sessionDAO.getActiveSessions(false).size());
		}
		
		// 如果已登录，再次访问主页，则退出原账号。
		if (Global.TRUE.equals(Global.getConfig("notAllowRefreshIndex"))){
			CookieUtils.setCookie(response, "LOGINED", "false");
		}
		logger.debug(FormAuthenticationFilter.DEFAULT_MOBILE_PARAM+" : "+request.getParameter(FormAuthenticationFilter.DEFAULT_MOBILE_PARAM));
		boolean mobile = WebUtils.isTrue(request, FormAuthenticationFilter.DEFAULT_MOBILE_PARAM);
		// 如果是移动端请求，则返回JSON字符串
		if (mobile){
			/*String code = WebUtils.getCleanParam(request, FormAuthenticationFilter.WE_CHAT_PARAM);
			if(code != null){
				//已关注过微信号
				request.getSession().setAttribute(FormAuthenticationFilter.WE_CHAT_PARAM,code);
				String openId = Auth2UserInfo.getOpenId(code);
			}else{
				//还未关注过微信号

			}*/
			bingOpenId(request);
		    return renderString(response, new ReturnObject(ConstantEnum.success.getCode(), ConstantEnum.success.getName(), null));
		}
		// 如果已经登录，则跳转到管理首页
		if(principal != null && !principal.isMobileLogin()){
			return "redirect:" + adminPath;
		}
		return "modules/sys/sysLogin";
	}

	/**
	 * 微信openId绑定用户
	 * @param request
	 */
	private void bingOpenId(HttpServletRequest request) {
		String openId = (String) request.getSession().getAttribute(FormAuthenticationFilter.WE_CHAT_OPENID);
		logger.debug("openId is :"+openId);
		//当前登录用户绑定微信
		if(StringUtils.isNotBlank(openId)){
			User curUser = UserUtils.getUser();
			curUser.setOpenId(openId);
			systemService.updateOpenId(curUser);
		}
	}

	/**
	 * 登录失败，真正登录的POST请求由Filter完成
	 */
	@RequestMapping(value = "${adminPath}/login", method = RequestMethod.POST)
	public String loginFail(HttpServletRequest request, HttpServletResponse response, Model model) {
		Principal principal = UserUtils.getPrincipal();
		
		// 如果已经登录，则跳转到管理首页
		if(principal != null && !principal.isMobileLogin()){
			return "redirect:" + adminPath;
		}

		String username = WebUtils.getCleanParam(request, FormAuthenticationFilter.DEFAULT_USERNAME_PARAM);
		boolean rememberMe = WebUtils.isTrue(request, FormAuthenticationFilter.DEFAULT_REMEMBER_ME_PARAM);
		boolean mobile = WebUtils.isTrue(request, FormAuthenticationFilter.DEFAULT_MOBILE_PARAM);
		String exception = (String)request.getAttribute(FormAuthenticationFilter.DEFAULT_ERROR_KEY_ATTRIBUTE_NAME);
		String message = (String)request.getAttribute(FormAuthenticationFilter.DEFAULT_MESSAGE_PARAM);
		
		if (StringUtils.isBlank(message) || StringUtils.equals(message, "null")){
			message = "用户或密码错误, 请重试.";
		}

		model.addAttribute(FormAuthenticationFilter.DEFAULT_USERNAME_PARAM, username);
		model.addAttribute(FormAuthenticationFilter.DEFAULT_REMEMBER_ME_PARAM, rememberMe);
		model.addAttribute(FormAuthenticationFilter.DEFAULT_MOBILE_PARAM, mobile);
		model.addAttribute(FormAuthenticationFilter.DEFAULT_ERROR_KEY_ATTRIBUTE_NAME, exception);
		model.addAttribute(FormAuthenticationFilter.DEFAULT_MESSAGE_PARAM, message);
		
		if (logger.isDebugEnabled()){
			logger.debug("login fail, active session size: {}, message: {}, exception: {}", 
					sessionDAO.getActiveSessions(false).size(), message, exception);
		}
		
		// 非授权异常，登录失败，验证码加1。
		if (!UnauthorizedException.class.getName().equals(exception)){
			model.addAttribute("isValidateCodeLogin", isValidateCodeLogin(username, true, false));
		}
		
		// 验证失败清空验证码
		request.getSession().setAttribute(ValidateCodeServlet.VALIDATE_CODE, IdGen.uuid());
		
		// 如果是手机登录，则返回JSON字符串
		if (mobile){
			bingOpenId(request);
			ReturnObject rtobj = new ReturnObject();
			rtobj.setMsg(message);
			if(StringUtils.isNotEmpty(message)){
				rtobj.setResult(-1);
			}else{
				rtobj.setResult(0);
			}
	        return renderString(response, rtobj);
		}
		
		return "modules/sys/sysLogin";
	}

	/**
	 * 登录成功，进入管理首页
	 */
	@RequiresPermissions("user")
	@RequestMapping(value = "${adminPath}")
	public String index(HttpServletRequest request, HttpServletResponse response) {
		Principal principal = UserUtils.getPrincipal();

		// 登录成功后，验证码计算器清零
		isValidateCodeLogin(principal.getLoginName(), false, true);
		
		if (logger.isDebugEnabled()){
			logger.debug("show index, active session size: {}", sessionDAO.getActiveSessions(false).size());
		}
		
		// 如果已登录，再次访问主页，则退出原账号。
		if (Global.TRUE.equals(Global.getConfig("notAllowRefreshIndex"))){
			String logined = CookieUtils.getCookie(request, "LOGINED");
			if (StringUtils.isBlank(logined) || "false".equals(logined)){
				CookieUtils.setCookie(response, "LOGINED", "true");
			}else if (StringUtils.equals(logined, "true")){
				UserUtils.getSubject().logout();
				return "redirect:" + adminPath + "/login";
			}
		}
		
		// 如果是手机登录，则返回JSON字符串
		if (principal.isMobileLogin()){
			bingOpenId(request);
			return renderString(response, new ReturnObject(ConstantEnum.success.getCode(), ConstantEnum.success.getName(), null)); 
			/*if (request.getParameter("login") != null){
				return renderString(response, new ReturnObject(0, "已登录", null));
			}
			if (request.getParameter("index") != null){
				return "modules/sys/sysIndex";
			}*/
		}
		
//		// 登录成功后，获取上次登录的当前站点ID
//		UserUtils.putCache("siteId", StringUtils.toLong(CookieUtils.getCookie(request, "siteId")));

//		System.out.println("==========================a");
//		try {
//			byte[] bytes = com.thinkgem.jeesite.common.utils.FileUtils.readFileToByteArray(
//					com.thinkgem.jeesite.common.utils.FileUtils.getFile("c:\\sxt.dmp"));
//			UserUtils.getSession().setAttribute("kkk", bytes);
//			UserUtils.getSession().setAttribute("kkk2", bytes);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
////		for (int i=0; i<1000000; i++){
////			//UserUtils.getSession().setAttribute("a", "a");
////			request.getSession().setAttribute("aaa", "aa");
////		}
//		System.out.println("==========================b");
		return "modules/sys/sysIndex";
	}
	
	/**
	 * 获取主题方案
	 */
	@RequestMapping(value = "/theme/{theme}")
	public String getThemeInCookie(@PathVariable String theme, HttpServletRequest request, HttpServletResponse response){
		if (StringUtils.isNotBlank(theme)){
			CookieUtils.setCookie(response, "theme", theme);
		}else{
			theme = CookieUtils.getCookie(request, "theme");
		}
		return "redirect:"+request.getParameter("url");
	}
	
	/**
	 * 是否是验证码登录
	 * @param useruame 用户名
	 * @param isFail 计数加1
	 * @param clean 计数清零
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static boolean isValidateCodeLogin(String useruame, boolean isFail, boolean clean){
		Map<String, Integer> loginFailMap = (Map<String, Integer>)CacheUtils.get("loginFailMap");
		if (loginFailMap==null){
			loginFailMap = Maps.newHashMap();
			CacheUtils.put("loginFailMap", loginFailMap);
		}
		Integer loginFailNum = loginFailMap.get(useruame);
		if (loginFailNum==null){
			loginFailNum = 0;
		}
		if (isFail){
			loginFailNum++;
			loginFailMap.put(useruame, loginFailNum);
		}
		if (clean){
			loginFailMap.remove(useruame);
		}
		return loginFailNum >= 3;
	}
	/**
	 * 获得用户授权菜单列表
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("${adminPath}/getUserMenuList")
	public String getUserMenuList(HttpServletRequest request,HttpServletResponse response){
		ReturnObject rtobj = new ReturnObject();
		boolean mobile = WebUtils.isTrue(request, FormAuthenticationFilter.DEFAULT_MOBILE_PARAM);
		List<Menu> menuList = null;
		if(mobile){//移动端菜单
			menuList = UserUtils.getMobileMenuList();
		}else{
			menuList = UserUtils.getMenuList();
		}
		rtobj.setData(menuList);
		if(menuList == null){
			rtobj.setResult(ConstantEnum.failed.getCode());
			rtobj.setMsg("用户未登录");
		}else{
			rtobj.setResult(ConstantEnum.success.getCode());
			rtobj.setMsg(ConstantEnum.success.getName());
		}
		return renderString(response,rtobj);
	}
	/**
	 * 退出登陆，主要提供给移动端，返回json数据
	 * @return
	 */
	@RequestMapping("${adminPath}/mobileLogout")
	public String mobileLogout(HttpServletRequest request, HttpServletResponse response, Model model){
		String openId = (String) request.getSession().getAttribute(FormAuthenticationFilter.WE_CHAT_OPENID);
		//当前登录用户解除绑定微信
		User curUser = UserUtils.getUser();
		curUser.setOpenId(null);
		systemService.updateOpenId(curUser);
		UserUtils.getSubject().logout();
		request.getSession().setAttribute(FormAuthenticationFilter.WE_CHAT_OPENID, openId);
		return renderString(response, new ReturnObject(ConstantEnum.success.getCode(), ConstantEnum.success.getName(), null));
	}
	
/*	@RequestMapping("${adminPath}/toMobileIndex")
	public String toMobileIndex(HttpServletRequest request, HttpServletResponse response){
		Cookie cookie = new Cookie("openId","8768668");
		cookie.setPath("/");
		cookie.setMaxAge(3600 * 24 * 100);
		response.addCookie(cookie);
		Cookie cookie1 = new Cookie("userName","xxxxxxxxx");
		cookie1.setPath("/");
		cookie1.setMaxAge(3600 * 24 * 100);
		response.addCookie(cookie1);
		request.getSession().setAttribute("openId", "xxaaaaaaaaaaaaa45465");
		return "modules/sys/mobileIndex";
	}*/
	
	/**
	 * 根据微信请求的code 判断该微信用户是否已绑定系统用户
	 * @param code
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="${adminPath}/checkWXBinding")
	public String checkWXBinding(HttpServletRequest request, HttpServletResponse response){
		ReturnObject rtnObj = new ReturnObject();
		String code = WebUtils.getCleanParam(request, FormAuthenticationFilter.WE_CHAT_PARAM);
		if(StringUtils.isNotBlank(code)){
			String openId = Auth2UserInfo.getOpenId(code);
//			String openId = "199238";
			if(StringUtils.isNotBlank(openId)){
				logger.debug("get openId is :"+openId);
				//检查openid是否已绑定用户
				User user = new User();
				user.setOpenId(openId);
				List<User> ulist = systemService.findUser(user);
				if(ulist != null && ulist.size()>0){
					//免密码登录
					//跳转到登录接口，默认登录
					/*attributes.addAttribute("username", ulist.get(0).getLoginName());
					attributes.addAttribute(FormAuthenticationFilter.DEFAULT_MOBILE_PARAM, 1);
					return "redirect:"+Global.getAdminPath()+"/login";*/
					//返回绑定的用户名和openId
					request.getSession().setAttribute(FormAuthenticationFilter.WE_CHAT_OPENID, openId);
					rtnObj.setResult(ConstantEnum.success.getCode());
					rtnObj.setMsg(ConstantEnum.success.getName());
					HashMap<String,String> umap =new HashMap<>();
					umap.put("username", ulist.get(0).getLoginName());
					rtnObj.setData(umap);
				}else{
					//微信openid存入session
					request.getSession().setAttribute(FormAuthenticationFilter.WE_CHAT_OPENID, openId);
					rtnObj.setResult(ConstantEnum.failed.getCode());
					rtnObj.setMsg("未绑定用户");
				}
			}else{
				rtnObj.setResult(ConstantEnum.failed.getCode());
				rtnObj.setMsg("code失效");
			}
		}else{
			rtnObj.setResult(ConstantEnum.failed.getCode());
			rtnObj.setMsg("未关注公众号");
		}
		return renderString(response, rtnObj);
	}
}
