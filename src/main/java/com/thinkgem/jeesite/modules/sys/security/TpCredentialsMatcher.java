/**
 * 
 */
package com.thinkgem.jeesite.modules.sys.security;

import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;

/**
 * @author hhm
 *
 */
public class TpCredentialsMatcher extends HashedCredentialsMatcher {

	@Override
	public boolean doCredentialsMatch(AuthenticationToken token, AuthenticationInfo info) {
		UsernamePasswordToken curToken = (UsernamePasswordToken) token;
		if(curToken.isNeedPassword()){
			return super.doCredentialsMatch(token, info);
		}else{//免密码登录，直接返回true
			return true;
		}
	}

	public TpCredentialsMatcher() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TpCredentialsMatcher(String hashAlgorithmName) {
		super(hashAlgorithmName);
		// TODO Auto-generated constructor stub
	}

	
	
}
