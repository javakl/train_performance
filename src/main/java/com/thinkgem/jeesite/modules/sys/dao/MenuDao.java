/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.sys.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.sys.entity.CustomTitle;
import com.thinkgem.jeesite.modules.sys.entity.Menu;

/**
 * 菜单DAO接口
 * @author ThinkGem
 * @version 2014-05-16
 */
@MyBatisDao
public interface MenuDao extends CrudDao<Menu> {

	public List<Menu> findByParentIdsLike(Menu menu);

	public List<Menu> findByUserId(Menu menu);
	
	public int updateParentIds(Menu menu);
	
	public int updateSort(Menu menu);

    List<String> getMenuRemark(@Param("str") String str);
    
    /**
     * 根据链接获取菜单的标题定制json串
     * @param str
     * @return
     */
    List<String> getMenuCustomTitle(@Param("str") String str);
    /**
     * 根据链接获取菜单的标题定制字段列表
     * @param str
     * @return
     */
    List<CustomTitle> getMenuCustomTitleList(@Param("menuId") String menuId);
}
