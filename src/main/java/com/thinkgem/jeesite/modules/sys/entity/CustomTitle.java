package com.thinkgem.jeesite.modules.sys.entity;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 标题定制字段实体类
 * @author hhm
 */
public class CustomTitle extends DataEntity<CustomTitle> {
	
	private static final long serialVersionUID = 1L;

	/**
	 * 所属菜单id
	 */
	private String menuId;
	/**
	 * 字段属性名称
	 */
	private String columnName;
	/**
	 * 字段显示名称
	 */
	private String labelName;
	/**
	 * 是否显：0=否  1=是
	 */
	private boolean show;
	/**
	 * 是否必填
	 */
	private boolean required;
	/**
	 * 描述
	 */
	private String description;
	public String getMenuId() {
		return menuId;
	}
	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}
	public String getColumnName() {
		return columnName;
	}
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	public String getLabelName() {
		return labelName;
	}
	public void setLableName(String lableName) {
		this.labelName = lableName;
	}
	public boolean isShow() {
		return show;
	}
	public void setShow(boolean show) {
		this.show = show;
	}
	public boolean isRequired() {
		return required;
	}
	public void setRequired(boolean required) {
		this.required = required;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
}
