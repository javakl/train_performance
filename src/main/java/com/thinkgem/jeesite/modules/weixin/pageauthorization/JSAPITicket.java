package com.thinkgem.jeesite.modules.weixin.pageauthorization;

/**
 * Created by yangrui on 2018/4/1.
 */
public class JSAPITicket {
    private String ticket;
    private long expires_in;
    private Integer errcode;
    private String errmsg;
    private long createTime;
    private String appId;

    public JSAPITicket() {
    }

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    public long getExpires_in() {
        return expires_in;
    }

    public void setExpires_in(long expires_in) {
        this.expires_in = expires_in;
    }

    public Integer getErrcode() {
        return errcode;
    }

    public void setErrcode(Integer errcode) {
        this.errcode = errcode;
    }

    public String getErrmsg() {
        return errmsg;
    }

    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public boolean isInVaild(){
        return ticket == null || (System.currentTimeMillis() - createTime) >= expires_in * 1000;
    }

    @Override
    public String toString() {
        return "JSAPITicket{" +
                "ticket='" + ticket + '\'' +
                ", expires_in=" + expires_in +
                ", errcode=" + errcode +
                ", errmsg='" + errmsg + '\'' +
                ", createTime=" + createTime +
                '}';
    }
}
