package com.thinkgem.jeesite.modules.weixin.pageauthorization;

/**
 * Created by yangrui on 2018/4/1.
 */
public class AccessToken {
    private String access_token;
    private long expires_in;
    private Integer errcode;
    private String errmsg;
    private long createTime;

    public AccessToken() {
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public long getExpires_in() {
        return expires_in;
    }

    public void setExpires_in(long expires_in) {
        this.expires_in = expires_in;
    }

    public Integer getErrcode() {
        return errcode;
    }

    public void setErrcode(Integer errcode) {
        this.errcode = errcode;
    }

    public String getErrmsg() {
        return errmsg;
    }

    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public boolean isInVaild(){
        return access_token == null || (System.currentTimeMillis() - createTime) >= expires_in * 1000;
    }
    @Override
    public String toString() {
        return "AccessToken{" +
                "access_token='" + access_token + '\'' +
                ", expires_in=" + expires_in +
                ", errcode=" + errcode +
                ", errmsg='" + errmsg + '\'' +
                ", createTime=" + createTime +
                '}';
    }
}
