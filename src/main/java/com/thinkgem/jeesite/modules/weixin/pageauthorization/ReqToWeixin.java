package com.thinkgem.jeesite.modules.weixin.pageauthorization;

import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * 向微信服务器发送请求
 *
 * @author liyuefan
 * @version 1.0
 * @since 2015-12-2
 */
public class ReqToWeixin {
    private static final Logger logger = LoggerFactory.getLogger(ReqToWeixin.class);

    private static String unicode = "utf-8";
    private static int retryCount = 3;

    /**
     * 使用Get方法发送请求
     *
     * @param url
     * @return jsonstr
     */
    public static String httpGet(String url) {
        String resp = "";
        HttpClient client = new HttpClient();
        GetMethod getMethod = new GetMethod(url);
        getMethod.getParams().setParameter(HttpMethodParams.RETRY_HANDLER,
                new DefaultHttpMethodRetryHandler(retryCount, false));

        try {
            int statusCode = client.executeMethod(getMethod);
            if (statusCode == HttpStatus.SC_OK) {
                resp = new String(getMethod.getResponseBody(), unicode);
                logger.debug("weixin_GetMethod_result: " + resp);
            }

        } catch (HttpException e) {
            logger.error("", e);
            e.printStackTrace();

        } catch (IOException e) {
            logger.error("", e);
            e.printStackTrace();

        } finally {
            getMethod.releaseConnection();

        }
        return resp;
    }

    /**
     * 使用Post方法发送请求
     *
     * @param url data
     * @return jsonstr
     */
    public static String httpPost(String url, String data) {
        String resp = "";
        HttpClient client = new HttpClient();
        PostMethod postMethod = new PostMethod(url);
        postMethod.getParams().setParameter(HttpMethodParams.RETRY_HANDLER,
                new DefaultHttpMethodRetryHandler(retryCount, false));

        try {
            StringRequestEntity requestEntity = new StringRequestEntity(data,
                    "application/json", "UTF-8");
            postMethod.setRequestEntity(requestEntity);
            int statusCode = client.executeMethod(postMethod);
            if (statusCode == HttpStatus.SC_OK) {
                resp = new String(postMethod.getResponseBody(), unicode);

                logger.debug("weixin_PostMethod_result: " + resp);

            }

        } catch (HttpException e) {
            logger.error("", e);
            e.printStackTrace();
        } catch (IOException e) {
            logger.error("", e);
            e.printStackTrace();
        } finally {
            postMethod.releaseConnection();
        }

        return resp;
    }

    /**
     * test
     */
    public static void main(String[] args) {

    }
}
