package com.thinkgem.jeesite.modules.weixin.pageauthorization;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;

/**
 * 配置服务号的appid和appsecret，生成的配置文件在classes目录下
 *
 * @author liyuefan
 * @version 1.0
 * @since 2015-12-23
 */
public class AccountConfig {
    private static final Logger logger = LoggerFactory.getLogger(AccountConfig.class);

    private static String appId = "";
    private static String appSecret = "";
    private static Boolean isInsideNet = false;
    public static String templateId = "";

    public static String Host = "";
    public static String Protocol = "";
    public static int Port = 0;
    public static String Sender = "";//发件人的email
    public static String Password = "";//发件人密码


    private static Properties props;
    private static String configFile = "weixin.properties";

    static {
        props = new Properties();
        InputStream in = AccountConfig.class.getClassLoader().getResourceAsStream(configFile);
        try {
            if (in != null) {
                props.load(in);
            }

        } catch (IOException e) {
            logger.error("", e);
            e.printStackTrace();

        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e2) {
                    logger.error("", e2);
                    e2.printStackTrace();
                }
            }

        }
        appId = props.getProperty("appid");
        appSecret = props.getProperty("appsecret");
        isInsideNet = Boolean.valueOf(props.getProperty("isInsideNet"));
        templateId = props.getProperty("sendWX.templateId");

        Sender = props.getProperty("sendEmail.Sender");
        Password = props.getProperty("sendEmail.Passowrd");
        Host = props.getProperty("sendEmail.Host");
        Protocol = props.getProperty("sendEmail.Protocol");
        Port = Integer.valueOf(props.getProperty("sendEmail.Port"));
    }

    /**
     * 获取配置信息
     */
    public void getConfig() {
        appId = props.getProperty("appid");
        appSecret = props.getProperty("appsecret");
        isInsideNet = Boolean.valueOf(props.getProperty("isInsideNet"));
        templateId = props.getProperty("sendWX.templateId");

        Sender = props.getProperty("sendEmail.Sender");
        Password = props.getProperty("sendEmail.Passowrd");
        Host = props.getProperty("sendEmail.Host");
        Protocol = props.getProperty("sendEmail.Protocol");
        Port = Integer.valueOf(props.getProperty("sendEmail.Port"));

    }

    /**
     * 保存配置信息
     *
     * @return
     */
    public boolean saveConfig() {
        HashMap<String, String> propMap = new HashMap<String, String>();
        propMap.put("appid", appId);
        propMap.put("appsecret", appSecret);

        return setProperties(propMap);
    }

    /**
     * 将配置信息写入文件
     *
     * @param propMap
     * @return
     */
    public boolean setProperties(HashMap<String, String> propMap) {
        boolean result = false;

        String filePath = this.getClass().getClassLoader().getResource("")
                + configFile;
        if (filePath.startsWith("file:")) {
            filePath = filePath.substring("file:".length());
        }

        // 配置文件不存在，创建一个
        if (!new File(filePath).exists()) {
            try {
                new File(filePath).createNewFile();
            } catch (IOException e) {
                logger.error("", e);
                e.printStackTrace();
            }
        }

        OutputStream out = null;
        try {
            out = new FileOutputStream(new File(filePath));
            if (out != null) {
                Set<String> keySet = propMap.keySet();
                Iterator<String> keyIterator = keySet.iterator();
                while (keyIterator.hasNext()) {
                    String key = keyIterator.next();
                    String value = propMap.get(key);

                    // 加入所有的键值对
                    props.put(key, value);

                }
                props.store(out, null);
                result = true;
            }

        } catch (IOException e) {
            logger.error("", e);
            e.printStackTrace();

        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e2) {
                    logger.error("", e2);
                    e2.printStackTrace();
                }
            }

        }

        return result;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        AccountConfig.appId = appId;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        AccountConfig.appSecret = appSecret;
    }

    public static boolean isInsideNet() {
        return isInsideNet.booleanValue();
    }

    public void setInsideNet(boolean isInsideNet) {
        AccountConfig.isInsideNet = isInsideNet;
    }

}
