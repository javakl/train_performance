/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.task.service;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.common.weixin.SendEmail;
import com.thinkgem.jeesite.common.weixin.SendMassage;
import com.thinkgem.jeesite.common.weixin.SendPhone;
import com.thinkgem.jeesite.common.weixin.SendWXMessage;
import com.thinkgem.jeesite.modules.sys.entity.User;
import com.thinkgem.jeesite.modules.task.dao.TpTaskDao;
import com.thinkgem.jeesite.modules.task.entity.TaskDispatch;
import com.thinkgem.jeesite.modules.task.entity.TpTask;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 消息推送主表Service
 *
 * @author 康力
 * @version 2018-03-11
 */
@Service
@Transactional
public class TpTaskService extends CrudService<TpTaskDao, TpTask> {
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH");

    public TpTask get(String id) {
        return super.get(id);
    }

    public List<TpTask> findList(TpTask tpTask) {
        return super.findList(tpTask);
    }

    public Page<TpTask> findPage(Page<TpTask> page, TpTask tpTask) {
        return super.findPage(page, tpTask);
    }

    @Transactional(readOnly = false)
    public void save(TpTask tpTask) {
		if (tpTask.getIsNewRecord()){
			tpTask.preInsert();
			dao.insert(tpTask);
		}else{
			tpTask.preUpdate();
			dao.update(tpTask);
		}
		if(tpTask.getRoleIdList()!=null&&tpTask.getRoleIdList().size()>0){
			dao.insertDispatchRoleId(tpTask.getId(),tpTask.getRoleIdList());
		}else if(tpTask.getUserIdList()!=null&&tpTask.getUserIdList().size()>0){
			dao.insertDispatchUserId(tpTask.getId(),tpTask.getUserIdList());
		}
    }

    @Transactional(readOnly = false)
    public void delete(TpTask tpTask) {
        super.delete(tpTask);
    }


    /**
     * 定时调度任务1：
     * 修改数据库中所有今天执行的任务状态，将其状态改为待调度
     * 调度频率：工厂实例化后0秒调度，每日凌晨
     */
    //@Scheduled(cron="0/5 * *  * * ? ")   //每半小时执行一次
    public void check_state() {
        System.out.print(new Date() + "-->：今日共计调度任务" + dao.check_task_everyDay() + "例（即时任务除外）。");
    }

    /**
     * 定时调度任务2：
     * 判断任务是否到达已调度状态
     * 每分钟判断一次进行调度
     */
    public void task_dispatch() {
		int count = dao.count_dispatch_task();
		if (count > 0) {
			System.out.print(new Date() + "-->：调度任务" + count + "例。");
			List<TpTask> list = dao.get_dispatch_task();
			SendMassage sm=null;
			for (TpTask task : list) {
				if(task.getDispatchs()!=null && task.getDispatchs().size()>0){
					task.setState(-2);dao.update(task);
					List<User> users=new ArrayList<>();
					for(TaskDispatch td:task.getDispatchs()){
						users.addAll(dao.queryUsers(td));
					}
					task.setUser(users);
					if(task.getUser().size()>0) {
						switch (task.getMsgType()) {
							case 0:
								sm = new SendEmail();
								break;
							case 1:
								try {
									sm = new SendWXMessage();
								} catch (Throwable throwable) {
									throwable.printStackTrace();
								}
								break;
							case 2:
								sm = new SendPhone();
								break;
						}
						task = sm.sendMassage(task);
					}else{
						task.setState(-9);
					}
					dao.update(task);
				}
			}
		}
    }
}