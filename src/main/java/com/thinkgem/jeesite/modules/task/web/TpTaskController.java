/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.task.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.thinkgem.jeesite.modules.sys.service.SystemService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.task.entity.TpTask;
import com.thinkgem.jeesite.modules.task.service.TpTaskService;

/**
 * 消息推送主表Controller
 * @author 康力
 * @version 2018-03-11
 */
@Controller
@RequestMapping(value = "${adminPath}/task/tpTask")
public class TpTaskController extends BaseController {

	@Autowired
	private TpTaskService tpTaskService;
	@Autowired
	private SystemService systemService;
	
	@ModelAttribute
	public TpTask get(@RequestParam(required=false) String id) {
		TpTask entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = tpTaskService.get(id);
		}
		if (entity == null){
			entity = new TpTask();
		}
		return entity;
	}
	
	@RequiresPermissions("task:tpTask:view")
	@RequestMapping(value = {"list", ""})
	public String list(TpTask tpTask, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<TpTask> page = tpTaskService.findPage(new Page<TpTask>(request, response), tpTask); 
		model.addAttribute("page", page);
		return "modules/task/tpTaskList";
	}

	@RequiresPermissions("task:tpTask:view")
	@RequestMapping(value = "form")
	public String form(TpTask tpTask, Model model) {
		model.addAttribute("tpTask", tpTask);
		model.addAttribute("allRoles", systemService.findAllRole());
		return "modules/task/tpTaskForm";
	}

	@RequiresPermissions("task:tpTask:edit")
	@RequestMapping(value = "save")
	public String save(TpTask tpTask, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, tpTask)){
			return form(tpTask, model);
		}
		tpTaskService.save(tpTask);
		addMessage(redirectAttributes, "保存消息推送成功");
		return "redirect:"+Global.getAdminPath()+"/task/tpTask/?repage";
	}
	
	@RequiresPermissions("task:tpTask:edit")
	@RequestMapping(value = "delete")
	public String delete(TpTask tpTask, RedirectAttributes redirectAttributes) {
		tpTaskService.delete(tpTask);
		addMessage(redirectAttributes, "删除消息推送成功");
		return "redirect:"+Global.getAdminPath()+"/task/tpTask/?repage";
	}

}