/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.task.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.sys.entity.User;
import com.thinkgem.jeesite.modules.task.entity.TaskDispatch;
import com.thinkgem.jeesite.modules.task.entity.TpTask;
import org.apache.ibatis.annotations.Param;
import org.springframework.security.access.method.P;

import java.util.Collection;
import java.util.List;

/**
 * 消息推送主表DAO接口
 * @author 康力
 * @version 2018-03-11
 */
@MyBatisDao
public interface TpTaskDao extends CrudDao<TpTask> {

    int check_task_everyDay();

    int count_dispatch_task();

    List<TpTask> get_dispatch_task();

    List<User> queryUsers(TaskDispatch td);

    void insertDispatchRoleId(@Param("id") String id,@Param("roleIdList") List<String> roleIdList);

    void insertDispatchUserId(@Param("id") String id,@Param("userIdList") List<String> userIdList);
}