/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.task.entity;

import com.thinkgem.jeesite.modules.sys.entity.User;
import org.hibernate.validator.constraints.Length;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 消息推送主表Entity
 * @author 康力
 * @version 2018-03-11
 */
public class TpTask extends DataEntity<TpTask> {
	
	private static final long serialVersionUID = 1L;
	private Integer msgType;		// 消息类型
	private String name;		// 任务名称
	private Date createTime;		// 创建时间
	private Date dispatchTime;		// 调度时间
	private String url;		// 发送链接
	private String title;		// 标题
	private String content;		// 发送内容
	private String remark;		// 备注
	private Integer state;		// 调度状态
	private String resultMsg;		// 发送结果
	private Date beginCreateTime;		// 开始 创建时间
	private Date endCreateTime;		// 结束 创建时间
	private Date beginDispatchTime;		// 开始 调度时间
	private Date endDispatchTime;		// 结束 调度时间
	private List<User> user;
	private List<TaskDispatch> dispatchs;
	private List<String> roleIdList;
	private List<String> userIdList;

	private String userIds;
	private String userNames;

    public String getUserNames() {
        return userNames;
    }

    public void setUserNames(String userNames) {
        this.userNames = userNames;
    }

    public String getUserIds() {
        return userIds;
    }

    public void setUserIds(String userIds) {
        this.userIds = userIds;
    }

    public List<TaskDispatch> getDispatchs() {
		return dispatchs;
	}
	public TpTask() {
		super();
	}
	
	public TpTask(Integer msgType, String name, Date dispatchTime, String url, String title, String content,
			String remark, Integer state, String resultMsg, User ... users) {
		super();
		this.msgType = msgType;
		this.name = name;
		this.dispatchTime = dispatchTime;
		this.url = url;
		this.title = title;
		this.content = content;
		this.remark = remark;
		this.state = state;
		this.resultMsg = resultMsg;
		this.user = Arrays.asList(users);
	}
	public TpTask(String id){
		super(id);
	}

	public Integer getMsgType() {
		return msgType;
	}

	public void setMsgType(Integer msgType) {
		this.msgType = msgType;
	}
	
	@Length(min=0, max=255, message="任务名称长度必须介于 0 和 255 之间")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getDispatchTime() {
		return dispatchTime;
	}

	public void setDispatchTime(Date dispatchTime) {
		this.dispatchTime = dispatchTime;
	}
	
	@Length(min=0, max=255, message="发送链接长度必须介于 0 和 255 之间")
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	@Length(min=0, max=255, message="标题长度必须介于 0 和 255 之间")
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	@Length(min=0, max=300, message="备注长度必须介于 0 和 300 之间")
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}
	
	public String getResultMsg() {
		return resultMsg;
	}

	public void setResultMsg(String resultMsg) {
		this.resultMsg = resultMsg;
	}
	
	public Date getBeginCreateTime() {
		return beginCreateTime;
	}

	public void setBeginCreateTime(Date beginCreateTime) {
		this.beginCreateTime = beginCreateTime;
	}
	
	public Date getEndCreateTime() {
		return endCreateTime;
	}

	public void setEndCreateTime(Date endCreateTime) {
		this.endCreateTime = endCreateTime;
	}
		
	public Date getBeginDispatchTime() {
		return beginDispatchTime;
	}

	public void setBeginDispatchTime(Date beginDispatchTime) {
		this.beginDispatchTime = beginDispatchTime;
	}
	
	public Date getEndDispatchTime() {
		return endDispatchTime;
	}

	public void setEndDispatchTime(Date endDispatchTime) {
		this.endDispatchTime = endDispatchTime;
	}

	public List<User> getUser() {
		return user;
	}

	public void setUser(List<User> user) {
		this.user = user;
	}

	public void setDispatchs(List<TaskDispatch> dispatchs) {
		this.dispatchs = dispatchs;
	}

	public List<String> getRoleIdList() {
		return roleIdList;
	}

	public void setRoleIdList(List<String> roleIdList) {
		this.roleIdList = roleIdList;
	}

	public List<String> getUserIdList() {
		return userIdList;
	}

	public void setUserIdList(List<String> userIdList) {
		this.userIdList = userIdList;
	}
}