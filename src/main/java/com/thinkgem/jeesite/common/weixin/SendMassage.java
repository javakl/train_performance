package com.thinkgem.jeesite.common.weixin;


import com.thinkgem.jeesite.modules.task.entity.TpTask;

/**
 * 发送模板消息接口
 */
public interface SendMassage {

    public TpTask sendMassage(TpTask task);
}
