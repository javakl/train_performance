package com.thinkgem.jeesite.common.weixin;

import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.sys.entity.User;
import com.thinkgem.jeesite.modules.task.entity.TpTask;
import com.thinkgem.jeesite.modules.weixin.pageauthorization.AccountConfig;
import com.thinkgem.jeesite.modules.weixin.pageauthorization.CacheTokenAndTicket;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class SendWXMessage implements SendMassage {
    /**
     * touser	    是	接收者openid
     * template_id	是	模板ID
     * url	        否	模板跳转链接
     * miniprogram	否	跳小程序所需数据，不需跳小程序可不用传该数据
     * appid	    是	所需跳转到的小程序appid（该小程序appid必须与发模板消息的公众号是绑定关联关系）
     * pagepath	    是	所需跳转到小程序的具体页面路径，支持带参数,（示例index?foo=bar）
     * data	        是	模板数据
     * color	    否	模板内容字体颜色，不填默认为黑色
     */
    @Override
    public TpTask  sendMassage(TpTask task) {
        try {
            Map datamap = new HashMap();
            Map first = new HashMap();
            Map keyword1 = new HashMap();
            Map keyword2 = new HashMap();
            Map keyword3 = new HashMap();
            Map remark = new HashMap();
            Map dataMap = new HashMap();
            SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
            first.put("value", "，您好！");
           /* keyword1.put("value", task.getTitle());
            keyword2.put("value", task.getContent());
            keyword3.put("value", sdf.format(new Date()));
            remark.put("value", task.getRemark());

            datamap.put("first", first);
            datamap.put("keyword1", keyword1);
            datamap.put("keyword2", keyword2);
            datamap.put("keyword3", keyword3);
            datamap.put("remark", remark);*/
            keyword1.put("value", task.getTitle());
            keyword2.put("value","PKUPH");
			remark.put("value", task.getContent() + (StringUtils.isNotBlank(task.getRemark()) ? task.getRemark() : "")
					+ "(" + sdf.format(new Date()) + ")");

            datamap.put("first", first);
            datamap.put("keyword1", keyword1);
            datamap.put("keyword2", keyword2);
            datamap.put("keyword3", keyword3);
            datamap.put("remark", remark);


            dataMap.put("template_id", AccountConfig.templateId);
            dataMap.put("data", datamap);
            StringBuilder sb=new StringBuilder();
            for(User ub:task.getUser()){
                if(StringUtils.trimToNull(ub.getOpenId())!=null){
                	keyword3.put("value", ub.getName());
                    String url = task.getUrl();
                    if(StringUtils.isNotBlank(url)){
	                    StringBuilder s = new StringBuilder();
	                    s.append(url);
	                    if(StringUtils.trimToNull(url) != null && task.getUrl().indexOf("?") != -1){
	                        s.append("&userId=").append(ub.getId()).append("&openId=").append(ub.getOpenId());
	                    }else{
	                        s.append("?userId=").append(ub.getId()).append("&openId=").append(ub.getOpenId());
	                    }
	                    System.out.println("url = " + s.toString());
	                    dataMap.put("url", s.toString());
                    }
                    first.put("value",ub.getName()+"，您好！");
                    dataMap.put("touser", ub.getOpenId());
                    sb.append(ub.getName()+"-->"+HttpUtils.doPost("https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=" + CacheTokenAndTicket.getAccessToken().getAccess_token(),dataMap)+"<br/>");
                }else{
                    sb.append(ub.getName()+"-->"+"此用户暂未关注微信公众号");
                }
            }
            task.setState(9);
            task.setResultMsg(sb.toString());
        }catch(Exception e){
            task.setState(-9);
            task.setResultMsg(e.getMessage());
        }
        return task;
    }
}
