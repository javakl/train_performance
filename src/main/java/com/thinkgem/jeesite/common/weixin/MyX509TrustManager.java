package com.thinkgem.jeesite.common.weixin;

import javax.net.ssl.X509TrustManager;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

/**
 * 自定义的信任管理器
 */
public class MyX509TrustManager implements X509TrustManager {
    /**
     * 检查客户端证书，若不信任，抛出异常
     */
    @Override
    public void checkClientTrusted(X509Certificate[] arg0, String arg1)
            throws CertificateException {
    }
    /**
     * 检查服务端证书，若不信任，抛出异常，反之，若不抛出异常，则表示信任（所以，空方法代表信任所有的服务端证书）
     */
    @Override
    public void checkServerTrusted(X509Certificate[] arg0, String arg1)
            throws CertificateException {
    }
    /**
     * 返回受信任的X509证书数组
     */
    @Override
    public X509Certificate[] getAcceptedIssuers() {
        return null;
    }
}