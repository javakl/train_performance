package com.thinkgem.jeesite.common.utils;

/**
 * controller返回对象
 * Created by yangrui on 2018/3/10.
 */
public class Msg {
    public static final int FAIL = -1;
    public static final int SUCCESS = 0;
    /**
     * 返回状态码
     */
    private int code;
    /**
     * 返回信息
     */
    private String info;

    public Msg() {
    }

    public Msg(int code, String info) {
        this.code = code;
        this.info = info;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    @Override
    public String toString() {
        return "Msg{" +
                "code=" + code +
                ", info='" + info + '\'' +
                '}';
    }
}
