/**
 */
package com.thinkgem.jeesite.common.service;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Maps;
import com.thinkgem.jeesite.common.persistence.ActEntity;
import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.modules.act.service.ActProcessService;
import com.thinkgem.jeesite.modules.act.service.ActTaskService;

/**
 * Service基类
 * @author hhm
 * @version 2018年3月15日
 */
@Transactional(readOnly = true)
public abstract class ActAuditService<D extends CrudDao<T>, T extends ActEntity<T>> extends CrudService<D,T> {
	
	/**
	 * 持久层对象
	 */
	@Autowired
	protected D dao;
	
	@Autowired
	private ActTaskService actTaskService;
	
	@Autowired
	private ActProcessService actProcessService;
	
	/**
	 * 审核审批保存
	 * @param testAudit
	 */
	@Transactional(readOnly = false)
	public void auditSave(T audit) {
		
		// 设置意见
		audit.getAct().setComment(("yes".equals(audit.getAct().getFlag())?"[同意] ":"[驳回] ")+audit.getAct().getComment());
		
		audit.preUpdate();
		
		// 对不同环节的业务逻辑进行操作
		String taskDefKey = audit.getAct().getTaskDefKey();
		//本次审核结果 1=通过 0=不通过
		Integer pass = "yes".equals(audit.getAct().getFlag())? 1 : 0;
		// 审核环节
		if (taskDefKey.startsWith("audit")){
			if(pass!=1){
				
			}
		} else if ("audit_end".equals(taskDefKey)){
			
		}else{// 未知环节，直接返回
			return;
		}
		dao.update(audit);
		
		// 提交流程任务
		Map<String, Object> vars = Maps.newHashMap();
		vars.put("pass", pass);
		actTaskService.complete(audit.getAct().getTaskId(), audit.getAct().getProcInsId(), audit.getAct().getComment(), vars);
	}

	@Override
	public void delete(T entity) {
		//删除流程实例
		if(entity.getAct()!=null && StringUtils.isNotBlank(entity.getAct().getProcInsId())){
			actProcessService.deleteProcIns(entity.getAct().getProcInsId(), "用户手动删除");
		}
		super.delete(entity);
	}
	
	
}
