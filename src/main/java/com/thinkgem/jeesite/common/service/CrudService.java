/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.common.service;

import java.lang.reflect.Method;
import java.util.List;

import com.thinkgem.jeesite.common.utils.excel.annotation.ExcelField;
import com.thinkgem.jeesite.modules.classgroup.dao.ClassGroupDao;
import com.thinkgem.jeesite.modules.file.dao.TpFileuploadInfoDao;
import com.thinkgem.jeesite.modules.file.entity.TpFileuploadInfo;
import com.thinkgem.jeesite.modules.sys.dao.UserDao;
import com.thinkgem.jeesite.modules.sys.service.SystemService;
import com.thinkgem.jeesite.modules.user.entity.student.TpPStudent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.DataEntity;
import com.thinkgem.jeesite.common.persistence.Page;

/**
 * Service基类
 * @author ThinkGem
 * @version 2014-05-16
 */
@Transactional(readOnly = true)
public abstract class CrudService<D extends CrudDao<T>, T extends DataEntity<T>> extends BaseService {
	
	/**
	 * 持久层对象
	 */
	@Autowired
	protected D dao;
	@Autowired
	protected UserDao userDao;
	/**
	 * 文件查询
	 */
	@Autowired
	private TpFileuploadInfoDao fileuploadInfoDao;
	/**
	 * 获取单条数据
	 * @param id
	 * @return
	 */
	public T get(String id) {
		TpFileuploadInfo entity = new TpFileuploadInfo();
		entity.setModel(id);
		T rtn = dao.get(id);
		if(rtn!=null){
			List<TpFileuploadInfo> fileList = fileuploadInfoDao.findList(entity);
			if(fileList!=null && fileList.size()>0)rtn.setFileList(fileList);
		}
		return rtn;
	}
	
	/**
	 * 获取单条数据
	 * @param entity
	 * @return
	 */
	public T get(T entity) {
		return dao.get(entity);
	}
	
	/**
	 * 查询列表数据
	 * @param entity
	 * @return
	 */
	public List<T> findList(T entity) {
		return dao.findList(entity);
	}
	
	/**
	 * 查询分页数据
	 * @param page 分页对象
	 * @param entity
	 * @return
	 */
	public Page<T> findPage(Page<T> page, T entity) {
		entity.setPage(page);
		page.setList(dao.findList(entity));
		return page;
	}

	/**
	 * 保存数据（插入或更新）
	 * @param entity
	 */
	@Transactional(readOnly = false)
	public void save(T entity) {
		if (entity.getIsNewRecord()){
			entity.preInsert();
			dao.insert(entity);
		}else{
			entity.preUpdate();
			dao.update(entity);
		}
	}
	
	/**
	 * 删除数据
	 * @param entity
	 */
	@Transactional(readOnly = false)
	public void delete(T entity) {
		dao.delete(entity);
	}

    /**
     * 批量删除
     * @param entitys
     */
	@Transactional(rollbackFor = Exception.class)
	public void batchDelete(List<T> entitys) {
		for(T entity:entitys){
		   delete(entity);
		}
	}

}
