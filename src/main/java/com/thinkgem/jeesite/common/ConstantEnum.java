/**
 * 
 */
package com.thinkgem.jeesite.common;

/**
 * @author hhm
 * 常量枚举类
 */
public enum ConstantEnum {
	success(0,"操作成功"),
	failed(-1,"操作失败"),
	AUDIT_AWAIT(0,"待审核"),
	AUDITING(1,"审核中"),
	AUDIT_NO_PASSED(-1,"审核未通过"),
	AUDIT_PASSED(2,"审核通过"),;
	private Integer code;
	private String name;
	private ConstantEnum(Integer code, String name) {
		this.code = code;
		this.name = name;
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
