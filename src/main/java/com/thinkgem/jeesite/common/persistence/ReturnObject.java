package com.thinkgem.jeesite.common.persistence;

/**
 * @author hhm
 * 接口请求结果封装实体类
 */
public class ReturnObject {
	/**
	 *0=成功 -1=失败 
	 */
	private Integer result;
	/**
	 * 信息描述
	 */
	private String msg;
	private String returnToUrl;
	/**
	 * 数据
	 */
	private Object data;
	
	public ReturnObject() {
		super();
	}

	public ReturnObject(Integer result, String msg, Object data) {
		super();
		this.result = result;
		this.msg = msg;
		this.data = data;
	}
	
	public Integer getResult() {
		return result;
	}
	public void setResult(Integer result) {
		this.result = result;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getReturnToUrl() {
		return returnToUrl;
	}
	public void setReturnToUrl(String returnToUrl) {
		this.returnToUrl = returnToUrl;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
}
